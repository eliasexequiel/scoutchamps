<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class PublicPagesTest extends TestCase
{
    // use WithoutMiddleware;
    /**
     * Test the public (200) pages.
     *
     * @return void
     */
    public function testPublicPagesAllowed()
    {
        // $this->get('/login')->assertStatus(200)->assertSee('Login');
        $response = $this->get('/login');
        $response->assertSuccessful();
        $response->assertViewIs('auth.login');
        // $this->get('/register')->assertSuccessful();
    }

}
