<?php

use Illuminate\Database\Seeder;
use jeremykenedy\LaravelRoles\Models\Permission;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        /*
         * Add Permissions
         *
         */
        if (Permission::where('name', '=', 'Can View Users')->first() === null) {
            Permission::create([
                'name'        => 'Can View Users',
                'slug'        => 'view.users',
                'description' => 'Can view users',
                'model'       => 'Permission',
            ]);
        }

        if (Permission::where('name', '=', 'Can Create Users')->first() === null) {
            Permission::create([
                'name'        => 'Can Create Users',
                'slug'        => 'create.users',
                'description' => 'Can create new users',
                'model'       => 'Permission',
            ]);
        }

        if (Permission::where('name', '=', 'Can Edit Users')->first() === null) {
            Permission::create([
                'name'        => 'Can Edit Users',
                'slug'        => 'edit.users',
                'description' => 'Can edit users',
                'model'       => 'Permission',
            ]);
        }

        if (Permission::where('name', '=', 'Can Delete Users')->first() === null) {
            Permission::create([
                'name'        => 'Can Delete Users',
                'slug'        => 'delete.users',
                'description' => 'Can delete users',
                'model'       => 'Permission',
            ]);
        }

        if (Permission::where('name', '=', 'Add Recruitments')->first() === null) {
            Permission::create([
                'name'        => 'Add Recruitments',
                'slug'        => 'add.recruitments',
                'description' => 'Add Recruitments',
                'model'       => 'Permission',
            ]);
        }

        if (Permission::where('name', '=', 'Add Favorites')->first() === null) {
            Permission::create([
                'name'        => 'Add Favorites',
                'slug'        => 'add.favorites',
                'description' => 'Add Favorites',
                'model'       => 'Permission',
            ]);
        }

        if (Permission::where('name', '=', 'Publish Posts')->first() === null) {
            Permission::create([
                'name'        => 'Publish Posts',
                'slug'        => 'publish.posts',
                'description' => 'Publish Posts',
                'model'       => 'Permission',
            ]);
        }

        if (Permission::where('name', '=', 'Add Followed')->first() === null) {
            Permission::create([
                'name'        => 'Add Followed',
                'slug'        => 'add.followed',
                'description' => 'Allow users follow other users',
                'model'       => 'Permission',
            ]);
        }

        if (Permission::where('name', '=', 'Upload 30 photos')->first() === null) {
            Permission::create([
                'name'        => 'Upload 30 photos',
                'slug'        => 'upload.30.photos',
                'description' => 'Upload 30 photos',
                'model'       => 'Permission',
            ]);
        }

        if (Permission::where('name', '=', 'Upload unlimited photos')->first() === null) {
            Permission::create([
                'name'        => 'Upload unlimited photos',
                'slug'        => 'upload.unlimited.photos',
                'description' => 'Upload unlimited photos and more',
                'model'       => 'Permission',
            ]);
        }

        if (Permission::where('name', '=', 'Upload unlimited videos')->first() === null) {
            Permission::create([
                'name'        => 'Upload unlimited videos',
                'slug'        => 'upload.unlimited.videos',
                'description' => 'Upload unlimited videos',
                'model'       => 'Permission',
            ]);
        }

        if (Permission::where('name', '=', 'Collegues Interest')->first() === null) {
            Permission::create([
                'name'        => 'Collegues Interest',
                'slug'        => 'collegues.interest',
                'description' => 'List of collegus or professionals teams of your preference so ScoutChamps contacts them',
                'model'       => 'Permission',
            ]);
        }

        $colleguesInterestPermissions = ['collegues.interest.10','collegues.interest.20','collegues.interest.30'];
        foreach ($colleguesInterestPermissions as $p) {
            $cant = explode(".",$p)[2];
            if (Permission::where('slug', '=', $p)->first() === null) {
                Permission::create([
                    'name'        => 'Collegues Interest' . $cant,
                    'slug'        => $p,
                    'description' => 'List of colleges cant:' . $cant,
                    'model'       => 'Permission',
                ]);
            }
        }    
    }
}
