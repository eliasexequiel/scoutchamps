<?php

use Illuminate\Database\Seeder;
use jeremykenedy\LaravelRoles\Models\Permission;
use jeremykenedy\LaravelRoles\Models\Role;

class ConnectRelationshipsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        /**
         * Get Available Permissions.
         */

        $arrayAdmin = ['view.users','create.users','edit.users','delete.users'];
        $permissionsAdmin = Permission::whereIn('slug',$arrayAdmin)->get();

        $roleAdmin = Role::where('slug', '=', 'admin')->first();
        foreach ($permissionsAdmin as $permission) {
            $roleAdmin->attachPermission($permission);
        }

        // SCOUT
        $arrayScout = ['add.favorites','add.followed','add.recruitments'];
        $permissionsScout = Permission::whereIn('slug',$arrayScout)->get();

        $roleScout = Role::where('slug', '=', 'scout')->first();
        foreach ($permissionsScout as $permission) {
            $roleScout->attachPermission($permission);
        }

        // ATHLETES
        $arrayAthlete = ['publish.posts', 'add.followed'];
        $permissionsAthlete = Permission::whereIn('slug',$arrayAthlete)->get();

        $roleAthlete = Role::where('slug', '=', 'athlete')->first();
        foreach ($permissionsAthlete as $permission) {
            $roleAthlete->attachPermission($permission);
        }

        // ATHLETES PREMIUN
        $arrayAthlete = ['publish.posts', 'add.followed','upload.30.photos','upload.unlimited.photos','upload.unlimited.videos','collegues.interest','collegues.interest.10'];
        $permissionsAthlete = Permission::whereIn('slug',$arrayAthlete)->get();

        $roleAthlete = Role::where('slug', '=', 'athlete.premiun')->first();
        foreach ($permissionsAthlete as $permission) {
            $roleAthlete->attachPermission($permission);
        }

        // ATHLETES PREMIUN SEMESTER
        $arrayAthlete = ['publish.posts', 'add.followed','upload.30.photos','upload.unlimited.photos','upload.unlimited.videos','collegues.interest','collegues.interest.20'];
        $permissionsAthlete = Permission::whereIn('slug',$arrayAthlete)->get();

        $roleAthlete = Role::where('slug', '=', 'athlete.premiun.semester')->first();
        foreach ($permissionsAthlete as $permission) {
            $roleAthlete->attachPermission($permission);
        }

        // ATHLETES PREMIUN ANUAL
        $arrayAthlete = ['publish.posts', 'add.followed','upload.30.photos','upload.unlimited.photos','upload.unlimited.videos','collegues.interest','collegues.interest.30'];
        $permissionsAthlete = Permission::whereIn('slug',$arrayAthlete)->get();

        $roleAthlete = Role::where('slug', '=', 'athlete.premiun.anual')->first();
        foreach ($permissionsAthlete as $permission) {
            $roleAthlete->attachPermission($permission);
        }
    }
}
