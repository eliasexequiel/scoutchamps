<?php

use Illuminate\Database\Seeder;

class StatisticTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        \DB::table('statistics')->truncate();
        \DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        $fields = [
            ['name' => 'conversationsAthletesScouts', 'value' => '0'],
            ['name' => 'conversationsScoutsAthletes', 'value' => '0'],
            ['name' => 'conversationsAthletes', 'value' => '0'],
            ['name' => 'popularAthletes', 'value' => ''],
            ['name' => 'popularScouts', 'value' => '']
        ];
        \DB::table('statistics')->insert($fields);
    }
}
