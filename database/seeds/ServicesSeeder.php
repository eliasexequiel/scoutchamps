<?php

use Illuminate\Database\Seeder;

class ServicesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        \DB::table('services')->truncate();
        \DB::table('service_items')->truncate();
        \DB::table('service_items_values')->truncate();
        \DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        $servicesitems = [
            ['name' => json_encode(["en" => "Player's Profile", "es" => "Perfil de atleta"])],
            ['name' => json_encode(["en" => "Player's goals", "es" => "Objetivos de atleta"])],
            ['name' => json_encode(["en" => 'Number of pictures', "es" => "Número de imágenes"])],
            ['name' => json_encode(["en" => 'Scouts will be able to contact you', "es" => "Reclutadores capaces de contactarte"])],
            ['name' => json_encode(["en" => 'About Me', "es" => "Sobre Mi"])],
            ['name' => json_encode(["en" => '1 Video editable when you want', "es" => "1 Video editable cuando quieras"])],
            ['name' => json_encode(["en" => 'Unlimited Videos', "es" => "Videos ilimitados"])],
            ['name' => json_encode(["en" => "Player's profile has greater exposure to Scouts", "es" => "Perfil con mas visibilidad para los Reclutadores"])],
            ['name' => json_encode(["en" => 'Invite Scouts to see your profile', "es" => "Invitaciones a reclutadores para que vean tu perfil"])],
            ['name' => json_encode(["en" => 'Sending messages to Scouts or Athletes', "es" => "Envio de mensajes a Reclutadores o Atletas"])],
            ['name' => json_encode(["en" => 'Know which Scout view your profile', "es" => "Saber que reclutadores vieron tu perfil"])],
            ['name' => json_encode(["en" => 'Profile counter', "es" => "Contador de perfil"])],
            ['name' => json_encode(["en" => 'Showcase: List of colleges or professionals teams of your preference so ScoutChamps contacts them and lets them know about your interest', "es" => "Vitrina: Lista de Universidades o equipos profesionales de tu preferencia que ScoutChamps pueda contactar y hacerles saber de tu interés"])],
            ['name' => json_encode(["en" => 'Contact Admin in Dashboard', "es" => "Contactar Administrador en Dashboard"])],
        ];
        \DB::table('service_items')->insert($servicesitems);

        $services = [
            ['name' => 'Free', 'type' => 'Standard', 'price' => 0, 'stripe_plan' => 'plan_free'],
            ['name' => 'Month', 'type' => 'Premium', 'price' => 9.99, 'stripe_plan' => 'plan_FmnTXbESSdR9MM'],
            ['name' => 'Semester', 'type' => 'Premium', 'price' => 53.99, 'stripe_plan' => 'plan_FwqEBuqDFWpw9Q'],
            ['name' => 'Anual', 'type' => 'Premium', 'price' => 95.99, 'stripe_plan' => 'plan_FwqFvjeuS2ySZo'],
        ];

        \DB::table('services')->insert($services);

        // FREE SERVICE
        $array = ['x', 'x', '30', 'x', 'x', 'x', '', '', '', '', '', '', '','x'];
        foreach ($servicesitems as $key => $s) {

            \DB::table('service_items_values')->insert([
                'service_id' => 1,
                'service_item_id' => $key + 1,
                'value' => $array[$key]
            ]);
        }

        // MONTH SERVICE
        $array = ['x', 'x', 'Unlimited', 'x', 'x', '', 'x', 'x', 'x', 'x', 'x', 'x', '10','x'];
        foreach ($servicesitems as $key => $s) {

            \DB::table('service_items_values')->insert([
                'service_id' => 2,
                'service_item_id' => $key + 1,
                'value' => $array[$key]
            ]);
        }

        // SEMESTRAL SERVICE
        $array = ['x', 'x', 'Unlimited', 'x', 'x', '', 'x', 'x', 'x', 'x', 'x', 'x', '20','x'];
        foreach ($servicesitems as $key => $s) {

            \DB::table('service_items_values')->insert([
                'service_id' => 3,
                'service_item_id' => $key + 1,
                'value' => $array[$key]
            ]);
        }

        // ANUAL SERVICE
        $array = ['x', 'x', 'Unlimited', 'x', 'x', '', 'x', 'x', 'x', 'x', 'x', 'x', '30','x'];
        foreach ($servicesitems as $key => $s) {

            \DB::table('service_items_values')->insert([
                'service_id' => 4,
                'service_item_id' => $key + 1,
                'value' => $array[$key]
            ]);
        }
    }
}
