<?php

use Illuminate\Database\Seeder;

class EntityTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        // \DB::table('entities')->truncate();
        // \DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        // $entities = [
        //     ['name' => 'Boca Juniors', 'entity_type_id' => 1, 'verified' => true],
        //     ['name' => 'Inter', 'entity_type_id' => 1, 'verified' => true],
        //     ['name' => 'Sevilla', 'entity_type_id' => 1, 'verified' => true],
        //     ['name' => 'Barcelona', 'entity_type_id' => 1, 'verified' => true],
        //     ['name' => 'Roma', 'entity_type_id' => 1, 'verified' => true]
        // ];
        // \DB::table('entities')->insert($entities);

        // factory(App\Models\Entity::class, 700)->create();
    }
}
