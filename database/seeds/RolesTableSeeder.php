<?php

use Illuminate\Database\Seeder;
use jeremykenedy\LaravelRoles\Models\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
         * Add Roles
         *
         */
        if (Role::where('slug', '=', 'admin')->first() === null) {
            $adminRole = Role::create([
                'name'        => 'Admin',
                'slug'        => 'admin',
                'description' => 'Admin Role',
                'level'       => 1,
            ]);
        }

        if (Role::where('slug', '=', 'scout')->first() === null) {
            $userRole = Role::create([
                'name'        => 'Scout',
                'slug'        => 'scout',
                'description' => 'Scout Role',
                'level'       => 1,
            ]);
        }

        if (Role::where('slug', '=', 'athlete')->first() === null) {
            $userRole = Role::create([
                'name'        => 'Athlete',
                'slug'        => 'athlete',
                'description' => 'Athlete Role',
                'level'       => 1,
            ]);
        }

        if (Role::where('slug', '=', 'athlete.free')->first() === null) {
            $userRole = Role::create([
                'name'        => 'Athlete Free',
                'slug'        => 'athlete.free',
                'description' => 'Athlete Free Role',
                'level'       => 1,
            ]);
        }

        if (Role::where('slug', '=', 'athlete.premiun')->first() === null) {
            $userRole = Role::create([
                'name'        => 'Athlete Premiun',
                'slug'        => 'athlete.premiun',
                'description' => 'Athlete Premiun Role',
                'level'       => 1,
            ]);
        }

        if (Role::where('slug', '=', 'athlete.premiun.semester')->first() === null) {
            $userRole = Role::create([
                'name'        => 'Athlete Premiun Semester',
                'slug'        => 'athlete.premiun.semester',
                'description' => 'Athlete Premiun Role - Semester Plan',
                'level'       => 1,
            ]);
        }

        if (Role::where('slug', '=', 'athlete.premiun.anual')->first() === null) {
            $userRole = Role::create([
                'name'        => 'Athlete Premiun Anual',
                'slug'        => 'athlete.premiun.anual',
                'description' => 'Athlete Premiun Role - Anual Plan',
                'level'       => 1,
            ]);
        }
    }
}
