<?php

use App\Models\User;
use Illuminate\Database\Seeder;

class ArticleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        \DB::table('articles')->truncate();
        \DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $seededAdminEmail = 'scoutchamps@gmail.com';
        $user = User::where('email', '=', $seededAdminEmail)->first();
        
        $articles = [
            [
                'title' => 'Federal A: la liga de las estrellas',
                'body' => 'Para soñar con jugar en la Primera Nacional y quedar sólo un escalón debajo de la Superliga, fueron varios los equipos del Federal A que se reforzaron a full: ¡incluso con figuritas que pasaron por la máxima categoría del fútbol argentino! Mirá la lista de cracks que juegan una de las divisiones más parejas del Ascenso.',
                'user_id' => $user->id
            ],
            [
                'title' => 'Owen: "Aún guardo rencor a Beckham por el Mundial de 1998"',
                'body' => 'En Inglaterra continúan sacando a la luz capítulos de "Reebot", el libro escrito por Michael Owen. El exjugador inglés habla esta vez de cómo le afectó la expulsión de David Beckham en el Mundial de Francia 98, cuando el jugador, desde el suelo, dio una pequeña patada a Simeone que acabó dejándoles con diez (más tarde perderían en la tanda de penaltis).',
                'user_id' => $user->id
            ],
            [
                'title' => 'Dimitrov jubila a Federer',
                'body' => 'Este martes, en el US Open, Grigor Dimitrov, que nunca había ganado a Federer en siete enfrentamientos, superó por fin a su espejo, al jugador que admira y de quien ha imitado el estilo. Le ganó, tras escapar de dos desventajas, en cinco sets y 3h:12: 3-6, 6-4, 3-6, 6-4 y 6-2. ',
                'user_id' => $user->id
            ],
            [
                'title' => 'El "Peque" Schwartzman vuelve a hacerse grande: echa a Zverev',
                'body' => 'En la Arthur Ashe, cubierta por culpa de la lluvia, Schwartzman no se descompuso cuando el primer set desaprovechó un quiebre inicial de ventaja y terminó perdiendo el parcial. A partir de ahí, se vino arriba y se comió al alemán con su rapidez y una capacidad para la defensa impresionante. También aprovechó los numerosos fallos de Zverev, desastroso con el saque (17 dobles faltas y un mal 62% de puntos ganados con primeros) y perdido en el trato de la pelota (65 errores no forzados).',
                'user_id' => $user->id
            ]
        ];

        \DB::table('articles')->insert($articles);
    }
}
