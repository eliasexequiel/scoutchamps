<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\Scout;
use App\Models\Country;
use Faker\Generator as Faker;

$factory->define(Scout::class, function (Faker $faker) {
    $country = Country::inRandomOrder()->first();
    return [
        'nationality_id' => $country->id,
        'residence_country_id' => $country->id
    ];
});
