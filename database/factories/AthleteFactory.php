<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\Athlete;
use App\Models\Sport;
use App\Models\Country;
use Faker\Generator as Faker;
use config\constants;
use Carbon\Carbon;

$factory->define(Athlete::class, function (Faker $faker) {
    $country = Country::inRandomOrder()->first();
    return [
        'nationality_id' => $country->id,
        'second_nationality_id' => $country->id,
        'residence_country_id' => $country->id,
        'residence_city' => $faker->city,
        'date_of_birth' => Carbon::now()->subYears(rand(15, 45))->format('d/m/Y'),
        'phone' => $faker->phoneNumber,
        'sport_id' => $faker->numberBetween(1,5),
        'sporting_goal' => 'ProfessionalContract',
        'professional_start_date' => null,
        'professional_end_date' => null,
        'height' => $faker->numberBetween(160,198),
        'status' => 'Professional',
    ];
});
