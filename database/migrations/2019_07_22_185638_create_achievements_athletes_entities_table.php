<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAchievementsAthletesEntitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('achievements_athletes_entities', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('athlete_entity_id');
            $table->foreign('athlete_entity_id')->references('id')->on('athlete_entities');
            
            $table->string('name');
            $table->string('month')->nullable();
            $table->integer('year')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('achievements_athletes_entities');
    }
}
