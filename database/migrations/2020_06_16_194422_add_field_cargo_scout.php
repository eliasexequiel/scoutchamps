<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldCargoScout extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('scouts', function (Blueprint $table) {
            $table->unsignedBigInteger('scout_position_id')->nullable();
            $table->foreign('scout_position_id')->references('id')->on('scout_positions'); 
            $table->unsignedBigInteger('scout_position_secondary_id')->nullable();
            $table->foreign('scout_position_secondary_id')->references('id')->on('scout_positions');  
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('scouts', function (Blueprint $table) {
            $table->dropColumn('scout_position_id');
            $table->dropColumn('scout_position_secondary_id');
        });
    }
}
