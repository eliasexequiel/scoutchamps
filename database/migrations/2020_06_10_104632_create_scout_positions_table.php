<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScoutPositionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scout_positions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->timestamps();
        });

        \DB::table('scout_positions')->insert([
            ['name' => 'Rivals Analyst'],
            ['name' => 'Tactical Analyst'],
            ['name' => 'Recruiter - Scouting Analyst'],
            ['name' => 'Assistant Coach'],
            ['name' => 'Assistant Physical Trainer'],
            ['name' => 'Management Team'],
            ['name' => 'Brand Manager'],
            ['name' => 'Head Coach'],
            ['name' => 'Kinesiologist'],
            ['name' => 'Sports Manager'],
            ['name' => 'Sports Doctor'],
            ['name' => 'Nutritionist'],
            ['name' => 'Physical Trainer'],
            ['name' => 'Athlete Representative'],
            ['name' => 'Sports Psychologist']
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scout_positions');
    }
}
