<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class VideoRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $url = $value;
        return (strpos($url, 'youtu.be') !== false || 
                strpos($url, 'youtube.com') !== false || 
                strpos($url, 'vimeo.com') !== false );
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('validation.videos.url');
    }
}
