<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class TermsAndConditions extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $terms;
    public $politics;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($terms, $politics)
    {
        $this->terms = $terms;
        $this->politics = $politics;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // return $this->view('view.name');
        return $this->markdown('emails.terms-and-politics');
    }
}
