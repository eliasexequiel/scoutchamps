<?php
namespace App\Traits;

use Carbon\Carbon;

trait DatesFormat
{

    public static function getDate($value)
    {
        $letters = array("_","-",":", "/", " ");
        $date = str_replace($letters,"",$value);
        if ($value != null) {
            if(strlen($date) == 8)
                return Carbon::createFromFormat('Y-m-d', $value)->format('d/m/Y');
            elseif(strlen($date) == 14)
                return Carbon::createFromFormat('Y-m-d H:i:s', $value)->format('d/m/Y H:i');
            else 
                return null;
        } else {
            return null;
        }
    }
    public static function setDate($value)
    {
        $date = str_replace('/', '', $value);
        $date = str_replace('_', '', $date);
        $date = str_replace('-', '', $date);
        if ($value != null) {
            return Carbon::createFromFormat('d/m/Y', $value)->format('Y-m-d');
        }
        return null;
        // else if ($value != null && is_numeric($date) && strlen($date) == 8) {
        //     return Carbon::createFromFormat('Y-m-d', $value)->format('Y-m-d');
        // } 
    }
}
