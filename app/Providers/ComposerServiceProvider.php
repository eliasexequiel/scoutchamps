<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function boot()
    {
        // View::composer(
        //     'layouts.app', 'App\Http\ViewComposers\ThemeComposer'
        // );
        View::composer('partials.nav', 'App\Http\ViewComposers\SportsComposer');
        View::composer(['partials.sidebar','usersmanagement.show-users','admin.clubes.index','admin.clubes.show','public.users.main','welcome.contact'], 
                        'App\Http\ViewComposers\ConfigComposer');
        View::composer('partials.sidebar', 'App\Http\ViewComposers\MessagesComposer');

        View::composer(['partials.publicities.publicity-above','partials.publicities.publicity-below','welcome'], 
                        'App\Http\ViewComposers\PublicitiesComposer');
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
