<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\DatesFormat;

class AthleteEntity extends Model
{
    use DatesFormat;

    protected $table = 'athlete_entities';

    protected $fillable = ['athlete_id', 'entity_id', 'info_aditional', 'start', 'end', 'isCurrent'];

    protected $casts = [
        'isCurrent' => 'boolean'
    ];

    protected $with = [
        'entity'
    ];

    public function athlete()
    {
        return $this->belongsTo('App\Models\Athlete');
    }

    public function entity()
    {
        return $this->belongsTo('App\Models\Entity');
    }

    public function achievements()
    {
        return $this->hasMany('App\Models\AchievementsAthletesEntities', 'athlete_entity_id');
    }

    public function achievementsReverse()
    {
        return $this->hasMany('App\Models\AchievementsAthletesEntities', 'athlete_entity_id')
            ->select(['*', \DB::raw('CASE WHEN month IS NULL THEN 1
                                          WHEN month="January" THEN 1
                                          WHEN month="February" THEN 2
                                          WHEN month="March" THEN 3
                                          WHEN month="April" THEN 4
                                          WHEN month="May" THEN 5
                                          WHEN month="June" THEN 6
                                          WHEN month="July" THEN 7
                                          WHEN month="August" THEN 8
                                          WHEN month="September" THEN 9
                                          WHEN month="October" THEN 10 
                                          WHEN month="November" THEN 11 
                                          WHEN month="December" THEN 12 
                                          END AS `monthNum`')])
            ->orderBy('year', 'desc')
            ->orderBy('monthNum', 'desc');
    }

    public function setStartAttribute($value)
    {
        $this->attributes['start'] = $this->setDate($value);
    }

    public function getStartAttribute($value)
    {
        return $this->getDate($value);
    }

    public function setEndAttribute($value)
    {
        $this->attributes['end'] = $this->setDate($value);
    }

    public function getEndAttribute($value)
    {
        return $this->getDate($value);
    }

    // public function getIsCurrentAttribute($value)
    // {
    //     return $this->value == 1 ? true : false;
    // }
}
