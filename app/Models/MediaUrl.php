<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MediaUrl extends Model
{

    use SoftDeletes;

    protected $table = 'media_urls';

    protected $dates = [
        'deleted_at',
    ];

    protected $fillable = [
        'user_id',
        'url',
        'type'
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function setUrlAttribute($value)
    {
        $url = $value;
        $newurl = $value;
        if (strpos($url, 'youtube.com') !== false) {
            preg_match('/[\\?\\&]v=([^\\?\\&]+)/', $url, $matches);
            $id = $matches[1];
            $newurl = 'https://www.youtube.com/embed/' . $id;
        } elseif (strpos($url, 'youtu.be') !== false) {
            $domain = preg_replace('#^http(s)?://#', '', $url);
            $matches = explode('/', $domain);
            // \Log::info($matches);
            $id = $matches[1];
            $newurl = 'https://www.youtube.com/embed/' . $id;
        } elseif (strpos($url, 'vimeo.com') !== false) {
            preg_match('/\/\/(www\.)?vimeo.com\/(\d+)($|\/)/', $url, $matches);
            $id = $matches[2];
            $newurl = 'https://player.vimeo.com/video/' . $id;
        }

        $this->attributes['url'] = $newurl;
    }

    public function report()
    {
        return $this->morphOne('App\Models\Report', 'reported');
    }
}
