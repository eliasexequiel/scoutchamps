<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\DatesFormat;
use Carbon\Carbon;

class Recruitment extends Model
{

    use DatesFormat;

    protected $table = 'recruitments';

    protected $fillable = [
        'day_start',
        'day_end',
        'hour',
        'place',
        'address',
        'city',
        'country_id', 
        'message', 
        'is_public', 
        'user_id'
    ];

    protected $casts = [
        'is_public' => 'boolean'
    ];

    protected $appends = [
        'date',
        'dateEnd',
        'addressComplete'
    ];

    public function country()
    {
        return $this->belongsTo('App\Models\Country','country_id');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function setDayStartAttribute($value)
    {
        $this->attributes['day_start'] = $this->setDate($value);
    }

    public function getDayStartAttribute($value)
    {
        return $this->getDate($value);
    }

    public function setDayEndAttribute($value)
    {
        $this->attributes['day_end'] = $this->setDate($value);
    }

    public function getDayEndAttribute($value)
    {
        return $this->getDate($value);
    }

    public function setIsPublicAttribute($value)
    {
        $this->attributes['is_public'] = $value == 'on' ? true : false;
    }

    public function getDateAttribute()
    {
        if ($this->day_start != null)
            return Carbon::createFromFormat('d/m/Y', $this->day_start)->format('Y-m-d') . ' ' . $this->hour;
        return null;
    }

    public function getDateEndAttribute()
    {
        if ($this->day_end != null)
            return Carbon::createFromFormat('d/m/Y', $this->day_end)->format('Y-m-d') . ' 23:59';
        return null;
    }

    public function getAddressCompleteAttribute()
    {
        $country = $this->country ? $this->country->name : '';
        if($this->attributes['address'] && $this->attributes['city'])
            return $this->attributes['address'] .', '. $this->attributes['city'] . ', ' . $country;
        return '';  
    }

    public function usersInvited()
    {
        return $this->hasMany('App\Models\RecruitmentAthlete', 'recruitment_id')->has('user');
    }
}
