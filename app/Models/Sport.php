<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sport extends Model
{
    use SoftDeletes;

    protected $table = 'sports';

    protected $fillable = ['name', 'description'];

    public function fields()
    {
        return $this->hasMany('App\Models\SportFields');
    }

    public function athlete()
    {
        return $this->hasMany('App\Models\Athlete','sport_id');
    }

    public function athleteSecondary()
    {
        return $this->hasMany('App\Models\Athlete','secondary_sport_id');
    }

    public function scout()
    {
        return $this->hasMany('App\Models\Scout','sport_id');
    }

    public function getAvatar()
    {
        return asset('img/sports/'. str_replace(' ','',$this->name) .'.png');;
    }

    public function getAvatarHome()
    {
        if(app()->getLocale() == "es")
            return asset('img/home/sports/'. str_replace(' ','',$this->name) .'_es.png');
        else
            return asset('img/home/sports/'. str_replace(' ','',$this->name) .'.png');
    }

    public function getAvatarSelectedHome()
    {
        if(app()->getLocale() == "es")
            return asset('img/home/sports/selected/'. str_replace(' ','',$this->name) .'_es.png');
        else
            return asset('img/home/sports/selected/'. str_replace(' ','',$this->name) .'.png');
    }
}
