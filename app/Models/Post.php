<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
    use SoftDeletes;

    protected $table = 'posts';

    protected $dates = ['deleted_at'];
    
    protected $fillable = ['body','user_id'];

    protected $appends = [
        'timeAgo'
    ];
    
    public function user()
    {
        return $this->belongsTo('App\Models\User','user_id');
    }
    
    public function postmedia()
    {
        return $this->hasMany('App\Models\PostMedia','post_id');
    }

    public function comments()
    {
        return $this->hasMany('App\Models\PostComment','post_id');
    }

    public function likes()
    {
        return $this->hasMany('App\Models\PostLikes','post_id');
    }

    public function report()
    {
        return $this->morphOne('App\Models\Report', 'reported');
    }

    public function getTimeAgoAttribute()
    {
        return \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$this->created_at)->diffForHumans();
    }
    
}
