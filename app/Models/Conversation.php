<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Conversation extends Model
{
    protected $table = 'conversations';

    protected $fillable = ['user_id_1','user_id_2', 'type'];

    public function user1()
    {
        return $this->belongsTo('App\Models\User','user_id_1');
    }

    public function user2()
    {
        return $this->belongsTo('App\Models\User','user_id_2');
    }
}
