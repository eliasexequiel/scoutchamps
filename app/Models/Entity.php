<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use App\Traits\DatesFormat;

class Entity extends Model implements HasMedia
{
    use HasMediaTrait;
    use SoftDeletes;
    use DatesFormat;

    protected $table = 'entities';

    protected $fillable = [
        'name',
        'entity_type_id',
        'website',
        'email',
        'phone',
        'verified',
        'country_id'
    ];

    protected $appends = [
        'avatar'
    ];

    protected $with = [
        'type'
    ];

    public function type()
    {
        return $this->belongsTo('App\Models\EntityType','entity_type_id');
    }

    public function country()
    {
        return $this->belongsTo('App\Models\Country','country_id');
    }

    public function getAvatarAttribute()
    {
        $img = $this->getMedia('club');
        if(count($img) == 0) {
            $img = asset('img/club-d.png');
        } else {
            $img = $img->last()->getFullUrl();
        }
        return $img;
    }

    // public function getCreatedAtAttribute($value)
    // {
    //     return $this->getDate($value);
    // }

    // public function getUpdatedAtAttribute($value)
    // {
    //     return $this->getDate($value);
    // }
}
