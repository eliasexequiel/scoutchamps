<?php

namespace App\Models;

use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Model;

class Country extends Model
{

    use Filterable;
    
    protected $table = 'countries';

    protected $fillable = ['name','code','lat','lng'];

    public function getNameAttribute($value) 
    {
        return trans('countries.'. $value);
    }

    public function athlete()
    {
        return $this->hasMany('App\Models\Athlete','nationality_id');
    }

    public function scout()
    {
        return $this->hasMany('App\Models\Scout','nationality_id');
    }

    public function entity()
    {
        return $this->hasMany('App\Models\Entity','country_id');
    }
}
