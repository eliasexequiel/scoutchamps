<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class Publicity extends Model implements HasMedia
{

    use HasMediaTrait;

    protected $table = 'publicities';

    protected $fillable = ['link','position'];

    protected $appends = ['image'];

    public function getImageAttribute()
    {
        $img = $this->getMedia('publicities');
        if (count($img) == 0) {
            $img = null;
        } else {
            $img = $img->last()->getFullUrl();
        }
        return $img;
    }
}
