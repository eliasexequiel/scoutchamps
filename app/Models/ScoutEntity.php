<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\DatesFormat;

class ScoutEntity extends Model
{
    use DatesFormat;
    
    protected $table = 'scout_entities';
    
    protected $fillable = ['scout_id', 'entity_id','info_aditional','start','end','isCurrent'];

    protected $casts = [
        'isCurrent' => 'boolean'
    ];

    protected $with = [
        'entity'
    ];
    
    public function scout()
    {
        return $this->belongsTo('App\Models\Scout','scout_id');
    }

    public function entity()
    {
        return $this->belongsTo('App\Models\Entity','entity_id');
    }

    public function setStartAttribute($value)
    {
        $this->attributes['start'] = $this->setDate($value);
    }

    public function getStartAttribute($value)
    {
        return $this->getDate($value);
    }

    public function setEndAttribute($value)
    {
        $this->attributes['end'] = $this->setDate($value);
    }

    public function getEndAttribute($value)
    {
        return $this->getDate($value);
    }
}
