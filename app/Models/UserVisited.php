<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserVisited extends Model
{
    protected $table = 'user_visited';
    
    protected $fillable = ['user_id', 'visitor_user_id'];

    public function user()
    {
        return $this->belongsTo('App\Models\User','user_id')->withTrashed();
    }

    public function visitor()
    {
        return $this->belongsTo('App\Models\User','visitor_user_id')->withTrashed();
    }
}
