<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ConversationMessages extends Model
{
    protected $table = 'conversation_messages';

    protected $fillable = ['from','to', 'body','conversation_id'];

    public function fromUser()
    {
        return $this->belongsTo('App\Models\User','from');
    }

    public function toUser()
    {
        return $this->belongsTo('App\Models\User','to');
    }

    public function conversation()
    {
        return $this->belongsTo('App\Models\Conversation','conversation_id');
    }
}
