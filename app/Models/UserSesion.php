<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserSesion extends Model
{
    protected $table = 'user_sesions';

    protected $fillable = [
        'ip', 
        'device',
        'user_id'
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User','user_id');
    }
}
