<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\DatesFormat;

class ScoutPosition extends Model
{
    
    protected $table = 'scout_positions';
    
    protected $fillable = ['name'];
    
    public function getNameAttribute($value) 
    {
        return trans('fields.positionsScout.'. $value);
    }
}
