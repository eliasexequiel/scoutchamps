<?php
namespace App\Models;

use Spatie\MediaLibrary\Models\Media as BaseMedia;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Media extends BaseMedia
{

    use SoftDeletes;

    protected $appends = ['url','fullUrl'];

    protected $dates = [
        'deleted_at',
    ];
    
    public function getUrlAttribute()
    {
        return $this->getUrl();
    }

    public function getFullUrlAttribute()
    {
        return $this->getFullUrl();
    }

    public function getCreatedAtAttribute($input)
    {
        if ($input != null) {
            return Carbon::createFromFormat('Y-m-d H:i:s', $input)->format('d/m/Y H:i');
        } else {
            return '';
        }
    }

    public function report()
    {
        return $this->morphOne('App\Models\Report', 'reported');
    }
}