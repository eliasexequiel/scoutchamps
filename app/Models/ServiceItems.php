<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class ServiceItems extends Model
{
    use HasTranslations;

    protected $table = 'service_items';
    
    protected $fillable = ['name'];
    public $translatable = ['name'];
}
