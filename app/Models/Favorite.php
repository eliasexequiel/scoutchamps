<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Favorite extends Model
{
    protected $table = 'favorites';

    protected $fillable = ['favorite_user_id','user_id',];

    public function favorite()
    {
        return $this->belongsTo('App\Models\User','favorite_user_id');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User','user_id');
    }
}
