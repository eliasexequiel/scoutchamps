<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class SportFieldsOptionsSelect extends Model
{
    use HasTranslations;
    protected $table = 'sport_fields_options_select';
    
    protected $fillable = ['name', 'sport_field_id'];
    public $translatable = ['name'];

}
