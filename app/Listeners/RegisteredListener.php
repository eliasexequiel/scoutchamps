<?php

namespace App\Listeners;

use Illuminate\Auth\Events\Registered;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Update as ModelUpdate;
use App\Models\User;
class RegisteredListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Registered  $event
     * @return void
     */
    public function handle(Registered $event)
    {
        $user = $event->user;
        $user = User::findOrFail($user->id);
        // $body = trans('titles.userRegistered',['name' => $user->fullName]);
        
        ModelUpdate::create(['user_id' => $user->id,'type' => 'registered','body' => '']);

        return true;
    }
}
