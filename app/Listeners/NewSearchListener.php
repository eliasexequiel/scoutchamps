<?php

namespace App\Listeners;

use App\Events\NewSearch;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Search;

class NewSearchListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewSearch  $event
     * @return void
     */
    public function handle(NewSearch $event)
    {
        $user = $event->user;
        $fields = $event->fields;

        Search::create(['user_id' => $user->id, 'text_searched' => json_encode($fields)]);

        return true;
    }
}
