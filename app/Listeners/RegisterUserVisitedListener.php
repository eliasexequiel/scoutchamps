<?php

namespace App\Listeners;

use App\Events\UserVisited;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\UserVisited as ModelUserVisited;

class RegisterUserVisitedListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserVisited  $event
     * @return void
     */
    public function handle(UserVisited $event)
    {
        $user = $event->user;
        $userVisited = $event->userVisited;

        ModelUserVisited::create(['user_id' => $userVisited->id, 'visitor_user_id' => $user->id]);
        
        return true;
    }
}
