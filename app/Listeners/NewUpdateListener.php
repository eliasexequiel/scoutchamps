<?php

namespace App\Listeners;

use App\Events\NewUpdate;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Update as ModelUpdate;

class NewUpdateListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewUpdate  $event
     * @return void
     */
    public function handle(NewUpdate $event)
    {
        $body = $event->body;
        $type = $event->type;
        $user = $event->user;
        ModelUpdate::create(['user_id' => $user->id,'type' => $type,'body' => $body]);

        return true;
    }
}
