<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CreateAthleteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // Allow only with domains google, yahoo, hotmail, etc
        // regex:/(.*)@(gmail|yahoo|outlook|hotmail)[.](.{0,6})$/
        $rules =  [
            
                'firstname'             => 'required|max:255',
                'lastname'              => 'required|max:255',
                'email'                 => ['required','email','max:255','unique:users'],
                'password'              => 'required|min:6|max:30|confirmed',
                'password_confirmation' => 'required|same:password',
                'phone'                 => ['required','regex:/^[+]?\(?[0-9]*\)?[0-9]*$/'],
                'tyc'                   => 'required'
        ];
        if(app()->getLocale() == "en") {
            $rules['date_of_birth'] = 'required|date_format:m/d/Y';
        } else {
            $rules['date_of_birth'] = 'required|date_format:d/m/Y';
        }
        if($this->age != '' && $this->age < 18)
        {
            $rules['emailTutor']      = 'required|email'; 
            $rules['firstnameTutor']  = 'required';
            $rules['lastnameTutor']   = 'required';
            $rules['phoneTutor']      = ['required','regex:/^[+]?\(?[0-9]*\)?[0-9]*$/'];
            
        }
        return $rules;
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    // public function withValidator($validator)
    // {
    //     $validator->after(function ($validator) {
    //         $validator->sometimes('emailTutor', 'required|email', function ($input) {
    //             return $input->age < 18;
    //         });
    //     });
    // }


    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'date_of_birth.date_format' => trans('auth.dobDateFormat'),
            'tyc.required'              => trans('auth.tycRequired'),
            'phone.regex'               => trans('validation.athlete.phoneRegex')
        ];
    }

}
