<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Input;
use FFMpeg;

class UploadVideosMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->has('url')) {
            return $next($request);
        }
        // $video = FFMpeg\FFProbe::create([
        //     'ffmpeg.binaries' => '/usr/bin/ffmpeg',
        //     'ffprobe.binaries' => '/usr/bin/ffprobe'
        // ]);
        // $duration = $video->format(Input::file('file'))->get('duration');

        if($request->user() && $request->user()->hasRole('athlete') && !$request->user()->hasPermission('upload.unlimited.videos')) {
            if((count($request->user()->getMedia('videos')) >= 1 || $request->user()->mediaurls()->count() >= 1)) {
                return response()->json(['message' => trans('validation.videos.limit')],401);
            }
            // if($duration > 90) {
            //     return response()->json(['message' => trans('validation.videos.duration',['duration' => '90 seg'])],401);   
            // } 
        }
        return $next($request);
    }
}
