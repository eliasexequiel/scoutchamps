<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Verified;

class AutoLoginVerifyMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $verifyUserId = $request->route('id');
         \Log::info("AUTO LOGIN");
        // Support cross-user verification.
        if ($request->user()) {
            return $next($request);
        }
        if ($request->user() && $request->user()->getKey() != $verifyUserId) {
            \Auth::guard()->logout();
        }

        // Support guest verification.
        if (!$request->user()) {
            \Auth::guard()->loginUsingId($verifyUserId, true);
        }

        if ($request->user()->markEmailAsVerified()) {
           event(new Verified($request->user()));
           return redirect($this->redirectTo())->with('verified', true);
        }
        return $next($request);

    }

    protected function redirectTo() 
    {
        if(\Auth::user()->hasRole('athlete')) return route('mysubcriptions.index');
        return route('home');
    }

}
