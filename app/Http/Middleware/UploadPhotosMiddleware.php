<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class UploadPhotosMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $imagesAthleteFree = \DB::table('config_site')->where('name', 'limitImagesAthleteFree')->value('value');
        $imagesScoutFree = \DB::table('config_site')->where('name', 'limitImagesScoutFree')->value('value');
        if($request->user() 
           && $request->user()->hasRole('athlete') 
           && count($request->user()->getMedia('images')) >= $imagesAthleteFree 
           && !$request->user()->hasPermission('upload.unlimited.photos')) {
            return response()->json(['message' => 'Not Authorized'],401);
        }
        return $next($request);
    }
}
