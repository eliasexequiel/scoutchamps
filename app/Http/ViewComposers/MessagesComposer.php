<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Models\Thread;
class MessagesComposer
{

    // protected $usersport;

    public function __construct()
    {
        // Dependencies automatically resolved by service container...
        // $this->users = $users;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $threads = Thread::with('participants','messages')->forUser(\Auth::id())->latest('updated_at')->get();
        $data = [
            'threadsSidebar' => $threads
        ];
        $view->with($data);
    }
}
