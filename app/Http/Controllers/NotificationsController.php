<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Notifications\DatabaseNotification;

class NotificationsController extends Controller
{
    public function index()
    {
        $paginationSize = 5000;
        $notifications = \Auth::user()
            ->unreadNotifications()
            ->orderBy('created_at','desc')
            ->paginate($paginationSize, ['*'], 'unread');
        $readNotifications = DatabaseNotification::where('notifiable_id', \Auth::user()->id)
            ->where('read_at', '!=', NULL)
            ->orderBy('created_at','desc')
            ->paginate($paginationSize, ['*'], 'read');
        // dd($readNotifications); 
        $index = 0;
        if(request()->query('index')) $index = request()->query('index');
        return view('notifications.index', compact('notifications', 'readNotifications','index'));
    }

    public function markAsRead($id)
    {
        $noti = DatabaseNotification::find($id);
        $noti->markAsRead();

        return redirect()->route('notifications.index',['index' => request()->query('index')]);
    }

    public function markAllAsRead()
    {
        $user = \Auth::user();
        $user->unreadNotifications()->update(['read_at' => now()]);

        return redirect()->back();
    }
}
