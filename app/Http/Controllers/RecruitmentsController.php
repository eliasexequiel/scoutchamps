<?php

namespace App\Http\Controllers;

use App\Events\NewNotification;
use App\Models\Country;
use Illuminate\Http\Request;
use Validator;
use App\Models\Recruitment;
use App\Models\RecruitmentAthlete;
use App\Models\User;
use App\Notifications\InvitationRecruitment;
use App\Repositories\Interfaces\RecruitmentRepositoryInterface;

class RecruitmentsController extends Controller
{
    protected $recruitmentRepository;

    public function __construct(RecruitmentRepositoryInterface $recruitmentRepository)
    {
        $this->recruitmentRepository = $recruitmentRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $recruitments = $this->recruitmentRepository->getByUser(\Auth::user());

        return view('scout.recruitments.index', compact('recruitments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countries = Country::get()->sortBy('name')->pluck('name','id');
        return view('scout.recruitments.create', compact('countries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd(date('d/m/Y') == $request->day_start);
        $yesterday = date('d/m/Y', strtotime('-1 days'));
        $validator = Validator::make(
            $request->all(),
            [
                'day_start'      => 'required|date_format:d/m/Y|after_or_equal:' . $yesterday,
                'day_end'        => 'required|date_format:d/m/Y|after_or_equal:day_start',
                'hour'           => 'required|date_format:H:i',
                'address'        => 'required',
                'place'          => 'required',
                'city'           => 'required',
                'country_id'     => 'required',
                'is_public'      => 'required'
            ]
        );

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        $data = $request->only('day_start', 'day_end', 'hour', 'address', 'place', 'city', 'country_id', 'message');
        $public = $request->is_public == 'public' ? 'on' : 'off';
        $data = array_merge($data, ['user_id' => \Auth::user()->id,'is_public' => $public]);
        Recruitment::create($data);

        return redirect()->route('recruitments.index')->with('success', trans('recruitments.createSuccess'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $recruitment = Recruitment::find($id);
        $recruitment->load('usersInvited.user.athlete.sport');
        return view('scout.recruitments.show', compact('recruitment'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $recruitment = Recruitment::find($id);
        // $countries = Country::pluck('name', 'id');
        $countries = Country::get()->sortBy('name')->pluck('name','id');
        return view('scout.recruitments.edit', compact('recruitment','countries'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request->all());
        $yesterday = date('d/m/Y', strtotime('-1 days'));
        $validator = Validator::make(
            $request->all(),
            [
                'day_start'      => 'required|date_format:d/m/Y|after_or_equal:' . $yesterday,
                'day_end'        => 'required|date_format:d/m/Y|after_or_equal:day_start',
                'hour'           => 'required|date_format:H:i',
                'address'        => 'required',
                'place'          => 'required',
                'city'           => 'required',
                'country_id'     => 'required',
                'is_public'      => 'required'
            ]
        );

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        $public = $request->is_public == 'public' ? 'on' : 'off';
        $data = $request->only('day_start', 'day_end', 'hour', 'address', 'place', 'city', 'country_id', 'message');
        $data = array_merge($data, ['is_public' => $public]);
        Recruitment::findOrFail($id)->update($data);

        return redirect()->route('recruitments.index')->with('success', trans('recruitments.updateSuccess'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $r = Recruitment::findOrFail($id);
        foreach ($r->usersInvited as $invitation) {
            $invitation->delete();
        }
        $r->delete();
        return redirect()->route('recruitments.index')->with('success', trans('recruitments.deleteSuccess'));
    }

    public function sendInvitations(Request $request)
    {
        $user = User::uuid($request->uuid)->firstOrFail();
        $currentUser = \Auth::user();
        // Invitations

        if ($request->has('recruitments') && count($request->recruitments) > 0) {
            $this->recruitmentRepository->sendInvitations($user, $request->recruitments);
            // Send Notifications to User by every Recruitment
            foreach ($request->recruitments as $r) {
                $recruitment = Recruitment::find($r);
                $user->notify(new InvitationRecruitment($currentUser, $recruitment));
                event(new NewNotification($user));
            }
        }

        return redirect()->back();
    }

    public function showInvitations($id)
    {
        $recruitment = Recruitment::with('usersInvited.user')->find($id);
        return view('scout.recruitments.show-invitations', compact('recruitment'));
    }


    /************ APIS METHODS *************/


    // RECRUIMENTS CREATED BY SCOUT
    public function getRecruitments()
    {
        $recruitments = $this->recruitmentRepository->latestByUsers(null, [\Auth::user()->id], 10);
        $recruitments->load('usersInvited.user.athlete');
        $data = ['recruitments' => $recruitments];
        return response()->json($data);
    }

    public function getRecruitment($id)
    {
        $r = Recruitment::findOrFail($id);
        $r->load('usersInvited.user.athlete');
        $data = ['recruitment' => $r];

        if (request()->query('withUsers')) {
            $user = \Auth::user();
            $user->load('followed.following.athlete.sport','followers.user', 'favorites.favorite.athlete.sport');
            $usersInvited = $r->usersInvited->pluck('user_id')->toArray();
            $followed = array_filter($user->followed->toArray(), function ($u) use ($usersInvited) {
                return !in_array($u['following']['id'], $usersInvited);
            });
            $followers = array_filter($user->followers->toArray(), function ($u) use ($usersInvited) {
                return !in_array($u['user']['id'], $usersInvited);
            });

            $favorites = array_filter($user->favorites->toArray(), function ($u) use ($usersInvited) {
                return !in_array($u['favorite']['id'], $usersInvited);
            });

            $data = [
                'recruitment' => $r,
                'followed' => array_values($followed),
                'followers' => array_values($followers),
                'favorites' => array_values($favorites)
            ];
        }
        return response()->json($data);
    }

    public function recruitmentSendInvitations(Request $request, $id)
    {
        $user = \Auth::user();
        $recruitment = Recruitment::find($id);
        $this->recruitmentRepository->sendInvitationsManyUsers($recruitment, $request->idUsers);
        // Send Notifications to every User
        foreach ($request->idUsers as $userid) {
            $userInvited = User::find($userid);
            $userInvited->notify(new InvitationRecruitment($user, $recruitment));
            event(new NewNotification($userInvited));
        }
        return response()->json(['res' => trans('recruitments.invitation.usersInvitedSuccess')]);
    }

    public function removeUserFromRecruitment($id, $idUser)
    {
        $recruitment = Recruitment::find($id);
        $user = User::find($idUser);
        $this->recruitmentRepository->deleteUserFromRecruitment($recruitment, $user);

        return response()->json(['res' => trans('recruitments.invitation.usersDeletedSuccess')]);
    }
}
