<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Auth;
use App\Models\Athlete;
use App\Models\SportFieldsAthlete;
use App\Models\Entity;
use App\Models\AchievementsAthletesEntities;
use App\Models\AthleteEntity;
use App\Models\User;
use jeremykenedy\LaravelRoles\Models\Role;
use App\Http\Requests\AthleteEntityRequest;
use App\Events\NewUpdate as NewUpdateEvent;
use App\Models\EntityType;
use App\Notifications\ClubCreated;
use Exception;
use Illuminate\Support\Facades\Notification;

class AthleteController extends Controller
{

    public function getAthlete($id)
    {
        $athlete = Athlete::with('fieldsSports', 'sport', 'sportSecondary')->where('id', $id)->first();
        return response()->json(['athlete' => $athlete]);
    }

    public function saveSportsAthlete(Request $request, $id)
    {
        $athlete = Athlete::findOrFail($id);
        if (!$request->secondary) {
            if ($athlete->sport_id != null && $athlete->sport_id != $request->sport['id']) {
                // Si el atleta ya tenia otro deporte eliminamos los campos asociados en SportFieldsAthlete
                SportFieldsAthlete::where('sport_id', $athlete->sport_id)
                    ->where('athlete_id', $id)
                    ->where('is_secondary', false)
                    ->delete();
            }
            $athlete->sport_id = $request->sport['id'];
        } else {
            if ($athlete->secondary_sport_id != null && $athlete->secondary_sport_id != $request->sport['id']) {
                // Si el atleta ya tenia otro deporte eliminamos los campos asociados en SportFieldsAthlete
                SportFieldsAthlete::where('sport_id', $athlete->secondary_sport_id)
                    ->where('athlete_id', $id)
                    ->where('is_secondary', true)
                    ->delete();
            }
            $athlete->secondary_sport_id = $request->sport['id'];
        }
        $athlete->save();

        $is_secondary = $request->secondary ? 1 : 0;
        if (is_array($request->fields)) {
            foreach ($request->fields as $f) {
                SportFieldsAthlete::updateOrCreate(
                    ['sport_field_id' => $f['id'], 'sport_id' => $request->sport['id'], 'athlete_id' => $athlete->id, 'is_secondary' => $is_secondary],
                    ['value' => $f['value']]
                );
            }
        }

        event(new NewUpdateEvent(\Auth::user(), '', 'updateSports'));

        // SportFieldsAthlete::updateOrCreate()
        return response()->json(['msg' => trans('profile.updateSuccess')]);
    }

    public function saveClubsAthlete(AthleteEntityRequest $request, $id)
    {
        try {
            if ($this->validateEntities($request->clubs)) throw new Exception(trans('clubes.forms.errorName'));

            // Borramos los CLUBES que no están mas presentes.
            $athlete = Athlete::find($id);
            $currentIds = $athlete->entities->pluck('id')->toArray() ?? array();
            $ids = array_filter($request->clubs, function ($c) {
                return isset($c['id']);
            });
            $ids = array_map(function ($c) {
                return $c['id'];
            }, $ids);
            $diffToDelete = array_diff($currentIds, $ids);
            AchievementsAthletesEntities::whereIn('athlete_entity_id', $diffToDelete)->delete();
            AthleteEntity::destroy($diffToDelete);

            foreach ($request->clubs as $c) {
                $end = $c['isCurrent'] ? null : $c['end'];
                if ($c['entity_id'] == 'other' && $c['other_entity'] != '') {
                    $type = EntityType::where('name', $c['type_entity'])->first();
                    $e = Entity::create([
                        'name' => $c['other_entity'],
                        'entity_type_id' => $type->id,
                        'verified' => false
                    ]);

                    // Notifiy to ADMIN Users.
                    $adminRol = Role::where('slug', 'admin')->first();
                    $usersAdmin = User::typeUser($adminRol)->get();
                    Notification::send($usersAdmin, new ClubCreated(\Auth::user(), $e));

                    $athleteEntity = AthleteEntity::create([
                        'entity_id' => $e->id,
                        'athlete_id' => $id,
                        'start' => $c['start'],
                        'end' => $end,
                        'isCurrent' => $c['isCurrent'],
                        'info_aditional' => $c['info_aditional']
                    ]);
                } else {
                    if (isset($c['id'])) {
                        $athleteEntity = AthleteEntity::find($c['id'])->update([
                            'entity_id' => $c['entity_id'],
                            'start' => $c['start'],
                            'end' => $end,
                            'isCurrent' => $c['isCurrent'],
                            'info_aditional' => $c['info_aditional']
                        ]);
                        $athleteEntity = AthleteEntity::find($c['id']);
                    } else {
                        $athleteEntity = AthleteEntity::create([
                            'entity_id' => $c['entity_id'],
                            'start' => $c['start'],
                            'end' => $end,
                            'athlete_id' => $id,
                            'isCurrent' => $c['isCurrent'],
                            'info_aditional' => $c['info_aditional']
                        ]);
                    }
                }
                $athleteEntity = AthleteEntity::where('id', $athleteEntity->id)->with('achievements')->first();

                // Borramos los ACHIEVEMENTS que no están mas presentes.
                $currentIds = $athleteEntity->achievements->pluck('id')->toArray() ?? array();
                $ids = array_filter($c['achievements'], function ($e) {
                    return isset($e['id']);
                });
                $ids = array_map(function ($e) {
                    return $e['id'];
                }, $ids);
                $diffToDelete = array_diff($currentIds, $ids);
                AchievementsAthletesEntities::destroy($diffToDelete);

                // dd($ids);
                foreach ($c['achievements'] as $a) {
                    if (isset($a['id'])) {
                        AchievementsAthletesEntities::find($a['id'])->update([
                            'name' => $a['name'],
                            'month' => $a['month'],
                            'year' => $a['year']
                        ]);
                    } else {
                        AchievementsAthletesEntities::create([
                            'athlete_entity_id' => $athleteEntity->id,
                            'name' => $a['name'],
                            'month' => $a['month'],
                            'year' => $a['year']
                        ]);
                    }
                }
            }

            if (!\Auth::user()->hasRole('admin')) {
                event(new NewUpdateEvent(\Auth::user(), '', 'updateClubs'));
            }

            return response()->json(['msg' => trans('profile.updateSuccess')]);
        } catch (Exception $e) {
            return response()->json(['errors' => [$e->getMessage()]], 500);
        }
    }

    private function validateEntities($entities)
    {
        $exist = false;
        foreach ($entities as $c) {
            if ($c['entity_id'] == 'other' && $c['other_entity'] != '') {
                if (Entity::where('name', $c['other_entity'])->exists()) {
                    $exist = true;
                    break;
                }
            }
        }
        return $exist;
    }
}
