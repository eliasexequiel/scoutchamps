<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Auth;
use App\Models\Scout;
use App\Models\ScoutEntity;
use App\Models\SportFieldsAthlete;
use App\Models\Entity;
use App\Http\Requests\ScoutEntityRequest;
use App\Events\NewUpdate as NewUpdateEvent;
use App\Models\EntityType;
use App\Models\User;
use jeremykenedy\LaravelRoles\Models\Role;
use App\Notifications\ClubCreated;
use Exception;
use Illuminate\Support\Facades\Notification;

class ScoutController extends Controller
{

    // public function getAthlete($id)
    // {
    //     $athlete = Athlete::with('fieldsSports','sport','sportSecondary')->where('id',$id)->first();
    //     return response()->json(['athlete' => $athlete]);
    // }

    public function saveClubsScout(ScoutEntityRequest $request, $id)
    {

        try {
            if ($this->validateEntities($request->clubs)) throw new Exception(trans('clubes.forms.errorName'));
            // Borramos los CLUBES que no están mas presentes.
            $scout = Scout::find($id);
            $currentIds = $scout->entities->pluck('id')->toArray() ?? array();
            $ids = array_filter($request->clubs, function ($c) {
                return isset($c['id']);
            });
            $ids = array_map(function ($c) {
                return $c['id'];
            }, $ids);
            $diffToDelete = array_diff($currentIds, $ids);
            ScoutEntity::destroy($diffToDelete);

            foreach ($request->clubs as $c) {
                $end = $c['isCurrent'] ? null : $c['end'];
                if ($c['entity_id'] == 'other' && $c['other_entity'] != '') {
                    $type = EntityType::where('name', $c['type_entity'])->first();
                    $e = Entity::create([
                        'name' => $c['other_entity'],
                        'entity_type_id' => $type->id,
                        'verified' => false
                    ]);

                    // Notifiy to ADMIN Users.
                    $adminRol = Role::where('slug', 'admin')->first();
                    $usersAdmin = User::typeUser($adminRol)->get();
                    Notification::send($usersAdmin, new ClubCreated(\Auth::user(), $e));

                    $scoutEntity = ScoutEntity::create([
                        'entity_id' => $e->id,
                        'scout_id' => $id,
                        'start' => $c['start'],
                        'end' => $end,
                        'isCurrent' => $c['isCurrent'],
                        'info_aditional' => $c['info_aditional']
                    ]);
                } else {
                    if (isset($c['id'])) {
                        $scoutEntity = ScoutEntity::find($c['id'])->update([
                            'entity_id' => $c['entity_id'],
                            'start' => $c['start'],
                            'end' => $end,
                            'isCurrent' => $c['isCurrent'],
                            'info_aditional' => $c['info_aditional']
                        ]);
                    } else {
                        $scoutEntity = ScoutEntity::create([
                            'entity_id' => $c['entity_id'],
                            'start' => $c['start'],
                            'end' => $end,
                            'scout_id' => $id,
                            'isCurrent' => $c['isCurrent'],
                            'info_aditional' => $c['info_aditional']
                        ]);
                    }
                }
            }
            if(!\Auth::user()->hasRole('admin')) {
                event(new NewUpdateEvent(\Auth::user(), '', 'updateClubs'));
            }
            return response()->json(['msg' => trans('profile.updateSuccess')]);
        } catch (Exception $e) {
            return response()->json(['errors' => [$e->getMessage()]], 500);
        }
    }

    public function saveSports(Request $request, $id)
    {
        $scout = Scout::findOrFail($id);
        $scout->sport_id = $request->sport;
        $scout->save();

        event(new NewUpdateEvent(\Auth::user(), '', 'updateSports'));
        return response()->json(['msg' => trans('profile.updateSuccess')]);
    }

    private function validateEntities($entities)
    {
        $exist = false;
        foreach ($entities as $c) {
            if ($c['entity_id'] == 'other' && $c['other_entity'] != '') {
                if (Entity::where('name', $c['other_entity'])->exists()) {
                    $exist = true;
                    break;
                }
            }
        }
        return $exist;
    }
}
