<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;
use App\Models\Update;
use App\Models\User;

class UpdatesController extends Controller
{
    public function getUpdates()
    {
        $types = ['registered', 'followed', 'newVideos', 'newPhotos', 'updateSports', 'updateClubs'];
        $data = Update::where('user_id', '!=', \Auth::user()->id)
            ->userNotDeleted()
            ->whereIn('type', $types)
            ->latest()
            ->take(20)
            ->get();
        $data->load('user');
        return response()->json(['updates' => $data]);
    }

    // public function getFavoritesUsers()
    // {
    //     $paginateSize = 5;
    //     $user = \Auth::user();
    //     // $types = ['registered', 'followed'];
    //     $types = ['newPhotos','newVideos','newPost','updateSports','updateClubs'];
    //     $favorites = $user->favorites->pluck('favorite_user_id')->all();
    //     $data = Update::whereIn('user_id', $favorites)
    //         ->userNotDeleted()
    //         ->whereIn('type', $types)
    //         ->orderByDesc('created_at')
    //         ->paginate($paginateSize)
    //         ->transform(function($update) {
    //             $post = null;

    //             if(is_array($update->body) && array_key_exists('post_id',$update->body)) {
    //                 $idPost = $update->body['post_id'];
    //                 $post = Post::find($idPost);
    //                 $post->load('comments.user','likes.user','user');
    //             }
    //             $update['post'] = $post;
    //             return $update;
    //         });
    //     $data->load('user.athlete.currentEntity.entity', 'user.athlete.sport');
    //     return response()->json(['updates' => $data]);
    // }

    public function getUpdatesFollowedFavoritesUsers()
    {
        $paginateSize = 5;
        $user = \Auth::user();
        //  $types = ['registered', 'followed'];
        if (\Auth::user()->hasRole('admin')) {
            $paginateSize = 10;
            $types = ['registered', 'followed','newPhotos', 'newVideos', 'newPost', 'updateSports', 'updateClubs'];
            $data = Update::userNotDeleted()
                ->whereIn('type', $types)
                ->orderByDesc('created_at')
                ->paginate($paginateSize)
                ->transform(function ($update) {
                    $post = null;

                    if (is_array($update->body) && array_key_exists('post_id', $update->body)) {
                        $idPost = $update->body['post_id'];
                        $post = Post::find($idPost);
                        $post->load('comments.user', 'likes.user', 'user');
                    }
                    $update['post'] = $post;
                    return $update;
                });
        } else {
            $types = ['newPhotos', 'newVideos', 'newPost', 'updateSports', 'updateClubs'];
            // $typesLoggedUser = ['newPhotos', 'newVideos', 'newPost'];
            $typesLoggedUser = ['newPost'];
            $followed = $user->followed->pluck('following_user_id')->all();
            $favorites = $user->favorites->pluck('favorite_user_id')->all();
            $usersUpdates = array_merge($followed, $favorites);
            $data = Update::userNotDeleted()
                ->whereIn('user_id', $usersUpdates)
                ->whereIn('type', $types)
                ->orWhereIn('type', $typesLoggedUser)
                ->where('user_id', $user->id)
                ->orderByDesc('created_at')
                ->paginate($paginateSize)
                ->transform(function ($update) {
                    $post = null;

                    if (is_array($update->body) && array_key_exists('post_id', $update->body)) {
                        $idPost = $update->body['post_id'];
                        $post = Post::find($idPost);
                        $post->load('comments.user', 'likes.user', 'user');
                    }
                    $update['post'] = $post;
                    return $update;
                });
        }

        $data->load('user.athlete.currentEntity.entity', 'user.athlete.sport');
        return response()->json(['updates' => $data]);
    }
}
