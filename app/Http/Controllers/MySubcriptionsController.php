<?php

namespace App\Http\Controllers;

use App\Models\Service;
use Illuminate\Http\Request;
use App\Models\User;
use Stripe\Stripe;
use Auth;

class MySubcriptionsController extends Controller
{
    public function __construct()
    {
        Stripe::setApiKey(config('services.stripe.secret'));
    }

    public function index()
    {
        if(!Auth::user()->hasRole('athlete')) return redirect()->route('home');
        $currentSubscription = Auth::user()->subscriptions()->whereNull('ends_at')->first();
        $plans = Service::orderBy('id')->with('items.item')->get();
        return view('mysubscriptions.index', compact('currentSubscription','plans'));
    }

    public function invoices()
    {
        $invoices = Auth::user()->invoicesIncludingPending();
        // dd($invoices);
        return view('mysubscriptions.invoices.index', compact('invoices'));
    }

    public function downloadInvoice(Request $request, $invoiceId)
    {
        // $planId = Auth::user()->subscription('main');
        $invoice = \Stripe\Invoice::retrieve($invoiceId);
        $product = '';
        if(count($invoice->lines->data) > 0) {
            $product = $invoice->lines->data[0]->plan->nickname;
        }
        return $request->user()->downloadInvoice($invoiceId, [
            'vendor' => env('app.name'),
            'product' => $product,
        ]);
    }

    public function paymentMethods()
    {
        $user = Auth::user();
        // $user->updateCardFromStripe();
        $cardDefault = $user->defaultCard();
        if (!$user->hasStripeId()) $user->createAsStripeCustomer();
        $paymentsMethods = \Stripe\PaymentMethod::all(['type' => 'card', 'customer' => $user->stripe_id]);
        // $intent = $user->createSetupIntent(['customer' => $user->stripe_id]);
        // dd($paymentsMethods);
        return view('mysubscriptions.paymentmethods.index', compact('cardDefault', 'paymentsMethods'));
    }

    public function createPaymentMethod()
    {
        $user = Auth::user();
        $stripe_id = $user->asStripeCustomer();
        $setup_intent = \Stripe\SetupIntent::create(['customer' => $stripe_id]);
        return view('mysubscriptions.paymentmethods.create', compact('setup_intent'));
    }

    public function storePaymentMethod(Request $request)
    {
        $user = Auth::user();
        $stripe_id = $user->asStripeCustomer();
        $payment_method = \Stripe\PaymentMethod::retrieve($request->payment_method);
        $payment_method->attach(['customer' => $stripe_id]);

        return redirect()->route('mysubcriptions.paymentmethods')->with('success', trans('subscriptions.mysubscriptions.storePaymentMethodSuccess'));
    }

    public function setDefaultPaymentMethod($idCard)
    {
        $user = Auth::user();
        if ($user->subscribed('main')) {
            // $card = \Stripe\Customer::retrieveSource($user->stripe_id,$idCard);
            // \Stripe\Customer::update($user->stripe_id, ['default_source' => $idCard]);

            $subscription = $user->subscription('main');
            \Stripe\Subscription::update($subscription->stripe_id, ['default_payment_method' => $idCard]);

            $user->updateCardFromStripe();
        }
        return redirect()->route('mysubcriptions.paymentmethods')->with('success', trans('subscriptions.mysubscriptions.defaultCardSuccess'));
    }

    public function destroyPaymentMethod($id)
    {
        $payment_method = \Stripe\PaymentMethod::retrieve($id);
        $payment_method->detach();

        return redirect()->route('mysubcriptions.paymentmethods')->with('success', trans('subscriptions.mysubscriptions.deleteCardSuccess'));
    }
}
