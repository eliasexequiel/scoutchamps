<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Entity;
use App\Models\EntityType;
use App\Models\Country;
use Validator;

class ClubesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $pagintaionEnabled = config('usersmanagement.enablePagination');
        if ($pagintaionEnabled) {
            $clubes = Entity::where('name', 'LIKE', "%$request->nameFilter%")
                ->orderBy('name')
                ->paginate(config('usersmanagement.paginateListSize'));
        } else {
            $clubes = Entity::orderBy('name')->get();
        }

        if (request()->query('success') == 'created') return redirect()->route('clubes.index')->with('success', trans('clubes.createSuccess'));
        if (request()->query('success') == 'updated') return redirect()->route('clubes.index')->with('success', trans('clubes.updateSuccess'));
        return view('admin.clubes.index', compact('clubes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $typesEntities = ['Club','University','Sponsor','Company'];
        $types = EntityType::select('name', 'id')->whereIn('name',$typesEntities)->get()->toArray();
        $data = [
            'types' => $types,
            'countries' => Country::get()->sortBy('name')->pluck('name', 'id')->prepend(trans('clubes.forms.country'), '')->toArray()
        ];
        // dd($data['countries']);
        return view('admin.clubes.create')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'name'            => 'required|unique:entities',
                'entity_type_id'  => 'required',
                'email'           => 'nullable|email'
            ]
        );
        // return response()->json(['url' => $request->all()], 402);

        if ($validator->fails()) {
            if ($request->ajax()) {
                $errors = $validator->errors()->all();
                return response()->json(['errors' => $errors], 403);
            }
            return back()->withErrors($validator)->withInput();
        }

        $data = $request->only('name', 'entity_type_id', 'email', 'website', 'phone', 'country_id');
        $data = array_merge($data, ['verified' => true]);
        $entity = Entity::create($data);

        // dd($request->all());
        if ($request->has('file')) {
            $avatar = $request->file;
            // dd($avatar);
            $entity->addMedia($avatar)->toMediaCollection('club');
        }

        if ($request->ajax()) {
            $route = route('clubes.index');
            return response()->json(['url' => $route], 200);
        } else {
            return redirect()->route('clubes.index')->with('success', trans('clubes.createSuccess'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $club = Entity::findOrFail($id);
        $club->load('country');
        return view('admin.clubes.show', compact('club'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $club = Entity::find($id);
        $typesEntities = ['Club','University','Sponsor','Company'];
        $types = EntityType::select('name', 'id')->whereIn('name',$typesEntities)->get()->toArray();
        $data = [
            'types' => $types,
            'countries' => Country::get()->sortBy('name')->pluck('name', 'id')->prepend(trans('clubes.forms.country'), '')->toArray(),
            'club' => $club
        ];

        return view('admin.clubes.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request->all());

        $validator = Validator::make(
            $request->all(),
            [
                'name'            => 'required|unique:entities,name,' . $id . ',id',
                'entity_type_id'  => 'required',
                'email'           => 'nullable|email'
            ]
        );

        if ($validator->fails()) {
            if ($request->ajax()) {
                $errors = $validator->errors()->all();
                return response()->json(['errors' => $errors], 403);
                // return response()->json(['errors' => $validator->errors()->all()],402);
            }
            return back()->withErrors($validator)->withInput();
        }

        $data = $request->only('name', 'entity_type_id', 'email', 'website', 'phone', 'country_id');
        $entity = Entity::findOrFail($id)->update($data);
        $entity = Entity::findOrFail($id);
        // dd($request->all());

        // Si tenia un logo y se eliminó lo eliminamos
        if ($request->imageDeleted == 'imageDeleted') {
            $entity->clearMediaCollection('club');
        }

        if ($request->has('file')) {
            $avatar = $request->file;
            // dd($avatar);
            $entity->addMedia($avatar)->toMediaCollection('club');
        }

        if ($request->ajax()) {
            $route = route('clubes.index');
            return response()->json(['url' => $route], 200);
        } else {
            return redirect()->route('clubes.index')->with('success', trans('clubes.updateSuccess'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $entity = Entity::findOrFail($id);
        $entity->name = $entity->name . '_deleted' . time();
        $entity->save();
        $entity->delete();
        return redirect()->route('clubes.index')->with('success', trans('clubes.deleteSuccess'));
    }

    public function verify(Request $request, $id)
    {
        $entity = Entity::findOrFail($id);
        $state = $entity->verified ? false : true;
        $entity->verified = $state;
        $entity->save();
        return redirect()->route('clubes.index')->with('success', trans('clubes.verifySuccess'));
    }

    public function getClubs(Request $request)
    {
        if ($request->query('type') == '') $entities = Entity::orderBy('name')->get();
        else {
            $type = EntityType::where('name', $request->query('type'))->first();
            $entities = Entity::where('entity_type_id', $type->id)->get();
        }
        return response()->json(['entities' => $entities]);
    }
}
