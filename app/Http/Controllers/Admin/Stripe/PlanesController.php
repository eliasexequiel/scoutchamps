<?php

namespace App\Http\Controllers\Admin\Stripe;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Stripe\Stripe;
use Illuminate\Pagination\LengthAwarePaginator;
use Auth;
use Validator;
use App\Models\User;
use jeremykenedy\LaravelRoles\Models\Role as JeremykenedyRole;
use Laravel\Cashier\Plan;
use Carbon\Carbon;

class PlanesController extends Controller
{
    public function __construct()
    {
        Stripe::setApiKey(config('services.stripe.secret'));
    }

    public function index()
    {
        try {
            $plans = \Stripe\Plan::all(['limit' => 100]);
            $plans = $plans['data'];
            // dd($plans);
        } catch (\Exception $e) {
            return view('admin.plans.index')->with(['message' => $e->getMessage(), 'status' => 'danger']);
        }
        return view('admin.plans.index', compact('plans'));
    }

    // public function edit($id)
    // {
    //     try {
    //         $plan = \Stripe\Plan::retrieve($id);
    //         return view('admin.plans.edit',compact('plan'));
    //     } catch (\Exception $e) {
    //         return view('admin.plans.index')->with(['message' => $e->getMessage(), 'status' => 'danger']);
    //     }

    //     // dd($invoices);
    //     return view('admin.plans.edit', compact('plan'));
    // }

    // public function update(Request $request,$id)
    // {
    //     try {
    //         dd($id);
    //         $plan = \Stripe\Plan::update($id,['amouint']);
    //     } catch (\Exception $e) {
    //         return back()->withErrors(['message' => $e->getMessage()]);
    //     }

    //     // dd($invoices);
    //     return redirect()->route('admin.planes.index')->with('success',trans('planes.admin.messages.updateSuccess'));
    // }

    public function destroy($id)
    {
        try {
            $plan = \Stripe\Plan::retrieve($id);
            $plan->delete();
            return redirect()->route('admin.planes.index')->with('success', trans('planes.admin.messages.deleteSuccess'));
        } catch (\Exception $e) {
            return back()->withErrors(['message' => $e->getMessage()]);
        }
    }
}
