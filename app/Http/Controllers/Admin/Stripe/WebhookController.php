<?php

namespace App\Http\Controllers\Admin\Stripe;

use App\Models\Service;
use App\Models\User;
use Carbon\Carbon;
use jeremykenedy\LaravelRoles\Models\Role;
use Symfony\Component\HttpFoundation\Response;
use Laravel\Cashier\Http\Controllers\WebhookController as CashierController;

class WebhookController extends CashierController
{
    /**
     * Handle invoice payment succeeded.
     */
    public function handleInvoicePaymentSucceeded($payload)
    {
        \Log::info("INVOICE PAYMENT SUCCESS");
        return response()->json('Webhook Handled: Invoice Payment Success', 200);
    }

    /**
     * Handle invoice payment action required.
     */
    public function handleInvoicePaymentActionRequired($payload)
    {
        \Log::info("ACTION REQUIRED");
        $id = $payload['data']['object']['id'];
        \Log::info($id);

        return response()->json('Webhook Handled: Action Required', 200);
    }

    /**
     * Handle a cancelled customer from a Stripe subscription.
     *
     * @param  array  $payload
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function handleCustomerSubscriptionDeleted(array $payload)
    {
        $userLocal = User::where('stripe_id',$payload['data']['object']['customer'])->first();
        if($userLocal && count($userLocal->roles) > 1) {
            // $role = Role::whereIn('name',['Athlete Premiun','Athlete Premiun Semester','Athlete Premiun Anual'])->first();
            $userLocal->detachRole($userLocal->roles->last());
            $userLocal->save();
            \Log::info("SUBSCRIPTION CANCELLED USER_ID:");
            \Log::info($userLocal->id);
            $this->deleteMediaUser($userLocal);
        }

        $user = $this->getUserByStripeId($payload['data']['object']['customer']);
        if ($user) {
            $user->subscriptions->filter(function ($subscription) use ($payload) {
                return $subscription->stripe_id === $payload['data']['object']['id'];
            })->each(function ($subscription) {
                $subscription->markAsCancelled();
            });
        }

        return new Response('Webhook Handled', 200);
    }

    /**
     * Handle a created customer from a Stripe subscription.
     *
     * @param  array  $payload
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function handleCustomerSubscriptionCreated(array $payload)
    {
        $user = $this->getUserByStripeId($payload['data']['object']['customer']);

        if ($user) {
            
        }
        
        \Log::info("CUSTOMER SUBSCRIPTION CREATED");
        return new Response('Webhook Handled: Customer Subscription Created', 200);
    }

    protected function handlePlanCreated(array $payload)
    {
        $planId = $payload['data']['object']['id'];
        $amount = $payload['data']['object']['amount'];
        $service = Service::where('stripe_plan',$planId)->first();
        if($service) {
            $service->price = $amount/100;
            $service->save();
        }
        \Log::info("PLAN CREATED");
        return new Response('Webhook Handled: PLAN Created', 200);
    }

    protected function deleteMediaUser(User $user) 
    {
        $imagesAthleteFree = \DB::table('config_site')->where('name', 'limitImagesAthleteFree')->value('value');
        $videosFree = 1;
        // DELETE IMAGES
        $images = $user->getMedia('images');
        if(isset($imagesAthleteFree) && count($images) > $imagesAthleteFree) {
            $user->clearMediaCollectionExcept('images',$images->take($imagesAthleteFree));
            // dd("Eliminar");
        }
        // DELETE VIDEOS
        $videosUrl = $user->mediaurls;
        $videos = $videosUrl->merge($user->getMedia('videos'));
        $videosSorted = $videos->sortBy(function($item) { 
            $created = $item->created_at;
            if(get_class($item) === 'App\Models\Media') {
                $created = Carbon::createFromFormat('d/m/Y H:i',$item->created_at)->format('Y-m-d H:i');
            }
            return $created;
         });
         if(count($videosSorted) > $videosFree) {
            $takeToDelete = count($videosSorted) - $videosFree;
            $videosToDelete = $videosSorted->take(-$takeToDelete);
            foreach ($videosToDelete as $video) {
                if(get_class($video) === 'App\Models\Media') {
                    $video->forceDelete();   
                } else {
                    $video->delete();
                }
            } 
         }
         return true;
    }
    
}