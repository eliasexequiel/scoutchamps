<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Service;
use App\Models\User;
use App\Notifications\CouponSubscriptionUsed;
use App\Notifications\UserAddedSubscription;
use App\Notifications\UserDeletedSubscription;
use App\Repositories\Interfaces\SubscriptionRepositoryInterface;
use Illuminate\Support\Facades\Notification;
use jeremykenedy\LaravelRoles\Models\Role;
use Stripe\Stripe;
use Auth;
use Validator;

class PlanesController extends Controller
{
    protected $subscriptionRepository;

    public function __construct(SubscriptionRepositoryInterface $subscriptionRepository)
    {
        Stripe::setApiKey(config('services.stripe.secret'));
        $this->subscriptionRepository = $subscriptionRepository;
    }

    public function index()
    {
        if(!Auth::user()->hasRole('athlete')) return redirect()->route('home');

        $planes = \Stripe\Plan::all(['limit' => 100]);
        $planesS = collect();
        foreach ($planes as $p) {
            $planesS->push($p);
        }
        $planes = $planesS->sortBy(function ($p) {
            return $p->amount;
        });
        // dd($planes);
        return view('services.index', compact('planes'));
    }

    public function show($id)
    {
        try {
            if(!Auth::user()->hasRole('athlete')) {
                return redirect()->route('profile.index'); 
            }
            if($id == 'plan_free') {
                $current = $this->checkCurrentSubscriptionAndDelete();
                if($current) {
                    return redirect()->route('profile.index')->with(['message' => trans('planes.messages.success'), 'status'  => 'success']);   
                }
                return redirect()->route('profile.index')->with('success', trans('planes.alreadySubscribed'));
            }
            // dd($plan->nickname);
            if (Auth::user()->subscribedToPlan($id, 'main')) {
                return redirect()->route('profile.index')->with('success', trans('planes.alreadySubscribed'));
            }

            $plan = \Stripe\Plan::retrieve($id);
        } catch (\Exception $e) {
            return redirect()->back()->withErrors(['message' => $e->getMessage()]);
        } 
        return view('services.show', compact('plan'));
    }

    public function saveSubscription(Request $request, $id)
    {
        try {
            $validator = Validator::make($request->all(),[ 'namecardholder' => 'required']);
            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            }
            
            $user = Auth::user();
            $plan = \Stripe\Plan::retrieve($id);
            // $plan = $this->subscriptionRepository->getById($id);
            if ($user->subscribed('main')) { 
                $this->subscriptionRepository->swapSubscription($user, $id);
            } else {
                $this->subscriptionRepository->createSubscription($user, $plan->nickname,$id,$request->coupon, $request->stripeToken,$request->namecardholder);
            }
            // Send notification to admin users
            $adminRol = Role::where('slug', 'admin')->first();
            $usersAdmin = User::typeUser($adminRol)->get();
            Notification::send($usersAdmin, new UserAddedSubscription(Auth::user(),$plan->nickname));
            if($request->coupon != '') {
                Notification::send($usersAdmin, new CouponSubscriptionUsed(Auth::user(),$request->coupon));
            }

            return redirect()->route('profile.index')->with(['message' => trans('planes.messages.success'), 'status'  => 'success']);
        } catch (\Exception $e) {
            \Log::error($e);
            // dd($e);
            if(strpos($e->getMessage(),'No such coupon') !== false) {
                $error = trans('planes.messages.errorCouponSubscription',['coupon' => $request->coupon]);
            } else {
                $error = trans('planes.messages.errorSubscription');
            }
            return redirect()->back()->withErrors(['message' => $error]);
            // return redirect()->back()->withErrors(['message' => trans('planes.messages.errorSubscription')]);
        }
    }

    public function cancelSubscription($id)
    {
        try {
            $plan = \Stripe\Plan::retrieve($id);
            $this->subscriptionRepository->cancelSubscription(Auth::user(), $id);

            // Send notification to admin users
            $adminRol = Role::where('slug', 'admin')->first();
            $usersAdmin = User::typeUser($adminRol)->get();
            Notification::send($usersAdmin, new UserDeletedSubscription(Auth::user(),$plan->nickname));

            return back()->with(['message' => trans('planes.messages.cancelled'), 'status'  => 'success']);
        } catch (\Exception $e) {
            return redirect()->back()->withErrors(['message' => $e->getMessage(),'status' => 'danger']);
        }
    }

    public function checkCurrentSubscriptionAndDelete()
    {
        $currentSubscription = Auth::user()->subscriptions()->whereNull('ends_at')->first();
        if($currentSubscription) {
            $plan = \Stripe\Plan::retrieve($currentSubscription->stripe_plan);
            $this->subscriptionRepository->cancelSubscription(Auth::user(), $currentSubscription->stripe_plan);

            // Send notification to admin users
            $adminRol = Role::where('slug', 'admin')->first();
            $usersAdmin = User::typeUser($adminRol)->get();
            Notification::send($usersAdmin, new UserDeletedSubscription(Auth::user(),$plan->nickname));

            return true;
        }
        return false;
    }

    public function checkPricePlanWithCoupon(Request $request)
    {
        $tax = round($request->porcentajetax / 100,2);
        $coupon = \Stripe\Coupon::retrieve($request->coupon);
        if($coupon['valid']) {
            $total = 0;
            if($coupon['amount_off']) {
                // Tiene descuento fijo
                $subtotal = $request->totalplan - round($coupon['amount_off']/100,2);
                $total = round($subtotal + round($subtotal*$tax,2),2);
            }
            if($coupon['percent_off']) {
                // Tiene descuento fijo
                $percentOff = 1 - round($coupon['percent_off']/100,2);
                $subtotal = $request->totalplan * $percentOff;
                $total = round($subtotal + round($subtotal*$tax,2),2);
            }    
        }
        return response()->json(['coupon' => $coupon, 'totalConDescuento' => $total]);
    }
}
