<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\UserVisited;
use App\Models\Athlete;
use App\Models\Scout;
use App\Models\Country;
use App\Models\Entity;
use App\Models\Faq;
use App\Models\Service;
use App\Models\Sport;
use App\Models\Thread;
use App\Models\User;
use DB;
use jeremykenedy\LaravelRoles\Models\Role;

class WelcomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function welcome()
    {
        // $cantScouts = Scout::select('residence_country')->groupBy('residence_country')->get();
        $athletesByCountry = DB::table('athletes')
            ->join('countries', 'athletes.residence_country_id', '=', 'countries.id')
            ->join('users', 'athletes.user_id', '=', 'users.id')
            ->select('countries.name', 'countries.lat', 'countries.lng', 'residence_country_id', DB::raw('count(*) as totalA'))
            ->where('users.deleted_at','=',NULL)
            ->groupBy('residence_country_id')
            ->orderBy('totalA', 'desc')
            ->get();

        // dd($athletesByCountry);
        $scoutsByCountry = DB::table('scouts')
            ->join('countries', 'scouts.residence_country_id', '=', 'countries.id')
            ->join('users', 'scouts.user_id', '=', 'users.id')
            ->select('countries.name', 'countries.lat', 'countries.lng', 'residence_country_id', DB::raw('count(*) as totalS'))
            ->where('users.deleted_at','=',NULL)
            ->groupBy('residence_country_id')
            ->orderBy('totalS', 'desc')
            ->get();
        // dd($athletesByCountry);
        $arrayMerged = $athletesByCountry->merge($scoutsByCountry)->groupBy('residence_country_id');
        $usersCountries = [];
        foreach ($arrayMerged as $value) {
            $collection = [
                'name' => $value[0]->name,
                'lat' => (float) $value[0]->lat,
                'lng' => (float) $value[0]->lng
            ];
            if (isset($value[0]->totalA)) $collection['totalA'] = $value[0]->totalA;
            if (isset($value[0]->totalS)) $collection['totalS'] = $value[0]->totalS;

            if (count($value) == 2) {
                if (isset($value[1]->totalA)) $collection['totalA'] = $value[1]->totalA;
                if (isset($value[1]->totalS)) $collection['totalS'] = $value[1]->totalS;
            }
            if (!isset($collection['totalA'])) $collection['totalA'] = 0;
            if (!isset($collection['totalS'])) $collection['totalS'] = 0;
            array_push($usersCountries, $collection);
        }
        // dd($usersCountries);

        $plans = Service::orderBy('id')->with('items.item')->get();

        $planScout = (object)trans('fields.planScout');

        $imagesScoutFree = \DB::table('config_site')->where('name', 'limitImagesScoutFree')->value('value');
        // dd($planScout);
        // $athletesCount = Athlete::has('user')->count();
        // $scoutsCount = Scout::has('user')->count();
        // $conversationsScoutsAthletes = \DB::table('statistics')->where('name', 'conversationsScoutsAthletes')->value('value');
        $loginCount = \DB::table('laravel_logger_activity')->where('description', 'Logged In')->count();
        $visitedDashboard = \DB::table('laravel_logger_activity')->where('description', 'Viewed home')->count();
        $visitedHome = \DB::table('laravel_logger_activity')->where('description', 'Viewed /')->count();
        $visits = (($loginCount + $visitedDashboard) * 2) + $visitedHome;

        $idUsersScout = \DB::table('statistics')->where('name', 'popularScouts')->value('value');
        $popularScouts = User::whereIn('id', explode(',', $idUsersScout))->get();

        $idUsersAthlete = \DB::table('statistics')->where('name', 'popularAthletes')->value('value');
        $popularAthletes = User::whereIn('id', explode(',', $idUsersAthlete))->get();

        $sports = Sport::where('name','!=','All Sports')->orderBy('name')->get();

        $sports = $sports->map(function($item) {
            $item['nameTranslated'] = trans('sportfields.titles.'. $item->name);
            return $item;
        })->sortBy('nameTranslated');
        // dd($popularAthletes);
        $data = [
            'usersCountries'   => $usersCountries,
            'plans'            => $plans,
            'planScout'        => $planScout,
            'imagesScoutFree'  => $imagesScoutFree,
            'countriesUsers'   => count($usersCountries),
            'visits'           => $visits,
            'popularAthletes'  => $popularAthletes,
            'popularScouts'    => $popularScouts,
            'sports'           => $sports
        ];
        return view('welcome')->with($data);
    }

    public function searchUser(Request $request)
    {
        // dd($request->all());
        $pagesize = 10;
        $filtersSport = $request->only(preg_grep('/sport_/', array_keys($request->query())));
        $filtersSport = array_filter($filtersSport, function ($e) {
            return isset($e);
        });

        // if ($request->typeUser != '') {
            $rol = Role::find($request->typeUser);
            // dd($rol);
            $users = User::notAdmin()
                ->notBlocked()
                // ->typeUser($rol)
                ->sports($filtersSport)
                ->filter($request->all())
                ->paginate($pagesize);
        // } else {
        //     $users = User::notAdmin()
        //         ->notBlocked()
        //         ->sports($filtersSport)
        //         ->filter($request->all())
        //         ->paginate($pagesize);
        // }

        $data = [
            'users'      => $users,
            'sports'     => Sport::orderBy('name')->get(),
            'countries'  => Country::pluck('name', 'id')->toArray()
        ];
        // dd($data);

        return view('guest.results')->with($data);
    }

    public function searchClub(Request $request)
    {
        $sectionClubs = \DB::table('config_site')->where('name', 'sectionClubsInHome')->first();
        $showSectionClubs = $sectionClubs && $sectionClubs->value;
        if(!$showSectionClubs) abort(403);
        
        $pagesize = 8;
        if ($request->country != '') {
            $clubs = Entity::where('country_id', $request->country)->orderBy('name')->paginate($pagesize);
        } else {
            $clubs = Entity::orderBy('name')->paginate($pagesize);
        }

        $countries = Country::pluck('name', 'id')->toArray();

        $data = [
            'clubs'      => $clubs,
            'countries'  => $countries
        ];
        return view('guest.resultsClubs')->with($data);
    }

    public function usersClub(Request $request, $id)
    {
        $pagesize = 8;
        if ($request->country != '') {
            $clubs = Entity::where('country_id', $request->country)->orderBy('name')->paginate($pagesize);
        } else {
            $clubs = Entity::orderBy('name')->paginate($pagesize);
        }

        $entity = Entity::find($id);
        $usersAthlete = User::whereHas('athlete.entities', function ($q) use ($id) {
            return $q->where('entity_id', $id)->where('isCurrent', true);
        })->get();

        $usersScout = User::whereHas('scout.entities', function ($q) use ($id) {
            return $q->where('entity_id', $id)->where('isCurrent', true);
        })->get();


        $data = [
            'usersAthlete'  => $usersAthlete,
            'usersScout'    => $usersScout,
            'currentClub'   => $entity,
            'clubs'         => $clubs,
            'countries'     => Country::pluck('name', 'id')->toArray()
        ];
        return view('guest.clubUsers')->with($data);
    }

    public function usersSport($id)
    {
        $pagesize = 9;
        $sport = Sport::find($id);
        $sports = Sport::where('name','!=','All Sports')->orderBy('name')->get();
        $sports = $sports->map(function($item) {
            $item['nameTranslated'] = trans('sportfields.titles.'. $item->name);
            return $item;
        })->sortBy('nameTranslated');

        $usersAthlete = User::whereHas('athlete', function ($q) use ($id) {
            return $q->where('sport_id', $id)->orWhere('secondary_sport_id', $id);
        })->paginate($pagesize);

        $usersScout = User::whereHas('scout', function ($q) use ($id) {
            return $q->where('sport_id', $id);
        })->paginate($pagesize);

        $data = [
            'usersAthlete' => $usersAthlete,
            'usersScout'    => $usersScout,
            'currentSport' => $sport,
            'sports'       => $sports,
        ];
        return view('guest.sportUsers')->with($data);
    }

    public function showAboutUs()
    {
        return view('guest.aboutus');
    }

    public function showFaqs()
    {
        $faqsAthlete = Faq::where('type', 'Athlete')->get();
        $faqsScout = Faq::where('type', 'Scout')->get();

        $data = [
            'faqsAthlete' => $faqsAthlete,
            'faqsScout'  => $faqsScout
        ];
        return view('guest.faqs')->with($data);
    }

    public function setLocale($locale)
    {
        \Session::put('locale', $locale);
        return redirect()->back();
    }
}
