<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Follower;
use App\Models\User;

class FollowController extends Controller
{
    public function index()
    {
        $paginationSize = 6;
        $followed = Follower::where('user_id', \Auth::user()->id)->pluck('following_user_id');
        $users = User::notBlocked()->whereIn('id', $followed)->paginate($paginationSize);
        return view('followed.index')->with(['users' => $users]);
    }
}
