<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPublicPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    public function view(User $user, User $userVisited)
    {
        // if($user->hasRole('scout')) return true;
        if($user->hasRole('admin')) return true;
        if($user->id == $userVisited->id) return true;
        if($user->hasRole('athlete')) {
            $usersFollowed = $user->followed->pluck('following_user_id')->all();
            return in_array($userVisited->id, $usersFollowed);
        }
        if($user->hasRole('scout')) {
            if($userVisited->hasRole('athlete')) return true;
            $usersFollowed = $user->followed->pluck('following_user_id')->all();
            return in_array($userVisited->id, $usersFollowed);
        }
        return false;
        // return $user->id === $post->user_id;
    }
}
