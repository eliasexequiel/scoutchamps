<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\BroadcastMessage;
use App\Models\User;
class FollowUser extends Notification implements ShouldQueue
{
    use Queueable;

    public $user;
    public $userFollowed;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(User $user,User $userFollowed)
    {
        $this->user = $user;
        $this->userFollowed = $userFollowed;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'broadcast'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'userid' => $this->user->id,
            'uuid'  => $this->user->uuid,
            'username' => $this->user->firstname .' '. $this->user->lastname,
            'userFollowedId' => $this->userFollowed->id,
            'userFollowedUuid'  => $this->userFollowed->uuid,
            'userFollowedUsername' => $this->userFollowed->firstname .' '. $this->userFollowed->lastname
        ];
    }

}
