<?php

namespace App\Notifications;

use App\Models\Recruitment;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class NewRequestInvitationRecruitmentToScout extends Notification implements ShouldQueue
{
    use Queueable;

    public $user;
    public $recruitment;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(User $user, Recruitment $recruitment)
    {
        $this->user = $user;
        $this->recruitment = $recruitment;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'userid' => $this->user->id,
            'uuid'  => $this->user->uuid,
            'username' => $this->user->firstname .' '. $this->user->lastname,
            'recruitment' => $this->recruitment->id
        ];
    }
}
