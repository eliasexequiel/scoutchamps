<?php

namespace App\Observers;

use App\Models\User;

class UserObserver
{
 
    public function created(User $user)
    {
        //
    }

    public function updated(User $user)
    {
        //
    }

    public function deleted(User $user)
    {
        $idUser = $user->id;
        \DB::table('user_visited')->where('user_id',$idUser)->orWhere('visitor_user_id', $idUser)->delete();
        \DB::table('followers')->where('user_id',$idUser)->orWhere('following_user_id', $idUser)->delete();
        \DB::table('favorites')->where('user_id',$idUser)->orWhere('favorite_user_id', $idUser)->delete();
        \DB::table('updates')->where('user_id',$idUser)->delete();
        \DB::table('user_sesions')->where('user_id',$idUser)->delete();
        \DB::table('posts')->where('user_id',$idUser)->delete();
        \DB::table('post_comments')->where('user_id',$idUser)->delete();
        \DB::table('post_likes')->where('user_id',$idUser)->delete();
        \DB::table('recruitments_athletes')->where('user_id',$idUser)->delete();

        $threadsParticipants = \DB::table('participants')->where('user_id',$idUser)->get();
        // Each to threads and delete.
        foreach($threadsParticipants as $tp) {
            \DB::table('threads')->where('id',$tp->thread_id)->delete();
        }
        
        $popularScouts = \DB::table('statistics')->where('name','popularScouts')->first();
        if($popularScouts) {
            // $scouts = explode(',',$popularScouts);
            $textPopularScout = preg_replace('#('. $idUser .')(,)?#', '', $popularScouts->value);
            if(substr($textPopularScout, -1) == ",") $textPopularScout = substr($textPopularScout, 0, -1);

            \DB::table('statistics')->where('name', 'popularScouts')->update(['value' => $textPopularScout]);
        }
        $popularAthletes = \DB::table('statistics')->where('name','popularAthletes')->first();
        if($popularAthletes) {
            // $scouts = explode(',',$popularScouts);
            $textPopularAthletes = preg_replace('#('. $idUser .')(,)?#', '', $popularAthletes->value);
            if(substr($textPopularAthletes, -1) == ",") $textPopularAthletes = substr($textPopularAthletes, 0, -1);

            \DB::table('statistics')->where('name', 'popularAthletes')->update(['value' => $textPopularAthletes]);
        }
    }

    public function forceDeleted(User $user)
    {
        //
    }
}
