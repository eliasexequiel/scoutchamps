<?php

namespace App\Observers;

use App\Models\Entity;

class EntityObserver
{
    /**
     * Handle the entity "created" event.
     *
     * @param  \App\Entity  $entity
     * @return void
     */
    public function created(Entity $entity)
    {
        //
    }

    /**
     * Handle the entity "updated" event.
     *
     * @param  \App\Entity  $entity
     * @return void
     */
    public function updated(Entity $entity)
    {
        //
    }

    /**
     * Handle the entity "deleted" event.
     *
     * @param  \App\Entity  $entity
     * @return void
     */
    public function deleted(Entity $entity)
    {
        \DB::table('athlete_entities')->where('entity_id', $entity->id)->delete();
        \DB::table('athlete_entities_preferences')->where('entity_id', $entity->id)->delete();
        \DB::table('scout_entities')->where('entity_id', $entity->id)->delete();
    }

    /**
     * Handle the entity "restored" event.
     *
     * @param  \App\Entity  $entity
     * @return void
     */
    public function restored(Entity $entity)
    {
        //
    }

    /**
     * Handle the entity "force deleted" event.
     *
     * @param  \App\Entity  $entity
     * @return void
     */
    public function forceDeleted(Entity $entity)
    {
        //
    }
}
