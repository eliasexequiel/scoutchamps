<?php

namespace App\Observers;

use App\Models\Media;

class MediaObserver
{
    /**
     * Handle the media "created" event.
     *
     * @param  \App\Media  $media
     * @return void
     */
    public function created(Media $media)
    { }

    /**
     * Handle the media "updated" event.
     *
     * @param  \App\Media  $media
     * @return void
     */
    public function updated(Media $media)
    {
        //
    }

    /**
     * Handle the media "deleted" event.
     *
     * @param  \App\Media  $media
     * @return void
     */
    public function deleted(Media $media)
    {
        $idMedia = $media->id;
        \DB::table('updates')
            ->where('type', 'newVideos')
            ->where('body', 'like', '%"url":false,"id_video":' . $idMedia . '%')
            ->delete();

        \DB::table('updates')
            ->where('type', 'newPhotos')
            ->where('body', 'like', '%"id_image":' . $idMedia . '%')
            ->delete();

        \DB::table('reports')
            ->where('reported_type', 'App\Models\Media')
            ->where('reported_id', $idMedia)
            ->delete();
    }

    /**
     * Handle the media "restored" event.
     *
     * @param  \App\Media  $media
     * @return void
     */
    public function restored(Media $media)
    {
        //
    }

    /**
     * Handle the media "force deleted" event.
     *
     * @param  \App\Media  $media
     * @return void
     */
    public function forceDeleted(Media $media)
    {
        $idMedia = $media->id;
        \DB::table('updates')
            ->where('type', 'newVideos')
            ->where('body', 'like', '%"url":false,"id_video":' . $idMedia . '%')
            ->delete();

        \DB::table('updates')
            ->where('type', 'newPhotos')
            ->where('body', 'like', '%"id_image":' . $idMedia . '%')
            ->delete();
    }
}
