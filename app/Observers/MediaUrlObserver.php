<?php

namespace App\Observers;

use App\Models\MediaUrl;

class MediaUrlObserver
{
    /**
     * Handle the media url "created" event.
     *
     * @param  \App\MediaUrl  $mediaUrl
     * @return void
     */
    public function created(MediaUrl $mediaUrl)
    {
        //
    }

    /**
     * Handle the media url "updated" event.
     *
     * @param  \App\MediaUrl  $mediaUrl
     * @return void
     */
    public function updated(MediaUrl $mediaUrl)
    {
        //
    }

    /**
     * Handle the media url "deleted" event.
     *
     * @param  \App\MediaUrl  $mediaUrl
     * @return void
     */
    public function deleted(MediaUrl $mediaUrl)
    {
        $idMedia = $mediaUrl->id;
        \DB::table('updates')
            ->where('type', 'newVideos')
            ->where('body', 'like', '%"url":true,"id_video":' . $idMedia . '%')
            ->delete();

        \DB::table('reports')
            ->where('reported_type', 'App\Models\MediaUrl')
            ->where('reported_id', $idMedia)
            ->delete();
    }

    /**
     * Handle the media url "restored" event.
     *
     * @param  \App\MediaUrl  $mediaUrl
     * @return void
     */
    public function restored(MediaUrl $mediaUrl)
    {
        //
    }

    /**
     * Handle the media url "force deleted" event.
     *
     * @param  \App\MediaUrl  $mediaUrl
     * @return void
     */
    public function forceDeleted(MediaUrl $mediaUrl)
    {
        //
    }
}
