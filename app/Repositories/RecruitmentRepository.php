<?php

namespace App\Repositories;

use App\Models\User;
use App\Models\Recruitment;
use App\Models\RecruitmentAthlete;
use App\Repositories\Interfaces\RecruitmentRepositoryInterface;

class RecruitmentRepository implements RecruitmentRepositoryInterface
{
    public function all()
    {
        return Recruitment::all();
    }

    public function getByUser(User $user)
    {
        $pagintaionEnabled = config('usersmanagement.enablePagination');
        if ($pagintaionEnabled) {
            $recruitments = Recruitment::where('user_id', $user->id)->orderByDesc('day_start')->paginate(config('usersmanagement.paginateListSize'));
        } else {
            $recruitments = Recruitment::where('user_id', $user->id)->orderByDesc('day_start')->get();
        }
        return $recruitments;
    }

    public function latestByUsers($from = null, array $idUsers, $cant)
    {
        // \Log::info($idUsers);
        if ($from == null) return Recruitment::with('user')->whereIn('user_id', $idUsers)->latest()->take($cant)->get();
        return Recruitment::with('user')->where('day_start', '>=', $from)->whereIn('user_id', $idUsers)->latest()->take($cant)->get();
    }

    public function getByDates($from, $to = null)
    {
        if ($to == null)
            return Recruitment::where('user_id', \Auth::user()->id)->where('day_start', '>=', $from)->get();
        else
            return Recruitment::where('user_id', \Auth::user()->id)->where('day_start', '>=', $from)->where('day_start', '<=', $to)->get();
    }

    public function sendInvitations(User $user, array $idRecruitments, $message = '')
    {
        try {
            foreach ($idRecruitments as $r) {
                \DB::table('recruitments_athletes')->updateOrInsert([
                    'recruitment_id' => $r,
                    'user_id' => $user->id,
                    'message' => $message
                ], []);
            }
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    public function getInvitation(User $user, Recruitment $recruitment)
    {
        return RecruitmentAthlete::with('recruitment.user')
            ->where('user_id', $user->id)
            ->where('recruitment_id', $recruitment->id)
            ->first();
    }

    public function getInvitedByUser(User $user, $from = null)
    {
        if ($from == null)
            return RecruitmentAthlete::with('recruitment.user')->where('user_id', $user->id)->get();
        else
            return RecruitmentAthlete::with('recruitment.user')->where('user_id', $user->id)
                ->whereHas('recruitment', function ($q) use ($from) {
                    $q->where('day_end', '>=', $from);
                })->get();
    }

    public function sendInvitationsManyUsers(Recruitment $recruitment, array $idUsers, $message = '')
    {
        try {
            foreach ($idUsers as $id) {
                \DB::table('recruitments_athletes')->updateOrInsert([
                    'recruitment_id' => $recruitment->id,
                    'user_id' => $id,
                    'message' => $message
                ], []);
            }
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    public function deleteUserFromRecruitment(Recruitment $recruitment, User $user)
    {
        \DB::table('recruitments_athletes')
            ->where(['recruitment_id' => $recruitment->id, 'user_id' => $user->id])
            ->delete();
        return true;
    }

    public function responseInvitation(Recruitment $recruitment, User $user, $assist)
    {
        \DB::table('recruitments_athletes')
            ->updateOrInsert(
                ['recruitment_id' => $recruitment->id, 'user_id' => $user->id],
                ['assist' => $assist]
            );
        return true;
    }

    public function requestInvitation(Recruitment $recruitment, User $user)
    {
        try {
            \DB::table('request_invitation_recruitment')->updateOrInsert(
                [
                    'recruitment_id' => $recruitment->id,
                    'user_id' => $user->id
                ],
                [
                    "created_at" =>  \Carbon\Carbon::now(),
                    "updated_at" => \Carbon\Carbon::now()
                ]
            );

            return true;
        } catch (\Exception $e) {
            return false;
        }
    }
}
