<?php

namespace App\Repositories\Interfaces;

use App\Models\User;
use App\Models\Recruitment;

interface RecruitmentRepositoryInterface
{
    public function all();

    public function getByUser(User $user);

    public function latestByUsers($from = null,array $idUsers, int $cant);

    public function getByDates($from, $to = null);

    public function sendInvitations(User $user, array $idRecruitments, $message = '');

    public function getInvitedByUser(User $user, $from = null);

    public function sendInvitationsManyUsers(Recruitment $recruitment, array $idUsers, $message = '');

    public function deleteUserFromRecruitment(Recruitment $recruitment, User $user);

    public function responseInvitation(Recruitment $recruitment, User $user, $assist);
    
    public function requestInvitation(Recruitment $recruitment, User $user);

    public function getInvitation(User $user, Recruitment $recruitment);
}
