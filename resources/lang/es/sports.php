<?php
return [
    // Titles
    'showing-all-sports'     => 'Deportes',
    'sports-menu-alt'        => 'Deportes',
    'create-new-sport'       => 'Crear Deporte',
    'editing-sport'          => 'Editar Deporte :name',
    'showing-sport'          => 'Ver Deporte :name',
    'showing-sport-title'    => 'Ver :name',
    'back-sports'            => 'Volver a Deportes',

    // Flash Messages
    'createSuccess'   => 'Deporte creado exitosamente! ',
    'updateSuccess'   => 'Deporte actualizado exitosamente! ',
    'deleteSuccess'   => 'Deporte eliminado exitosamente! ',

    'sportTotal'    => 'Total Deportes',

    'sports-table' => [
        'caption'   => '{1} :sportscount sport total|[2,*] :sportscount total sports',
        'id'        => 'ID',
        'name'      => 'Nombre',
        'description' => 'Descripción',
        'created'   => 'Creado',
        'updated'   => 'Actualizado',
        'actions'   => 'Acciones'
    ],

    'forms' => [
        'id'        => 'ID',
        'name'      => 'Nombre',
        'description'   => 'Descripción',
        'fields'    => 'Campos',
        'field-name'    => 'Nombre',
        'field-options' => 'Opcioens Combo',
        'field-type'    => 'Tipo',
        'field-add'     => 'Agregar campo',
        'create'    => 'Crear Deporte',
        'update'    => 'Actualizar Deporte',
        'options-select' => 'Opciones separadas por coma'
    ],

    'buttons' => [
        'create-new'    => 'Nuevo Deporte',
        'delete'        => '<i class="fa fa-trash-o fa-fw" aria-hidden="true"></i>  <span class="hidden-xs hidden-sm">Eliminar</span>',
        'show'          => '<i class="fa fa-eye fa-fw" aria-hidden="true"></i> <span class="hidden-xs hidden-sm">Mostrar</span>',
        'edit'          => '<i class="fa fa-pencil fa-fw" aria-hidden="true"></i> <span class="hidden-xs hidden-sm">Editar</span>',
        'back-to-sports' => '<span class="hidden-sm hidden-xs">Volver a </span><span class="hidden-xs">Deportes</span>',
        'back-to-sport'  => 'Volver  <span class="hidden-xs">a deporte</span>',
        'delete-sport'   => '<i class="fa fa-trash-o fa-fw" aria-hidden="true"></i>  <span class="hidden-xs">Eliminar</span><span class="hidden-xs"> Sport</span>',
        'edit-sport'     => '<i class="fa fa-pencil fa-fw" aria-hidden="true"></i> <span class="hidden-xs">Editar</span><span class="hidden-xs"> Sport</span>',
    ],
];
