<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Modals Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which modals share.
    |
    */

    // Default Save Modal;
    'confirm_modal_title_text'    => 'Confirm Save',
    'confirm_modal_title_std_msg' => 'Please confirm your request.',

    // Confirm Save Modal;
    'confirm_modal_button_save_text'        => 'Guardar cambios',
    'confirm_modal_button_save_icon'        => 'fa-save',
    'confirm_modal_button_cancel_text'      => 'Cancelar',
    'confirm_modal_button_cancel_icon'      => 'fa-times',
    'edit_user__modal_text_confirm_title'   => 'Confirmar',
    'edit_user__modal_text_confirm_message' => 'Please confirm your changes.',

    // Form Modal
    'form_modal_default_title'      => 'Confirmar',
    'form_modal_default_message'    => 'Please Confirm',
    'form_modal_default_btn_cancel' => 'Cancelar',
    'form_modal_default_btn_submit' => 'Confirm Submit',

    'form_modal_send'   => '<i class="fa fa-fw fa-envelope" aria-hidden="true"></i> Enviar',
    'form_modal_cancel' => '<i class="fa fa-fw fa-close" aria-hidden="true"></i> Cancelar'


];
