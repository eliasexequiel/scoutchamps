<?php

return [
    'title' => 'Cupones',
    'create-new-coupon'     => 'Nuevo Cupón',
    'show-deleted-coupons'    => 'Mostrar cupones eliminados',
    'editing-coupon'          => 'Editar Cupón :name',
    'showing-coupon'          => 'Ver Cupón :name',
    'back-coupons'            => 'Volver a Cupones',

    // Flash Messages
    'createSuccess'   => 'Cupón creado correctamente! ',
    'updateSuccess'   => 'Cupón actualizado correctamente! ',
    'deleteSuccess'   => 'Cupón eliminado correctamente! ',

    'couponTotal'    => 'Total Cupones',

    'coupons-table' => [
        'id'          => 'ID',
        'name'        => 'Nombre',
        'discount'    => 'Descuento',
        'duration'    => 'Duración',
        'used'        => 'Usado',
        'created'     => 'Creado',
        'updated'     => 'Actualizado',
        'actions'     => 'Acciones'
    ],

    'forms' => [
        'id'        => 'ID',
        'name'      => 'Nombre',
        'type'      => 'Tipo',
        'type_percent_off'  => 'Porcentaje de descuento',
        'type_amount_off'  => 'Cantidad fija',
        'code'      => 'Código',
        'discount'  => 'Descuento',
        'duration'  => 'Duración',
        'durationMonths'    => 'Meses',
        'months'    => 'meses',
        'create'    => 'Crear Cupón',
        'update'    => 'Actualizar Cupón',
    ],

    'buttons' => [
        'create-new'    => 'Nuevo Cupón',
        'delete'        => '<i class="fa fa-trash-o fa-fw" aria-hidden="true"></i>  <span class="hidden-xs hidden-sm">Eliminar</span>',
        'show'          => '<i class="fa fa-eye fa-fw" aria-hidden="true"></i> <span class="hidden-xs hidden-sm">Mostrar</span>',
        'edit'          => '<i class="fa fa-pencil fa-fw" aria-hidden="true"></i> <span class="hidden-xs hidden-sm">Editar</span>',
        'back-to-coupons' => '<span class="hidden-sm hidden-xs">Volver a </span><span class="hidden-xs">Cupones</span>',
        'delete-coupon'   => '<i class="fa fa-trash-o fa-fw" aria-hidden="true"></i>  <span class="hidden-xs">Eliminar</span><span class="hidden-xs"> Coupon</span>',
        'edit-coupon'     => '<i class="fa fa-pencil fa-fw" aria-hidden="true"></i> <span class="hidden-xs">Editar</span><span class="hidden-xs"> Coupon</span>',
    ],
    
];