<?php

return [
    'title'         => 'Notificaciones',
    'noResults'     => "No hay notificaciones",
    'markAllAsRead' => 'Marcar todas las notificaciones como leídas',
    'markAsRead'    => 'Marcar como leída',
    'read'          => 'Leídas',
    'unread'        => 'No leídas',
    'Subscriptions' => 'Suscripciones',
    'NewUsers'      => 'Nuevos Usuarios',
    'Clubs'         => 'Equipos',
    'Showcase'      => 'Showcase',
    'Reports'       => 'Denuncias',
    'ContactsBetweenScoutsAndAthletes' => 'Contactos Scouts/Atletas',

    'messages'  => [
        'favoriteUser'          => ' te agregó como favorito.',
        'followUser'            => ' empezó a seguirte.',
        'invitationRecruitment' => ' te invitó a un nuevo ',
        'requestInvitationRecruitment' => ' solicitó una invitación para un ',
        'recruitment'           => 'reclutamiento.',
        'clubCreated'           => ' creó una entidad ',
        'addedSusbcription'     => ' se suscribió al plan ',
        'deletedSusbcription'   => ' canceló su suscripción al plan ',
        'registered'            => ' se registró en ScoutChamps.',
        'showcase'              => ' agregó :typeEntity :nameEntity a su ',
        'showcaseLink'          => 'Showcase',
        'report'                => ' creó una nueva ',
        'reportTitle'           => 'denuncia.',
        'couponUsed'            => ' usó el cupón ',
        'reportedDeleted'       => 'Un :type ha sido eliminado por el Administrador.',
        'contactMessagePrivate' => ' contactó al atleta ',
        'entityContactedByAdmin'=> '<strong>Scoutchamps</strong> ha contactado a <strong> :nameEntity </strong> que ha agregado en su '
    ]
];