<?php

return [

    /*
    |--------------------------------------------------------------------------
    | User Profiles Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match user profile
    | pages, messages, forms, labels, placeholders, links, and buttons.
    |
    */

    'templateTitle' => 'Editar tu perfil',

    // profile errors
    'notYourProfile'      => 'El usuario que estás buscando no se encuentra. Intente nuevamente.',
    'notYourProfileTitle' => 'Hmmm, something went wrong...',
    'noProfileYet'        => 'No profile yet.',

    // USER profile title
    'showProfileUsername'        => 'Correo',
    'showProfileFirstName'       => 'Nombre',
    'showProfileLastName'        => 'Apellido',
    'showProfileEmail'           => 'Correo',
    'showProfileLocation'        => 'Ciudad',
    'showProfileBio'             => 'Biografía',

    // USER profile page
    'showProfileTitle' => 'Perfil :username',

    // USER EDIT profile page
    'editProfileTitle' => 'Perfil - Cuenta',

    // User edit profile form
    'label-theme' => 'Your theme:',
    'ph-theme'    => 'Select your theme',

    'label-location' => 'Location:',
    'ph-location'    => 'Enter your location',

    'label-nationality'             => 'Seleccionar Nacionalidad',
    'label-second-nationality'      => 'Seleccionar Segunda Nacionalidad',
    'label-residence-country'       => 'Seleccionar País de Residencia',
    'label-residence-city'          => 'Ciudad de Residencia',
    'label-phone'                   => '(Código de país) Teléfono',
    'label-gender'                  => 'Seleccionar Género',
    'label-sporting-goal'           => 'Seleccionar Objetivo deportivo',
    'label-height'                  => 'Altura',
    'label-sport'                   => 'Deporte',
    'label-status'                  => 'Seleccionar Estado actual',
    'label-professional_start_date' => 'Fecha comienzo como profesional',
    'label-professional_end_date'   => 'Fecha fin como profesional',
    'label-doesnotApply'            => 'No Aplica',
    'label-cargo-scout'             => 'Especialidad',
    'label-cargo-secundario-scout'  => 'Especialidad Secundaria',
    'label-select'                  => 'Seleccionar',
    'publicPofile'                  => 'Mi perfil público',

    // User Account Settings Tab
    'editTriggerAlt'        => 'Toggle User Menu',
    'editAccountTitle'      => 'Cuenta',
    'editAccountAdminTitle' => 'Administración',
    'editMedia'             => 'Multimedia',
    'editAthleticCarrer'    => 'Trayectoria Deportiva',
    'updateAccountSuccess'  => 'Tu cuenta fue actualizada correctamente',
    'submitProfileButton'   => 'Guardar cambios',

    // User Account Admin Tab
    'submitPWButton'    => 'Actualizar Contraseña',
    'changePwTitle'     => 'Cambiar Contraseña',
    'changePwPill'      => 'Cambiar Contraseña',
    'inviteRecruitments'  => 'Invitar a reclutamientos',
    'deleteAccountPill' => 'Eliminar cuenta',
    'updatePWSuccess'   => 'Tu contraseña fue actualizada correctamente',

    // Delete Account Tab
    'deleteAccountTitle'        => 'Eliminar Cuenta',
    'deleteAccountBtn'          => 'Eliminar Mi Cuenta',
    'deleteAccountBtnConfirm'   => 'Eliminar Mi Cuenta',
    'deleteAccountConfirmTitle' => 'Confirmar Eliminación de Cuenta',
    'deleteAccountConfirmMsg'   => 'Está seguro que quiere eliminar su cuenta?',
    'confirmDeleteRequired'     => 'Confirm Account Deletion is required',

    'errorDeleteNotYour'        => 'Solo puedes eliminar tu cuenta',
    'successUserAccountDeleted' => 'Su cuenta ha sido eliminada',

    // Messages
    'updateSuccess' => 'Su perfil fue actualizado correctamente.',
    'submitButton'  => 'Guardar cambios',
    'adviceFollowUserToSendMessage' => 'Tiene que seguir al usuario para enviarle un mensaje.',
    'adviceUserPremiumToSendMessage' => 'Suscribete a un <a href="/mySubscriptions"> Plan Premium </a> para enviar un mensaje.',

    // Restore User Account
    'errorRestoreUserTime' => 'Lo sentimos, su cuenta no pudo ser reestablecida',
    'successUserRestore'   => 'Welcome back :username! Account Successfully Restored',

    'image'                  => 'Imagen',   
    'images'                 => 'Imágenes',
    'imagesAvailables'       => 'Imágenes disponibles',
    'updatePlanLimitImages'  => 'Se alcanzó el limite para subir imágenes. Para seguir subiendo imágenes actualiza tu plan <a class="hover:underline" href="/mySubscriptions"> aquí </a>.',
    'video'                  => 'Video',
    'videos'                 => 'Videos',
    'videosAvailables'       => 'Videos disponibles',
    'updatePlanLimitVideos'  => 'Se alcanzó el limite para subir videos. Para seguir subiendo videos actualiza tu plan <a class="hover:underline" href="/mySubscriptions"> aquí </a>.',
    'publicRecruitments'     => 'Reclutamientos Públicos',
    'trayectory'             => 'Trayectoria Deportiva',
    'noTrayectory'           => 'Sin trayectoria',
    'posts'                  => 'Publicaciones',
    'follow'                 => 'Seguir',
    'following'              => 'Siguiendo',
    'unfollow'               => 'Dejar de seguir',
    'favorites'              => 'Favoritos',
    'addedFavorites'         => 'Favorito agregado',
    'addFavorite'            => 'Agregar a Favoritos',
    'removeFavorite'         => 'Quitar de Favoritos',
    'sendMessage'            => 'Enviar Mensaje',
    'questionRemoveFavorite' => 'Está seguro que quiere quitar a :name de sus favoritos?',
    'questionUnfollow'  => 'Está seguro que quiere dejar de seguir a :name?',
    'adviceDeleteAccount' => '<strong>Si eliminas</strong> tu cuenta es <u><strong> de forma definitiva </strong></u> y <u><strong>no podrás</strong></u> recuperarla.',

    'showcase'  => 'Vitrina',
    'sports' => 'Deportes',
    'clubs' => 'Clubes',
    'clubsUniversitiesInstitutes' => 'Clubes/Universidades/Academias',
    'clubsUniversitiesInstitutesSponsors' => 'Clubes/Universidades/Academias/Sponsors',
    'moreInformation' => 'Más información',
    'readmore'  => 'Ver más',
    'readless'  => 'Ver menos',

    'athlete' => [
        'labelPremiun'  => 'Atleta Premiun',
        'scoutVisits'   => 'Scout que vieron tu perfil',
        'visits'        => 'Visitas al perfil',
        'followers'     => 'Seguidores',
        'following'     => 'Seguidos',
        'deleteImage'   => 'Está seguro que quiere eliminar esta imagen?',
        'deleteVideo'   => 'Está seguro que quiere eliminar este video?',
        'delete'        => 'Eliminar',
        'addClub'       => 'Agregar',
        'removeClub'    => 'Eliminar Club',
        'otherClub'     => 'Otro',
        'nameOtherClub' => 'Nombre',
        'from'          => 'Desde',
        'to'            => 'Hasta',
        'month'         => 'Mes',
        'year'          => 'Año',
        'current' => ':name Actual',
        'achievements' => 'Logros',
        'addAchievement' => 'Agregar logros',
        'infoAdditional' => 'Información adicional',
        'upload' => 'Subir',
        'url' => 'Url',
        'urlVideo' => 'Url Video',
        'uploadImageFromUrl' => 'Subir imagen desde url',
        'uploadVideoFromUrl' => 'Subir video desde url',
        'about'  => 'Sobre mi',
        'tutorFirstName' => 'Nombre Padres/Tutor Legal',
        'tutorLastname'  => 'Apellido Padres/Tutor Legal',
        'tutorEmail'     => 'Correo Padres/Tutor Legal',
        'tutorPhone'     => 'Teléfono Padres/Tutor Legal',
        'phone'  => 'Teléfono',
        'uploadPhotoProfileMaxSize' => 'Tamaño máximo de foto de perfil: 50mb',
        'uploadImageAdvice' => 'Las imágenes no pueden tener un tamaño mayor a 300mb. Formatos de imagen aceptados: jpeg, png, jpg, svg.',
        'uploadVideoAdvice' => 'Los videos no pueden tener un tamaño mayor a 300mb. Formatos de videos aceptados: mp4, avi, x-flv.',
        'uploadVideoAdviceFreeAthlete' => 'Duración máxima de video permitida: 90 segundos.',
        'email'  => 'Correo',
        'nationality' => 'Nacionalidad',
        'secondNationality' => 'Segunda Nacionalidad',
        'residenceCountry' => 'País de Residencia',
        'residenceCity' => 'Ciudad de Residencia',
        'dob'   => 'Fecha de nacimiento',
        'height' => 'Altura',
        'heightCm' => 'cm',
        'heightFt' => 'pies',
        'cm' => 'cm',
        'feets' => 'pies',
        'sport' => 'Deporte',
        'secondarySport' => 'Deporte secundario',
        'sportingGoal' => 'Objetivo deportivo',
        'status' => 'Estado',
        'contract' => 'Contrato',
        'currentClub' => 'Actual',
        'club' => 'Club',
        'achievements' => 'Logros',
        'noAchievements' => 'No hay logros',
        'noClubs'        => 'No hay equipos',
        'noInstitutes'   => 'No hay academias',
        'noUniversities' => 'No hay universidades',
        'noSponsors'     => 'No hay sponsors',
        'noVideos' => 'No hay videos',
        'noImages' => 'No hay imágenes'
    ],
    'scout' => [
        'cargo-scout'             => 'Especialidad',
        'cargo-secundario-scout'  => 'Especialidad Secundaria',  
    ],
    'results'     => 'Resultados',
    'totalUsers'  => 'Total Usuarios',
    'noResults'   => 'Sin resultados',
    'viewProfile' => 'Ver perfil',
    'filters' => [
        'filterBy'          => 'Filtros',
        'typeUser'          => 'Tipo de usuario',
        'name'              => 'Nombre o Apellido',
        'search'            => 'Buscar',
        'sport'             => 'Seleccionar Deporte',
        'gender'            => 'Seleccionar Género',
        'age'               => 'Seleccionar Edad',
        'typeAthlete'       => 'Seleccionar Tipo de atleta',
        'sportingGoal'      => 'Seleccionar Objetivo deportivo',
        'residenceCountry'  => 'Seleccionar País de residencia',
        'country'           => 'Seleccionar País',
        'nationality'       => 'Seleccionar Nacionalidad',
        'allSports'         => 'Todos los deportes',
        'allNationalities'  => 'Todas las nacionalidades',
        'allCountries'      => 'Todos los países',
        'searchText'        => 'Escriba un nombre y presione enter',
        'entities'          => 'Entidades',
        'selectEntity'      => 'Seleccionar Entidad',
        'selectSpecialityScout' => 'Seleccionar Especialidad Scout',
        'specialitiesScout'     => 'Especialidad Scout'

    ],
    'publicities' => [
        'deletePublicity'      => 'Eliminar',
        'deletePublicityTitle' => 'Eliminar Publicidad',
        'deletePublicityModal' => 'Está seguro que quiere eliminar la publicidad?',

    ],
    'btnAccept' => 'Aceptar',
    'completeAllSteps' => '¡Completá todos los pasos para incrementar tus oportunidades de éxito!',

    'errors' => [
        'clubRequired' => 'Club es requerido',
        'achievementNameRequired' => 'El nombre del logro es requerido',
        'clubStartDateFormat' => 'Fecha Desde debe tener el formato d/m/Y',
        'clubEndDateFormat' => 'Fecha Hasta debe tener el formato d/m/Y',
        'clubEndGreaterThanStartDate' => 'Fecha Hasta debe ser mayor a Fecha Desde',
        'clubInfoAditionalMaxLength' => 'Información Adicional no puede ser mayor a :max caracteres'
    ]
];
