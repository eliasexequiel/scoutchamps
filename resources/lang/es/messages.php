<?php 

return [
    'title'            => 'Mensajes',
    'newMessage'       => 'Nuevo Mensaje',
    'noResults'        => 'No hay mensajes',
    'to'               => 'Para',
    'message'          => 'Mensaje',
    'send'             => 'Enviar',
    'conversations'    => 'Conversaciones',
    'noConversations'  => 'No hay Conversaciones',
    'inputPlaceholder' => 'Escriba aquí...',
    'conversationWith' => 'Conversación Con',
    'conversationWithAdmin' => 'Conversación Con Administrador',
    'archive'          => 'Archivar Conversación', 
    'archiveConversation' => 'Está seguro que quiere archivar la conversación?',
    'moreResults'       => 'Cargar Más',
    'defaultMessageToScout' => 'Te invito a ver <a href=":link">mi perfil</a>, creo contar con las habilidades suficientes para destacar en este deporte. Gracias.',
    'defaultMessages'  => [
        [ 'value' => 'hi', 'name' => 'Hi' ],
        [ 'value' => 'how are you?', 'name' => 'How are you?' ],
        [ 'value' => 'nice to meet you', 'name' => 'Nice to meet you' ]
    ]   
];