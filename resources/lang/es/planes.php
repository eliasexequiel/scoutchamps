<?php 
return [
    'title'             => 'Planes',
    'plan'              => 'Plan',
    'subscribed'        => 'Suscripto',
    'alreadySubscribed' => 'Ya estás suscripto al plan',
    'buttons' => [
        'subscribe' => 'Subscribirse',
        'cancel'    => 'Cancelar suscripción',
        'pay'       => 'Pagar'

    ],
    'forms'  => [
        'coupon'    => 'Cupón',
        'applyDiscount' => 'Aplique Código de descuento',
        'cardholderUser' => 'Titular tarjeta'
    ],
    'premiunService'    => 'Servicio Premiun',
    'paymentData' => 'Información de pago',
    'resume'      => 'Total Orden',
    'subtotal'    => 'Subtotal',
    'taxes'       => 'Impuestos',
    'total'       => 'Total',
    'totalWithDiscount' => 'Total con Cupón',
    'errorCoupon' => 'Cupón no válido',
    'advicePlan'  => 'En el Plan Mensual, una vez suscrito, en el primer cobro ScoutChamps cobrará un proporcional con respecto a los días del mes restante hasta que finalice el mes en curso. Una vez que comience el nuevo mes, se le cobrará el precio total del plan premium. Si utiliza un cupón de descuento, el mismo se aplicará al mes actual y a los siguientes meses, según la duración estipulada en el cupón.',
    'messages' => [
        'cancelled' => 'Suscripción cancelada',
        'errorSubscription' => 'There was an error proccessing your card. Try with another card.',
        'errorCouponSubscription' => 'Cupón :coupon incorrecto.',
        'success'   => 'Suscripto al plan correctamente!'
    ],

    'admin' => [
        'editPricePlan' => 'Editar Precio Plan',
        'back-plans'  => 'Volver a Planes',
        'planTotal' => 'Plan Total',
        'table' => [
            'name' => 'Nombre',
            'price' => 'Precio',
            'actions' => 'Acciones'
        ],

        'forms' => [
            'price'   => 'Precio',
            'update'  => 'Actualizar Plan',
        ],

        'messages' => [
            'updateSuccess' => 'Plan creado correctamente!',
            'deleteSuccess' => 'Plan eliminado correctamente!'
        ],
        'buttons' => [
            'create-new'    => 'Nuevo Plan',
            'delete'        => '<i class="fa fa-trash-o fa-fw" aria-hidden="true"></i>  <span class="hidden-xs hidden-sm">Eliminar</span>',
            'show'          => '<i class="fa fa-eye fa-fw" aria-hidden="true"></i> <span class="hidden-xs hidden-sm">Mostrar</span>',
            'edit'          => '<i class="fa fa-pencil fa-fw" aria-hidden="true"></i> <span class="hidden-xs hidden-sm"> Editar precio </span>',
            'back-to-plans' => '<span class="hidden-sm hidden-xs">Back to </span><span class="hidden-xs">Planes</span>',
            'delete-plan'   => '<i class="fa fa-trash-o fa-fw" aria-hidden="true"></i>  <span class="hidden-xs">Eliminar</span><span class="hidden-xs"> Plan</span>',
            'edit-plan'     => '<i class="fa fa-pencil fa-fw" aria-hidden="true"></i> <span class="hidden-xs">Editar</span><span class="hidden-xs"> Plan</span>',
            'delete-plan-question' => 'Seguro que quieres eliminar el plan?'
        ],
    ]

];