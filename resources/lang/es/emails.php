<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Emails Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for various emails that
    | we need to display to the user. You are free to modify these
    | language lines according to your application's requirements.
    |
    */

    /*
     * Activate new user account email.
     *
     */

    'activationSubject'  => 'Activación requerida',
    'activationGreeting' => 'Bienvenido a ScoutChamps!',
    'activationMessage'  => 'Necesitas activar tu cuenta antes de comenzar a usar nuestros servicios.',
    'activationButton'   => 'Activar',
    'activationThanks'   => 'Gracias por usar nuestra aplicación!',

    /*
     * Goobye email.
     *
     */
    'goodbyeSubject'  => 'Sorry to see you go...',
    'goodbyeGreeting' => 'Hola :username,',
    'goodbyeMessage'  => 'We are very sorry to see you go. We wanted to let you know that your account has been deleted. Thank for the time we shared. You have '.config('settings.restoreUserCutoff').' days to restore your account.',
    'goodbyeButton'   => 'Restablecer Cuenta',
    'goodbyeThanks'   => 'Esperamos verte de nuevo!',

];
