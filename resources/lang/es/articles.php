<?php
return [
    // Titles
    'showing-all-articles'     => 'Articulos',
    'articles-menu-alt'        => 'Articulos Menu',
    'create-new-article'       => 'Crear Articulo',
    'editing-article'          => 'Editar Articulo',
    'showing-article'          => 'Ver Articulo',
    'showing-article-title'    => 'Articulo Información',
    'back-articles'            => 'Volver a Articulos',

    // Flash Messages
    'createSuccess'   => 'Articulo creado correctamente! ',
    'updateSuccess'   => 'Articulo actualizado correctamente! ',
    'deleteSuccess'   => 'Articulo eliminado correctamente! ',

    'articleTotal'  => 'Articulos Total',

    'articles-table' => [
        'id'        => 'ID',
        'title'     => 'Titulo',
        'resume'    => 'Resumen',
        'body'      => 'Contenido',
        'created'   => 'Creado',
        'updated'   => 'Actualizado',
        'actions'   => 'Acciones'
    ],

    'forms' => [
        'id'        => 'ID',
        'title'     => 'Titulo',
        'resume'    => 'Resumen',
        'image'     => 'Imagen',
        'body'      => 'Contenido',
        'title_en'     => 'Titulo (en)',
        'resume_en'    => 'Resumen (en)',
        'body_en'      => 'Contenido (en)',
        'title_es'     => 'Titulo (es)',
        'resume_es'    => 'Resumen (es)',
        'body_es'      => 'Contenido (es)',
        'create'    => 'Crear Articulo',
        'update'    => 'Actualizar Articulo',
    ],

    'buttons' => [
        'create-new'    => 'Nuevo Articulo',
        'delete'        => '<i class="fa fa-trash-o fa-fw" aria-hidden="true"></i>  <span class="hidden-xs hidden-sm">Eliminar</span>',
        'show'          => '<i class="fa fa-eye fa-fw" aria-hidden="true"></i> <span class="hidden-xs hidden-sm">Mostrar</span>',
        'edit'          => '<i class="fa fa-pencil fa-fw" aria-hidden="true"></i> <span class="hidden-xs hidden-sm">Editar</span>',
        'back-to-articles' => '<span class="hidden-sm hidden-xs">Volver a </span><span class="hidden-xs">Articulos</span>',
        'back-to-article'  => 'Volver  <span class="hidden-xs">a Articulo</span>',
        'delete-article'   => '<i class="fa fa-trash-o fa-fw" aria-hidden="true"></i>  <span class="hidden-xs">Eliminar</span><span class="hidden-xs"> Articulo</span>',
        'edit-article'     => '<i class="fa fa-pencil fa-fw" aria-hidden="true"></i> <span class="hidden-xs">Editar</span><span class="hidden-xs"> Articulo</span>',
    ],
];
