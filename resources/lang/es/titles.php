<?php

return [

    'app'               => 'ScoutChamps',
    'app2'              => 'Auth :version',
    'home'              => 'Home',
    'dashboard'         => 'Dashboard',
    'login'             => 'Ingresar',
    'logout'            => 'Salir',
    'register'          => 'Registrarse',
    'joinnow'           => 'Únete Ahora',
    'resetPword'        => 'Resetear Contraseña',
    'toggleNav'         => 'Toggle Navigation',
    'profile'           => 'Profile',
    'editProfile'       => 'Editar Perfil',
    'createProfile'     => 'Crear Perfil',
    'adminDropdownNav'  => 'Admin',

    'activation'        => 'Activación Requerida',
    'exceeded'          => 'Activación Error',

    'editProfile'       => 'Editar Perfil',
    'createProfile'     => 'Crear Perfil',
    'adminUserList'     => 'Usuarios',
    'popularUsers'      => 'Usuarios Populares',
    'adminEditUsers'    => 'Editar Usuarios',
    'adminNewUser'      => 'Crear Nuevo Usuario',

    'adminLogs'         => 'Log Files',
    'adminActivity'     => 'Activity Log',
    'adminPHP'          => 'PHP Information',
    'adminRoutes'       => 'Routing Details',
    'privacy'           => 'Politicas de privacidad',
    'termsConditions'   => 'Términos y Condiciones',

    'activeUsers'       => 'Usuarios Activos',
    'laravelBlocker'    => 'Blocker',

    'laravelroles'      => 'Administrar Roles',

    'administrator'     => 'Administrador',
    'allUsers'          => 'Todos los usuarios',
    'scout'             => 'Scout',
    'scouts'            => 'Scouts',
    'athlete'           => 'Atleta',
    'athletes'          => 'Atletas',

    'club'              => 'Equipo',
    'adminClubesList'   => 'Entidades',

    'sportScout'        => 'Scout del siguiente deporte',
    'sport'             => 'Deporte',
    'sport-secondary'   => 'Deporte Secundario',
    'adminSportsList'   => 'Deportes',
    'fields'            => 'Campos',
    'edit'              => 'Editar',
    'show'              => 'Ver',
    'delete'            => 'Eliminar',
    'followedUpdate'    => ':rol :name1 followed to :name2.',
    'userRegistered'    => ':name joined to ScoutChamps.',

    'favorites'         => 'Favoritss',
    'followed'          => 'Usuarios Seguidos',
    'recruitments'      => 'Reclutamientos',
    'detailRecruitment' => 'Detalle Reclutamiento',
    'articles'          => 'Articulos',
    'recruitments-invitations' => 'Invitaciones a Reclutamientos',
    'filters'           => 'Filtros',
    'showFilters'       => 'Mostrar Filtros',
    'hideFilters'       => 'Ocultar Filtros',
    'all'               => 'Todo',
    'publicities'       => 'Publicidades',
    'coupons'           => 'Cupones',
    'planes'            => 'Mi Plan',
    'planesAdmin'       => 'Planes',
    'subscriptions'     => 'Subscripciones',
    'mysubscriptions'   => 'Mis Subscripciones', 
    'showcase'          => 'Showcase',
    'unlimited'         => 'Ilimitado',
    'Unlimited'         => 'Ilimitado',
    // Plans 
    'Free'              => 'Gratuito',
    'Month'             => 'Mes',
    'Semester'          => 'Semestre',
    'Anual'             => 'Anual',
    // To use as roel labels
    'Scout'             => 'Scout',
    'Athlete'           => 'Atleta',
    'Athlete Premiun'   => 'Atleta Premiun',
    'Athlete Premiun Semester' => 'Atleta Premiun',
    'Athlete Premiun Anual'   => 'Atleta Premiun',
    'Admin'             => 'Administrador' ,

    


    'notFound'        => 'Página no encontrada.',
    'forbbiden'       => 'Acceso restringido.',
    'btnGoDashboard'  => 'Ir al Dashboard'

];
