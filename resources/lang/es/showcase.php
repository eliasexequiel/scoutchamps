<?php 
return [
    'title' => 'Equipos preferidos',
    'addAssociation' => 'Agregar Asociación',
    'association'   => 'Asociación',
    'associationsAvailables'   => 'Asociaciones restantes',
    'updateSuccess' => 'Equipos actualizados exitosamente!',
    'associationContacted' => 'ScoutChamps contactó a la asociación.',
    'admin' => [
        'table' => [
            'athlete'     => 'Atleta',
            'plan'        => 'Plan',
            'association' => 'Asociación',
            'associationsTotal' => ':used de :total',
            'actions'     => 'Acciones'
        ],
        'buttons' => [
            'show' => '<i class="fa fa-eye fa-fw" aria-hidden="true"></i> <span class="hidden-xs hidden-sm">Mostrar</span>',
            'back-to-showcase' => '<span class="hidden-sm hidden-xs">Volver a </span><span class="hidden-xs"> Showcase </span>',
            'store' => 'Guardar'
        ],
        'back-showcase' => 'Volver a Showcase',
        'countAssociations' => 'Asociaciones',
        'contacted' => 'Contactado',
        'storeSuccess' => 'Showcase actualizado exitosamente!',
        'noEntities'    => 'No hay asociaciones'
    ],
    'adviceToAthleteFree' => 'Suscribete a un <a href="/mySubscriptions"> Plan Premium </a> para acceder.',
    'adviceToAthleteFreeText' => '<strong>Vitrina</strong> te permite elegir Universidades, Sponsors o equipos profesionales de tu preferencia que ScoutChamps puede contactar y hacerles saber de tu interés.'
];