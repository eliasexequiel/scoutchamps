<?php
return [
    // Titles
    'showing-all-recruitments'     => 'Reclutamientos',
    'my-recruitments'              => 'Mis Reclutamientos', 
    'recruitmentsTotal'            => 'Total reclutamientos', 
    'recruitments-menu-alt'        => 'Reclutamientos',
    'create-new-recruitment'       => 'Crear Reclutamiento',
    'editing-recruitment'          => 'Editar Reclutamiento',
    'showing-recruitment'          => 'Mostrar Reclutamiento',
    'detail-recruitment'           => 'Detalle Reclutamiento',
    'detail'                       => 'Detalle',
    'showing-recruitment-title'    => 'Información :name',
    'noResults'                    => 'No hay reclutamientos',
    'notFound'                     => 'Reclutamiento no encontrado',

    // Flash Messages
    'createSuccess'   => 'Reclutamiento creado exitosamente! ',
    'updateSuccess'   => 'Reclutamiento actualizado exitosamente! ',
    'deleteSuccess'   => 'Reclutamiento eliminado exitosamente! ',

    'recruitments-table' => [
        'id'        => 'ID',
        'type'      => 'Tipo',
        'date'      => 'Fecha',
        'hour'      => 'Hora',
        'place'     => 'Lugar',
        'address'   => 'Dirección',
        'isPublic'  => 'Público',
        'created'   => 'Creado',
        'updated'   => 'Actualizado',
        'actions'   => 'Acciones',
    ],

    'forms' => [
        'id'        => 'ID',
        'dateStart' => 'Fecha Comienzo',
        'day'       => 'Dia',
        'dayStart'  => 'Dia Comienzo',
        'dayEnd'    => 'Dia finalización',
        'message'   => 'Mensaje',
        'hour'      => 'Hora',
        'place'     => 'Lugar',
        'address'   => 'Dirección',
        'city'      => 'Ciudad',
        'country'   => 'País',
        'public'    => 'Público',
        'private'   => 'Privado',
        'create'    => 'Siguiente',
        'update'    => 'Actualizar Reclutamiento',
    ],

    'buttons' => [
        'create-new'    => 'Nuevo Reclutamiento',
        'delete'        => '<i class="fa fa-trash-o fa-fw" aria-hidden="true"></i>  <span class="hidden-xs hidden-sm">Eliminar</span>',
        'show'          => '<i class="fa fa-eye fa-fw" aria-hidden="true"></i> <span class="hidden-xs hidden-sm">Ver</span>',
        'edit'          => '<i class="fa fa-pencil fa-fw" aria-hidden="true"></i> <span class="hidden-xs hidden-sm">Editar</span>',
        'back-to-recruitments' => '<span class="hidden-sm hidden-xs">Volver a </span><span class="hidden-xs">Reclutamientos</span>',
        'back-to-recruitment'  => 'Volver  <span class="hidden-xs">a reclutamientos</span>',
        'delete-recruitment'   => '<i class="fa fa-trash-o fa-fw" aria-hidden="true"></i>  <span class="hidden-xs">Eliminar</span><span class="hidden-xs"> Recruitment</span>',
        'edit-recruitment'     => '<i class="fa fa-pencil fa-fw" aria-hidden="true"></i> <span class="hidden-xs">Editar</span><span class="hidden-xs"> Recruitment</span>',
    ],

    'tooltips' => [
        'back-to-recruitments' => 'Back to Recruitments'
    ],
    'invitation' => [
        'title'             => 'Quieres invitar a :name a tus reclutamientos?',
        'title-2'           => 'Invitar',
        'sendInvitations'   => 'Enviar invitaciones',
        'send'              => 'Enviar',
        'sentBy'            => 'Enviada por',
        'invitation'        => 'Invitación',
        'athletesInvited'   => 'Atletas invidatos',
        'sport'             => 'Deporte',
        'scout'             => 'Scout',
        'place'             => 'Lugar',
        'address'           => 'Dirección',
        'date'              => 'Fecha',
        'hour'              => 'Hora',
        'followed'          => 'Seguidos',
        'followers'         => 'Seguidores',
        'favorites'         => 'Favoritos',
        'all'               => 'Todo',
        'noUsersInvited'    => 'No hay usuarios invitados',
        'noAthletesInvited' => 'No hay atletas invitados',
        'usersInvitedSuccess' => 'Invitaciones a usuarios enviadas!',
        'goToEvent'         => 'Va al reclutamiento',
        'yes'               => 'Asistiré',
        'no'                => 'No asistiré',
        'perhaps'           => "Tal vez asista",
        'assist'            => 'asistirán',
        'noAssist'          => 'No asiste',
        'perhapsAssist'     => 'Quizás asiste',
        'usersDeletedSuccess' => 'Usuario eliminado de reclutamiento',
        'delete'            => 'Eliminar',
        'messageDelete'     => 'Eliminar a :name del reclutamiento?',
        'responseSuccess'   => 'Respuesta enviada.'
    ],
    'request'   => [
        'responseSuccess'   => 'Solicitud de Invitación enviada.',
        'sendRequest'       => 'Solicitar invitación a reclutamiento'
    ]
];
