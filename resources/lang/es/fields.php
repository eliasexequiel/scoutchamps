<?php
return [
    'typeUsers' => [
        ''        => 'Todo',
        'Admin'   => 'Administrador',
        'Athlete' => 'Atleta',
        'Scout'   => 'Scout'
    ],

    'status' => [
        'Amateur'      => 'Amateur',
        'Youth Level'  => 'Principiante',
        'Professional' => 'Profesional'
    ],
    'sportingGoal' => [
        'UniversityScholarship' => 'Beca Universitaria',
        'ProfessionalContract' => 'Contrato Profesional',
        'UniversityScholarship-ProfessionalContract' => 'Beca Universitaria y Contrato Profesional'
    ],
    'genders' => [
        'Male'   => 'Hombre',
        'Female' => 'Mujer'
    ],

    'typesFieldsSports' => [
        'text'   => 'texto',
        'number' => 'número',
        'select' => 'combo'
    ],

    // Duration discounts Stripe
    'durationDiscounts' => [
        'forever'   => 'Para siempre',
        'once'      => 'Una vez',
        'repeating' => 'Varias veces'
    ],

    // Type Entities 
    'entitiesType' => [
        'Club'       => 'Academia/Equipo',
        'University' => 'Universidad',
        // 'Institute'  => 'Academia',
        'Sponsor'    => 'Sponsor',
        'Company'    => 'Empresa',
        'Other'      => 'Otro'
    ],

    'planScout' => [
        'name' => 'Scout Gratuito',
        'items' => [
            0 => "Perfil",
            1 => "Fotos",
            2 => "Acceso completo al perfil de todos los Atletas y de otros Scouts",
            3 => "Acerca de mi",
            4 => "Invitaciones públicas o privadas a atletas a pruebas de reclutamiento",
            5 => "Contactar Administrador en Dashboard"
        ]
    ],
    'positionsScout' => [
        'Rivals Analyst'                => 'Analista de Rivales',
        'Tactical Analyst'              => 'Analista Táctico',
        'Recruiter - Scouting Analyst'  => 'Analista de Scouting',
        'Assistant Coach'               => 'Asistente de Entrenador',
        'Assistant Physical Trainer'    => 'Asistente de Preparador Físico',
        'Management Team'               => 'Directivo',
        'Brand Manager'                 => 'Ejecutivo de Marca',
        'Head Coach'                    => 'Entrenador Principal',
        'Kinesiologist'                 => 'Kinesiólogo',
        'Sports Manager'                => 'Manager Deportivo',
        'Sports Doctor'                 => 'Médico Deportivo',
        'Nutritionist'                  => 'Nutricionista',
        'Physical Trainer'              => 'Preparador Físico',
        'Athlete Representative'        => 'Representante de Atletas',
        'Sports Psychologist'           => 'Psicólogo Deportivo'
    ]
];
