<?php
return [
    // Titles
    'showing-all-clubs'     => 'Entities',
    'clubs-menu-alt'        => 'Show Entities Management Menu',
    'create-new-club'       => 'Create New Entity',
    'show-deleted-clubs'    => 'Show Deleted Entity',
    'editing-club'          => 'Editing Entity :name',
    'showing-club'          => 'Showing Entity :name',
    'showing-club-title'    => ':name\'s Information',

    // Flash Messages
    'createSuccess'   => 'Successfully created entity! ',
    'updateSuccess'   => 'Successfully updated entity! ',
    'verifySuccess'   => 'Successfully verified entity! ',
    'deleteSuccess'   => 'Successfully deleted entity! ',

    'clubTotal'     => 'Entity Total',

    'clubes-table' => [
        'id'        => 'ID',
        'name'      => 'Name',
        'website'   => 'Website',
        'type'      => 'Type',
        'email'     => 'Email',
        'phone'     => 'Phone',
        'country'   => 'Country',
        'created'   => 'Created',
        'updated'   => 'Updated',
        'actions'   => 'Actions',
        'updated'   => 'Updated',
        'verified'  => 'Verified',
        'noVerified'  => 'No Verified',
    ],

    'forms' => [
        'id'        => 'ID',
        'name'      => 'Name',
        'website'   => 'Website',
        'type'     => 'Type',
        'email'      => 'Email',
        'phone'      => 'Phone',
        'country'   => 'Country',
        'create'    => 'Create Entity',
        'update'    => 'Update Entity',
        'errorName' => 'There is another entity with that name.'
    ],

    'buttons' => [
        'create-new'    => 'New Entity',
        'delete'        => '<i class="fa fa-trash-o fa-fw" aria-hidden="true"></i>  <span class="hidden-xs hidden-sm">Delete</span>',
        'verify'        => '<i class="fa fa-check fa-fw" aria-hidden="true"></i>  <span class="hidden-xs hidden-sm">Verify</span>',
        'unverify'      => '<i class="fa fa-close fa-fw" aria-hidden="true"></i>  <span class="hidden-xs hidden-sm">Unverify</span>',
        'show'          => '<i class="fa fa-eye fa-fw" aria-hidden="true"></i> <span class="hidden-xs hidden-sm">Show</span>',
        'edit'          => '<i class="fa fa-pencil fa-fw" aria-hidden="true"></i> <span class="hidden-xs hidden-sm">Edit</span>',
        'back-to-clubs' => '<span class="hidden-sm hidden-xs">Back to </span><span class="hidden-xs">Entities</span>',
        'back-to-club'  => 'Back  <span class="hidden-xs">to Entity</span>',
        'delete-club'   => '<i class="fa fa-trash-o fa-fw" aria-hidden="true"></i>  <span class="hidden-xs">Delete</span><span class="hidden-xs"> Entity </span>',
        'edit-club'     => '<i class="fa fa-pencil fa-fw" aria-hidden="true"></i> <span class="hidden-xs">Edit</span><span class="hidden-xs"> Entity </span>',
    ],

    'types' => [
        'Club'          => 'Academy/Team',
        'Institute'     => 'Academy',
        'University'    => 'University/College',
        'Sponsor'       => 'Sponsor',
        'Company'       => 'Company',
        'clubs'         => 'Academies/Teams',
        'institutes'    => 'Academies',
        'universities'  => 'Universities/Colleges',
        'sponsors'      => 'Sponsors',
        'companies'     => 'Companies',
        'addClub'       => 'Add Academy/Team',
        'addInstitute'  => 'Add Academy',
        'addUniversity' => 'Add University',
        'addSponsor'    => 'Add Sponsor',
        'addCompany'    => 'Add Company'
    ],
    'messageDeleteTitle' => 'Delete entity',
    'messageDelete' => 'Are you sure you want to delete this entity?'
];
