<?php
return [
    'typeUsers' => [
        ''        => 'All',
        'Admin'   => 'Admin',
        'Athlete' => 'Athlete',
        'Scout'   => 'Scout'
    ],

    'status' => [
        'Amateur'      => 'Amateur',
        'Youth Level'  => 'Youth Level',
        'Professional' => 'Professional'
    ],
    'sportingGoal' => [
        'UniversityScholarship' => 'University Scholarship',
        'ProfessionalContract' => 'Professional Contract',
        'UniversityScholarship-ProfessionalContract' => 'University Scholarship and Professional Contract'
    ],
    'genders' => [
        'Male'   => 'Male',
        'Female' => 'Female'
    ],

    'typesFieldsSports' => [
        'text'   => 'text',
        'number' => 'number',
        'select' => 'select'
    ],

    // Duration discounts Stripe
    'durationDiscounts' => [
        'forever'   => 'Forever',
        'once'      => 'Once',
        'repeating' => 'Repeating'
    ],

    // Type Entities 
    'entitiesType' => [
        'Club'       => 'Academy/Team',
        'University' => 'University',
        // 'Institute'  => 'Academy',
        'Sponsor'    => 'Sponsor',
        'Company'    => 'Company',
        'Other'      => 'Other'
    ],

    'planScout' => [
        'name' => 'Free Scout',
        'items' => [
            0 => "Scout's Profile",
            1 => "Pictures",
            2 => "Complete Access to all Athlete´s profile and other Scouts profile",
            3 => "About Me",
            4 => "Invite Athletes to public or private recruitment try-outs",
            5 => "Contact Admin in Dashboard"
        ]
    ],
    'positionsScout' => [
        'Rivals Analyst'                => 'Rivals Analyst',
        'Tactical Analyst'              => 'Tactical Analyst',
        'Recruiter - Scouting Analyst'  => 'Recruiter - Scouting Analyst',
        'Assistant Coach'               => 'Assistant Coach',
        'Assistant Physical Trainer'    => 'Assistant Physical Trainer',
        'Management Team'               => 'Management Team',
        'Brand Manager'                 => 'Brand Manager',
        'Head Coach'                    => 'Head Coach',
        'Kinesiologist'                 => 'Kinesiologist',
        'Sports Manager'                => 'Sports Manager',
        'Sports Doctor'                 => 'Sports Doctor',
        'Nutritionist'                  => 'Nutritionist',
        'Physical Trainer'              => 'Physical Trainer',
        'Athlete Representative'        => 'Athlete Representative',
        'Sports Psychologist'           => 'Sports Psychologist'
    ]
];
