<?php 
return [
    'title'             => 'Plans',
    'plan'              => 'Plan',
    'subscribed'        => 'Subscribed',
    'alreadySubscribed' => 'You have already subscribed the plan',
    'buttons' => [
        'subscribe' => 'Subscribe',
        'cancel'    => 'Cancel subscription',
        'pay'       => 'Pay'

    ],
    'forms'  => [
        'coupon'    => 'Coupon',
        'applyDiscount' => 'Apply A Discount Code',
        'cardholderUser' => 'Cardholder'
    ],
    'premiunService'    => 'Service Premiun',
    'paymentData' => 'Payment Info',
    'resume'      => 'Total Order',
    'subtotal'    => 'Subtotal',
    'taxes'       => 'Taxes',
    'total'       => 'Total',
    'totalWithDiscount' => 'Total with Coupon',
    'errorCoupon' => 'Coupon not valid',
    'advicePlan'  => 'On Monthly Plan, once subscribed, ScoutChamps will charge a proportion regarding the days of the Month left until the current month is finished. Once the new month begins you will be charged the full price of the premium plan. If you use a discount coupon code, the discount will be applied to the current month and the following months depending on the duration of your coupon code.',
    'messages' => [
        'cancelled' => 'Subscription cancelled',
        'errorSubscription' => 'There was an error proccessing your card. Try with another card.',
        'errorCouponSubscription' => 'Coupon :coupon incorrect.',
        'success'   => 'Successfully subscribed to plan!'
    ],

    'admin' => [
        'editPricePlan' => 'Edit Price Plan',
        'back-plans'  => 'Back to Plans',
        'planTotal' => 'Plan Total',
        'table' => [
            'name' => 'Name',
            'price' => 'Price',
            'actions' => 'Actions'
        ],

        'forms' => [
            'price'   => 'Price',
            'update'  => 'Update Plan',
        ],

        'messages' => [
            'updateSuccess' => 'Successfully updated plan!',
            'deleteSuccess' => 'Successfully deleted plan!'
        ],
        'buttons' => [
            'create-new'    => 'New Plan',
            'delete'        => '<i class="fa fa-trash-o fa-fw" aria-hidden="true"></i>  <span class="hidden-xs hidden-sm">Delete</span>',
            'show'          => '<i class="fa fa-eye fa-fw" aria-hidden="true"></i> <span class="hidden-xs hidden-sm">Show</span>',
            'edit'          => '<i class="fa fa-pencil fa-fw" aria-hidden="true"></i> <span class="hidden-xs hidden-sm"> Edit price </span>',
            'back-to-plans' => '<span class="hidden-sm hidden-xs">Back to </span><span class="hidden-xs">Plans</span>',
            'delete-plan'   => '<i class="fa fa-trash-o fa-fw" aria-hidden="true"></i>  <span class="hidden-xs">Delete</span><span class="hidden-xs"> Plan</span>',
            'edit-plan'     => '<i class="fa fa-pencil fa-fw" aria-hidden="true"></i> <span class="hidden-xs">Edit</span><span class="hidden-xs"> Plan</span>',
            'delete-plan-question' => 'Are you sure you want to delete this plan?'
        ],
    ]

];