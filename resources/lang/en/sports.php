<?php
return [
    // Titles
    'showing-all-sports'     => 'Sports',
    'sports-menu-alt'        => 'Show Sports Management Menu',
    'create-new-sport'       => 'Create New Sport',
    'show-deleted-sports'    => 'Show Deleted Sport',
    'editing-sport'          => 'Editing Sport :name',
    'showing-sport'          => 'Showing Sport :name',
    'showing-sport-title'    => ':name\'s Information',
    'back-sports'            => 'Back to Sports',

    // Flash Messages
    'createSuccess'   => 'Successfully created sport! ',
    'updateSuccess'   => 'Successfully updated sport! ',
    'deleteSuccess'   => 'Successfully deleted sport! ',

    'sportTotal'    => 'Sport Total',

    'sports-table' => [
        'caption'   => '{1} :sportscount sport total|[2,*] :sportscount total sports',
        'id'        => 'ID',
        'name'      => 'Name',
        'description' => 'Description',
        'created'   => 'Created',
        'updated'   => 'Updated',
        'actions'   => 'Actions',
        'updated'   => 'Updated',
    ],

    'forms' => [
        'id'        => 'ID',
        'name'      => 'Name',
        'description'   => 'Description',
        'fields'    => 'Fields',
        'field-name'    => 'Name',
        'field-options' => 'Options Select',
        'field-type'    => 'Type',
        'field-add'     => 'Add field',
        'create'    => 'Create Sport',
        'update'    => 'Update Sport',
        'options-select' => 'Options separated by coma'
    ],

    'buttons' => [
        'create-new'    => 'New Sport',
        'delete'        => '<i class="fa fa-trash-o fa-fw" aria-hidden="true"></i>  <span class="hidden-xs hidden-sm">Delete</span>',
        'show'          => '<i class="fa fa-eye fa-fw" aria-hidden="true"></i> <span class="hidden-xs hidden-sm">Show</span>',
        'edit'          => '<i class="fa fa-pencil fa-fw" aria-hidden="true"></i> <span class="hidden-xs hidden-sm">Edit</span>',
        'back-to-sports' => '<span class="hidden-sm hidden-xs">Back to </span><span class="hidden-xs">Sports</span>',
        'back-to-sport'  => 'Back  <span class="hidden-xs">to Sport</span>',
        'delete-sport'   => '<i class="fa fa-trash-o fa-fw" aria-hidden="true"></i>  <span class="hidden-xs">Delete</span><span class="hidden-xs"> Sport</span>',
        'edit-sport'     => '<i class="fa fa-pencil fa-fw" aria-hidden="true"></i> <span class="hidden-xs">Edit</span><span class="hidden-xs"> Sport</span>',
    ],
];
