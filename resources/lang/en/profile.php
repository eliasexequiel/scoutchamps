<?php

return [

    /*
    |--------------------------------------------------------------------------
    | User Profiles Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match user profile
    | pages, messages, forms, labels, placeholders, links, and buttons.
    |
    */

    'templateTitle' => 'Edit your profile',

    // profile errors
    'notYourProfile'      => 'This isn\'t the profile you\'re looking for. Try again.',
    'notYourProfileTitle' => 'Hmmm, something went wrong...',
    'noProfileYet'        => 'No profile yet.',

    // USER profile title
    'showProfileUsername'        => 'Username',
    'showProfileFirstName'       => 'First Name',
    'showProfileLastName'        => 'Last Name',
    'showProfileEmail'           => 'E-mail Address',
    'showProfileLocation'        => 'Location',
    'showProfileBio'             => 'Bio',
    

    // USER profile page
    'showProfileTitle' => ':username\'s Profile',

    // USER EDIT profile page
    'editProfileTitle' => 'Profile - Account Settings',

    // User edit profile form
    'label-theme' => 'Your theme:',
    'ph-theme'    => 'Select your theme',

    'label-location' => 'Location:',
    'ph-location'    => 'Enter your location',

    'label-nationality'             => 'Select Nationality',
    'label-second-nationality'      => 'Select Second Nationality',
    'label-residence-country'       => 'Select Residence Country',
    'label-residence-city'          => 'Residence City',
    'label-phone'                   => '(Country code) Phone',
    'label-gender'                  => 'Select Gender',
    'label-sporting-goal'           => 'Select Sporting Goal',
    'label-height'                  => 'Height',
    'label-sport'                   => 'Sport',
    'label-status'                  => 'Select Status',
    'label-professional_start_date' => 'Professional start date',
    'label-professional_end_date'   => 'Professional end date',
    'label-doesnotApply'            => 'Does not Apply',
    'label-cargo-scout'             => 'Speciality',
    'label-cargo-secundario-scout'  => 'Secondary Speciality',
    'label-select'                  => 'Select',
    'publicPofile'                  => 'My public profile',

    // User Account Settings Tab
    'editTriggerAlt'        => 'Toggle User Menu',
    'editAccountTitle'      => 'Account Settings',
    'editAccountAdminTitle' => 'Account Administration',
    'editMedia'             => 'Multimedia',
    'editAthleticCarrer'    => 'Athletic Carrer',
    'updateAccountSuccess'  => 'Your account has been successfully updated',
    'submitProfileButton'   => 'Save Changes',

    // User Account Admin Tab
    'submitPWButton'    => 'Update Password',
    'changePwTitle'     => 'Change Password',
    'changePwPill'      => 'Change Password',
    'inviteRecruitments'  => 'Invite to recruitments',
    'deleteAccountPill' => 'Delete Account',
    'updatePWSuccess'   => 'Your password has been successfully updated',

    // Delete Account Tab
    'deleteAccountTitle'        => 'Delete Account',
    'deleteAccountBtn'          => 'Delete My Account',
    'deleteAccountBtnConfirm'   => 'Delete My Account',
    'deleteAccountConfirmTitle' => 'Confirm Account Deletion',
    'deleteAccountConfirmMsg'   => 'Are you sure you want to delete your account?',
    'confirmDeleteRequired'     => 'Confirm Account Deletion is required',

    'errorDeleteNotYour'        => 'You can only delete your own profile',
    'successUserAccountDeleted' => 'Your account has been deleted',

    // Messages
    'updateSuccess' => 'Your profile has been successfully updated',
    'adviceFollowUserToSendMessage' => 'You have to follow the user to send a message.',
    'adviceUserPremiumToSendMessage' => 'Subscribe to <a href="/mySubscriptions"> Premium Plan </a> to send a message.',
    'submitButton'  => 'Save Changes',

    // Restore User Account
    'errorRestoreUserTime' => 'Sorry, account cannot be restored',
    'successUserRestore'   => 'Welcome back :username! Account Successfully Restored',

    'image'                  => 'Image',
    'images'                 => 'Images',
    'imagesAvailables'       => 'Availables Images',
    'updatePlanLimitImages'  => 'Limit Images reached. To upload more images update your plan <a class="hover:underline" href="/mySubscriptions"> here </a>.',
    'video'                  => 'Video',
    'videos'                 => 'Videos',
    'videosAvailables'       => 'Availables Videos',
    'updatePlanLimitVideos'  => 'Limit Videos reached. To upload more videos update your plan <a class="hover:underline" href="/mySubscriptions"> here </a>.',
    'publicRecruitments'     => 'Public Recruitments',
    'trayectory'             => 'Athletic Carrer',
    'noTrayectory'           => 'No trayectory',
    'posts'                  => 'Posts',
    'follow'                 => 'Follow',
    'following'              => 'Following',
    'unfollow'               => 'Unfollow',
    'favorites'              => 'Favorites',
    'addedFavorites'         => 'Favorite added',
    'addFavorite'            => 'Add To Favorites',
    'removeFavorite'         => 'Remove to Favorites',
    'sendMessage'            => 'Send Message',
    'questionRemoveFavorite' => 'Are you sure you want to remove :name from favorites?',
    'questionUnfollow'       => 'Are you sure you want to unfollow :name?',
    'adviceDeleteAccount'    => '<strong>Deleting</strong> your account is <u><strong>permanent</strong></u> and <u><strong>cannot</strong></u> be undone.',
    
    'showcase'  => 'Showcase',
    'sports' => 'Sports',
    'clubs' => 'Clubs',
    'clubsUniversitiesInstitutes' => 'Clubs/Universities/Academies',
    'clubsUniversitiesInstitutesSponsors' => 'Clubs/Universities/Academies/Sponsors',
    'moreInformation' => 'More information',
    'readmore'  => 'Read more',
    'readless'  => 'Read less',

    'athlete' => [
        'labelPremiun'  => 'Athlete Premiun',
        'scoutVisits'   => 'Scout Visits',
        'visits'        => 'Visits',
        'followers'     => 'Followers',
        'following'     => 'Following',
        'deleteImage'   => 'Are you sure you want to delete this image?',
        'deleteVideo'   => 'Are you sure you want to delete this video?',
        'delete'        => 'Delete',
        'addClub'       => 'Add',
        'removeClub'    => 'Remove Club',
        'otherClub'     => 'Other',
        'nameOtherClub' => 'Name',
        'from'          => 'From',
        'to'            => 'To',
        'month'         => 'Month',
        'year'          => 'Year',
        'current' => 'Current :name',
        'achievements' => 'Achievements',
        'addAchievement' => 'Add Achievement',
        'infoAdditional' => 'Info additional',
        'upload' => 'Upload',
        'url' => 'Url',
        'urlVideo' => 'Url Video',
        'uploadImageFromUrl' => 'Upload image from url',
        'uploadVideoFromUrl' => 'Upload video from url',
        'about'  => 'About me',
        'tutorFirstName' => 'First Name Parent/Legal Guardian',
        'tutorLastname'  => 'Last Name Parent/Legal Guardian',
        'tutorEmail'     => 'E-Mail Parent/Legal Guardian',
        'tutorPhone'     => 'Phone Parent/Legal Guardian',
        'phone'  => 'Phone',
        'uploadPhotoProfileMaxSize' => 'Max size profile photo: 50mb',
        'uploadImageAdvice' => 'Images should be smaller than 300mb. Images type accepted: jpeg, png, jpg, svg.',
        'uploadVideoAdvice' => 'Videos should be smaller than 300mb. Videos type accepted: mp4, avi, x-flv.',
        'uploadVideoAdviceFreeAthlete' => 'Max duration video allowed: 90 seconds.',
        'email'  => 'E-mail Address',
        'nationality' => 'Nationality',
        'secondNationality' => 'Second Nationality',
        'residenceCountry' => 'Residence Country',
        'residenceCity' => 'Residence City',
        'dob'   => 'Date of birth',
        'height' => 'Height',
        'heightCm' => 'cm',
        'heightFt' => 'feets',
        'cm' => 'cm',
        'feets' => 'ft',
        'sport' => 'Sport',
        'secondarySport' => 'Secondary Sport',
        'sportingGoal' => 'Sporting Goal',
        'status' => 'Status',
        'contract' => 'Contract',
        'currentClub' => 'Current',
        'club' => 'Club',
        'achievements' => 'Achievements',
        'noAchievements' => 'No Achievements',
        'noClubs'        => 'No teams',
        'noInstitutes'   => 'No academies',
        'noUniversities' => 'No universities',
        'noSponsors'     => 'No sponsors',
        'noVideos'       => 'No videos',
        'noImages'       => 'No images'
    ],
    'scout' => [
        'cargo-scout'             => 'Speciality',
        'cargo-secundario-scout'  => 'Secondary Speciality'  
    ],
    'results'     => 'Results',
    'totalUsers'  => 'Users Total',
    'noResults'   => 'No results',
    'viewProfile' => 'View Profile',
    'filters' => [
        'filterBy'          => 'Filters',
        'typeUser'          => 'User type',
        'name'              => 'Firstname or Lastname',
        'search'            => 'Search',
        'sport'             => 'Select Sport',
        'gender'            => 'Select Gender',
        'age'               => 'Select Age',
        'typeAthlete'       => 'Select Athlete Type',
        'sportingGoal'      => 'Select Athletic Goal',
        'residenceCountry'  => 'Select Country of residence',
        'country'           => 'Select Country',
        'nationality'       => 'Select Nationality',
        'allSports'         => 'All Sports',
        'allNationalities'  => 'All Nationalities',
        'allCountries'      => 'All Countries',
        'searchText'        => 'Type a name to search and press enter',
        'entities'          => 'Entities',
        'selectEntity'      => 'Select Entity',
        'selectSpecialityScout' => 'Select Scout Speciality',
        'specialitiesScout'     => 'Scout Specialities'

    ],
    'publicities' => [
        'deletePublicity'      => 'Delete',
        'deletePublicityTitle' => 'Delete Publicity',
        'deletePublicityModal' => 'Are you sure you want to delete this publicity?',

    ],
    'btnAccept' => 'Accept',
    'completeAllSteps' => 'Complete all steps to increase your chances of success!',

    'errors' => [
        'clubRequired' => 'The club is required',
        'achievementNameRequired' => 'The achievement name is required',
        'clubStartDateFormat' => 'Start format date should be d/m/Y',
        'clubEndDateFormat' => 'End format date should be d/m/Y',
        'clubEndGreaterThanStartDate' => 'End date must be a date after start date',
        'clubInfoAditionalMaxLength' => 'Info Aditional may not be greater than :max characters'
    ]
];
