<?php

return [

    'title-main'            => 'Platform for high performance athletes',
    'title-1'               => 'Welcome to ScoutChamps',
    'text-1'                => 'Through this platform, Scouts (Sports Directors, Team Coaches and Sponsors) can observe countless of high performance athletes to recruit them and thus, increase the competitiveness of their teams without the necessity to travel locally and overseas. Get ready to become the next Champ!',
    'title-2'               => 'Abaut ScoutChamps',
    'text-2'                => 'Our work includes helping universities/colleges and professional teams get in contact with talented high performance athletes who wait for an opportunity to succeed. We have Scouts in all sports searching for talented athletes in our platform, and thus to inform their teams about them. This will increase the chance of being discovered and propelled into your dreams.',
    'dreams'                => 'Making dreams possible',
    'ourServices'           => 'Our Services',
    'howWorks'              => [
        'title' => 'How Does It Work',
        'steps' => [
            '1' => 'Create your profile: Scouts (Free), Athletes (Free or Premium). Complete your profile and show your skills, upload photos and videos. You can find out which Scouts saw your profile and which ones registered you as one of their favorite athletes.',
            '2' => 'Invite Scouts (Sports Directors, Team Coaches and Sponsors) to check out your profile and videos. Scouts will look for the best athletes to improve the competitiveness of their teams (Professional or University).',
            '3' => 'If you are the ideal athlete for a team, the Scout will request to contact you and he will automatically receive your contact information (e-mail, phone number). Contacting you does not guarantee anything.',
            '4' => 'ScoutChamps does not interfere in the negotiation and wishes success to both Parties.'
        ]
    ],
    'home'                  => 'Home',
    'aboutUs'               => [
        'title'       => 'About Us',
        'text'        => 'We are a company with global reach who focuses on creating a network for athletes to help them achieve their goals: Scouts (Sports Directors, Team Coaches and Sponsors) get to know athletes to help them obtain sports scholarships in universities, play professionally or get sponsorship of a brand.',
        'misionTitle' => 'Mission',
        'mision'      => 'Create a network where thousands of high performance athletes can be scouted globally by Sports Directors, Team Coaches and Sponsors at the professional and collegiate level.',
        'visionTitle' => 'Vision',
        'vision'      => 'Be the world´s leading company in collaborating with professional/amateur teams and colleges/universities in the recruitment of elite athletes.'
    ],
    'sports'                => 'Sports',
    'clubs'                 => 'Teams / Universities',
    'faqs'                  => 'Faqs',
    'ultimasNoticias'       => 'Latest News',
    'titleMap'              => 'Athletes and Scouts from all around the world',
    'choosePlan'            => 'Select',
    'atlethesRegistered'    => 'Athletes registered',
    'scoutsRegistered'      => 'Scouts registered',
    'newConversations'      => 'New conversations',
    'contactBetweenScoutAndAthletes' => 'Contacts Scouts <i class="fa fa-arrow-right" aria-hidden="true"></i> Athletes',
    'countriesAthletesScouts'   => 'Countries with Athletes/Scouts',
    'visits'                => 'Visits',
    'popularAthletes'       => 'Trusted Athletes',
    'popularScouts'         => 'Trusted Scouts',
    'contact'               => [
        'title' => 'Contact',
        'email' => 'E-mail',
        'whatsapp' => 'WhatsApp'
    ],
    'searchText' => 'Search',
    'savePlan'   => 'Save'

];
