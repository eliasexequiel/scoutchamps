<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Forms Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match forms.
    |
    */

    // CREATE NEW USER FORM
    'create_user_label_email' => 'User Email',
    'create_user_ph_email'    => 'User Email',
    'create_user_icon_email'  => 'fa-envelope',

    'create_user_label_username' => 'Username',
    'create_user_ph_username'    => 'Username',
    'create_user_icon_username'  => 'fa-user',

    'create_user_label_firstname' => 'First Name',
    'create_user_ph_firstname'    => 'First Name',
    'create_user_icon_firstname'  => 'fa-user',

    'create_user_label_lastname' => 'Last Name',
    'create_user_ph_lastname'    => 'Last Name',
    'create_user_icon_lastname'  => 'fa-user',

    'create_user_label_password' => 'Password',
    'create_user_ph_password'    => 'Password',
    'create_user_icon_password'  => 'fa-lock',

    'aboutme'   => 'About Me',

    'create_user_label_pw_confirmation' => 'Confirm Password',
    'create_user_ph_pw_confirmation'    => 'Confirm Password',
    'create_user_icon_pw_confirmation'  => 'fa-lock',

    'create_user_label_location' => 'User Location',
    'create_user_ph_location'    => 'User Location',
    'create_user_icon_location'  => 'fa-map-marker',

    'create_user_label_role' => 'User Role',
    'create_user_ph_role'    => 'Select User Role',
    'create_user_icon_role'  => 'fa fa-fw fa-shield',

    'create_user_button_text' => 'Create New User',

    // EDIT USER AS ADMINISTRATOR FORM
    'edit-user-admin-title' => 'Edit User Information',

    'label-username' => 'Username',
    'ph-username'    => 'Username',

    'label-useremail' => 'User Email',
    'ph-useremail'    => 'User Email',

    'label-userrole_id' => 'User Access Level',
    'option-label'      => 'Select a Level',
    'option-user'       => 'User',
    'option-editor'     => 'Editor',
    'option-admin'      => 'Administrator',
    'submit-btn-text'   => 'Edit the User!',

    'submit-btn-icon' => 'fa-save',
    'username-icon'   => 'fa-user',
    'useremail-icon'  => 'fa-envelope-o',

    'edit-clubs'     => 'Edit Clubs',
    'change-pw'      => 'Change Password',
    'cancel'         => 'Cancel',
    'save-changes'   => '<i class="fa fa-fw fa-save" aria-hidden="true"></i> Save Changes',

    // Search Users Form
    'search-users-ph' => 'Search Users',
    'upload-file-profile' => 'Profile Picture',
    'upload-file'     => 'Drop files here to upload',
    'uploading-file'  => 'Uploading...',   
    'upload-failed'   => 'Upload Failed',
    'upload-file-profile-error' => 'File size exceeded.',
    'remove-file'     => 'Remove file',
    'removing-file'   => 'Removing...',

    'months' => [
        'January' => 'January',
        'February' => 'February',
        'March' => 'March',
        'April' => 'April',
        'May' => 'May',
        'June' => 'June',
        'July' => 'July',
        'August' => 'August',
        'September' => 'September',
        'October' => 'October',
        'November' => 'November',
        'December' => 'December'
    ]

];
