<?php
return [
    'title' => 'Subscriptions',

    'back-subscriptions'    => 'Back to Subscriptions',
    'subscriptionsTotal'    => 'Subscription Total',
    'subscriptionsActiveTotal' => 'Active subscription total',

    'subscriptions-table' => [
        'id'          => 'ID',
        'user'        => 'User',
        'name'        => 'Name',
        'status'      => 'Status',
        'hasPendingPayments' => 'Pending Payments',
        'latestInvoice' => 'Latest Invoice',
        'created'     => 'Created',
        'updated'     => 'Updated',
        'actions'     => 'Actions',
        'updated'     => 'Updated',
    ],

    'status' => [
        'cancelled' => 'Cancelled',
        'active'    => 'Active'
    ],

    'buttons'   => [
        'invoices'  => 'Show invoices',
        'back-to-subscriptions' => '<span class="hidden-sm hidden-xs">Back to </span><span class="hidden-xs">Subscriptions</span>',
    ],

    'invoices' => [
        'title'           => 'Invoices',
        'invoicesTotal'   => 'Invoices Total',
        'table' => [
            'total'      => 'Total',
            'subtotal'   => 'Subtotal',
            'tax'        => 'Tax',
            'amountPaid' => 'Amount Paid',
            'created'    => 'Date',
            'status'     => 'Status'
        ],
        'download' => 'Download',
        'status' => [
            'paid'      => 'Paid',
            'pending'   => 'Pending',
            'open'      => 'Open'
        ]
    ],
    'mysubscriptions' => [
        'title'             => 'My plan',
        'currentPlan'       => 'Current plan',
        'modifyPlan'        => 'Modify',
        'newPlan'           => 'Choose your new Plan',
        'invoices'          => 'Invoices',
        'paymentsMethods'   => 'Payments Methods',
        'noPaymentsMethods' => "You don't have payments methods",
        'noSubscriptions'   => "You don't have subscriptions",
        'noInvoices'        => "You don't have invoices",
        'plans'             => 'Show plans',
        'addPaymentMethod'  => 'Add Payment Method',
        'saveCard'          => 'Save card',
        'storePaymentMethodSuccess' => 'Payment method added successfully!',
        'defaultCardSuccess'        => 'Card saved by default successfully!',
        'deleteCardSuccess'         => 'Card deleted successfully!',
        'labelDefaultPaymentMethod' => 'Default Method',
        'fields' => [
            'cardholderName' => 'CardHolder Name',
        ],
        'buttons' => [
            'defaultCard'  => 'Use on current subscription',
            'delete'       => '<i class="fa fa-trash-o fa-fw" aria-hidden="true"></i>  <span class="hidden-xs hidden-sm">Delete</span>',
        ],
    ],

    'changeSubscription' => 'Change Subscription',
    'changeLabel'        => 'Do you want to change your current plan to :name plan?',
    'changeButtonAccept' => 'Accept',
    'changeButtonCancel' => 'Cancel',
];
