<?php
return [
    // Titles
    'showing-all-articles'     => 'Articles',
    'articles-menu-alt'        => 'Show Articles Management Menu',
    'create-new-article'       => 'Create New Article',
    'show-deleted-articles'    => 'Show Deleted Article',
    'editing-article'          => 'Editing Article',
    'showing-article'          => 'Showing Article',
    'showing-article-title'    => 'Article Information',
    'back-articles'            => 'Back to Articles',

    // Flash Messages
    'createSuccess'   => 'Successfully created article! ',
    'updateSuccess'   => 'Successfully updated article! ',
    'verifySuccess'   => 'Successfully verified article! ',
    'deleteSuccess'   => 'Successfully deleted article! ',

    'articleTotal'  => 'Article Total',

    'articles-table' => [
        'caption'   => '{1} :articlescount articles total|[2,*] :articlescount total articles',
        'id'        => 'ID',
        'title'     => 'Title',
        'resume'    => 'Resume',
        'body'      => 'Body',
        'created'   => 'Created',
        'updated'   => 'Updated',
        'actions'   => 'Actions',
        'updated'   => 'Updated'
    ],

    'forms' => [
        'id'        => 'ID',
        'title'     => 'Title',
        'resume'    => 'Resume',
        'image'     => 'Image',
        'body'      => 'Body',
        'title_en'     => 'Title (en)',
        'resume_en'    => 'Resume (en)',
        'body_en'      => 'Body (en)',
        'title_es'     => 'Title (es)',
        'resume_es'    => 'Resume (es)',
        'body_es'      => 'Body (es)',
        'create'    => 'Create Article',
        'update'    => 'Update Article',
    ],

    'buttons' => [
        'create-new'    => 'New Article',
        'delete'        => '<i class="fa fa-trash-o fa-fw" aria-hidden="true"></i>  <span class="hidden-xs hidden-sm">Delete</span>',
        'verify'        => '<i class="fa fa-check fa-fw" aria-hidden="true"></i>  <span class="hidden-xs hidden-sm">Verify</span>',
        'show'          => '<i class="fa fa-eye fa-fw" aria-hidden="true"></i> <span class="hidden-xs hidden-sm">Show</span>',
        'edit'          => '<i class="fa fa-pencil fa-fw" aria-hidden="true"></i> <span class="hidden-xs hidden-sm">Edit</span>',
        'back-to-articles' => '<span class="hidden-sm hidden-xs">Back to </span><span class="hidden-xs">Articles</span>',
        'back-to-article'  => 'Back  <span class="hidden-xs">to Article</span>',
        'delete-article'   => '<i class="fa fa-trash-o fa-fw" aria-hidden="true"></i>  <span class="hidden-xs">Delete</span><span class="hidden-xs"> Article</span>',
        'edit-article'     => '<i class="fa fa-pencil fa-fw" aria-hidden="true"></i> <span class="hidden-xs">Edit</span><span class="hidden-xs"> Article</span>',
    ],
];
