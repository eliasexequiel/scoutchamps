<?php

return [

    'app'               => 'ScoutChamps',
    'app2'              => 'Auth :version',
    'home'              => 'Home',
    'dashboard'         => 'Dashboard',
    'login'             => 'Login',
    'logout'            => 'Logout',
    'register'          => 'Register',
    'joinnow'           => 'Join Now',
    'resetPword'        => 'Reset Password',
    'toggleNav'         => 'Toggle Navigation',
    'profile'           => 'Profile',
    'editProfile'       => 'Edit Profile',
    'createProfile'     => 'Create Profile',
    'adminDropdownNav'  => 'Admin',

    'activation'        => 'Registration Started  | Activation Required',
    'exceeded'          => 'Activation Error',

    'editProfile'       => 'Edit Profile',
    'createProfile'     => 'Create Profile',
    'adminUserList'     => 'Users',
    'popularUsers'      => 'Popular Users',
    'adminEditUsers'    => 'Edit Users',
    'adminNewUser'      => 'Create New User',

    'adminLogs'         => 'Log Files',
    'adminActivity'     => 'Activity Log',
    'adminPHP'          => 'PHP Information',
    'adminRoutes'       => 'Routing Details',
    'privacy'           => 'Privacy Policy',
    'termsConditions'   => 'Terms and Conditions',

    'activeUsers'       => 'Active Users',
    'laravelBlocker'    => 'Blocker',

    'laravelroles'      => 'Roles Administration',

    'administrator'     => 'Administrator',
    'allUsers'          => 'All Users',
    'scout'             => 'Scout',
    'scouts'            => 'Scouts',
    'athlete'           => 'Athlete',
    'athletes'          => 'Athletes',

    'club'              => 'Team',
    'adminClubesList'   => 'Entities',

    'sportScout'        => 'Scout of the following Sport',
    'sport'             => 'Sport',
    'sport-secondary'   => 'Sport Secondary',
    'adminSportsList'   => 'Sports',
    'fields'            => 'Fields',
    'edit'              => 'Edit',
    'show'              => 'Show',
    'delete'            => 'Delete',
    'followedUpdate'    => ':rol :name1 followed to :name2.',
    'userRegistered'    => ':name joined to ScoutChamps.',

    'favorites'         => 'Favorites',
    'followed'          => 'Users Following',
    'recruitments'      => 'Recruitments',
    'detailRecruitment' => 'Detail Recruitment',
    'articles'          => 'Articles',
    'recruitments-invitations' => 'Invitations Recruitments',
    'filters'           => 'Filters',
    'showFilters'       => 'Show Filters',
    'hideFilters'       => 'Hide Filters',
    'all'               => 'All',
    'publicities'       => 'Publicities',
    'coupons'           => 'Coupons',
    'planes'            => 'My Plan',
    'planesAdmin'       => 'Plans',
    'subscriptions'     => 'Subscriptions',
    'mysubscriptions'   => 'My Subscriptions',
    'showcase'          => 'Showcase', 
    'unlimited'         => 'Unlimited',
    'Unlimited'         => 'Unlimited',
    // Plans 
    'Free'              => 'Free',
    'Month'             => 'Month',
    'Semester'          => 'Semester',
    'Anual'             => 'Annual',
    // To use as roel labels
    'Scout'             => 'Scout',
    'Athlete'           => 'Athlete',
    'Athlete Premiun'   => 'Athlete Premiun',
    'Athlete Premiun Semester' => 'Athlete Premiun',
    'Athlete Premiun Anual'   => 'Athlete Premiun',
    'Admin'             => 'Admin',
    
    


    'notFound'        => 'Page not found.',
    'forbbiden'       => 'Forbidden.',
    'btnGoDashboard'  => 'Go Dashboard'

];
