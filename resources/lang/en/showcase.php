<?php 
return [
    'title' => 'Showcase',
    'addAssociation' => 'Add Association',
    'association'   => 'Association',
    'associationsAvailables'   => 'Availables Associations',
    'updateSuccess' => 'Clubs updated successfully!',
    'associationContacted' => 'ScoutChamps contacted the association.',
    'admin' => [
        'table' => [
            'athlete'     => 'Athlete',
            'plan'        => 'Plan',
            'association' => 'Association',
            'associationsTotal' => ':used of :total',
            'actions'     => 'Actions'
        ],
        'buttons' => [
            'show' => '<i class="fa fa-eye fa-fw" aria-hidden="true"></i> <span class="hidden-xs hidden-sm">Show</span>',
            'back-to-showcase' => '<span class="hidden-sm hidden-xs">Back to </span><span class="hidden-xs"> Showcase </span>',
            'store' => 'Save'
        ],
        'back-showcase' => 'Back to Showcase',
        'countAssociations' => 'Associations',
        'contacted' => 'Contacted',
        'storeSuccess' => 'Showcase updated successfully!',
        'noEntities'    => 'There are not associations'
    ],
    'adviceToAthleteFree' => 'Subscribe to <a href="/mySubscriptions"> Premium Plan </a> to access this feature.',
    'adviceToAthleteFreeText' => '<strong>Showcase</strong> allows you to list colleges/universities or teams of your preference so ScoutChamps contacts them and lets them know about your interest.'
];