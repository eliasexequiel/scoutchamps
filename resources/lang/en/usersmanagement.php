<?php

return [

    // Titles
    'showing-all-users'     => 'Users',
    'users-menu-alt'        => 'Show Users Management Menu',
    'create-new-user'       => 'Create New User',
    'show-blocked-users'    => 'Show Blocked User',
    'editing-user'          => 'Editing User :name',
    'editing-user-clubs'    => 'Editing Clubs - :name',
    'showing-user'          => 'Showing User :name',
    'showing-user-title'    => ':name\'s Information',

    // Flash Messages
    'createSuccess'      => 'Successfully created user! ',
    'updateSuccess'      => 'Successfully updated user! ',
    'deleteSuccess'      => 'Successfully deleted user! ',
    'deleteSelfError'    => 'You cannot delete yourself! ',
    'verifiedSuccess'    => 'Successfully verified user!' ,
    'unverifiedSuccess'  => 'Successfully deleted verification user!' ,
    'blockedSuccess'     => 'Successfully blocked user!',
    'unblockedSuccess'   => 'Successfully unblocked user!' ,

    // Show User Tab
    'viewProfile'            => 'View Profile',
    'editUser'               => 'Edit User',
    'showPublicProfile'      => 'Show profile',
    'deleteUser'             => 'Delete User',
    'usersBackBtn'           => 'Back to Users',
    'usersPanelTitle'        => 'User Information',
    'labelUserName'          => 'Username:',
    'labelEmail'             => 'Email:',
    'labelPhone'             => 'Phone:',
    'labelFirstName'         => 'First Name:',
    'labelLastName'          => 'Last Name:',
    'labelRole'              => 'Role:',
    'labelStatus'            => 'Status:',
    'labelAccessLevel'       => 'Access',
    'labelPermissions'       => 'Permissions:',
    'labelCreatedAt'         => 'Created:',
    'labelUpdatedAt'         => 'Updated:',
    'labelIpEmail'           => 'Email Signup IP:',
    'labelIpEmail'           => 'Email Signup IP:',
    'labelIpConfirm'         => 'Confirmation IP:',
    'labelIpSocial'          => 'Socialite Signup IP:',
    'labelIpAdmin'           => 'Admin Signup IP:',
    'labelIpUpdate'          => 'Last Update IP:',
    'labelDeletedAt'         => 'Deleted on',
    'labelIpDeleted'         => 'Deleted IP:',
    'usersDeletedPanelTitle' => 'Blocked User Information',
    'usersBackDelBtn'        => 'Back to Deleted Users',

    'successRestore'    => 'User successfully restored.',
    'successDestroy'    => 'User record successfully destroyed.',
    'errorUserNotFound' => 'User not found.',

    'labelUserLevel'  => 'Level',
    'labelUserLevels' => 'Levels',

    'no-records' => 'Users not found',
    'showing-user-blocked' => 'Blocked users',
    'userTotal'   => 'User Total',
    'users-table' => [
        'caption'   => '{1} :userscount user total|[2,*] :userscount total users',
        'id'        => 'ID',
        'name'      => 'Username',
        'fname'     => 'First Name',
        'lname'     => 'Last Name',
        'email'     => 'Email',
        'role'      => 'Role',
        'created'   => 'Created',
        'lastLogin' => 'Last Connection',
        'updated'   => 'Updated',
        'actions'   => 'Actions',
        'deleted'   => 'Deleted',
    ],

    'buttons' => [
        'create-new'    => 'New User',
        'delete'        => '<i class="fa fa-trash-o fa-fw" aria-hidden="true"></i>  <span class="hidden-xs hidden-sm">Delete</span>',
        'show'          => '<i class="fa fa-eye fa-fw" aria-hidden="true"></i> <span class="hidden-xs hidden-sm">Show</span>',
        'edit'          => '<i class="fa fa-pencil fa-fw" aria-hidden="true"></i> <span class="hidden-xs hidden-sm">Edit</span>',
        'verify'        => '<i class="fa fa-check fa-fw" aria-hidden="true"></i>  <span class="hidden-xs hidden-sm">Verify</span>',
        'unverify'      => '<i class="fa fa-times fa-fw" aria-hidden="true"></i>  <span class="hidden-xs hidden-sm">Unverify</span>',
        'block'         => '<i class="fa fa-lock fa-fw" aria-hidden="true"></i>  <span class="hidden-xs hidden-sm">Block</span>',
        'unblock'       => '<i class="fa fa-unlock fa-fw" aria-hidden="true"></i>  <span class="hidden-xs hidden-sm">Unblock</span>',
        'back-to-users' => '<span class="hidden-sm hidden-xs">Back to </span><span class="hidden-xs">Users</span>',
        'back-to-user'  => 'Back  <span class="hidden-xs">to User</span>',
        'restoreUser'   => '<i class="fa fa-refresh" aria-hidden="true"></i> <span class="hidden-xs hidden-sm"> Restore User </span>',
        'destroyUser'   => '<i class="fa fa-times" aria-hidden="true"></i> <span class="hidden-xs hidden-sm">Destroy User Record </span>',
        'delete-user'   => '<i class="fa fa-trash-o fa-fw" aria-hidden="true"></i>  <span class="hidden-xs">Delete</span><span class="hidden-xs"> User</span>',
        'edit-user'     => '<i class="fa fa-pencil fa-fw" aria-hidden="true"></i> <span class="hidden-xs">Edit</span><span class="hidden-xs"> User</span>',
    ],

    'tooltips' => [
        'delete'        => 'Delete',
        'show'          => 'Show',
        'edit'          => 'Edit',
        'create-new'    => 'Create New User',
        'back-users'    => 'Back to users',
        'email-user'    => 'Email :user',
        'submit-search' => 'Submit Users Search',
        'clear-search'  => 'Clear Search Results',
    ],

    'messages' => [
        'userNameTaken'          => 'Username is taken',
        'userNameRequired'       => 'Username is required',
        'fNameRequired'          => 'First Name is required',
        'lNameRequired'          => 'Last Name is required',
        'emailRequired'          => 'Email is required',
        'emailInvalid'           => 'Email is invalid',
        'passwordRequired'       => 'Password is required',
        'PasswordMin'            => 'Password needs to have at least 6 characters',
        'PasswordMax'            => 'Password maximum length is 20 characters',
        'captchaRequire'         => 'Captcha is required',
        'CaptchaWrong'           => 'Wrong captcha, please try again.',
        'roleRequired'           => 'User role is required.'
    ],

    'show-user' => [
        'id'                => 'User ID',
        'name'              => 'Username',
        'email'             => 'Email',
        'role'              => 'User Role',
        'created'           => 'Created',
        'updated'           => 'Updated',
        'labelRole'         => 'User Role',
        'labelAccessLevel'  => '<span class="hidden-xs">User</span> Access Level|<span class="hidden-xs">User</span> Access Levels',
    ],

    'search'  => [
        'title'             => 'Showing Search Results',
        'found-footer'      => ' Record(s) found',
        'no-results'        => 'No Results',
        'search-users-ph'   => 'Search Users',
        'allTypeUsers'      => 'All Users',
        'createdDate'       => 'Created Date',
        'lastLoginDate'     => 'Last Login Date',
        'typeUser'          => 'User Type'
    ],

    'modals' => [
        'delete_user_message' => 'Are you sure you want to delete :user?',
        'btnCancel' => '<i class="fa fa-fw fa-close" aria-hidden="true"></i> Cancel',
        'btnDelete' => '<i class="fa fa-fw fa-trash-o" aria-hidden="true"></i> Delete'
    ],
];
