@extends('layouts.app')

@section('template_title')
    {!! trans('posts.showing-post') !!}
@endsection

@section('content')

    @php
        $previousUrl = url()->previous();
        // dd($previousUrl);
    @endphp

    <show-post previous-url="{{ $previousUrl }}" :post="{{ $post }}"></show-post>

@endsection
