@extends('layouts.app')

@section('template_title')
    {!! trans('posts.new-post') !!}
@endsection

@section('content')

    <edit-post :edit="false"></edit-post>

@endsection

@section('footer_scripts')

<link href="{{ asset('/assets/summernote/dist/summernote.css') }}" rel="stylesheet">
<link href="{{ asset('/assets/summernote/dist/summernote-bs4.css') }}" rel="stylesheet">
<script src="{{ asset('assets/summernote/dist/summernote.min.js') }}"></script>
<script src="{{ asset('assets/summernote/dist/summernote-bs4.min.js') }}"></script>

@endsection