@extends('layouts.app')

@section('template_title')
    Dashboard
@endsection

@section('template_fastload_css')
@endsection

@section('content')

    <div class="w-full px-6">
        <div class="flex flex-wrap w-full">

            <div class="mb-4 w-full">
                <p-articles></p-articles>   
            </div>
            
            {{-- ATHLETE --}}
            @role('athlete')
                
                <div class="flex flex-wrap w-full">
                    <div class="w-full sm:pr-2 sm:w-1/2 md:w-full lg:w-1/2">
                        <p-recruitments></p-recruitments>
                    </div>

                    @permission('publish.posts')            
                    <div class="w-full mt-4 sm:mt-0 sm:pl-2 sm:w-1/2 md:w-full lg:w-1/2">
                        <p-my-posts></p-my-posts>   
                    </div>
                    @endpermission
                    
                    {{-- FOLLOWED USERS --}}
                    {{-- <div class="w-full mt-2">
                        <p-followed-users class="h-64 overflow-y-scroll"></p-followed-users>
                    </div> --}}
                </div>
            @endrole

            {{-- SCOUT --}}
            @role('scout')
            
            <div class="flex flex-wrap w-full">
                <div class="w-full sm:pr-2 sm:w-1/2 md:w-full lg:w-1/2">
                    <p-recruitments-scout></p-recruitments-scout>
                </div>
                {{-- FOLLOWED and FAVORITES USERS --}}
                <div class="w-full mt-4 sm:mt-0 sm:pl-2 sm:w-1/2 md:w-full lg:w-1/2">
                        <p-followed-users></p-followed-users>
                </div>
            </div>
                {{-- <div class="w-full mt-2">
                    <p-favorites-users class="h-64 overflow-y-scroll"></p-favorites-users>
                </div> --}}
            @endrole
            {{-- @role('athlete')
            <div class="w-full mt-2 sm:pl-2 sm:w-1/2 md:w-full lg:w-1/2">
                <p-recruitments-invited class="h-64 overflow-y-scroll"></p-recruitments-invited>
            </div>
            @endrole --}}
            <div class="w-full mt-4">
                    <p-updates></p-updates>   
            </div>


        </div>
        
    </div>

    {{-- Publicities --}}
    @include('partials.publicities.publicity-below')
    

@endsection

@section('footer_scripts')

<link href="{{ asset('/assets/summernote/dist/summernote.css') }}" rel="stylesheet">
<link href="{{ asset('/assets/summernote/dist/summernote-bs4.css') }}" rel="stylesheet">
<script src="{{ asset('assets/summernote/dist/summernote.min.js') }}"></script>
<script src="{{ asset('assets/summernote/dist/summernote-bs4.min.js') }}"></script>

@endsection
