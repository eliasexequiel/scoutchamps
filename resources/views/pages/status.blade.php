@extends('layouts.app')

@section('template_title')
	See Message
@endsection

@section('head')
@endsection

@section('content')

 <div class="container">
	<div class="row justify-center">
	    <div class="col-12 col-md-8 col-lg-6">
			 @include('partials.form-status')
			<div class="card-p">
				@if($error)
					{{ $error }}
				@endif
			 </div>
        </div>
    </div>
</div>

@endsection