<footer class="w-full py-6 bg-primary text-sm text-white">
    <div class="flex flex-wrap justify-center items-center">
        <div class="text-xs sm:text-sm px-2"> &copy; Scout Champs @php echo \Carbon\Carbon::now()->format('Y'); @endphp </div>
        <div> - </div>
        <div class="px-2"> <a class="font-medium text-xs sm:text-sm" href="{{ route('conditions') }}"> {{ trans('titles.termsConditions') }} </a> </div>
        <div class="px-2"> <a class="font-medium text-xs sm:text-sm" href="{{ route('privacy') }}"> {{ trans('titles.privacy') }} </a> </div>
    </div>
</footer>

<script>

</script>