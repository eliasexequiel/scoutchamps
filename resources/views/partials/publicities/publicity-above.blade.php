@if(isset($publicityAbove) && $publicityAbove->image)
    <div class="mb-4 w-full">
        @if(isset($publicityAbove->link) && $publicityAbove->link != 'null')
            <a class="cursor-pointer" href="{{ $publicityAbove->link }}">
                <img class="h-24 md:h-48 object-center object-contain mx-auto w-full rounded-lg" src="{{ $publicityAbove->image }}" alt="Publicity">
            </a>
        @else
            <img class="h-24 md:h-48 object-center object-contain mx-auto w-full rounded-lg" src="{{ $publicityAbove->image }}" alt="Publicity">
        @endif
    </div>
@endif