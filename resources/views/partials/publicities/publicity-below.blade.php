@if(count($publicitiesBelow) > 0)
    <div class="flex flex-wrap justify-center px-6 w-full mt-4 max-w-3xl mx-auto">
        @foreach($publicitiesBelow as  $p)
        @if($p->image)
        <div class="p-2 w-auto md:w-1/4">
                @if($p->link)
                    <a class="cursor-pointer" href="{{ $p->link }}">
                        <img class="object-cover w-32 md:w-full h-24 rounded-lg" src="{{ $p->image }}" alt="Publicity">
                    </a>
                @else
                    <img class="object-cover w-32 md:w-full h-24 rounded-lg" src="{{ $p->image }}" alt="Publicity">
                @endif
        </div>
        @endif
        @endforeach
    </div>
@endif