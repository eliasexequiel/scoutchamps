    <div class="w-full">
        {{-- {!! Form::open(['route' => 'search-users', 'method' => 'POST', 'role' => 'form', 'class' => 'needs-validation', 'id' => 'search_users']) !!} --}}
        {!! Form::open(array('route' => 'users', 'method' => 'GET','id' => 'formSearchNavbar', 'class' => 'form flex flex-wrap items-end w-full relative')) !!}
            
            <div class="w-1/2 sm:w-1/3 md:w-1/6 mt-2">
                <label for=""> {{ trans('usersmanagement.search.createdDate') }} </label>
                <input type="text" class="lastSession form-control border-primary" name="createdAt" placeholder="dd/mm/yyyy" value="{{ request()->input('createdAt') }}">
            </div>

            <div class="w-1/2 sm:w-1/3 md:w-1/6 mt-2 pl-2">
                <label for=""> {{ trans('usersmanagement.search.lastLoginDate') }} </label>
                <input type="text" class="lastSession form-control border-primary" name="lastSession" placeholder="dd/mm/yyyy" value="{{ request()->input('lastSession') }}">
            </div>
            {{-- {!! csrf_field() !!} --}}
            <div class="w-full sm:w-1/3 md:w-1/6 mt-2 sm:w-32 sm:pl-2">
                <label for=""> {{ trans('usersmanagement.search.typeUser') }} </label>
                <select name="typeUser" class="form-control border-primary" value="{{ request()->input('typeUser') }}">
                    <option value=""> {{ trans('usersmanagement.search.allTypeUsers') }} </option>
                    @foreach ($roles as $role)
                        <option value="{{ $role->name }}" {{(request()->input('typeUser') == $role->name) ? 'selected' : '' }}> {{ trans('titles.'.$role->name) }} </option>
                    @endforeach
                </select>
            </div>
            <div class="w-full md:w-1/2 flex-grow mt-2 md:pl-2">
                <div class="input-group input-search-users">
                    <input id="input-navbar" class="hover:border-primary border-primary form-control" value="{{ request()->input('fullnameFilter') }}" name="fullnameFilter" type="text" placeholder="{{ trans('profile.filters.search') }}">
                    {{-- {!! Form::text('user_search_box', NULL, ['id' => 'user_search_box', 'class' => 'form-control', 'placeholder' => trans('usersmanagement.search.search-users-ph'), 'aria-label' => trans('usersmanagement.search.search-users-ph'), 'required' => false]) !!} --}}
                    <div class="input-group-append">
                        <button type="submit" class="input-group-addon btn px-2 btn-primary rounded-br-lg rounded-tr-lg">
                                <img src="{{ asset('img/search.png') }}" alt="Search">
                        </button>
                    </div>
                </div>
            </div>
        {!! Form::close() !!}
    </div>
