<div class="@if(isset($size)) {{ $size }} @endif relative">
    @auth
        <a href="{{ $user->publicLink }}">
            <img class="@if(isset($size)) {{ $size }} @else profile-sm @endif object-cover rounded-full border border-gray-500" src="{{ $user->avatar }}" alt="{{ $user->fullName }}">
        </a>
    @else
        <a href="{{ route('register') }}">
            <img class="@if(isset($size)) {{ $size }} @else profile-sm @endif object-cover rounded-full border border-gray-500" src="{{ $user->avatar }}" alt="{{ $user->fullName }}">
        </a>
    @endauth
    @if($user->avatarSport)
        <img class="sport-sm" src="{{ $user->avatarSport }}" alt="{{ $user->fullName }}">
    @endif
</div>