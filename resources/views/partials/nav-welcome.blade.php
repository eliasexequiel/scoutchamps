<nav id="navWelcome" class="fixed-top navbar nav-welcome navbar-expand-lg bg-primary navbar-laravel">
        <div class="w-full flex flex-wrap justify-between">
            <a class="navbar-brand" href="{{ url('/') }}">
                {{-- {!! config('app.name', trans('titles.app')) !!} --}}
                <img class="img-fluid" src="{{ asset('img/logo.png') }}" alt="ScoutChamps">
            </a>
            <button type="button" id="navbar-toggler" class="navbar-toggler" data-toggle="collapse" data-target="#navbarCollapse">
                    <div class="animated-hamburguer">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </button>

            <div class="collapse navbar-collapse" id="navbarCollapse">
                
                {{-- Right Side Of Navbar --}}
                <div class="block ml-auto">
                <ul class="flex flex-col lg:flex-row">
                    {{-- Home --}}
                    <li id="linkWelcomeHome" class="mx-1 link-navbar {{ Route::currentRouteName() == 'welcome' ? 'active' : '' }}">
                        <a href="{{ route('welcome') }}"> {{ trans('welcome.home') }} </a>
                    </li>
                    {{-- About Us --}}
                    <li class="mx-1 link-navbar {{ Route::currentRouteName() == 'guest.aboutus' ? 'active' : '' }}">
                        <a href="{{ route('guest.aboutus') }}"> {{ trans('welcome.aboutUs.title') }} </a>
                    </li>
                    {{-- Sports --}}
                    <li id="linkWelcomeSports" class="mx-1 link-navbar {{ Request::is('g/sport/**') ? 'active' : '' }}">
                        <a href="{{ (Route::currentRouteName() == 'welcome') ? '#welcomeSports' : route('welcome').'#welcomeSports' }}"> {{ trans('welcome.sports') }} </a>
                    </li>
                    {{-- Scouts --}}
                    <li class="mx-1 link-navbar {{ request()->get('typeUser') == '2' ? 'active' : '' }}">
                        {!! Form::open(array('route' => 'guest.search', 'method' => 'GET', 'class' => 'form')) !!}
                            <input type="hidden" name="typeUser" value="2">
                            <button type="submit"> {{ trans('titles.scouts') }} </button>
                        {!! Form::close() !!}
                    </li>
                    {{-- Athletes --}}
                    <li class="mx-1 link-navbar {{ request()->get('typeUser') == '3' ? 'active' : '' }}">
                        {!! Form::open(array('route' => 'guest.search', 'method' => 'GET', 'class' => 'form')) !!}
                            <input type="hidden" name="typeUser" value="3">
                            <button type="submit"> {{ trans('titles.athletes') }} </button>
                        {!! Form::close() !!}
                    </li>
                    {{-- Clubs --}}
                    @php
                        $sectionClubs = \DB::table('config_site')->where('name', 'sectionClubsInHome')->first();
                        $showSectionClubs = $sectionClubs && $sectionClubs->value;
                    @endphp
                    @if($showSectionClubs)
                        <li class="mx-1 link-navbar {{ ( Request::is('g/searchclub') || Request::is('g/club/**') ) ? 'active' : '' }}">
                            {!! Form::open(array('route' => 'guest.search.club', 'method' => 'GET', 'class' => 'form')) !!}
                                <button type="submit"> {{ trans('welcome.clubs') }} </button>
                            {!! Form::close() !!}
                        </li>
                    @endif
                    {{-- FAQ --}}
                    <li class="mx-1 link-navbar {{ Route::currentRouteName() == 'guest.faqs' ? 'active' : '' }}">
                        <a href="{{  route('guest.faqs' )}}"> {{ trans('welcome.faqs') }} </a>
                    
                </ul>
                <ul class="lg:mt-2 justify-end flex flex-col lg:flex-row">
                    </li>
                        {{-- SEARCH --}}
                        {!! Form::open(array('route' => 'guest.search', 'method' => 'GET','id' => 'formSearchNavbarHome', 'class' => 'form w-full lg:max-w-sm mt-1 lg:mt-0 lg:mx-2 relative')) !!}
                        
                            <div style="display: -webkit-flex; display: flex;" class="w-full hover:shadow-xl">
                                <input id="input-navbar" class="w-full p-2" value="{{ request()->input('fullname') }}" name="fullname" type="text" placeholder="{{ trans('welcome.searchText') }}">
                                
                                <button type="submit" id="search-button" class="bg-white">
                                    <img src="{{ asset('img/search-red.png') }}" alt="Search">
                                </button>
                            </div>
                        {!! Form::close() !!}
                    {{-- Authentication Links --}}
                    @guest
                        <div class="lg:hidden flex justify-center">
                            <li class="mt-1 lg:mt-0 lg:mb-0"><a class="btn btn-login mx-2" href="{{ route('login') }}">{{ trans('titles.login') }}</a></li>
                            <li class="mt-1 lg:mt-0 lg:mb-0"><a class="btn btn-register mx-2" href="{{ route('register') }}">{{ trans('titles.register') }}</a></li>
                        </div>    
                        <li class="hidden lg:block mt-1 lg:mt-0 lg:mb-0"><a class="btn btn-login mx-2" href="{{ route('login') }}">{{ trans('titles.login') }}</a></li>
                        <li class="hidden lg:block mt-1 lg:mt-0 lg:mb-0"><a class="btn btn-register mx-2" href="{{ route('register') }}">{{ trans('titles.register') }}</a></li>
                    @else
                        <li class="nav-item dropdown no-arrow show">
                                <a class="flex items-center text-white nav-link dropdown-toggle px-3 lg:px-1" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                <span class="capitalize"> {{ Auth::user()->fullName }} </span>
                                </a>
                                <!-- Dropdown - User Information -->
                                <div class="dropdown-menu p-0 dropdown-menu-right rounded shadow animated--grow-in" aria-labelledby="userDropdown">
                                    {{-- <li class="mx-1 link-navbar"> --}}
                                    <a class="flex font-bold text-gray-900 pl-6 pb-1 pt-2 hover:bg-gray-300" href="{{ url('/home') }}"> {{ trans('sidebar.dashboard') }}  </a>
                                    <a class="flex font-bold text-gray-900 pl-6 py-1 hover:bg-gray-300" href="{{ url('/profile') }}"> {{ trans('sidebar.myprofile') }}  </a>
                                    {{-- </li> --}}
                                    <a class="flex font-bold text-gray-900 pl-6 pt-1 pb-2 hover:bg-gray-300" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                        {{-- <i class="inline-flex fa fa-sign-out" aria-hidden="true"></i> --}}
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                        </li>
                        
                    @endguest
                    <li class="nav-item-lang link-navbar">
                            @if(app()->getLocale() == 'en')
                                <a href="{{ url('locale/es') }}" > <img style="max-width: 32px; height: 22px;" src="{{ asset('img/home/flag_spain.png') }}" alt="Spain Flag"> ES </a>
                            @else
                                <a href="{{ url('locale/en') }}" >  <img style="max-width: 32px; height: 22px;" src="{{ asset('img/home/flag_usa.png') }}" alt="EEUU Flag"> EN </a>
                            @endif

                            {{-- <a class="flex items-center text-white nav-link dropdown-toggle px-3 lg:px-1" href="#" id="langDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                @if(app()->getLocale() == "en")
                                    <img style="width: 32px; height: 19px;" class="pr-2" src="{{ asset('img/home/flag_usa.png') }}" alt="EEUU Flag">
                                @else
                                    <img style="width: 32px; height: 19px;" class="pr-2" src="{{ asset('img/home/flag_spain.png') }}" alt="Spain Flag">
                                @endif
                                <span class="uppercase"> {{ app()->getLocale() }} </span>
                                
                            </a>
                            <div class="dropdown-menu dropdown-menu-right rounded shadow animated--grow-in min-w-0 w-20" aria-labelledby="langDropdown">
                                @if(app()->getLocale() == 'en')
                                    <a href="{{ url('locale/es') }}" > <img src="{{ asset('img/home/flag_spain.png') }}" alt="Spain Flag"> ES </a>
                                @else
                                    <a href="{{ url('locale/en') }}" >  <img src="{{ asset('img/home/flag_usa.png') }}" alt="EEUU Flag"> EN </a>
                                @endif
                            </div> --}}
                    </li>
                </ul>
                </div>
            </div>
        </div>
    </nav>

@section('footer_scripts')
<script>
        document.getElementById('navbar-toggler').addEventListener('click', function () {
            $('.animated-hamburguer').toggleClass('open');
        });
</script>
@append