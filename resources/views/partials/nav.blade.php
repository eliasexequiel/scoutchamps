<nav class="navbar navbar-expand-md navbar-laravel px-0">
    <div class="flex w-full px-6">
        
        {{-- <a class="navbar-brand ml-2" href="{{ url('/') }}">
            {!! config('app.name', trans('titles.app')) !!}
        </a> --}}
        <button type="button" id="sidebarCollapse" class="btn-collapse-sidebar btn-collapse-navbar block md:hidden btn btn-primary">
            Menu
        </button>
        {{-- <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            <span class="sr-only">{!! trans('titles.toggleNav') !!}</span>
        </button> --}}
        <div class="flex items-center w-full" id="navbarSupportedContent">
            {{-- Left Side Of Navbar --}}
            <ul class="navbar-nav mr-auto">
            </ul>
            {{-- Right Side Of Navbar --}}
            <ul class="navbar-nav w-full flex ml-4 md:ml-0 flex-row">
                {{-- Authentication Links --}}
                @guest
                    <li><a class="nav-link" href="{{ route('login') }}">{{ trans('titles.login') }}</a></li>
                    <li><a class="nav-link" href="{{ route('register') }}">{{ trans('titles.register') }}</a></li>
                @else
                   {!! Form::open(array('route' => 'public.profile.search', 'method' => 'GET','id' => 'formSearchNavbar', 'class' => 'form w-full relative')) !!}
                   <div class="w-full">
                        @role('admin|athlete')
                        <div class="filters-panel-athlete flex flex-wrap w-full">
                            {{-- Estadisticas --}}
                            <div class="hidden md:flex mb-4 w-full flex-wrap justify-around">
                                <div class="pl-2 flex w-1/3 justify-center items-center text-xs">
                                    <div class="pr-2">
                                        <img class="mx-auto object-contain h-8 w-auto" src="{{asset('img/home/cantidadAtletas.png')}}" alt="{{ trans('welcome.scoutsRegistered') }}">
                                    </div> 
                                    <div class="text-center">
                                        <span> {{ $athletesCount }} </span>
                                        <span> {{ trans('welcome.atlethesRegistered') }} </span>
                                    </div>
                                </div>
                                <div class="pl-2 flex w-1/3 justify-center items-center text-xs">
                                    <div class="pr-2">
                                        <img class="mx-auto object-contain h-8 w-auto" src="{{asset('img/home/cantidadScout.png')}}" alt="{{ trans('welcome.scoutsRegistered') }}">
                                    </div>
                                    <div class="text-center"> 
                                        <span> {{ $scoutsCount }} </span>
                                        <span> {{ trans('welcome.scoutsRegistered') }} </span>
                                    </div>
                                </div>
                                <div class="pl-2 flex w-1/3 justify-center items-center text-xs">
                                    <div class="pr-2">
                                        <img class="mx-auto object-contain h-10 w-auto object-contain" src="{{asset('img/home/cantidadConversacion.png')}}" alt="{{ trans('welcome.scoutsRegistered') }}">
                                    </div>   
                                    <div class="text-center"> 
                                        <span> {{ $conversationsScoutsAthletes ?? 0 }} </span>
                                        <span> {!! trans('welcome.contactBetweenScoutAndAthletes') !!} </span>
                                    </div>
                                </div>
                            </div>

                            <div class="flex flex-wrap w-full">
                                <div class="w-full sm:w-1/5 lg:w-1/4 sm:pr-2 mb-2 sm:mb-0">
                                    {!! Form::select('entity', $entitiesAll, request()->input('entity'), ['id' => 'input-navbar-entities','class' => 'bg-white w-full p-2','data-placeholder' => trans('profile.filters.selectEntity'), 'placeholder' => trans('profile.filters.selectEntity') ]) !!}
                                </div>

                                <div class="w-full sm:w-2/5 lg:w-1/4 sm:pr-2 mb-2 sm:mb-0">
                                    {!! Form::select('scout_speciality', $specialitiesScouts, request()->input('scout_speciality'), ['id'=> 'input-navbar-specialities-scout','class' => 'bg-white w-full p-2','data-placeholder' => trans('profile.filters.selectSpecialityScout'),'placeholder' => trans('profile.filters.selectSpecialityScout') ]) !!}
                                </div>

                                <div class="w-full sm:w-2/5 lg:w-2/4 flex flex-1">
                                    <input id="input-navbar" class="w-full p-2" value="{{ request()->input('fullname') }}" name="fullname" type="text" placeholder="{{ trans('profile.filters.searchText') }}">
                                    {{-- {!! Form::select('sport', $sportsAll, request()->input('sport'), ['id'=>'id', 'class' => 'border-r-0 w-auto form-control', 'placeholder' => 'Sport']) !!} --}}
                                    <button type="submit" id="search-button" class="bg-primary">
                                        <img src="{{ asset('img/search.png') }}" alt="Search">
                                    </button>
                                </div>
                            </div>
                            
                        </div>
                        @endrole 
                        @role('scout')
                        <div class="flex flex-wrap w-full">
                        {{-- Estadisticas --}}
                        <div class="hidden md:flex mb-4 w-full justify-around flex-wrap">
                                <div class="pl-2 flex w-auto items-center text-xs">
                                    <div class="pr-2">
                                        <img class="mx-auto h-8 object-contain w-auto" src="{{asset('img/home/cantidadAtletas.png')}}" alt="{{ trans('welcome.scoutsRegistered') }}">
                                    </div> 
                                    <div class="text-center">
                                        <span> {{ $athletesCount }} </span>
                                        <span> {{ trans('welcome.atlethesRegistered') }} </span>
                                    </div>
                                </div>
                                <div class="pl-2 flex w-auto items-center text-xs">
                                    <div class="pr-2">
                                        <img class="mx-auto h-8 object-contain w-auto" src="{{asset('img/home/cantidadScout.png')}}" alt="{{ trans('welcome.scoutsRegistered') }}">
                                    </div>
                                    <div class="text-center"> 
                                        <span> {{ $scoutsCount }} </span>
                                        <span> {{ trans('welcome.scoutsRegistered') }} </span>
                                    </div>
                                </div>
                                <div class="pl-2 flex w-auto items-center text-xs">
                                    <div class="pr-2">
                                        <img class="mx-auto h-10 object-contain w-auto" src="{{asset('img/home/cantidadConversacion.png')}}" alt="{{ trans('welcome.scoutsRegistered') }}">
                                    </div>   
                                    <div class="text-center"> 
                                        <span> {{ $conversationsScoutsAthletes ?? 0 }} </span>
                                        <span> {!! trans('welcome.contactBetweenScoutAndAthletes') !!} </span>
                                    </div>
                                </div>
                        </div>

                        <div class="w-full h-full card filters-navbar">
                            <div class="bg-white text-black p-0" id="headingOne">
                                <h5 class="mb-0 px-2 flex justify-between items-center">
                                    <span class="text-base font-normal"> {{ trans('titles.filters') }} </span>
                                    <button type="button" class="hidden md:block btn arrow-font btn-collapse-filters {{ Route::currentRouteName() != 'home' && Route::currentRouteName() != 'public.profile.search' ? 'collapsed' : '' }}">
                                       @if(Route::currentRouteName() != 'home' && Route::currentRouteName() != 'public.profile.search') 
                                        {{ trans('titles.showFilters') }} 
                                       @else
                                       {{ trans('titles.hideFilters') }} 
                                       @endif
                                       <i></i>
                                    </button>
                                    {{-- Button to Filter Mobile --}}
                                    <button type="button" class="btn-collapse-filters inline-flex md:hidden btn btn-sm btn-outline-primary">
                                            {{ trans('titles.showFilters') }}
                                    </button>
                                </h5>
                            </div>
                            
                            <div id="collapseFilters" style="{{ Route::currentRouteName() != 'home' && Route::currentRouteName() != 'public.profile.search' ? 'display:none;' : '' }}"> 
                                
                                <button type="button" id="close-filters-mobile" class="close-filters-mobile"> <img class="w-auto" src="{{ asset('img/close-icon.png') }}" alt="Close"></button>
                                <div class="mt-4">
                                    <div class="border-b pb-2 flex flex-wrap">
                                        {{-- TYPE USER --}}
                                        <div class="w-1/2 sm:w-auto lg:w-2/12 xl:w-2/12 px-0 sm:px-2 border-r border-gray-400">
                                            <label class="text-gray-900 text-sm w-full text-center font-semibold mb-2" for="typeUser"> {{ trans('profile.filters.typeUser') }} </label>    
                                            <div class="pl-2">
                                                <label class="w-full flex items-center mb-0">
                                                    <input name="typeUser" value="{{null}}" class="mr-2 form-radio" type="radio" {{ request()->input('typeUser')==null ? 'checked' : '' }}>
                                                    <span class="text-xs"> {{ trans('titles.allUsers') }}</span>
                                                </label>
                                                <label class="w-full flex items-center mb-0">
                                                    <input name="typeUser" value="3" class="mr-2 form-radio" type="radio" {{ request()->input('typeUser') == '3' ? 'checked' : '' }}>
                                                    <span class="text-xs"> {{ trans('titles.athlete') }}</span>
                                                </label>
                                                <label class="w-full flex items-center mb-0">
                                                    <input name="typeUser" value="2" class="mr-2 form-radio" type="radio" {{ request()->input('typeUser') == '2' ? 'checked' : '' }}>
                                                    <span class="text-xs"> {{ trans('titles.scout') }}</span>
                                                </label>
                                            </div>
                                        </div>
                                        {{-- Age --}}
                                        <div class="w-1/2 sm:w-1/6 lg:w-2/12 xl:w-2/12 px-2 border-r border-gray-400">
                                            <label class="text-gray-900 w-full text-center font-semibold mb-2" for="age"> {{ trans('profile.filters.age') }} </label>
                                            {{-- <div>
                                                <label class="text-age-1 pl-1 mb-0"></label>
                                                <label class="text-age-2 mb-0"></label>
                                            </div> --}}
                                            <div class="range-slider extra-controls">
                                                
                                                <input class="hidden js-input-from w-10" type="text" value="10"/>
                                                <input class="hidden js-input-to w-10" type="text" value="40"/>
                                                
                                                {{-- to keep value after filter --}}
                                                <input id="navbarOldValueAge" value="{{ request()->input('age') }}" type="hidden">

                                                <input type="text" class="js-range-slider" value="" />

                                                {{-- <input id="ageRange1" value="10"  min="8" max="70" step="1" type="range"/> --}}
                                                {{-- <input id="ageRange2" value="40" min="8" max="70" step="1" type="range"/> --}}
                                                {{-- <svg width="100%" height="24">
                                                    <line x1="4" y1="0" x2="300" y2="0" stroke="#fff" stroke-width="2" stroke-dasharray="1 28"></line>
                                                </svg> --}}
                                            </div>
                                            {!! Form::hidden('age', request()->input('age'), ['id' => 'ageNavbar','class' => 'form-control', 'placeholder' => 'Age']) !!}
                                        </div>
                                        
                                        {{-- Gender --}}
                                        <div class="w-1/2 sm:w-auto lg:w-auto px-0 sm:px-2 border-r border-gray-400">
                                            <label class="text-gray-900 w-full text-center font-semibold mb-2" for="gender"> {{ trans('profile.filters.gender') }} </label>
                                            <div class="pl-2">
                                                <label class="w-full flex items-center mb-0">
                                                    <input name="gender" value="" class="mr-2 form-radio" type="radio" {{ request()->input('gender') == '' ? 'checked' : '' }}>
                                                    <span class="text-xs"> {{ trans('titles.all') }}</span>
                                                </label>
                                                @foreach($genders as $val => $gender)
                                                    <label class="w-full flex items-center mb-0">
                                                        <input name="gender" value="{{ $val }}" class="mr-2 form-radio" type="radio" {{ request()->input('gender') == $val ? 'checked' : '' }}>
                                                        <span class="text-xs"> {{ $gender }}</span>
                                                    </label>
                                                @endforeach
                                            </div>
                                            {{-- {!! Form::select('gender', $genders, request()->input('gender'), ['class' => 'form-control', 'placeholder' => 'Gender']) !!} --}}
                                        </div>
                                        {{-- Type Athelte --}}
                                        <div class="w-1/2 sm:w-auto lg:w-3/12 xl:w-2/12 px-2 border-r border-gray-400">
                                            <label class="text-gray-900 w-full text-center font-semibold mb-2" for="status"> {{ trans('profile.filters.typeAthlete') }} </label>
                                            <div class="pl-2">
                                                <label class="w-full flex items-center mb-0">
                                                    <input name="status" value="" class="mr-2 form-radio" type="radio" {{ request()->input('status') == '' ? 'checked' : '' }}>
                                                    <span class="text-xs"> {{ trans('titles.all') }} </span>
                                                </label>
                                                @foreach($statusAthlete as $val => $type)
                                                    <label class="w-full flex items-center mb-0">
                                                        <input name="status" value="{{ $val }}" class="mr-2 form-radio" type="radio" {{ request()->input('status') == $val ? 'checked' : '' }}>
                                                        <span class="text-xs"> {{ $type }}</span>
                                                    </label>
                                                @endforeach
                                            </div>
                                            {{-- {!! Form::select('status', $statusAthlete, request()->input('status'), ['class' => 'form-control', 'placeholder' => 'Type Athlete']) !!} --}}
                                        </div>
                                        {{-- Sporting Goal --}}
                                        <div class="w-full sm:w-auto xl:w-2/12 px-0 pt-2 sm:pt-0 sm:px-2 xl:border-r xl:border-gray-400">
                                            <label class="text-gray-900 w-full text-center font-semibold mb-2" for="sporting_goal"> {{ trans('profile.filters.sportingGoal') }} </label>
                                            <div class="pl-2">
                                                <label class="w-full flex items-center mb-0">
                                                    <input name="sporting_goal" value="" class="mr-2 form-radio" type="radio" {{ request()->input('sporting_goal') == '' ? 'checked' : '' }}>
                                                    <span class="text-xs"> {{ trans('titles.all') }} </span>
                                                </label>
                                                @foreach($sportingGoal as $val => $goal)
                                                    <label class="w-full flex items-center mb-0">
                                                        <input name="sporting_goal" value="{{ $val }}" class="mr-2 form-radio" type="radio" {{ request()->input('sporting_goal') == $val ? 'checked' : '' }}>
                                                        <span class="text-xs"> {{ $goal }}</span>
                                                    </label>
                                                @endforeach
                                            </div>
                                            {{-- {!! Form::select('sporting_goal', $sportingGoal, request()->input('sporting_goal'), ['class' => 'form-control', 'placeholder' => 'Sporting Goal']) !!} --}}
                                        </div>
                                        {{-- Nationality --}}
                                        <div class="mt-2 xl:mt-0 flex-wrap flex w-full xl:flex-1 lg:w-full xl:w-2/12 px-2 pt-1">
                                            {{-- <div id="nav_nationality_value" class="hidden">{{ request()->input('nationality') }}</div> --}}
                                            <div class="mb-2 w-full md:w-1/2 xl:w-full px-1 sm:border-r xl:border-none border-gray-400">
                                                <label class="text-gray-900 w-full text-center font-semibold mb-0" for="nationality"> {{ trans('profile.filters.nationality') }} </label>
                                                {!! Form::select('nationality', $countriesAll, request()->input('nationality'), ['class' => 'mt-2 form-control form-control-sm', 'placeholder' => trans('profile.filters.allNationalities') ]) !!}
                                            </div>
                                            {{-- Residence Country --}}
                                            {{-- <div id="nav_country_value" class="hidden">{{ request()->input('residence_country') }}</div> --}}
                                            <div class="w-full md:w-1/2 xl:w-full px-1">
                                                <label class="text-gray-900 w-full text-center font-semibold mb-0" for="residence_country"> {{ trans('profile.filters.residenceCountry') }} </label>
                                                {!! Form::select('residence_country', $countriesAll, request()->input('residence_country'), ['class' => 'mt-2 form-control form-control-sm', 'placeholder' => trans('profile.filters.allCountries') ]) !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mt-2 flex flex-wrap items-start">
                                        {{-- Scout Speciality --}}
                                        <div class="w-full sm:w-1/2 md:w-1/2 lg:w-3/12 px-2 sm:border-r border-gray-400">
                                            <label class="text-gray-900 w-full text-center font-semibold mb-0" for="entity"> {{ trans('profile.filters.selectSpecialityScout') }}  </label>
                                            {!! Form::select('scout_speciality', $specialitiesScouts, request()->input('scout_speciality'), ['id'=> 'input-navbar-specialities-scout','class' => 'mt-2 form-control form-control-sm','data-placeholder' => trans('profile.filters.specialitiesScout'),'placeholder' => trans('profile.filters.specialitiesScout') ]) !!}
                                        </div>
                                        {{-- Entity --}}
                                        <div class="w-full sm:w-1/2 md:w-1/2 lg:w-2/12 px-2 sm:border-r border-gray-400">
                                            <label class="text-gray-900 w-full text-center font-semibold mb-0" for="entity"> {{ trans('profile.filters.selectEntity') }}  </label>
                                            {!! Form::select('entity', $entitiesAll, request()->input('entity'), ['id' => 'input-navbar-entities','class' => 'mt-2 form-control form-control-sm','data-placeholder' => trans('profile.filters.entities'), 'placeholder' => trans('profile.filters.entities') ]) !!}
                                        </div>
                                        {{-- Sport --}}
                                        <div class="w-full sm:w-1/2 md:w-1/2 lg:w-3/12 xl:w-2/12 px-2 sm:border-r border-gray-400">
                                            <label class="text-gray-900 w-full text-center font-semibold mb-0" for="sport"> {{ trans('profile.filters.sport') }} </label>
                                            <select class="form-control mt-2 form-control-sm" id="sportFilter" name="sport" value="{{ request()->input('sport') }}">
                                                <option value=""> {{ trans('profile.filters.sport') }} </option>
                                                @foreach($sportsAll as $s)
                                                <option value="{{$s->id}}" {{(request()->input('sport') == $s->id) ? 'selected' : '' }} > {{ trans('sportfields.titles.'.$s->name) }} </option>
                                                @endforeach
                                            </select>
                                            {{-- {!! Form::select('sport', $sportsAll, request()->input('sport'), ['id' => 'sportFilter','class' => 'mt-2 form-control form-control-sm', 'placeholder' => 'Sport']) !!} --}}
                                        </div>
                                        {{-- Sport Fields --}}
                                        <div id="sportFieldsWrapper" style="display: contents;" class="hidden flex flex-wrap w-auto px-2 border-r border-gray-400">

                                        </div>
                                        <div class="flex flex-grow w-auto mt-4 px-2">
                                                <input id="input-navbar" class="w-full p-2" value="{{ request()->input('fullname') }}" name="fullname" type="text" placeholder="{{ trans('profile.filters.searchText') }}">
                                                
                                                {{-- {!! Form::select('sport', $sportsAll, request()->input('sport'), ['id'=>'id', 'class' => 'border-r-0 w-auto form-control', 'placeholder' => 'Sport']) !!} --}}
                                                <button type="submit" id="search-button" class="bg-primary">
                                                    <img src="{{ asset('img/search.png') }}" alt="Search">
                                                </button>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        
                        </div>
                        @endrole
                   </div>
                        {!! Form::close() !!}
                    
                        {{-- <a class="bg-white border-primary border btn-logout px-2 md:px-6 py-2 font-semibold text-primary shadow rounded-lg" href="{{ route('logout') }}"
                        onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                            <i class="inline-flex md:hidden fa fa-sign-out" aria-hidden="true"></i>
                            <span class="hidden md:inline-flex"> {{ __('Logout') }} </span>
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form> --}}
                @endguest
            </ul>
        </div>
    </div>
</nav>

@section('footer_scripts')
@include('scripts.range-slider')
    <script type="text/javascript">

        /* Collapse and Open sidebar MOBILE */ 
        document.getElementById('sidebarCollapse').addEventListener('click', function () {
            $('.sidebar').toggleClass('small');
            $('.wrapper-content').toggleClass('large');
        });


        let showText = "@lang('titles.showFilters')";
        let hideText = "@lang('titles.hideFilters')";
        let btnCollapseFilters = document.getElementsByClassName('btn-collapse-filters'); 
        if(btnCollapseFilters.length > 0) {
            btnCollapseFilters[0].addEventListener('click',function() {
                if($('#collapseFilters').is( ":hidden" )) {
                    $('#collapseFilters').slideDown(350);
                    $('.btn-collapse-filters').removeClass('collapsed').html(`${hideText}  <i class=""></i>`);
                } else {
                    $('#collapseFilters').slideUp(350);    
                    $('.btn-collapse-filters').addClass('collapsed').html(`${showText}  <i class=""></i>`);
                }
            });
        }
        if(btnCollapseFilters.length > 1) {
            btnCollapseFilters[1].addEventListener('click',function() {
                if($('#collapseFilters').is( ":hidden" )) {
                    $('#collapseFilters').slideDown(350);
                    $('.btn-collapse-filters').removeClass('collapsed').html(`${hideText}  <i class=""></i>`);
                } else {
                    $('#collapseFilters').slideUp(350);    
                    $('.btn-collapse-filters').addClass('collapsed').html(`${showText}  <i class=""></i>`);
                }
            });
        }

        // Close filters mobile
        let closeFilterMobile = document.getElementById('close-filters-mobile');
        if(closeFilterMobile != null) {
            closeFilterMobile.addEventListener('click',function() {
                $('#collapseFilters').slideUp(350);
                $('.btn-collapse-filters').addClass('collapsed').html(`${showText}  <i class=""></i>`);
            });
        }

        $('#input-navbar').bind('keypress',function(event) {
            // event.preventDefault();
            if(event.which == 13) {
                // $('#formSearchNavbar').collapse('show');
                $('#formSearchNavbar').submit();
            }
        });


        // SPORTS
        (function() {

            let sports = [];
            function createSports() {
                let fields = [];
                let s = [];
                @foreach($sportsAll as $s)
                    // if($s->id == sportSelected) {
                    //     console.log($s);
                    // }
                    fields = {!! json_encode($s->fields) !!};
                    // console.log({!! json_encode($s->fields) !!});
                    s["{{$s->id}}"] = ["{{$s->id}}","{{$s->name}}",fields]
                    // console.log(fields);
                    sports = s; 
                @endforeach

                showFieldsSportSelected();
            };

            // Show fields Sport Selected
            var urlParams = new URLSearchParams(window.location.search);

            function showFieldsSportSelected() {
                if($('#sportFilter').length > 0) {
                    let sportSelected = $('#sportFilter').val(); 
                    if(sportSelected != '') {
                    let fields = sports[`${sportSelected}`][2];
                    //  console.log(fields);
                    if(fields.length > 0) {
                        let html = "";
                        fields.forEach((f,j) => {
                            if(f.fieldtype == 'select') {
                                let options = f.options_select;
                                // console.log(options);
                                let fieldname = window.lang == 'en' ? f.name.en : f.name.es;
                                let fieldnameFilter = fieldname.replace(/ /g, "_");
                                var urlParam = urlParams.get(`sport_${fieldnameFilter}_${f.id}`);
                                // console.log(urlParam);

                                html += `<div class="w-auto px-2 ${ (j+1 == fields.length) ? 'border-r' : 'border-r border-primary' }">`;
                                html += `<label class="text-gray-900 font-thin mb-0"> ${fieldname} </label>`;
                                html += `<div class="flex flex-wrap">`;
                                let optionsLength = options.length;
                                html += `<div class="pl-2">`;
                                        html += `<label class="w-full flex items-center mb-0">
                                                    <input name="sport_${fieldnameFilter}_${f.id}" value="" class="mr-2 form-radio" type="radio" ${ urlParam == '' ? 'checked' : ''}>
                                                    <span class="text-xs" > @lang('titles.all') </span>
                                                </label>`;
                                    options.forEach((o,i) => {
                                        let optionname = window.lang == 'en' ? o.name.en : o.name.es;
                                        html += `<label class="w-full flex items-center mb-0">`;
                                        html += `<input name="sport_${fieldnameFilter}_${f.id}" value="${o.name.en}" class="mr-2 form-radio" type="radio" ${ urlParam == o.name.en ? 'checked' : ''}>`;
                                        html += `<span class="text-xs" > ${optionname} </span>`;
                                        html += `</label>`
                                        if((i+1)%5 == 0) {
                                            html += '</div><div class="pl-2">'
                                        }
                                    });
                                html += `</div>`;
                                html += "</div>";
                                html += '</div>';

                            }
                        });
                        $('#sportFieldsWrapper').html(html);   
                        // console.log(fields);
                        $('#sportFieldsWrapper').removeClass('hidden');
                    } else {
                        $('#sportFieldsWrapper').html("").addClass('hidden');
                    }
                    //  let sport = null;
                    //  console.log(sports);
                    } else {
                        $('#sportFieldsWrapper').html("").addClass('hidden');
                    }
                }
            };


            
            createSports();
            $('#sportFilter').bind('change',function() {
                showFieldsSportSelected();
            });

            $("#input-navbar-entities").chosen({ width: "100%" });
            $("#input-navbar-specialities-scout").chosen({ width: "100%" });

        })();

        $('#formSearchNavbar').submit(function(event) {
            // event.stopPropagation();
            // event.preventDefault();
            let from = $('.js-input-from').val();
            let to = $('.js-input-to').val();
            if(from && to) {
                $('#ageNavbar').val(from +"-"+to);
            }
        });

        

    </script>
    @include('scripts.gtag-events')
@append