<div class="pt-4 sidebar shadow-sm min-h-screen">
    <div class="h-full pb-20 overflow-y-scroll">
        {{-- <button type="button" id="sidebarCollapse" class="btn-collapse-sidebar hidden md:block btn btn-primary rounded-full"></button> --}}
        <div class="flex flex-col justify-center items-center px-10">
            <a href="{{ route('welcome') }}"><div class="logo"> </div> </a>
            <div class="border-b-2 border-primary w-full h-2 pt-4"></div>
        </div>
    <div class="mt-6 px-auto"> 
        
        <ul class="menu">
            <li>
                <!-- Home -->
                <a class="flex {{ strpos(Request::url(),'home') ? 'active' : '' }}" href="{{ route('home') }}">  
                    <span class="img-sidebar img-dashboard"></span>
                    <span class="item-title pl-4"> {{ trans('sidebar.dashboard') }} </span>
                </a>
                <!-- Profile -->
                <a class="flex {{ (strpos(Request::url(),'profile') && !strpos(Request::url(),'p/profile')) ? 'active' : '' }}" href="{{ route('profile.index') }}">  
                    <span class="img-sidebar img-myprofile"></span> 
                    <span class="item-title pl-4"> {{ trans('sidebar.myprofile') }} </span>
                </a>
                
                <!-- Messages -->
                <a class="flex {{ strpos(Request::url(),'messages') ? 'active' : '' }} justify-between items-center" href="{{ route('messages.index') }}">  
                    <span class="flex">    
                        <span class="img-sidebar img-messages"></span>  
                        <span class="item-title pl-4"> {{ trans('sidebar.messages') }} </span>
                    </span>
                    {{-- @if(isset(Auth::user()) --}}
                    {{-- <span v-if="userV && userV.messagesUnread > 0"> {{ userV.messagesUnread }} </span> --}}
                        <span class="@if(Auth::user()->newThreadsCount() == 0) hidden @endif pull-right font-semibold count-messages"> {{ \Auth::user()->newThreadsCount() }} </span>
                    {{-- @endif --}}
                </a>

               {{-- Recruitments --}}
                @permission('add.recruitments')
                <a class="flex {{ strpos(Request::url(),'recruitments') ? 'active' : '' }}" href="{{ route('recruitments.index') }}">  
                    <span class="img-sidebar img-recruitments"></span>
                    <span class="item-title pl-4"> {{ trans('sidebar.recruitments') }} </span>
                </a>
                @endpermission
                {{-- Followed --}}
                @permission('add.followed')
                <a class="flex {{ strpos(Request::url(),'followed') ? 'active' : '' }}" href="{{ route('followed.index') }}">  
                    <span class="img-sidebar img-followed"></span>
                    <span class="item-title pl-4"> {{ trans('sidebar.followed') }} </span>
                </a>
                @endpermission
                {{-- Favorites --}}
                @permission('add.favorites')
                <a class="flex {{ strpos(Request::url(),'favorites') ? 'active' : '' }}" href="{{ route('favorites.index') }}">  
                    {{-- <img src="{{ asset('img/icons/favorites.svg') }}" alt="Favorites">  --}}
                    <span class="img-sidebar img-favorites"></span>
                    <span class="item-title pl-4"> {{ trans('sidebar.favorites') }} </span>
                </a>
                @endpermission
                {{-- Posts --}}
                @permission('publish.posts')
                <a class="flex {{ strpos(Request::url(),'posts') ? 'active' : '' }}" href="{{ route('posts.index') }}">  
                    <span class="img-sidebar img-myposts"></span>
                    {{-- <img src="{{ asset('img/icons/box.png') }}" alt="Home">  --}}
                    <span class="item-title pl-4"> {{ trans('sidebar.posts') }} </span>
                </a>
                @endpermission        

                <!-- Notifications -->
                <a class="flex {{ strpos(Request::url(),'notifications') ? 'active' : '' }} justify-between items-center" href="{{ route('notifications.index') }}">  
                    <span class="flex">
                        <span class="img-sidebar img-notifications"></span> 
                        <span class="item-title pl-4"> {{ trans('sidebar.notifications') }} </span>
                    </span>
                    <span class="@if(Auth::user()->unreadNotifications()->count() == 0) hidden @endif pull-right font-semibold count-notifications"> {{ Auth::user()->unreadNotifications()->count() }} </span> 
                </a>
                @role('athlete')
                    <a class="flex {{ strpos(Request::url(),'mySubscriptions') ? 'active' : '' }}" href="{{ route('mysubcriptions.index') }}">
                        <span class="img-sidebar img-plans"></span> 
                        <span class="item-title pl-4"> {{ trans('titles.planes') }} </span>
                    </a>

                @endrole

                @role('athlete|scout')
                    {{-- Share profile --}}
                    <button class="flex w-full" type="button" data-toggle="modal" data-target="#modalShareSocialMedia"> 
                            <span class="img-sidebar img-showcase"></span> 
                            <span class="item-title pl-4"> {{ trans('sidebar.shareSocialMedia') }} </span> 
                    </button>
                @endrole

                @role('admin')
                <!-- List Users -->
                <a class="flex {{ strpos(Request::url(),'users') ? 'active' : '' }}" href="{{ url('/users') }}">
                    <span class="img-sidebar img-users"></span> 
                    <span class="item-title pl-4"> {{ trans('titles.adminUserList') }} </span>
                </a>
                <!-- Popular Users -->
                <a class="flex {{ strpos(Request::url(),'popularUsers') ? 'active' : '' }}" href="{{ url('/popularUsers') }}">
                    <span class="img-sidebar img-users"></span> 
                    <span class="item-title pl-4"> {{ trans('titles.popularUsers') }} </span>
                </a>
                <!-- Clubes -->
                <a class="flex {{ strpos(Request::url(),'clubes') ? 'active' : '' }}" href="{{ url('/clubes') }}">
                    <span class="img-sidebar img-clubes"></span> 
                    <span class="item-title pl-4"> {{ trans('titles.adminClubesList') }} </span>
                </a>
                <!-- Sports -->
                <a class="flex {{ strpos(Request::url(),'sports') ? 'active' : '' }}" href="{{ url('/sports') }}">
                    <span class="img-sidebar img-sports"></span> 
                    <span class="item-title pl-4"> {{ trans('titles.adminSportsList') }} </span>
                </a>
                <!-- Articles -->
                <a class="flex {{ strpos(Request::url(),'articles') ? 'active' : '' }}" href="{{ url('/articles') }}">
                    <span class="img-sidebar img-articles"></span> 
                    <span class="item-title pl-4"> {{ trans('titles.articles') }} </span>
                </a>
                <!-- Publicidad -->
                <a class="flex {{ strpos(Request::url(),'publicidad') ? 'active' : '' }}" href="{{ route('publicidad.index') }}">  
                    {{-- <img src="{{ asset('img/icons/config.png') }}" alt="Home">  --}}
                    <span class="img-sidebar img-publicities"></span> 
                    <span class="item-title pl-4"> {{ trans('sidebar.publicities') }} </span>
                </a>
                <!-- Faqs -->
                <a class="flex {{ strpos(Request::url(),'faqs') ? 'active' : '' }}" href="{{ route('faqs.index') }}">  
                    <span class="img-sidebar img-faqs"></span> 
                    <span class="item-title pl-4"> {{ trans('sidebar.faqs') }} </span>
                </a>
                <!-- Config -->
                <a class="flex {{ strpos(Request::url(),'config') ? 'active' : '' }}" href="{{ route('config.index') }}">  
                    {{-- <img src="{{ asset('img/icons/config.png') }}" alt="Home">  --}}
                    <span class="img-sidebar img-config"></span> 
                    <span class="item-title pl-4"> {{ trans('sidebar.config') }} </span>
                </a>
                {{-- Plans --}}
                <a class="flex {{ strpos(Request::url(),'admin/planes') ? 'active' : '' }}" href="{{ route('admin.planes.index') }}">
                    <span class="img-sidebar img-plans"></span> 
                    <span class="item-title pl-4"> {{ trans('titles.planesAdmin') }} </span>
                </a>
                {{-- Coupons --}}
                <a class="flex {{ strpos(Request::url(),'coupons') ? 'active' : '' }}" href="{{ url('/coupons') }}">
                    <span class="img-sidebar img-coupons"></span> 
                    <span class="item-title pl-4"> {{ trans('titles.coupons') }} </span>
                </a>
                {{-- Subscriptions --}}
                <a class="flex {{ strpos(Request::url(),'subscriptions') ? 'active' : '' }}" href="{{ url('/subscriptions') }}">
                    <span class="img-sidebar img-subscriptions"></span> 
                    <span class="item-title pl-4"> {{ trans('titles.subscriptions') }} </span>
                </a>
                {{-- Showcase --}}
                <a class="flex {{ strpos(Request::url(),'showcase') ? 'active' : '' }}" href="{{ url('/showcase') }}">
                    <span class="img-sidebar img-showcase"></span> 
                    <span class="item-title pl-4"> {{ trans('titles.showcase') }} </span>
                </a>
                {{-- Reports --}}
                <a class="flex {{ strpos(Request::url(),'admin/report') ? 'active' : '' }}" href="{{ route('admin.reports.index') }}">
                    <span class="img-sidebar img-reports"></span> 
                    <span class="item-title pl-4"> {{ trans('sidebar.reports') }} </span>
                </a>
                @endrole
                {{-- <a class="flex" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                    <span class="pl-4"> {{ __('Logout') }} </span>
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form> --}}
                
                @if(app()->getLocale() == 'en')
                    <a class="flex" href="{{ url('locale/es') }}" >
                        {{-- <i class="fa fa-language"></i> --}}
                        <span class="item-title font-semibold pl-4"> {{ trans('sidebar.changeLanguageToSpanish') }} </span>
                    </a>
                @else
                    <a class="flex" href="{{ url('locale/en') }}" >
                        {{-- <i class="fa fa-language"></i> --}}
                        <span class="item-title font-semibold pl-4"> {{ trans('sidebar.changeLanguageToEnglish') }} </span>
                    </a>
                @endif
                    
            </li>
        </ul>
    </div>
    <!-- Profile -->
    <div class="mt-12">
        <div class="w-full text-center flex flex-col">
            <div class="relative w-auto block mx-auto">
                <img class="user-avatar-photo" src="{{ Auth::user()->avatar }}" alt="{{ Auth::user()->fullName }}">
                @if(Auth::user()->avatarSport)
                    <img class="sport-md" src="{{ Auth::user()->avatarSport }}" alt="{{ Auth::user()->fullName }}">
                @endif
            </div>    
            <div class="mt-4 mb-2 text-center text-sm name-user text-uppercase">
                <span class="font-semibold"> {{ Auth::user()->fullName }} </span>
                @if(Auth::user()->isPremiun())
                    @if(in_array(Auth::user()->roles->last()->name,array('Athlete Premiun Semester','Athlete Premiun Anual','Athlete Premiun')))
                        <div class="flex justify-center">
                            <span class="mt-2 badge badge-success text-white text-sm p-2 rounded-lg"> {{ trans('profile.athlete.labelPremiun') }} </span>
                        </div>
                    @endif
                @else
                    <div> 
                        ( {{ trans('titles.'. Auth::user()->profileText) }} ) 
                    </div>
                @endif
            </div>

            @if( $verifyScoutsClubs && $verifyScoutsClubs->value ) <!-- Table config_site -->
                @role('scout')
                <div>
                    @if(!Auth::user()->verified)
                        <div class="badge badge-danger text-white text-sm px-2 py-1 rounded-lg">
                            {{ trans('sidebar.userNotVerified') }}
                        </div>
                    @else
                        <div class="badge badge-success text-white text-sm px-2 py-1 rounded-lg">
                            {{ trans('sidebar.userVerified') }}
                        </div>
                    @endif
                </div>
                @endrole
            @endif
            
            {{-- @role('athlete|scout') --}}
            {{-- VISITS PROFILE --}}
            @if((Auth::user()->hasRole('athlete') && Auth::user()->isPremiun()) || Auth::user()->hasRole('scout'))
                <div class="statics">
                    <div class="flex justify-between">
                        {{-- <div class="title-statics"> {{ trans('sidebar.visits.profile') }} </div> --}}
                        <button type="button" data-toggle="modal" data-target="#modalVisitsProfile" class="title-statics hover:underline"> {{ trans('sidebar.visits.profile') }} </button>
                        <div class="count-statics font-semibold"> {{ Auth::user()->visited_count() }} </div>
                    </div>
                    <div class="flex justify-between mt-1">
                        <button type="button" data-toggle="modal" data-target="#modalScoutVisitedProfile" class="title-statics hover:underline"> {{ trans('sidebar.visits.scouts') }} </button>
                        <div class="count-statics font-semibold"> {{ Auth::user()->visited_scout_count() }} </div>
                    </div>
                    <div class="flex justify-between mt-1">
                        <button type="button" data-toggle="modal" data-target="#modalFollowersProfile" class="title-statics hover:underline"> {{ trans('sidebar.followers') }} </button>
                        <div class="count-statics font-semibold"> {{ Auth::user()->followers->count() }} </div>
                    </div>
                    <div class="flex justify-between mt-1">
                        <button type="button" data-toggle="modal" data-target="#modalFollowingProfile" class="title-statics hover:underline"> {{ trans('sidebar.followed') }} </button>
                        <div class="count-statics font-semibold"> {{ Auth::user()->followed->count() }} </div>
                    </div>
                </div>
            @elseif(Auth::user()->hasRole('athlete') && !Auth::user()->isPremiun())
                <div class="statics">
                    <div class="flex justify-between">
                        {{-- <div class="title-statics"> {{ trans('sidebar.visits.profile') }} </div> --}}
                        <div class="title-statics"> {{ trans('sidebar.visits.profile') }} </div>
                        <div class="count-statics font-semibold"> {{ Auth::user()->visited_count() }} </div>
                    </div>
                    <div class="flex justify-between mt-1">
                        <div class="title-statics"> {{ trans('sidebar.visits.scouts') }} </div>
                        <div class="count-statics font-semibold"> {{ Auth::user()->visited_scout_count() }} </div>
                    </div>
                    <div class="flex justify-between mt-1">
                        <div class="title-statics"> {{ trans('sidebar.followers') }} </div>
                        <div class="count-statics font-semibold"> {{ Auth::user()->followers->count() }} </div>
                    </div>
                    <div class="flex justify-between mt-1">
                        <div class="title-statics"> {{ trans('sidebar.followed') }} </div>
                        <div class="count-statics font-semibold"> {{ Auth::user()->followed->count() }} </div>
                    </div>
                </div>

            @endif

            {{-- LOGOUT --}}
            <div class="flex justify-center mt-4">
                <a class="bg-white border-primary border btn-logout px-2 md:px-4 py-1 font-semibold text-primary shadow-lg rounded-lg" href="{{ route('logout') }}"
                        onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                    <i class="inline-flex md:hidden fa fa-sign-out" aria-hidden="true"></i>
                    <span class="inline-flex"> {{ __('Logout') }} </span>
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </div>

            @role('athlete|scout')
            <div class="pt-4">
                <a class="text-black text-sm" href="{{ route('conditions') }}"> {{ trans('sidebar.termsAndConditions') }} </a>
            </div>
            <div class="pt-1">
                <a class="text-black text-sm" href="{{ route('privacy') }}"> {{ trans('sidebar.privacyPolicy') }} </a>
            </div>
            <div class="pt-1">
                <a class="text-black text-sm" href="{{ route('messages.admin') }}"> {{ trans('sidebar.contactAdmin') }} </a>
            </div>
            @endrole
        </div>
    </div>
    <div id="sidebar-close-icon" class="sidebar-close-icon">
        <img src="{{ asset('img/close-icon.png') }}" alt="Close Sidebar">
    </div>
</div>
<div class="sidebar-out">

</div>
</div>

{{-- @include('modals.modal-scout-visited-profile') --}}
@include('modals.modal-users',['id' => 'modalScoutVisitedProfile','title' => trans('profile.athlete.scoutVisits'),'followersUsers' => \Auth::user()->visited_scout()])
@include('modals.modal-users',['id' => 'modalFollowersProfile','title' => trans('profile.athlete.followers'),'followersUsers' => \Auth::user()->followers_users()])
@include('modals.modal-users',['id' => 'modalFollowingProfile','title' => trans('profile.athlete.following'),'followersUsers' => \Auth::user()->followed_users()])
@include('modals.modal-users',['id' => 'modalVisitsProfile','title' => trans('profile.athlete.visits'),'followersUsers' => \Auth::user()->visits()])
{{-- Modal to share profile on social media --}}
@include('modals.modal-share-socialmedia')

@section('footer_scripts')
    <script type="text/javascript">
        
        document.getElementById('sidebar-close-icon').addEventListener('click',function() {
            $('.sidebar').removeClass('small');
            $('.wrapper-content').removeClass('large');
        });

        @foreach($threadsSidebar as $t)
        // console.log("{{ $t->id }}");
        Echo.private("thread.{{ $t->id }}").listen("MessagePushed", e => {
            // console.log("LISTENED");
            let cant = isNaN(parseInt($('.sidebar .count-messages').html())) ? 1 : parseInt($('.sidebar .count-messages').html()) + 1;
            // console.log(cant);
            $('.sidebar .count-messages').html(cant);
            $('.sidebar .count-messages').removeClass('hidden');    
        });
        @endforeach

        // console.log("users.{{ Auth::user()->id }}");
        // Echo.private("users.{{ Auth::user()->id }}")
        //     .notification((notification) => {
        //         console.log(notification.type);
        //     });

            Echo.private("user.notifications.{{ Auth::user()->id }}").listen("NewNotification", (e) => {
                // console.log(e);
                let cant = isNaN(parseInt($('.sidebar .count-notifications').html())) ? 1 : parseInt($('.sidebar .count-notifications').html()) + 1;
                $('.sidebar .count-notifications').html(cant);
                $('.sidebar .count-notifications').removeClass('hidden');
            });
    </script>
@append