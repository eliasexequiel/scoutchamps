@extends('layouts.app')

@section('template_title')
    {{ trans('profile.templateTitle') }}
@endsection

@section('content')

            {{-- ALERTS  WITH JSON --}}
            <div id="edit-athlete-success" class="hidden mx-6 alert alert-success alert-dismissable fade show" role="alert">
                    <a href="#" class="close" aria-label="close">&times;</a>
                    <span id="message"></span>
            </div>
            {{-- ERRORS --}}
            <div id="edit-athlete-error" class="hidden mx-6 alert alert-danger alert-dismissable fade show" role="alert">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <h5>
                    <i class="icon fa fa-warning fa-fw" aria-hidden="true"></i>
                    {{ Lang::get('auth.someProblems') }}
                    </h5>
                    <ul class="pl-8 text-sm">
                    
                    </ul>
            </div>
            {{--  --}}


        <div class="w-full px-6 py-2">
            <div class="w-full">
                        @if ($user->profile)
                            @if (Auth::user()->id == $user->id)
                            <div class="">
                                <div class="row">
                                    <div class="col-12 col-sm-12 col-md-3 profile-sidebar text-white">
                                        <a class="btn btn-outline-primary rounded-lg bg-white flex justify-center items-center btn-block mb-4" href="{{ Auth::user()->publicLink }}">
                                            <img class="mr-2" src="{{ asset('img/icons/my-profile.png') }}" alt="{{trans('profile.publicPofile')}}">
                                            {{ trans('profile.publicPofile') }}
                                        </a>
                                        
                                        <div class="nav flex-row md:flex-col nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                            <a class="nav-link @if(!session()->has('changeTabProfile')) active @endif" data-toggle="pill" href=".edit-profile-tab" role="tab" aria-controls="edit-profile-tab" aria-selected="true">
                                                <img src="{{ asset('img/profile/accountSettings.png') }}" alt="Account Settings">
                                                <span class="text"> {{ trans('profile.editProfileTitle') }} </span>
                                                <span class="number">1</span>
                                            </a>
                                            {{-- <a class="nav-link " data-toggle="pill" href=".edit-settings-tab" role="tab" aria-controls="edit-settings-tab" aria-selected="false">
                                                {{ trans('profile.editAccountTitle') }}
                                            </a> --}}
                                            <a class="nav-link @if(session()->has('changeTabProfile') && session('changeTabProfile') == '1') active @endif" data-toggle="pill" href=".edit-account-tab" role="tab" aria-controls="edit-settings-tab" aria-selected="false">
                                                    <img src="{{ asset('img/profile/accountAdministration.png') }}" alt="Account Administration">
                                                    <span class="text"> {{ trans('profile.editAccountAdminTitle') }} </span>
                                                    <span class="number">2</span>
                                            </a>
                                            <a class="nav-link @if(session()->has('changeTabProfile') && session('changeTabProfile') == '2') active @endif" data-toggle="pill" href=".media-files-tab" role="tab" aria-controls="media-files-tab" aria-selected="false">
                                                    <img src="{{ asset('img/profile/multimedia.png') }}" alt="Multimedia">  
                                                    <span class="text"> {{ trans('profile.editMedia') }} </span>
                                                    <span class="number">3</span>
                                            </a>
                                            <a class="nav-link" data-toggle="pill" href=".sports-files-tab" role="tab" aria-controls="sports-files-tab" aria-selected="false">
                                                    <img src="{{ asset('img/profile/sport.png') }}" alt="Sports">  
                                                    <span class="text"> {{ trans('profile.sports') }} </span>
                                                    <span class="number">4</span>
                                            </a>
                                            <a class="nav-link" data-toggle="pill" href=".clubs-tab" role="tab" aria-controls="clubs-tab" aria-selected="false">
                                                    <img src="{{ asset('img/profile/clubs.png') }}" alt="Clubs">    
                                                    <span class="text"> {{ trans('profile.editAthleticCarrer') }} </span>
                                                    <span class="number">5</span>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="mt-2 md:mt-0 col-12 col-sm-12 col-md-9 px-0 md:pl-4">
                                        <div class="mb-4 text-primary text-base font-semibold bg-white py-4 px-2 text-center rounded-lg shadow-lg">
                                            {{ trans('profile.completeAllSteps') }}
                                        </div>
                                        <div class="tab-content border-primary bg-white rounded-lg shadow-lg overflow-hidden" id="v-pills-tabContent">
                                            <!-- PROFILE -->
                                            <div class="py-4 tab-pane fade edit-profile-tab @if(!session()->has('changeTabProfile')) show active @endif" role="tabpanel" aria-labelledby="edit-profile-tab">
                                                <div class="flex flex-wrap">
                                                <div class="flex w-full md:w-full lg:w-1/3 justify-center mb-1">
                                                    <div class="w-auto">
                                                        <div id="avatar_container">
                                                                <div class="card-body">
                                                                    <div class="dz-preview"></div>
                                                                    {!! Form::open(array('route' => 'avatar.upload', 'method' => 'POST', 'name' => 'avatarDropzone','id' => 'avatarDropzone', 'class' => 'form single-dropzone dropzone single', 'files' => true)) !!}
                                                                        <img id="user_selected_avatar" class="user-avatar" src="{{ $user->avatar }}" alt="{{ $user->fullName }}">
                                                                    {!! Form::close() !!}

                                                                    <button class="px-2 flex items-center text-primary text-sm py-1 mx-auto" id="remove-avatar"> 
                                                                        <img src="{{ asset('img/profile/trash.png') }}" alt="Delete">
                                                                        <span class="pl-1">{{ trans('forms.remove-file') }} </span>
                                                                    </button>
                                                                    <div class="photoProfileError hidden mt-1 px-2 py-1 alert alert-danger alert-dismissable text-xs sm:text-sm rounded flex justify-center">
                                                                    </div>
                                                                    <div class="mt-1 px-2 py-1 bg-gray-200 text-xs sm:text-sm border border-gray-500 rounded flex items-center">
                                                                        <i class="fa fa-info-circle"></i>
                                                                        <span class="pl-2">{{ trans('profile.athlete.uploadPhotoProfileMaxSize') }} </span>
                                                                    </div>
                                                                </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="w-full md:w-full lg:w-2/3">
                                                {!! Form::model($user->profile, ['method' => 'PUT', 'route' => ['profile.update', $user->id], 'id' => 'user_profile_form', 'class' => 'form-horizontal', 'role' => 'form', 'enctype' => 'multipart/form-data']) !!}
                                                    {{ csrf_field() }}
                                                    
                                                        {{-- Email --}}
                                                        <div class="form-group has-feedback {{ $errors->has('email') ? ' has-error ' : '' }}">
                                                            {!! Form::label('email', trans('forms.create_user_label_email'), array('class' => 'col-12 control-label')); !!}
                                                            <div class="col-12">
                                                                {!! Form::text('email', $user->email, array('id' => 'email', 'class' => 'form-control', 'placeholder' => trans('forms.create_user_ph_email'))) !!}
                                                            </div>
                                                        </div>

                                                        {{-- Phone --}}
                                                        <div class="form-group has-feedback {{ $errors->has('phone') ? ' has-error ' : '' }}">
                                                            {!! Form::label('phone', trans('profile.label-phone') , array('class' => 'col-12 control-label')); !!}
                                                            <div class="col-12">
                                                                <div class="flex flex-wrap items-stretch mb-4 relative w-full">
                                                                    <div style="height:2.3rem" class="absolute text-lg text-gray-700 left-0 ml-2 flex items-center">
                                                                        <span> + </span>
                                                                    </div>
                                                                    {!! Form::text('phone', old('phone'), array('class' => 'pl-6 form-control','autocomplete' => 'off')) !!}
                                                                </div>
                                                            </div>
                                                        </div>
    
                                                        <div class="row col-12 px-0">
                                                            <!-- FirstName -->
                                                            <div class="col-12 col-md-6 px-0 form-group has-feedback {{ $errors->has('firstname') ? ' has-error ' : '' }}">
                                                                {!! Form::label('firstname', trans('forms.create_user_label_firstname'), array('class' => 'col-12 control-label')); !!}
                                                                <div class="col-12">
                                                                    {!! Form::text('firstname', $user->firstname, array('id' => 'firstname', 'class' => 'form-control', 'placeholder' => trans('forms.create_user_ph_firstname'))) !!}
                                                                </div>
                                                            </div>
        
                                                            <!-- Lastname -->
                                                            <div class="col-12 col-md-6 px-0 form-group has-feedback {{ $errors->has('lastname') ? ' has-error ' : '' }}">
                                                                {!! Form::label('lastname', trans('forms.create_user_label_lastname'), array('class' => 'col-12 control-label')); !!}
                                                                <div class="col-12">
                                                                    {!! Form::text('lastname', $user->lastname, array('id' => 'lastname', 'class' => 'form-control', 'placeholder' => trans('forms.create_user_ph_lastname'))) !!}
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <!-- AboutMe -->
                                                        <div class="form-group has-feedback {{ $errors->has('lastname') ? ' has-error ' : '' }}">
                                                            {!! Form::label('about', trans('forms.aboutme'), array('class' => 'col-12 control-label')); !!}
                                                            <div class="col-12">
                                                                {!! Form::textarea('about', $user->about, array('id' => 'about', 'class' => 'form-control','rows' => 3, 'placeholder' => trans('forms.aboutme'))) !!}
                                                            </div>
                                                        </div>

                                                        {{-- Nationality --}}
                                                        <div class="form-group has-feedback">
                                                                {!! Form::label('nationality_id', trans('profile.label-nationality') , array('class' => 'col-12 control-label')); !!}
                                                                <div class="col-12">
                                                                    {!! Form::select('nationality_id',$countries, old('nationality_id'), array('id' => 'nationality', 'class' => 'form-control','placeholder' => trans('profile.label-select'))) !!}
                                                                </div>
                                                        </div>

                                                        {{-- Country residence --}}
                                                        <div class="form-group has-feedback">
                                                                {!! Form::label('residence_country_id', trans('profile.label-residence-country') , array('class' => 'col-12 control-label')); !!}
                                                                <div class="col-12">
                                                                    {!! Form::select('residence_country_id',$countries, old('residence_country_id'), array('id' => 'residence_country', 'class' => 'form-control','placeholder' => trans('profile.label-select'))) !!}
                                                                </div>
                                                        </div>

                                                        <div class="row col-12 px-0">
                                                            {{-- Speciality --}}
                                                            <div class="col-12 col-md-6 px-0 form-group has-feedback">
                                                                    {!! Form::label('scout_position_id', trans('profile.label-cargo-scout') , array('class' => 'col-12 control-label')); !!}
                                                                    <div class="col-12">
                                                                        {!! Form::select('scout_position_id',$cargosScout, old('scout_position_id'), array('id' => 'scout_position_id', 'class' => 'form-control','placeholder' => trans('profile.label-select'))) !!}
                                                                    </div>
                                                            </div>
                                                            {{-- Secondary Speciality --}}
                                                            <div class="col-12 col-md-6 px-0 form-group has-feedback">
                                                                    {!! Form::label('scout_position_secondary_id', trans('profile.label-cargo-secundario-scout') , array('class' => 'col-12 control-label')); !!}
                                                                    <div class="col-12">
                                                                        {!! Form::select('scout_position_secondary_id',$cargosScout, old('scout_position_secondary_id'), array('id' => 'scout_position_secondary_id', 'class' => 'form-control','placeholder' => trans('profile.label-select'))) !!}
                                                                    </div>
                                                            </div>
                                                        </div>

                                                    <div class="form-group m-0">
                                                        <div class="px-4 flex justify-end items-center">
                                                                {!! Form::button('<i class="fa fa-fw fa-save" aria-hidden="true"></i> ' . trans('profile.submitProfileButton'), array( 'class' => 'mr-2 btn btn-success rounded-full','type' => 'submit','disabled' => false)) !!}
                                                                <a class="text-primary" href="{{ route('profile.index') }}"> {{ trans('forms.cancel') }} </a>
                                                        </div>
                                                    </div>
                                                {!! Form::close() !!}
                                                </div>
                                                </div>
                                            </div>

                                            
                                            <!-- Password Account -->
                                            @include('profiles.passwordAccount')

                                            <!-- Media -->
                                            <div class="py-4 tab-pane fade media-files-tab @if(session()->has('changeTabProfile') && session('changeTabProfile') == '2') show active @endif" role="tabpanel" aria-labelledby="media-files-tab">
                                                <div class="w-full px-4">
                                                    <user-images :user="{{ Auth::user() }}"></user-images>
                                                    <scout-videos :user="{{ Auth::user() }}"></scout-videos>
                                                </div>
                                            </div>

                                            <!-- SPORTS -->
                                            <div class="py-4 tab-pane fade sports-files-tab" role="tabpanel" aria-labelledby="sports-files-tab">
                                                    <div class="w-full px-4">
                                                            <scout-sport :user="{{ Auth::user() }}"></scout-sport>
                                                    </div> 
                                                </div>

                                            <!-- CLUBS -->
                                            <div class="tab-pane fade clubs-tab" role="tabpanel" aria-labelledby="clubs-tab">
                                                <div class="w-full"> 
                                                        <index-scout-clubs></index-scout-clubs>   
                                                </div> 
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            @else
                                <p>{{ trans('profile.notYourProfile') }}</p>
                            @endif
                        @else
                            <p>{{ trans('profile.noProfileYet') }}</p>
                        @endif

                </div>
                
            
        </div>
    

    @include('modals.modal-form')

@endsection

@section('footer_scripts')

    <link href="{{ asset('/assets/summernote/dist/summernote.css') }}" rel="stylesheet">
    <link href="{{ asset('/assets/summernote/dist/summernote-bs4.css') }}" rel="stylesheet">
    <script src="{{ asset('assets/summernote/dist/summernote.min.js') }}"></script>
    <script src="{{ asset('assets/summernote/dist/summernote-bs4.min.js') }}"></script>

    @include('scripts.form-modal-script')


    @include('scripts.user-avatar-dz')

    <script type="text/javascript">

        $('#about').summernote({
            height: 100,
            toolbar: false
        });

        $('.dropdown-menu li a').click(function() {
            $('.dropdown-menu li').removeClass('active');
        });

        $('.profile-trigger').click(function() {
            $('.panel').alterClass('card-*', 'card-default');
        });

        $('.settings-trigger').click(function() {
            $('.panel').alterClass('card-*', 'card-info');
        });

        $('.admin-trigger').click(function() {
            $('.panel').alterClass('card-*', 'card-warning');
            $('.edit_account .nav-pills li, .edit_account .tab-pane').removeClass('active');
            $('#changepw')
                .addClass('active')
                .addClass('in');
            $('.change-pw').addClass('active');
        });

        $('.warning-pill-trigger').click(function() {
            $('.panel').alterClass('card-*', 'card-warning');
        });

        $('.danger-pill-trigger').click(function() {
            $('.panel').alterClass('card-*', 'card-danger');
        });

        $('#user_basics_form').on('keyup change', 'input, select, textarea', function(){
            $('#account_save_trigger').attr('disabled', false).removeClass('disabled').show();
        });

        $('#user_profile_form').on('keyup change', 'input, select, textarea', function(){
            $('#confirmFormSave').attr('disabled', false).removeClass('disabled').show();
        });

        $("#password_confirmation").keyup(function() {
            checkPasswordMatch();
        });

        $("#password, #password_confirmation").keyup(function() {
            enableSubmitPWCheck();
        });

        $('#password, #password_confirmation').hidePassword(true);

        $('#password').password({
            shortPass: 'The password is too short',
            badPass: 'Weak - Try combining letters & numbers',
            goodPass: 'Medium - Try using special charecters',
            strongPass: 'Strong password',
            containsUsername: 'The password contains the username',
            enterPass: false,
            showPercent: false,
            showText: true,
            animate: true,
            animateSpeed: 50,
            username: false, // select the username field (selector or jQuery instance) for better password checks
            usernamePartialMatch: true,
            minimumLength: 6
        });

        function checkPasswordMatch() {
            var password = $("#password").val();
            var confirmPassword = $("#password_confirmation").val();
            if (password != confirmPassword) {
                $("#pw_status").html("Passwords do not match!");
            }
            else {
                $("#pw_status").html("Passwords match.");
            }
        }

        function enableSubmitPWCheck() {
            var password = $("#password").val();
            var confirmPassword = $("#password_confirmation").val();
            var submitChange = $('#pw_save_trigger');
            if (password != confirmPassword) {
                submitChange.attr('disabled', true);
            }
            else {
                submitChange.attr('disabled', false);
            }
        }

        $('#edit-athlete-success a.close').on('click',function() {
            $('#edit-athlete-success').addClass('hidden');
        });

    </script>

@endsection
