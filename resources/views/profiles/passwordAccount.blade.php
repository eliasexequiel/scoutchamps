<div class="py-4 tab-pane fade edit-account-tab @if(session()->has('changeTabProfile') && session('changeTabProfile') == '1') show active @endif" role="tabpanel" aria-labelledby="edit-account-tab">
                                                
        <div class="w-full px-2 md:px-6 lg:px-10">
        <div class="w-full">

                <h4 class="my-2 pb-1 border-b">
                    {{ trans('profile.changePwTitle') }}
                </h4>

                {!! Form::model($user, array('action' => array('ProfilesController@updateUserPassword', $user->id), 'method' => 'PUT', 'autocomplete' => 'new-password')) !!}

                    <div class="pw-change-container margin-bottom-2">

                        <div class="form-group has-feedback {{ $errors->has('password') ? ' has-error ' : '' }}">
                            {!! Form::label('password', trans('forms.create_user_label_password'), array('class' => 'col-12 control-label')); !!}
                            <div class="col-12">
                                {!! Form::password('password', array('id' => 'password', 'class' => 'form-control ', 'placeholder' => trans('forms.create_user_ph_password'), 'autocomplete' => 'new-password')) !!}
                            </div>
                        </div>

                        <div class="form-group has-feedback {{ $errors->has('password_confirmation') ? ' has-error ' : '' }}">
                            {!! Form::label('password_confirmation', trans('forms.create_user_label_pw_confirmation'), array('class' => 'col-12 control-label')); !!}
                            <div class="col-12">
                                {!! Form::password('password_confirmation', array('id' => 'password_confirmation', 'class' => 'form-control', 'placeholder' => trans('forms.create_user_ph_pw_confirmation'))) !!}
                                <span id="pw_status"></span>
                                
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="w-full flex px-4 justify-between items-center">
                                <div class="w-1/12 hidden md:inline-flex"></div>
                                <div>
                                    {!! Form::button('<i class="fa fa-fw fa-save" aria-hidden="true"></i> ' . trans('profile.submitProfileButton'), array( 'class' => 'btn btn-success rounded-full','type' => 'submit','disabled' => false)) !!}
                                </div>
                                <a class="text-primary" href="{{ route('profile.index') }}"> {{ trans('forms.cancel') }} </a>
                        </div>
                    </div>
                {!! Form::close() !!}

            </div>

            @role('scout|athlete')
            <div id="deleteAccount" class="my-4 w-full px-4 sm:px-8">
                <h4 class="my-2 pb-1 border-b">
                    {{ trans('profile.deleteAccountTitle') }}
                </h4>
                <div class="mt-2 mb-2 flex justify-center items-center">
                    <span class="hidden sm:inline-flex">
                        <img class="px-2" src="{{ asset('img/profile/deleteAccount.png') }}" alt=""> 
                    </span>
                    <span> {!! trans('profile.adviceDeleteAccount') !!} </span>
                    <span class="hidden sm:inline-flex">
                        <img class="px-2" src="{{ asset('img/profile/deleteAccount.png') }}" alt="">
                    </span>
                </div>
                <div class="flex justify-center">
                    <div class="">

                        {!! Form::model($user, array('action' => array('ProfilesController@deleteUserAccount', $user->id), 'method' => 'DELETE')) !!}

                            <div class="flex justify-between">
                            {!! Form::button(
                                '<i class="fa fa-trash-o fa-fw" aria-hidden="true"></i> ' . trans('profile.deleteAccountBtn'),
                                array(
                                    'class'             => 'btn btn-primary rounded-full',
                                    'id'                => 'delete_account_trigger',
                                    'disabled'          => false,
                                    'type'              => 'button',
                                    'data-toggle'       => 'modal',
                                    'data-submit'       => trans('profile.deleteAccountBtnConfirm'),
                                    'data-target'       => '#confirmForm',
                                    'data-modalClass'   => '',
                                    'data-title'        => trans('profile.deleteAccountConfirmTitle'),
                                    'data-message'      => trans('profile.deleteAccountConfirmMsg')
                                )
                            ) !!}
                            </div>

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
            @endrole

            
            
        </div>
    </div>