@extends('layouts.app')

@section('template_title')
    {{ trans('profile.templateTitle') }}
@endsection

@section('content')
        <div class="w-full px-6 py-2">
            <div class="w-full">
                
                        @role('admin')
                            @if (Auth::user()->id == $user->id)
                            <div class="">
                                <div class="row">
                                    <div class="col-12 col-sm-4 col-md-3 profile-sidebar text-white">
                                        <div class="nav flex-row md:flex-col nav-pills nav-pills-admin" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                            <a class="nav-link active" data-toggle="pill" href=".edit-settings-tab" role="tab" aria-controls="edit-settings-tab" aria-selected="false">
                                                    <img src="{{ asset('img/profile/accountSettings.png') }}" alt="Account Settings">
                                                    <span class="text"> {{ trans('profile.editAccountTitle') }} </span>
                                            </a>
                                            <a class="nav-link" data-toggle="pill" href=".edit-account-tab" role="tab" aria-controls="edit-settings-tab" aria-selected="false">
                                                    <img src="{{ asset('img/profile/accountAdministration.png') }}" alt="Account Administration">
                                                    <span class="text"> {{ trans('profile.editAccountAdminTitle') }} </span>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="mt-2 md:mt-0 col-12 col-sm-12 col-md-9 px-0 md:pl-4">
                                        <div class="tab-content py-4 border-primary bg-white rounded-lg shadow-lg" id="v-pills-tabContent">
                                            <!-- PROFILE -->
                                            <div class="tab-pane fade show active edit-settings-tab" role="tabpanel" aria-labelledby="edit-settings-tab">
                                                <div class="flex flex-wrap">
                                                    <div class="flex w-full md:w-full lg:w-1/3 justify-center mb-1">
                                                        <div class="w-auto">
                                                            <div id="avatar_container">
                                                                        <div class="dz-preview"></div>
                                                                        {!! Form::open(array('route' => 'avatar.upload', 'method' => 'POST', 'name' => 'avatarDropzone','id' => 'avatarDropzone', 'class' => 'form single-dropzone dropzone single', 'files' => true)) !!}
                                                                            <img id="user_selected_avatar" class="user-avatar" src="{{ $user->avatar }}" alt="{{ $user->fullName }}">
                                                                        {!! Form::close() !!}

                                                                        <button class="px-2 flex items-center text-primary text-sm py-1 mx-auto" id="remove-avatar"> 
                                                                                <img src="{{ asset('img/profile/trash.png') }}" alt="Delete">
                                                                                <span class="pl-1">{{ trans('forms.remove-file') }} </span>
                                                                        </button>
                                                                        <div class="photoProfileError hidden mt-1 px-2 py-1 alert alert-danger alert-dismissable text-xs sm:text-sm rounded flex justify-center">
                                                                        </div>
                                                                        <div class="mt-1 px-2 py-1 bg-gray-200 text-xs sm:text-sm border border-gray-500 rounded flex items-center">
                                                                            <i class="fa fa-info-circle"></i>
                                                                            <span class="pl-2">{{ trans('profile.athlete.uploadPhotoProfileMaxSize') }} </span>
                                                                        </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <div class="w-full md:w-full lg:w-2/3">
                                                {!! Form::model($user, ['method' => 'PUT', 'route' => ['profile.update', $user->id], 'id' => 'user_basics_form', 'class' => 'pt-4']) !!}

                                                    {!! csrf_field() !!}

                                                    {{-- Email --}}
                                                    <div class="form-group has-feedback {{ $errors->has('email') ? ' has-error ' : '' }}">
                                                        {!! Form::label('email', trans('forms.create_user_label_email'), array('class' => 'col-12 control-label')); !!}
                                                        <div class="col-12">
                                                            {!! Form::text('email', $user->email, array('id' => 'email', 'class' => 'form-control', 'placeholder' => trans('forms.create_user_ph_email'))) !!}
                                                        </div>
                                                    </div>

                                                    <div class="row col-12 px-0">
                                                        <!-- FirstName -->
                                                        <div class="col-12 col-md-6 px-0 form-group has-feedback {{ $errors->has('firstname') ? ' has-error ' : '' }}">
                                                            {!! Form::label('firstname', trans('forms.create_user_label_firstname'), array('class' => 'col-12 control-label')); !!}
                                                            <div class="col-12">
                                                                {!! Form::text('firstname', $user->firstname, array('id' => 'firstname', 'class' => 'form-control', 'placeholder' => trans('forms.create_user_ph_firstname'))) !!}
                                                            </div>
                                                        </div>

                                                        <!-- Lastname -->
                                                        <div class="col-12 col-md-6 px-0 form-group has-feedback {{ $errors->has('lastname') ? ' has-error ' : '' }}">
                                                            {!! Form::label('lastname', trans('forms.create_user_label_lastname'), array('class' => 'col-12 control-label')); !!}
                                                            <div class="col-12">
                                                                {!! Form::text('lastname', $user->lastname, array('id' => 'lastname', 'class' => 'form-control', 'placeholder' => trans('forms.create_user_ph_lastname'))) !!}
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group mb-0">
                                                        <div class="px-4 flex justify-end items-center">
                                                            {!! Form::button('<i class="fa fa-fw fa-save" aria-hidden="true"></i> ' . trans('profile.submitProfileButton'), array( 'class' => 'mr-2 btn btn-success rounded-full','type' => 'submit')) !!}
                                                            <a class="text-primary" href="{{ route('profile.index') }}"> {{ trans('forms.cancel') }} </a>
                                                        </div>
                                                    </div>
                                                {!! Form::close() !!}
                                                </div>
                                            </div>
                                            </div>

                                            <!-- Password Account -->
                                            @include('profiles.passwordAccount')

                                        </div>
                                    </div>
                                </div>
                            </div>
                            @else
                                <p>{{ trans('profile.notYourProfile') }}</p>
                            @endif
                        @endrole

                    </div>
                
        </div>
    

    @include('modals.modal-form')

@endsection

@section('footer_scripts')

    @include('scripts.form-modal-script')
    {{-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCZtkZggdWRXxTMsShH8gnhhyafkWlCx5k&libraries=places"></script> --}}

    @if(config('settings.googleMapsAPIStatus'))
        {{-- @include('scripts.gmaps-address-lookup-api3') --}}
    @endif

    @include('scripts.user-avatar-dz')

    <script type="text/javascript">

        $('.dropdown-menu li a').click(function() {
            $('.dropdown-menu li').removeClass('active');
        });

        $('.profile-trigger').click(function() {
            $('.panel').alterClass('card-*', 'card-default');
        });

        $('.settings-trigger').click(function() {
            $('.panel').alterClass('card-*', 'card-info');
        });

        $('.admin-trigger').click(function() {
            $('.panel').alterClass('card-*', 'card-warning');
            $('.edit_account .nav-pills li, .edit_account .tab-pane').removeClass('active');
            $('#changepw')
                .addClass('active')
                .addClass('in');
            $('.change-pw').addClass('active');
        });

        $('.warning-pill-trigger').click(function() {
            $('.panel').alterClass('card-*', 'card-warning');
        });

        $('.danger-pill-trigger').click(function() {
            $('.panel').alterClass('card-*', 'card-danger');
        });

        $('#user_basics_form').on('keyup change', 'input, select, textarea', function(){
            $('#account_save_trigger').attr('disabled', false).removeClass('disabled').show();
        });

        $('#user_profile_form').on('keyup change', 'input, select, textarea', function(){
            $('#confirmFormSave').attr('disabled', false).removeClass('disabled').show();
        });

        // $("#password_confirmation").keyup(function() {
        //     checkPasswordMatch();
        // });

        $("#password, #password_confirmation").keyup(function() {
            enableSubmitPWCheck();
        });

        $('#password, #password_confirmation').hidePassword(true);

        $('#password').password({
            shortPass: 'The password is too short',
            badPass: 'Weak - Try combining letters & numbers',
            goodPass: 'Medium - Try using special charecters',
            strongPass: 'Strong password',
            containsUsername: 'The password contains the username',
            enterPass: false,
            showPercent: false,
            showText: true,
            animate: true,
            animateSpeed: 50,
            username: false, // select the username field (selector or jQuery instance) for better password checks
            usernamePartialMatch: true,
            minimumLength: 6
        });

        function checkPasswordMatch() {
            var password = $("#password").val();
            var confirmPassword = $("#password_confirmation").val();
            if (password != confirmPassword) {
                $("#pw_status").html("Passwords do not match!");
            }
            else {
                $("#pw_status").html("Passwords match.");
            }
        }

        function enableSubmitPWCheck() {
            var password = $("#password").val();
            var confirmPassword = $("#password_confirmation").val();
            var submitChange = $('#pw_save_trigger');
            if (password != confirmPassword) {
                submitChange.attr('disabled', true);
            }
            else {
                submitChange.attr('disabled', false);
            }
        }

    </script>

@endsection
