@php
$categories = [
    array('Subscriptions',['App\Notifications\UserAddedSubscription','App\Notifications\UserDeletedSubscription','App\Notifications\CouponSubscriptionUsed']),
    array('Clubs',['App\Notifications\ClubCreated']),
    array('NewUsers',['App\Notifications\UserRegistered']),
    array('Showcase',['App\Notifications\UserAddClubToShowCase']),
    array('Reports',['App\Notifications\NewReportCreated']),
    array('ContactsBetweenScoutsAndAthletes',['App\Notifications\FollowUser'])
];
@endphp
<app-tabs :default-index="{{ $index }}">
    @foreach ($categories as $index => $c)
        
    @php
        $notificationsF = $notifications->filter(function($n) use ($c) { 
            return in_array($n->type,$c[1]); 
        })
    @endphp
    <app-tab title="{{ trans('notifications.'.$c[0]) }}">
            @forelse ($notificationsF as $n)
            <div class="w-full">
                {{-- @php $data = json_encode($n->data) @endphp --}}
                <div class="flex justify-between items-center w-full border-b pb-1">
                    <div>
                    <div class="w-full">
                        {{-- @if($n->type == 'App\Notifications\FavoriteUser' || $n->type == 'App\Notifications\FollowUser' || $n->type == 'App\Notifications\InvitationRecruitment') --}}
                        <a class="capitalize inline-flex" href="{{ url('p/profile/'.$n->data['username'].'_'.$n->data['uuid']) }}">
                            {{ $n->data['username'] }} </a>
                        {{-- @endif --}}

                        @switch($n->type)
                        @case('App\Notifications\FavoriteUser')
                            {{ trans('notifications.messages.favoriteUser') }}
                            @break
                        @case('App\Notifications\FollowUser')
                            {{-- {{ trans('notifications.messages.followUser') }} --}}
                            {{ trans('notifications.messages.contactMessagePrivate') }} 
                            <a class="capitalize inline-flex" href="{{ url('p/profile/'.$n->data['userFollowedUsername'].'_'.$n->data['userFollowedUuid']) }}"> {{ $n->data['userFollowedUsername'] }} </a>.
                            @break
                        @case('App\Notifications\UserRegistered')
                            {{ trans('notifications.messages.registered') }}
                            @break
                        @case('App\Notifications\UserAddClubToShowCase')
                            {{ trans('notifications.messages.showcase',['typeEntity' => strtolower(trans('clubes.types.'.$n->data['typeEntity'])), 'nameEntity' => $n->data['nameEntity']]) }}
                            <a class="cursor-pointer inline-flex" href="{{ url('showcase/user/'.$n->data['userid']) }}">
                                {{ trans('notifications.messages.showcaseLink') }} 
                            </a>
                            @break
                        @case('App\Notifications\InvitationRecruitment')
                            {{ trans('notifications.messages.invitationRecruitment') }} 
                            <a class="cursor-pointer inline-flex" href="{{ url('p/recruitment/'.$n->data['recruitment']) }}">
                                {{ trans('notifications.messages.recruitment') }} 
                            </a>
                            @break
                        @case('App\Notifications\ClubCreated')
                            {{ trans('notifications.messages.clubCreated') }} 
                            <a class="cursor-pointer inline-flex" href="{{ url('clubes/'.$n->data['entityid']) }}"> 
                                {{ $n->data['entityname'] }}
                            </a>
                            @break
                        @case('App\Notifications\UserAddedSubscription')
                            {{ trans('notifications.messages.addedSusbcription') }} 
                            <span class="font-semibold"> {{ $n->data['plan'] }} </span>
                            @break
                        @case('App\Notifications\CouponSubscriptionUsed')
                            {{ trans('notifications.messages.couponUsed') }} 
                            <span class="font-semibold"> {{ $n->data['coupon'] }} </span>
                            @break
                        @case('App\Notifications\UserDeletedSubscription')
                            {{ trans('notifications.messages.deletedSusbcription') }} 
                            <span class="font-semibold"> {{ $n->data['plan'] }} </span>
                            @break
                        @case('App\Notifications\NewReportCreated')
                            {{ trans('notifications.messages.report') }} 
                            <a class="font-semibold inline-flex" href="{{ url('/admin/report/'.$n->data['reportid']) }}"> {{ trans('notifications.messages.reportTitle')  }} </a>
                            @break
                        @case('App\Notifications\NewThread')
                            {{ trans('notifications.messages.contactMessagePrivate') }} 
                            <a class="capitalize inline-flex" href="{{ url('p/profile/'.$n->data['userToUuid']) }}"> {{ $n->data['userToUsername'] }} </a>.
                            @break
                            @default

                        @endswitch
                                
                    </div>
                    <div class="text-xs text-gray-600"> {{ $n->created_at->diffForHumans() }}</div>
                    </div>
                    @if(isset($unread))
                    <div>
                            <a title="{{ trans('notifications.markAsRead') }}" href="{{ route('notifications.markAsRead',['id' => $n->id]) . '?index=' . $index }}"> 
                                <i class="fa fa-check fa-fw"></i> 
                            </a>
                    </div>
                    @endif

                </div>

            </div>
            @empty
                <div class="text-base pt-2">
                    {{ trans('notifications.noResults') }}
                </div>
            @endforelse
            <div class="w-full mt-3">
                @if(isset($unread))
                <div class="w-full mb-2">
                    {{ $notifications->appends(['tab'=> 'unread','index' => $index,'unread' => $notifications->currentPage()])->links() }}
                </div>
                @else
                <div class="w-full mb-2">
                    {{ $notifications->appends(['tab'=> 'read','index' => $index,'read' => $notifications->currentPage()])->links() }}
                </div>
                @endif
            </div>
    </app-tab>
    @endforeach
</app-tabs>