@extends('layouts.app')

@section('template_title')
{{ trans('notifications.title') }}
@endsection

@section('template_fastload_css')
@endsection

@section('content')
<div class="w-full px-6 py-2">
    <div class="card-p">
            
        <div class="flex justify-between items-center mb-2 border-b pb-2">
            <h4 class="font-bold"> {{ trans('notifications.title') }} </h4>
            @if(count($notifications) > 0)
            <div>
                <a class="btnMarkAsRead btn btn-gray btn-sm" href="{{ route('notifications.markAllAsRead') }}">
                    {{ trans('notifications.markAllAsRead') }} </a>
            </div>
            @endif
        </div>
        
        <div class="w-full pt-4">

            <div class="mb-4 flex filters-buttons">
                <button class="{{ request()->query('tab') == 'read' ? '' : 'active' }} unread mr-2"> {{ trans('notifications.unread') }} </button>
                <button class="{{ request()->query('tab') == 'read' ? 'active' : '' }} read"> {{ trans('notifications.read') }} </button>
            </div>

            <div class="{{ request()->query('tab') == 'read' ? '' : 'hidden' }} readnotifications">
                
                @role('admin')
                    @include('notifications.admin-index',['notifications' => $readNotifications])
                @else
                @foreach ($readNotifications as $n)
                <div class="w-full">
                    {{-- @php $data = json_encode($n->data) @endphp --}}
                    <div class="w-full border-b pb-1">
                        <div class="w-full">
                            {{-- @if($n->type == 'App\Notifications\FavoriteUser' || $n->type == 'App\Notifications\FollowUser' || $n->type == 'App\Notifications\InvitationRecruitment') --}}
                            @if(array_key_exists('uuid',$n->data))
                                <a class="capitalize inline-flex" href="{{ url('p/profile/'.preg_replace('/\s+/','',$n->data['username']).'_'.$n->data['uuid']) }}"> {{ $n->data['username'] }} </a>
                            @endif

                            @switch($n->type)
                            @case('App\Notifications\FavoriteUser')
                                {{ trans('notifications.messages.favoriteUser') }}
                                @break
                            @case('App\Notifications\FollowUser')
                                {{ trans('notifications.messages.followUser') }}
                                @break
                            @case('App\Notifications\InvitationRecruitment')
                                {{ trans('notifications.messages.invitationRecruitment') }} 
                                <a class="cursor-pointer inline-flex" href="{{ url('p/recruitment/'.$n->data['recruitment']) }}">
                                    {{ trans('notifications.messages.recruitment') }} 
                                </a>
                                @break
                            @case('App\Notifications\NewRequestInvitationRecruitmentToScout')
                                {{ trans('notifications.messages.requestInvitationRecruitment') }} 
                                <a class="cursor-pointer inline-flex" href="{{ url('recruitments/'.$n->data['recruitment']. '/invitations') }}">
                                    {{ trans('notifications.messages.recruitment') }} 
                                </a>
                                @break
                            @case('App\Notifications\ClubCreated')
                                {{ trans('notifications.messages.clubCreated') }} 
                                <a class="cursor-pointer inline-flex" href="{{ url('clubes/'.$n->data['entityid']) }}"> 
                                    {{ $n->data['entityname'] }}
                                </a>
                                @break
                            @case('App\Notifications\UserAddedSubscription')
                                {{ trans('notifications.messages.addedSusbcription') }} 
                                <span> {{ $n->data['plan'] }} </span>
                                @break
                            @case('App\Notifications\UserDeletedSubscription')
                                {{ trans('notifications.messages.deletedSusbcription') }} 
                                <span> {{ $n->data['plan'] }} </span>
                                @break
                            @case('App\Notifications\ReportClosedWithReportedDeleted')
                                {{ trans('notifications.messages.reportedDeleted',['type' => trans('reports.types.'.$n->data['reportname'])]) }}
                                @break
                            @case('App\Notifications\AdminContactedClubInShowcase')
                                {!! trans('notifications.messages.entityContactedByAdmin',['typeEntity' => trans('clubes.types.'.$n->data['typeEntity']),'nameEntity' => $n->data['nameEntity']]) !!}
                                <a class="font-semibold" href="{{ route('profile.index').'?changeTabProfile=6' }}"> {{ trans('profile.showcase') }}</a>.
                                @break
                                @default

                            @endswitch
                                    
                        </div>
                        <div class="text-xs text-gray-600"> {{ $n->created_at->diffForHumans() }}</div>

                    </div>

                </div>

                @endforeach
                    <div class="w-full mt-3">
                        <div class="w-full mb-2">
                            {{ $readNotifications->appends(['tab'=> 'read','read' => $readNotifications->currentPage()])->links() }}
                        </div>
                    </div>
                

                    @if(count($readNotifications) == 0)
                    <div class="text-base pt-2">
                        {{ trans('notifications.noResults') }}
                    </div>
                    @endif
                @endrole
            </div>
            
            <div class="{{ request()->query('tab') == 'read' ? 'hidden' : '' }} unreadnotifications">
                @role('admin')
                    @include('notifications.admin-index',['notifications' => $notifications, 'unread' => true])
                @else

                @foreach ($notifications as $n)
                <div class="w-full">
                    {{-- @php $data = json_encode($n->data) @endphp --}}
                    <div class="flex justify-between items-center border-b pb-1">
                        <div>
                            <div>
                                @if(array_key_exists('uuid',$n->data))
                                    <a class="capitalize inline-flex" href="{{ url('p/profile/'.$n->data['uuid']) }}"> {{ $n->data['username'] }} </a>
                                @endif

                                @switch($n->type)
                                @case('App\Notifications\FavoriteUser')
                                    {{ trans('notifications.messages.favoriteUser') }}
                                    @break
                                @case('App\Notifications\FollowUser')
                                    {{ trans('notifications.messages.followUser') }}
                                    @break
                                @case('App\Notifications\InvitationRecruitment')
                                    {{ trans('notifications.messages.invitationRecruitment') }} 
                                    <a class="cursor-pointer inline-flex" href="{{ url('p/recruitment/'.$n->data['recruitment']) }}">
                                        {{ trans('notifications.messages.recruitment') }} 
                                    </a>
                                    @break
                                @case('App\Notifications\NewRequestInvitationRecruitmentToScout')
                                    {{ trans('notifications.messages.requestInvitationRecruitment') }} 
                                    <a class="cursor-pointer inline-flex" href="{{ url('recruitments/'.$n->data['recruitment']. '/invitations') }}">
                                        {{ trans('notifications.messages.recruitment') }} 
                                    </a>
                                    @break
                                @case('App\Notifications\ClubCreated')
                                        {{ trans('notifications.messages.clubCreated') }} 
                                        <a class="cursor-pointer inline-flex" href="{{ url('clubes/'.$n->data['entityid']) }}"> 
                                            {{ $n->data['entityname'] }}
                                        </a>
                                        @break
                                @case('App\Notifications\UserAddedSubscription')
                                        {{ trans('notifications.messages.addedSusbcription') }} 
                                        <span> {{ $n->data['plan'] }} </span>
                                        @break
                                @case('App\Notifications\UserDeletedSubscription')
                                        {{ trans('notifications.messages.deletedSusbcription') }} 
                                        <span> {{ $n->data['plan'] }} </span>
                                        @break
                                @case('App\Notifications\ReportClosedWithReportedDeleted')
                                        {{ trans('notifications.messages.reportedDeleted',['type' => trans('reports.types.'.$n->data['reportname'])]) }}
                                        @break
                                @case('App\Notifications\AdminContactedClubInShowcase')
                                        {!! trans('notifications.messages.entityContactedByAdmin',['typeEntity' => trans('clubes.types.'.$n->data['typeEntity']),'nameEntity' => $n->data['nameEntity']]) !!}
                                        <a class="font-semibold" href="{{ route('profile.index').'?changeTabProfile=6' }}"> {{ trans('profile.showcase') }}</a>.
                                @default

                                @endswitch
                            </div>
                            <div class="text-xs text-gray-600"> {{ $n->created_at->diffForHumans() }}</div>
                        </div>
                        <div class="pull-right">
                            <a title="{{ trans('notifications.markAsRead') }}"
                                href="{{ route('notifications.markAsRead',['id' => $n->id]) }}"> <i
                                    class="fa fa-check fa-fw"></i> </a>
                        </div>

                    </div>

                </div>
                @endforeach
                @if(count($notifications) > 0)
                <div class="w-full mt-3">
                    <div class="w-full mb-2">
                        {{ $notifications->appends(['tab'=> 'unread','unread' => $notifications->currentPage()])->links() }}
                    </div>
                </div>
                @else
                    <div class="text-base pt-2">
                        {{ trans('notifications.noResults') }}
                    </div>
                @endif
                @endrole
            </div>
        </div>

    </div>
</div>
@endsection

@section('footer_scripts')
<script type="text/javascript">

    $('.filters-buttons button').on('click', function () {
        if ($(this).hasClass('read') && !$(this).hasClass('active')) {
            $(this).addClass('active');
            $('button.unread').removeClass('active');
            $('.unreadnotifications, .btnMarkAsRead').addClass('hidden');
            $('.readnotifications').removeClass('hidden');

        } else if ($(this).hasClass('unread') && !$(this).hasClass('active')) {
            $(this).addClass('active');
            $('button.read').removeClass('active');
            $('.readnotifications').addClass('hidden');
            $('.unreadnotifications, .btnMarkAsRead').removeClass('hidden');
        }
    })
</script>
@endsection