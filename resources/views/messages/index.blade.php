@extends('layouts.app')

@section('template_title')
    {{ trans('messages.title') }}
@endsection

@section('template_fastload_css')
@endsection

@section('content')
<div class="w-full px-6">
    <user-messages></user-messages>
</div>
@include('modals.modal-advice-follow-user-to-send-message')
@endsection

@section('footer_scripts')
@endsection