@extends('layouts.app')

@section('template_title')
    {{ trans('messages.title') }}
@endsection

@section('template_fastload_css')
@endsection

@section('content')
<div class="w-full px-6">
    <contact-admin :thread="{{ json_encode($thread) }}"></contact-admin>
</div>
@endsection

@section('footer_scripts')
@endsection