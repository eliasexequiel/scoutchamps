@extends('layouts.app')

@section('template_title')
    {!! trans('subscriptions.invoices.title') !!}
@endsection

@section('content')
    <div class="container-fluid">
        <div class="w-full flex justify-center">
            <div class="w-full sm:w-5/6 lg:w-2/3">
                <div class="card-p">

                        <div class="flex justify-between items-center pb-2 border-b mb-2">

                            <div class="flex flex-wrap items-center">
                                <h4 class="font-bold"> {!! trans('subscriptions.invoices.title') !!}: {{ $user->fullName }}</h4>
                            </div>
                            <div class="pull-right">
                                    <a href="{{ route('subscriptions.index') }}" class="btn btn-light btn-sm float-right" title="{{ trans('subscriptions.back-subscriptions') }}">
                                        <i class="fa fa-fw fa-reply-all" aria-hidden="true"></i>
                                        {!! trans('subscriptions.buttons.back-to-subscriptions') !!}
                                    </a>
                            </div>
                            {{-- <div class="btn-group pull-right btn-group-xs">
                                    <a class="btn btn-gray rounded-full" href="/subscriptions/create">
                                        <i class="fa fa-plus fa-fw" aria-hidden="true"></i>
                                        {!! trans('subscriptions.buttons.create-new') !!}
                                    </a>
                            </div> --}}
                        </div>

                    <div class="w-full">

                        <div class="w-full border-b mb-2">
                        
                        <div class="table-responsive users-table">
                            <table class="table table-striped table-sm primary">
                                
                                <thead class="thead">
                                    <tr>
                                        <th>{!! trans('subscriptions.invoices.table.created') !!}</th>
                                        <th>{!! trans('subscriptions.invoices.table.total') !!}</th>
                                        <th>{!! trans('subscriptions.invoices.table.amountPaid') !!}</th>
                                        <th>{!! trans('subscriptions.invoices.table.status') !!}</th>
                                        {{-- <th>{!! trans('subscriptions.invoices.table.created') !!}</th> --}}
                                        {{-- <th>{!! trans('subscriptions.subscriptions-table.updated') !!}</th> --}}
                                        {{-- <th>{!! trans('subscriptions.subscriptions-table.actions') !!}</th> --}}
                                        {{-- <th class="no-search no-sort"></th> --}}
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody id="users_table">
                                    @foreach($invoices as $invoice)
                                    @if($invoice->status != 'void')
                                        <tr>
                                            <td>
                                                {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$invoice->date())->format('d/m/Y H:i') }}
                                            </td>
                                            <td>
                                                <span class="text-sm"> {{ $invoice->subtotal / 100 }} {{ $invoice->currency }} </span>
                                                @if($invoice->discount)
                                                    <span class="text-xs text-gray-600"> ({{ $invoice->discount->coupon->name }}) </span>
                                                @endif
                                            </td>
                                            <td>
                                                    {{ $invoice->amount_paid / 100 }} {{ $invoice->currency }}
                                            </td>
                                            <td>
                                                @if($invoice->paid)
                                                    <span class="badge badge-success rounded-full leading-relaxed px-2 text-xs"> {{ trans('subscriptions.invoices.status.paid') }} </span>
                                                @elseif($invoice->status == 'open')
                                                    <span class="badge badge-info rounded-full leading-relaxed px-2 text-xs"> {{ trans('subscriptions.invoices.status.open') }} </span>
                                                @endif
                                            </td>
                                            <td>
                                                <a class="btn btn-sm rounded" href="{{ $invoice->invoice_pdf }}"> 
                                                    <i class="fa fa-download"></i>   
                                                    {{ trans('subscriptions.invoices.download') }}    
                                                </a>
                                            </td>
                                            {{-- <td>
                                                @if($sub->ended())
                                                    <span class="badge badge-danger rounded-full"> {{ trans('subscriptions.status.cancelled') }} </span>
                                                @else
                                                <span class="badge badge-success rounded-full"> {{ trans('subscriptions.status.active') }} </span>
                                                @endif
                                            </td>
                                            <td>
                                                @if($subscription->latest_invoice)
                                                    @php  $invoice = \Stripe\Invoice::retrieve($subscription->latest_invoice); @endphp
                                                    <span class="text-sm"> {{ $invoice->amount_paid / 100 }} {{ $invoice->currency }} </span>
                                                    @if($invoice->discount)
                                                        <span class="text-xs text-gray-600"> ({{ $invoice->discount->coupon->name }}) </span>
                                                    @endif
                                                @endif
                                            </td> --}}
                                            {{-- <td>  --}}
                                                {{-- {{ $sub->hasIncompletePayment() }} --}}
                                                {{-- @if(!$sub->ended() && $sub->hasIncompletePayment())
                                                    Yes
                                                @else
                                                    No
                                                @endif --}}
                                            {{-- </td> --}}
                                            <td>
                                                {{-- @if($i->invoice->created)
                                                {{ \Carbon\Carbon::createFromTimestamp($i->invoice->created)->format('d/m/Y H:i')  }} 
                                                @endif --}}
                                            </td>
                                        </tr>
                                    @endif
                                    @endforeach
                                </tbody>

                            </table>

                            
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

@section('footer_scripts')
    
@endsection
