@extends('layouts.app')

@section('template_title')
    {!! trans('dashboard.popularUsers') !!}
@endsection

@section('template_linked_css')
@endsection

@section('content')

{{-- ALERTS  WITH JSON --}}
<div id="edit-athlete-success" class="hidden mx-6 alert alert-success alert-dismissable fade show" role="alert">
        <a href="#" class="close" aria-label="close">&times;</a>
        <span id="message"></span>
</div>

<div class="flex justify-center">
        
    <div class="w-11/12 lg:w-3/4">
            <div class="card-p">
                <div class="flex border-b pb-2">
                    <h4 class="font-bold">  {!! trans('dashboard.popularUsers') !!} </h4>
                </div>

                <div class="flex flex-wrap w-full">
                    <div class="w-full md:w-1/2">
                        <admin-popular-users :popular-users="{{ $usersScout }}" :type-user="'Scout'"></admin-popular-users>
                    </div>
                    <div class="w-full md:w-1/2">
                        <admin-popular-users :popular-users="{{ $usersAthlete }}" :type-user="'Athlete'"></admin-popular-users>
                    </div>
                </div>
            </div>
    </div>
</div>
@endsection

@section('footer_scripts')
<script type="text/javascript">
    $('#edit-athlete-success a.close').on('click',function() {
        $('#edit-athlete-success').addClass('hidden');
    });
</script>
@stop