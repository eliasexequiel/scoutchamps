@extends('layouts.app')

@section('template_title')
    {!! trans('sports.showing-all-sports') !!}
@endsection

@section('template_linked_css')
    @if(config('sports.enabledDatatablesJs'))
        <link rel="stylesheet" type="text/css" href="{{ config('sports.datatablesCssCDN') }}">
    @endif
    <style type="text/css" media="screen">
        .users-table {
            border: 0;
        }
        .users-table tr td:first-child {
            padding-left: 15px;
        }
        .users-table tr td:last-child {
            padding-right: 15px;
        }
        .users-table.table-responsive,
        .users-table.table-responsive table {
            margin-bottom: 0;
        }
    </style>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card-p">

                        <div class="flex flex-wrap justify-between items-center pb-2 border-b mb-2">

                                <div class="flex flex-wrap items-center">
                                    <h4 class="font-bold pr-4"> {!! trans('sports.showing-all-sports') !!}</h4>

                                    <div class="w-auto"> 
                                        <span class="badge badge-primary rounded-full text-white">{{ trans('sports.sportTotal') }}: {{ $sports->total() }} </span>
                                    </div>
                                </div>
                            {{-- <div class="mt-2 sm:mt-0 btn-group btn-group-xs">
                                    <a class="btn btn-gray rounded-full" href="/sports/create">
                                        <i class="fa fa-plus fa-fw" aria-hidden="true"></i>
                                        {!! trans('sports.buttons.create-new') !!}
                                    </a>
                            </div> --}}
                        </div>

                    <div class="w-full">

                        {{-- @if(config('sports.enableSearchUsers'))
                            @include('partials.search-users-form')
                        @endif --}}

                        <div class="table-responsive users-table">
                            <table class="table table-striped table-sm data-table primary">
                                
                                <thead class="thead">
                                    <tr>
                                        <th>{!! trans('sports.sports-table.name') !!}</th>
                                        <th>{!! trans('sports.sports-table.created') !!}</th>
                                        {{-- <th>{!! trans('sports.sports-table.updated') !!}</th> --}}
                                        <th>{!! trans('sports.sports-table.actions') !!}</th>
                                        <th class="no-search no-sort"></th>
                                        <th class="no-search no-sort"></th>
                                    </tr>
                                </thead>
                                <tbody id="users_table">
                                    @foreach($sports as $c)
                                        <tr>
                                            <td>{{ trans('sportfields.titles.'.$c->name) }}</td>
                                            <td>
                                                @if($c->created_at)
                                                    {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$c->created_at)->format(config('settings.formatDate'))  }}
                                                @endif
                                            </td>
                                            {{-- <td>
                                                @if($c->updated_at)
                                                    {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$c->updated_at)->format(config('settings.formatDate'))  }}
                                                @endif
                                            </td> --}}
                                            
                                            <td class="buttons-actions">
                                                <a class="btn btn-sm btn-outline-success rounded-full" href="{{ route('sports.show',[$c->id]) }}" data-toggle="tooltip" title="Show">
                                                    {!! trans('sports.buttons.show') !!}
                                                </a>

                                                <a class="btn btn-sm btn-outline-info rounded-full" href="{{ route('sports.edit',[$c->id]) }}" data-toggle="tooltip" title="Edit">
                                                    {!! trans('sports.buttons.edit') !!}
                                                </a>
                                                
                                                {!! Form::open(array('url' => 'sports/' . $c->id, 'class' => 'inline-flex', 'data-toggle' => 'tooltip', 'title' => 'Delete')) !!}
                                                    {!! Form::hidden('_method', 'DELETE') !!}
                                                    {!! Form::button(trans('sports.buttons.delete'), array('class' => 'btn btn-outline-danger rounded-full btn-sm','type' => 'button' ,'data-toggle' => 'modal', 'data-target' => '#confirmDelete', 'data-title' => 'Delete Sport', 'data-message' => 'Are you sure you want to delete this sport ?')) !!}
                                                {!! Form::close() !!}
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>

                            </table>

                            
                        </div>
                        @if(config('usersmanagement.enablePagination'))
                            <div class="w-full mt-4">
                                {{ $sports->links() }}
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('modals.modal-delete')

@endsection

@section('footer_scripts')
    @if ((count($sports) > config('sports.datatablesJsStartCount')) && config('sports.enabledDatatablesJs'))
        @include('scripts.datatables')
    @endif
    @include('scripts.delete-modal-script')
    @include('scripts.save-modal-script')
    @if(config('sports.tooltipsEnabled'))
        @include('scripts.tooltips')
    @endif
    {{-- @if(config('sports.enableSearchUsers'))
        @include('scripts.search-users')
    @endif --}}
@endsection
