@extends('layouts.app')

@section('template_title')
  {!! trans('sports.showing-sport', ['name' => $sport->name]) !!}
@endsection

@section('content')

  <div class="container">
      <div class="flex w-full justify-center">
          <div class="w-full sm:w-4/5 md:w-3/5">

        <div class="card-p">
            <div class="flex justify-between items-center pb-2 border-b mb-2">
              <h4 class="font-bold"> {!! $sport->name !!} </h4>
              <div class="float-right">
                <a href="{{ route('sports.index') }}" class="btn btn-light btn-sm float-right">
                  <i class="fa fa-fw fa-mail-reply" aria-hidden="true"></i>
                  {!! trans('sports.buttons.back-to-sports') !!}
                </a>
              </div>
            
          </div>

          <div class="w-full">

            <div class="pb-2 row flex justify-start items-center">
              
              <div class="w-full">
                
                  <div class="mt-2">
                    <a href="{{ route('sports.edit',[$sport->id]) }}" class="btn btn-sm btn-outline-info rounded-full" title="{{ trans('titles.edit') }}">
                      <i class="fa fa-pencil fa-fw" aria-hidden="true"></i> 
                      <span> {{ trans('titles.edit') }} </span>
                    </a>
                    {!! Form::open(array('url' => 'sports/' . $sport->id, 'class' => 'form-inline', 'title' => trans('titles.delete'))) !!}
                      {!! Form::hidden('_method', 'DELETE') !!}
                      {!! Form::button('<i class="fa fa-trash-o fa-fw" aria-hidden="true"></i> <span>' . trans('titles.delete') . '</span>' , array('class' => 'btn btn-outline-danger rounded-full btn-sm','type' => 'button', 'data-toggle' => 'modal', 'data-target' => '#confirmDelete', 'data-title' => 'Delete sport', 'data-message' => 'Are you sure you want to delete this sport?')) !!}
                    {!! Form::close() !!}
                  </div>
              </div>
            </div>

            {{-- Fields --}}
            <div class="flex">
                <strong> {{ trans('sports.forms.fields') }} </strong>
            </div>
    
            {{-- {{ $sport->fields }} --}}
            <div class="col-12 border-b pb-2 mb-1">
              @foreach ($sport->fields as $f)
                <div class="pl-2 flex pb-1">
                  <span> - {{ $f->name ?? '--' }} </span>
                  {{-- <span> {{ $f->fieldtype ?? '--' }} </span> --}}
                  {{-- <span> {{ $f->options_select ?? '' }} </span> --}}
                </div>
              @endforeach
            </div>

            {{-- Description --}}
            <div class="w-full flex flex-wrap border-b mb-1 pb-2">
                <div class="mr-2"> <strong> {{ trans('sports.forms.description') }}: </strong> </div>
                <div> {{ $sport->description ?? '--' }} </div>
            </div>

            @if ($sport->created_at)
              <div class="w-full flex flex-wrap border-b mb-1 pb-2">
                  <div class="mr-2"> <strong> {{ trans('clubes.clubes-table.created') }}: </strong> </div>
                  <div> {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$sport->created_at)->format(config('settings.formatDate')) }} </div>
              </div>
            @endif

            @if ($sport->updated_at)
              <div class="w-full flex flex-wrap border-b mb-1 pb-2">
                  <div class="mr-2"> <strong> {{ trans('clubes.clubes-table.updated') }}: </strong> </div>
                  <div> {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$sport->updated_at)->format(config('settings.formatDate')) }} </div>
              </div>
            @endif

          </div>

        </div>
      </div>
    </div>
  </div>

  @include('modals.modal-delete')

@endsection

@section('footer_scripts')
  @include('scripts.delete-modal-script')
@endsection
