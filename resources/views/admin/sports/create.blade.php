@extends('layouts.app')

@section('template_title')
    {!! trans('clubes.create-new-club') !!}
@endsection

@section('template_linked_css')
<style>
#remove-field svg {
    width: 35px;
    height: 35px;
}
</style>
@endsection

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-lg-10 offset-lg-1">
                <div class="card-p">
                        <div class="flex justify-between items-center pb-2 border-b mb-2">
                            <h4 class="font-bold">  {!! trans('sports.create-new-sport') !!} </h4>
                            <div class="pull-right">
                                <a href="{{ route('sports.index') }}" class="btn btn-light btn-sm float-right" title="{{ trans('sports.back-sports') }}">
                                    <i class="fa fa-fw fa-reply-all" aria-hidden="true"></i>
                                    {!! trans('sports.buttons.back-to-sports') !!}
                                </a>
                            </div>
                        
                        </div>

                    <div class="w-full">
                        {!! Form::open(array('route' => 'sports.store', 'method' => 'POST', 'role' => 'form','id' => 'clubForm', 'class' => 'needs-validation')) !!}

                            {!! csrf_field() !!}

                            <div class="form-group has-feedback">
                                {!! Form::label('name', trans('sports.forms.name'), array('class' => 'col-12 control-label')); !!}
                                <div class="col-12">
                                        {!! Form::text('name', NULL, array('id' => 'name', 'class' => 'form-control' )) !!}                                        
                                </div>
                            </div>

                            <div class="form-group has-feedback">
                                {!! Form::label('description', trans('sports.forms.description'), array('class' => 'col-12 control-label')); !!}
                                <div class="col-12">
                                    {!! Form::textarea('description', NULL, array('id' => 'description', 'class' => 'form-control' )) !!}                                        
                                </div>
                            </div>

                            {{-- Fields --}}
                            <!--
                            <div class="col-12 py-4">
                                    <h6 class="font-bold"> {{ trans('sports.forms.fields') }} </h6>
                                </div>
                                <div id="wrapper-options" class="row col-12 px-0">
                                        <div class="field-option row col-12 px-0">
                                            <div class="col-12 px-0 col-sm-4 form-group has-feedback">
                                                    {!! Form::label('entity_type_id', trans('sports.forms.field-name'), array('class' => 'col-12 control-label')); !!}
                                                    <div class="col-12">
                                                            {!! Form::text('fields[name][]',NULL, array('id' => 'name', 'class' => 'form-control' )) !!}                                        
                                                    </div>
                                            </div>
                                            <div class="col-12 px-0 col-sm-3 form-group has-feedback">
                                                        {!! Form::label('email', trans('sports.forms.field-type'), array('class' => 'col-12 control-label')); !!}
                                                        <div class="col-12">
                                                                {!! Form::select('fields[type][]',$types, NULL, array('class' => 'form-control', 'id' => 'field-type' )) !!}                                        
                                                        </div>
                                            </div>
        
                                            <div id="options-select" class="hidden col-12 px-0 col-sm-4 form-group has-feedback">
                                                        {!! Form::label('email', trans('sports.forms.field-options'), array('class' => 'col-12 control-label')); !!}
                                                        <div class="col-12">
                                                                {!! Form::text('fields[options][]', NULL, array('class' => 'form-control','placeholder' => trans('sports.forms.options-select') )) !!}                                        
                                                        </div>
                                            </div>
                                                
                                        </div>

                                </div>

                                <div class="col-12">
                                    <button id="btn-add-field" class="btn btn-gray rounded-full" type="button" title="{{ trans('sports.forms.field-add') }}"> 
                                        <i class="fa fa-plus fa-fw" aria-hidden="true"></i>    
                                        {{ trans('sports.forms.field-add') }} 
                                    </button>
                                </div>

                                -->


                            <div class="flex w-full justify-end mt-2 px-2">
                                {!! Form::button(trans('sports.forms.create'), array('class' => 'btn btn-success rounded-full','type' => 'submit' )) !!}
                            </div>
                            {!! Form::close() !!}
                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection

@section('footer_scripts')

<script type="text/javascript">

$(function() {
    $('#btn-add-field').on('click',function() {
        let option = $('.field-option')[0];
        // console.log(option);
        let html = `<div class="field-option row col-12 px-0">
                        <div class="col-12 px-0 col-sm-4 form-group has-feedback">
                            <label for="entity_type_id" class="col-12 control-label">Name</label> 
                            <div class="col-12"><input id="name" name="fields[name][]" type="text" class="form-control"></div>
                        </div> 
                        <div class="col-12 px-0 col-sm-3 form-group has-feedback">
                            <label for="email" class="col-12 control-label">Type</label> 
                            <div class="col-12"><select id="field-type" name="fields[type][]" class="form-control"><option value="text">text</option><option value="number">number</option><option value="select">select</option></select></div>
                        </div> 
                        <div id="options-select" class="hidden col-12 px-0 col-sm-4 form-group has-feedback">
                            <label for="email" class="col-12 control-label">Options Select</label> 
                            <div class="col-12"><input placeholder="Options separated by coma" name="fields[options][]" type="text" class="form-control"></div>
                        </div>
                        <div class="col-2 col-sm-1 self-center mt-4" id="remove-field"> 
                                <svg viewPort="0 0 12 12" version="1.1" xmlns="http://www.w3.org/2000/svg">
                                    <line x1="8" y1="20" x2="20" y2="8" stroke="black" stroke-width="2"/>
                                    <line x1="8" y1="8"  x2="20" y2="20" stroke="black" stroke-width="2"/>
                                </svg>
                        </div>
                    </div>`;

        $('#wrapper-options').append(html);
    })

    $(document).on("change",'#field-type',function() {
        let value = $(this).val();
        if(value == 'select') {
            $(this).parent().parent().siblings('#options-select').removeClass('hidden');
        }
        else {
            $(this).parent().parent().siblings('#options-select').addClass('hidden');
        }   
    });

    $(document).on("click",'#remove-field',function() {
        $(this).parent().remove();
    });

});

</script>
@endsection