@extends('layouts.app')

@section('template_title')
    {!! trans('config.title') !!}
@endsection

@section('template_linked_css')
@endsection

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-11 col-md-9 col-lg-7">
                <div class="card-p">
                    <div class="flex flex-wrap border-b justify-between pb-1">
                        <h4 class="font-bold">  {!! trans('config.title') !!} </h4>
                    </div>

                    <div class="w-full pt-2">
                        {!! Form::open(array('route' => 'config.store', 'method' => 'POST', 'role' => 'form','class' => 'needs-validation')) !!}

                            {!! csrf_field() !!}

                            <div class="border-b pt-2 my-2 pb-1">
                                <h6 class="font-semibold"> {{ trans('config.globalConfig') }} </h6>
                            </div>
                            {{-- Verify Users --}}
                            <div class="col-12 mb-2 px-0 form-group has-feedback">
                                    <label class="col-12 flex items-center font-semibold">
                                        <input name="verifyUsers" class="mr-2 form-checkbox" {{ $verifyUsers->value ? 'checked' : '' }} type="checkbox">
                                        <span class="">  {{ trans('config.verifyUsers') }} </span>
                                    </label>    
                            </div>

                            {{-- Verify Scout Clubs --}}
                            <div class="col-12 mb-1 px-0 form-group has-feedback">
                                    <label class="col-12 flex items-center font-semibold">
                                        <input name="verifyScoutsClubs" class="mr-2 form-checkbox" {{ $verifyScoutsClubs->value ? 'checked' : '' }} type="checkbox">
                                        <span class="">  {{ trans('config.verifyScoutsClubs') }} </span>
                                    </label>    
                            </div>
                            <div class="mb-2 mx-2 rounded px-2 py-1 bg-gray-300 flex items-center">
                                <i class="fa fa-info-circle"></i>
                                <span class="pl-2"> {{ trans('config.infoVerifyUsers') }} </span>
                            </div>

                            {{-- Verify Clubs --}}
                            <div class="col-12 mb-1 px-0 form-group">
                                    <label class="col-12 flex items-center font-semibold">
                                        <input name="verifyClubs" class="mr-2 form-checkbox" {{ $verifyClubs->value ? 'checked' : '' }} type="checkbox">
                                        <span class="">  {{ trans('config.verifyClubs') }} </span>
                                    </label>    
                            </div>
                            <div class="mb-2 mx-2 rounded px-2 py-1 bg-gray-300 flex items-center">
                                <i class="fa fa-info-circle"></i>
                                <span class="pl-2"> {{ trans('config.infoVerifyClubs') }} </span>
                            </div>

                            {{-- Section Clubs in Home  --}}
                            <div class="col-12 mb-1 px-0 form-group">
                                    <label class="col-12 flex items-center font-semibold">
                                        <input name="sectionClubs" class="mr-2 form-checkbox" {{ ($sectionClubs && $sectionClubs->value) ? 'checked' : '' }} type="checkbox">
                                        <span class="">  {{ trans('config.showClubsInHome') }} </span>
                                    </label>    
                            </div>
                            {{-- Limit Images plan Athlete Free --}}
                            <div class="mb-1 mt-2 col-12 form-group">
                                <label for=""> {{ trans('config.limitImagesAtleteFree') }} </label>
                                {!! Form::text('limitImages', $limitImages, array('class' => 'w-full md:w-1/3 lg:w-1/4 form-control' )) !!} 
                            </div>
                            {{-- Limit Images plan SCOUT Free --}}
                            <div class="mb-1 mt-2 col-12 form-group">
                                    <label for=""> {{ trans('config.limitImagesScoutFree') }} </label>
                                    {!! Form::text('limitImagesScout', $limitImagesScout, array('class' => 'w-full md:w-1/3 lg:w-1/4 form-control' )) !!} 
                                </div>


                            <div class="border-b pt-2 my-2 pb-1">
                                <h6 class="font-semibold"> {{ trans('config.contactInfo') }} </h6>
                            </div>
                            <div class="row col-12 px-0">
                                <div class="col-12 col-md-6 form-group">
                                        <label for=""> {{ trans('config.contactEmail') }} </label>
                                        {!! Form::text('email', $email, array('class' => 'form-control' )) !!} 
                                    </div>
                                <div class="col-12 col-md-6 form-group">
                                    <label for=""> {{ trans('config.contactPhone') }} </label>
                                    {!! Form::text('phone', $phone, array('class' => 'form-control' )) !!} 
                                </div>
                            </div>


                            <div class="w-full flex flex-wrap justify-between items-end">
                                <div>
                                    <a class="btn btn-sm btn-outline-secondary rounded-full mr-1" href="{{url('/config/tyc')}}"> 
                                        <i class="fa fa-pencil fa-fw" aria-hidden="true"></i>
                                        <span>{{ trans('config.terms') }} </span>
                                    </a>
                                    <a class="btn btn-sm btn-outline-secondary rounded-full mr-1" href="{{url('/config/privacy')}}"> 
                                        <i class="fa fa-pencil fa-fw" aria-hidden="true"></i>
                                        <span>{{ trans('config.privacy') }} </span>
                                    </a>
                                </div>
                                <div>
                                    {!! Form::button(trans('config.save'), array('class' => 'btn btn-success rounded-full px-6','type' => 'submit' )) !!}
                                </div>
                            </div>
                            {!! Form::close() !!}
                    </div>

                </div>
            </div>
        
        
        </div>
    </div>

@endsection

@section('footer_scripts')

@endsection