@extends('layouts.app')

@section('template_title')
    {!! trans('reports.title') !!}
@endsection

@section('template_linked_css')
    <style type="text/css" media="screen">
        .users-table {
            border: 0;
        }
        .users-table tr td:first-child {
            padding-left: 15px;
        }
        .users-table tr td:last-child {
            padding-right: 15px;
        }
        .users-table.table-responsive,
        .users-table.table-responsive table {
            margin-bottom: 0;
        }
    </style>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card-p">

                        <div class="flex flex-wrap justify-between items-center pb-2 border-b mb-2">
                            <div class="flex flex-wrap items-center">
                                <h4 class="font-bold pr-4"> {!! trans('reports.title') !!}</h4>

                                <div class="w-auto"> 
                                    <span class="badge badge-primary rounded-full text-white">{{ trans('reports.reportTotal') }}: {{ $reports->total() }} </span>
                                </div>
                            </div>
                    </div>

                    <div class="w-full">
                        <div class="table-responsive users-table">
                            <table class="table table-striped table-sm vertical-middle data-table primary">
                                
                                <thead class="thead">
                                    <tr>
                                        <th>{!! trans('reports.table.type') !!}</th>
                                        <th>{!! trans('reports.table.createdBy') !!}</th>
                                        <th>{!! trans('reports.table.created') !!}</th>
                                        {{-- <th>{!! trans('articles.articles-table.updated') !!}</th> --}}
                                        <th>{!! trans('articles.articles-table.actions') !!}</th>
                                    </tr>
                                </thead>
                                <tbody id="users_table">
                                    @foreach($reports as $c)
                                        <tr>
                                            <td>
                                                <span> {{ trans('reports.types.'.$c->tipo) }} </span>
                                                
                                                @if($c->solved)
                                                    <span class="rounded text-xs ml-2 p-1 bg-success text-white">
                                                        {{ trans('reports.solved') }}
                                                    </span>
                                                @endif
                                                @if($c->reported_blocked)
                                                    <span class="rounded text-xs ml-2 p-1 bg-danger text-white">
                                                        {{ trans('reports.types.'.$c->tipo) }} {{ trans('reports.deleted') }}
                                                    </span>
                                                @endif

                                            </td>
                                            <td>
                                                {{ $c->user->fullName }}
                                            </td>
                                            <td>
                                                @if(isset($c->created_at))
                                                    {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$c->created_at)->format(config('settings.formatDate'))  }}
                                                @endif
                                            </td>
                                            {{-- <td>{{$c->updated_at}}</td> --}}
                                            
                                            <td class="buttons-actions">
                                                <a class="btn btn-sm btn-outline-success rounded-full" href="{{ route('admin.reports.show',[$c->id]) }}" data-toggle="tooltip" title="Show">
                                                    {!! trans('reports.buttons.show') !!}
                                                </a>
{{--                                             
                                                {!! Form::open(array('url' => 'reports/' . $c->id, 'class' => 'form-inline', 'data-toggle' => 'tooltip', 'title' => 'Delete')) !!}
                                                    {!! Form::hidden('_method', 'DELETE') !!}
                                                    {!! Form::button(trans('reports.buttons.delete'), array('class' => 'btn btn-outline-danger btn-sm rounded-full','type' => 'button','data-toggle' => 'modal', 'data-target' => '#confirmDelete', 'data-title' => 'Delete Report', 'data-message' => 'Are you sure you want to delete this report ?')) !!}
                                                {!! Form::close() !!} --}}

                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>

                            </table>

                            
                        </div>
                        @if(config('usersmanagement.enablePagination'))
                            <div class="w-full mt-4">
                                {{ $reports->links() }}
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('modals.modal-delete')

@endsection

@section('footer_scripts')
    @include('scripts.delete-modal-script')
    @include('scripts.save-modal-script')
@endsection
