@extends('layouts.app')

@section('template_title')
    {!! trans('clubes.showing-all-clubs') !!}
@endsection

@section('template_linked_css')
<style>
    .input-search-users,
    .input-search-users input,
    .input-search-users button {
        height: 37px;   
    }
    .input-search-users button img {
        width: 24px;
    }
</style>

@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card-p">

                    <div class="flex flex-wrap justify-between items-center pb-2 border-b mb-2">
                        <div class="flex flex-wrap items-center">
                        <h4 class="font-bold pr-4"> 
                            {{-- {!! trans('clubes.types.clubs') !!} /  {!! trans('clubes.types.institutes') !!} / {!! trans('clubes.types.universities') !!} / {!! trans('clubes.types.sponsors') !!}  --}}
                            {!! trans('titles.adminClubesList') !!}
                        </h4>

                                <div class="w-auto"> 
                                    <span class="badge badge-primary rounded-full text-white">{{ trans('clubes.clubTotal') }}: {{ $clubes->total() }} </span>
                                </div>
                            </div>

                            <div class="mt-2 sm:mt-0 btn-group btn-group-xs">
                                {{-- <div class="dropdown-menu dropdown-menu-right"> --}}
                                    <a class="btn btn-gray rounded-full" href="/clubes/create">
                                        <i class="fa fa-plus fa-fw"></i>
                                        {!! trans('clubes.buttons.create-new') !!}
                                    </a>
                                {{-- </div> --}}
                            </div>
                    </div>

                    <div class="w-full">
                        {!! Form::open(array('route' => 'clubes.index', 'method' => 'GET','id' => 'formSearchNavbar', 'class' => 'w-full relative')) !!}
                            <div class="w-full md:w-1/2 my-4">
                                <div class="input-group input-search-users">
                                    <input id="input-navbar" class="hover:border-primary border-primary form-control" value="{{ request()->input('nameFilter') }}" name="nameFilter" type="text" placeholder="{{ trans('profile.filters.search') }}">
                                    {{-- {!! Form::text('user_search_box', NULL, ['id' => 'user_search_box', 'class' => 'form-control', 'placeholder' => trans('usersmanagement.search.search-users-ph'), 'aria-label' => trans('usersmanagement.search.search-users-ph'), 'required' => false]) !!} --}}
                                    <div class="input-group-append">
                                        <button type="submit" class="input-group-addon btn px-2 btn-primary rounded-br-lg rounded-tr-lg">
                                                <img src="{{ asset('img/search.png') }}" alt="Search">
                                        </button>
                                    </div>
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>

                    <div class="w-full mt-4">

                        {{-- @if(config('clubes.enableSearchUsers'))
                            @include('partials.search-users-form')
                        @endif --}}

                        <div class="table-responsive users-table">
                            <table class="table table-striped table-sm vertical-middle data-table primary">
                                
                                <thead class="thead">
                                    <tr>
                                        <th>{!! trans('clubes.clubes-table.name') !!}</th>
                                        <th>{!! trans('clubes.clubes-table.type') !!}</th>
                                        <th>{!! trans('clubes.clubes-table.email') !!}</th>
                                        <th>{!! trans('clubes.clubes-table.created') !!}</th>
                                        {{-- <th>{!! trans('clubes.clubes-table.updated') !!}</th> --}}
                                        <th>{!! trans('clubes.clubes-table.actions') !!}</th>
                                        <th class="no-search no-sort"></th>
                                        <th class="no-search no-sort"></th>
                                    </tr>
                                </thead>
                                <tbody id="users_table">
                                    @foreach($clubes as $c)
                                        <tr>
                                            <td class="flex items-center w-40">
                                                <img class="img-fluid h-10" src="{{ $c->avatar }}" alt="{{ $c->name }}">
                                                <span class="pl-2"> {{$c->name}} </span> 
                                            </td>
                                            <td> 
                                                <span class="block"> 
                                                    @if(isset($c->type))
                                                        {{  trans('clubes.types.' . $c->type->name) }}
                                                    @endif
                                                    {{-- @if(isset($c->type)) 
                                                        @if($c->type->name == 'Club')
                                                            <div class="flex">    
                                                                <div> {{$c->type->name}} </div>
                                                                <div class="pl-2">
                                                                    <span class="rounded-full border border-success text-success"> <i class="fa fa-check fa-fw" aria-hidden="true"></i> </span>   
                                                                </div>
                                                            </div>
                                                            @else
                                                            <div class="flex">
                                                                <div> Club </div>
                                                                <div class="pl-2">
                                                                    <span class="rounded-full border border-primary text-primary"> <i class="fa fa-close fa-fw" aria-hidden="true"></i> </span>   
                                                                </div>
                                                            </div>
                                                        @endif
                                                    @else
                                                        <div class="flex">
                                                            <div> Club </div>
                                                            <div class="pl-2">
                                                                <span class="rounded-full border border-primary text-primary"> <i class="fa fa-close fa-fw" aria-hidden="true"></i> </span>   
                                                            </div>
                                                        </div>
                                                    @endif  --}}
                                                </span> 
                                            </td>
                                            <td>{{$c->email}}</td>
                                            <td> 
                                                @if($c->created_at) 
                                                    {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$c->created_at)->format(config('settings.formatDate')) }} 
                                                @endif
                                            </td>
                                            {{-- <td>{{$c->updated_at}}</td> --}}
                                            
                                            <td class="buttons-actions">
                                                <a class="btn btn-sm btn-outline-success rounded-full" href="{{ route('clubes.show',[$c->id]) }}" data-toggle="tooltip" title="Show">
                                                    {!! trans('clubes.buttons.show') !!}
                                                </a>
                                                <a class="btn btn-sm btn-outline-info rounded-full" href="{{ route('clubes.edit',[$c->id]) }}" data-toggle="tooltip" title="Edit">
                                                    {!! trans('clubes.buttons.edit') !!}
                                                </a>

                                                {!! Form::open(array('url' => 'clubes/' . $c->id, 'class' => 'form-inline', 'data-toggle' => 'tooltip', 'title' => 'Delete')) !!}
                                                    {!! Form::hidden('_method', 'DELETE') !!}
                                                    {!! Form::button(trans('clubes.buttons.delete'), array('class' => 'btn btn-outline-danger rounded-full btn-sm','type' => 'button', 'style' =>'width: 100%;' ,'data-toggle' => 'modal', 'data-target' => '#confirmDelete', 'data-title' => trans('clubes.messageDeleteTitle'), 'data-message' => trans('clubes.messageDelete'))) !!}
                                                {!! Form::close() !!}
                                                @if( $verifyClubs && $verifyClubs->value ) <!-- Table config_site -->
                                                    @if(!$c->verified)
                                                    {!! Form::open(array('url' => 'clubes/' . $c->id. '/verify', 'class' => 'form-inline', 'data-toggle' => 'tooltip', 'title' => 'Verify')) !!}
                                                        {!! Form::hidden('_method', 'POST') !!}
                                                        {!! Form::button(trans('clubes.buttons.verify'), array('class' => 'btn btn-success rounded-full btn-sm','type' => 'submit', 'style' =>'width: 100%;' , 'title' => 'Verify')) !!}
                                                    {!! Form::close() !!}
                                                    @else
                                                    {!! Form::open(array('url' => 'clubes/' . $c->id. '/verify', 'class' => 'form-inline', 'data-toggle' => 'tooltip', 'title' => 'Unverify')) !!}
                                                        {!! Form::hidden('_method', 'POST') !!}
                                                        {!! Form::button(trans('clubes.buttons.unverify'), array('class' => 'btn btn-primary rounded-full btn-sm','type' => 'submit', 'style' =>'width: 100%;' , 'title' => 'Unverify')) !!}
                                                    {!! Form::close() !!}
                                                    @endif
                                                @endif

                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>

                            </table>

                            
                        </div>
                        @if(config('usersmanagement.enablePagination'))
                        <div class="w-full mt-4">
                            {{ $clubes->appends(Request::only('nameFilter'))->links() }}
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('modals.modal-delete')

@endsection

@section('footer_scripts')
    @if ((count($clubes) > config('clubes.datatablesJsStartCount')) && config('clubes.enabledDatatablesJs'))
        @include('scripts.datatables')
    @endif
    @include('scripts.delete-modal-script')
    @include('scripts.save-modal-script')
    @if(config('clubes.tooltipsEnabled'))
        @include('scripts.tooltips')
    @endif
    {{-- @if(config('clubes.enableSearchUsers'))
        @include('scripts.search-users')
    @endif --}}
@endsection
