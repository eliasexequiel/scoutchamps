@extends('layouts.app')

@section('template_title')
    {!! trans('showcase.title') !!}
@endsection

@section('template_linked_css')
    
@endsection

@section('content')
<div class="container-fluid">
        <div class="w-full flex justify-center">
            <div class="w-ful md:w-4/5">
                <div class="card-p">

                        <div class="flex flex-wrap justify-between items-center pb-2 border-b mb-2">
                               <div class="flex flex-wrap items-center">
                                    <h4 class="pr-4"> <span class="font-bold"> 
                                        {!! trans('showcase.admin.table.athlete') !!}: </span>
                                        {{ $user->fullname }} 
                                    </h4>
                                    <div class="w-auto"> 
                                        <span class="pr-1"> {{ trans('showcase.admin.countAssociations') }}: </span>
                                        <span class="badge badge-primary rounded-lg text-white"> {{ $user->athlete->entitiesPrefered->count() }} </span>
                                    </div>
                                </div>
                                <div class="pull-right">
                                    <a href="{{ route('admin.showcase.index') }}" class="btn btn-light btn-sm float-right" title="{{ trans('showcase.admin.back-showcase') }}">
                                        <i class="fa fa-fw fa-reply-all" aria-hidden="true"></i>
                                        {!! trans('showcase.admin.buttons.back-to-showcase') !!}
                                    </a>
                                </div>
                        </div>

                        @if(count($user->athlete->entitiesPrefered) > 0)
                        {!! Form::open(array('route' => 'admin.showcase.store', 'method' => 'POST', 'role' => 'form','class' => 'w-full')) !!}
                            <div class="w-full flex flex-wrap">
                                @foreach ($user->athlete->entitiesPrefered as $ep)
                                <div class="col-12 col-lg-6 p-2">
                                <div class="border border-primary py-1 px-2 col-12">
                                        
                                        <div class="border-b pb-1">
                                            <strong> {{ trans('showcase.association') }}: </strong> {{ trans('clubes.types.' . $ep->entity->type->name) }}
                                        </div>
                                        
                                        <div class="border-b pb-1 pt-1">
                                            <strong>  {{ trans('clubes.forms.name') }}: </strong> {{ $ep->entity->name }}
                                        </div>
                                        
                                        <input type="hidden" name="athlete_id" value="{{ $user->athlete->id }}">
                                        <div class="w-full flex items-center pt-1">
                                            <div class="pr-2"> <strong> {{ trans('showcase.admin.contacted') }}: </strong> </div>
                                            <div>
                                                <label class="w-full flex items-center font-semibold">
                                                    <input {{ $ep->contacted ? 'checked ' : '' }} name="showcase[{{ $ep->id }}]" class="mr-2 form-checkbox" type="checkbox">
                                                </label>
                                            </div> 
                                        </div>
                                            
                                </div>
                                </div>
                                @endforeach
                                <div class="flex w-full justify-end mt-2 px-2">
                                        {!! Form::button(trans('showcase.admin.buttons.store'), array('class' => 'btn btn-success rounded-full','type' => 'submit' )) !!}
                                </div>
                            </div>
                            {!! Form::close() !!}
                            @else 
                                <div>
                                    {{ trans('showcase.admin.noEntities') }}
                                </div>
                            @endif
                    </div>
                </div>
            </div>
</div>
@endsection