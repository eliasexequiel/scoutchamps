@extends('layouts.app')

@section('template_title')
    {!! trans('showcase.title') !!}
@endsection

@section('template_linked_css')
    
@endsection

@section('content')
<div class="container-fluid">
        <div class="w-full flex justify-center">
            <div class="w-full sm:w-4/5 md:w-3/4">
                <div class="card-p">

                        <div class="w-full">

                                <div class="table-responsive users-table">
                                    <table class="table table-striped table-sm data-table primary">
                                        
                                        <thead class="thead">
                                            <tr>
                                                <th>{!! trans('showcase.admin.table.athlete') !!}</th>
                                                <th>{!! trans('showcase.admin.table.plan') !!}</th>
                                                <th>{!! trans('showcase.admin.table.association') !!}</th>
                                                <th>{!! trans('showcase.admin.table.actions') !!}</th>
                                                {{-- <th class="no-search no-sort"></th> --}}
                                                {{-- <th class="no-search no-sort"></th> --}}
                                            </tr>
                                        </thead>
                                        <tbody id="users_table">
                                            @foreach($users as $u)
                                                @php
                                                    $subActive = $u->subscriptions()->whereNull('ends_at')->first();
                                                    if($subActive) $subscription = \Stripe\Subscription::retrieve($subActive->stripe_id);
                                                    if($subActive) $planName =  $subscription->plan->nickname;
                                                    else $planName = '';
                                                @endphp
                                                <tr>
                                                    <td>
                                                        <span class="font-semibold"> {{$u->fullname}} </span>
                                                        {{-- <span class="block text-sm text-gray-600"> {{ $c->id }} </span> --}}
                                                    </td>
                                                    <td>
                                                        {{ $planName }}
                                                    </td>
                                                    <td>
                                                        {{ trans('showcase.admin.table.associationsTotal',['used' => $u->athlete->entitiesPrefered->count(),'total' => $u->entitiesPreferedTotal()]) }}
                                                    </td>
                                                    <td style="min-width: 80px;">
                                                            <a class="btn btn-sm btn-outline-success rounded-full" href="{{ route('admin.showcase.show',[$u->id]) }}" data-toggle="tooltip" title="Show">
                                                                {!! trans('showcase.admin.buttons.show') !!}
                                                            </a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
        
                                    </table>
        
                                </div>
                        </div>
                </div>
            </div>
        </div>
</div>
@endsection