@extends('layouts.app')

@section('template_title')
{!! trans('subscriptions.mysubscriptions.createPaymentMethod') !!}
@endsection

@section('template_linked_css')
<style type="text/css">
.StripeElement {
    box-sizing: border-box;
    width: 100%;
    color: #495057;
    height: 37px;
    padding: 0.5rem 0.75rem;
    border: 1px solid #cacaca;
    border-radius: .5rem;
    background-color: white;
  }
  
  .StripeElement--focus {
    box-shadow: 1px solid #000;
  }
  
  .StripeElement--invalid {
    border-color: #fa755a;
  }
  
  .StripeElement--webkit-autofill {
    background-color: #fefde5 !important;
  }

  ::-webkit-input-placeholder { /* Edge */
    color: #606f7b;
  }

  :-ms-input-placeholder { /* Internet Explorer 10-11 */
    color: #606f7b;
  }

  ::placeholder {
    color: #606f7b;
  }

</style>
@endsection

@section('content')
<div class="container-fluid">
    <div class="w-full flex justify-center">
        <div class="w-full sm:w-5/6 lg:w-2/5">
            <div class="card-p">
                <div class="flex justify-between items-center pb-2 border-b mb-2">
                    <div class="flex flex-wrap items-center">
                        <h4 class="font-bold"> {!! trans('subscriptions.mysubscriptions.addPaymentMethod') !!}</h4>
                    </div>
                    <div class="pull-right">
                        <a href="{{ route('mysubcriptions.paymentmethods') }}" class="btn btn-light btn-sm float-right"
                            title="{{ trans('subscriptions.mysubscriptions.paymentsMethods') }}">
                            <i class="fa fa-fw fa-reply-all" aria-hidden="true"></i>
                            {{ trans('subscriptions.mysubscriptions.paymentsMethods') }}
                        </a>
                    </div>
                </div>
                <div class="w-full">

                    <div class="spinner flex items-center h-24">
                        <spinner class="mx-auto"></spinner>
                    </div>

                    <form class="hidden" action="{{ route('mysubcriptions.paymentmethods.store') }}" method="post" id="payment-form">
                        @csrf

                        <div class="form-group">
                            <label for="cardholder-name"> {{ trans('subscriptions.mysubscriptions.fields.cardholderName') }}
                            </label>
                            <input class="form-control" name="cardholder-name" id="cardholder-name" type="text" placeholder="{{ trans('subscriptions.mysubscriptions.fields.cardholderName') }}">
                        </div>
                        <!-- placeholder for Elements -->
                        <div id="card-element"></div>

                        <div class="w-full pt-2 flex justify-end">
                            <button type="button" class="btn btn-dark rounded-full" id="card-button" data-secret="{{ $setup_intent->client_secret }}"> {{ trans('subscriptions.mysubscriptions.saveCard') }} </button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer_scripts')
<script src="https://js.stripe.com/v3/"></script>

<script type="text/javascript">

    var stripe = Stripe('pk_test_nU1v7q93fZpiVAgObGOzxbx700lIJ8brh6');

    var elements = stripe.elements();
    var cardElement = elements.create('card');
    cardElement.mount('#card-element');

    $('.spinner').addClass('hidden');
    $('#payment-form').removeClass('hidden');


    var cardholderName = document.getElementById('cardholder-name');
    var cardButton = document.getElementById('card-button');
    var clientSecret = cardButton.dataset.secret;

    cardButton.addEventListener('click', function (ev) {
        stripe.handleCardSetup(
            clientSecret, cardElement, {
            payment_method_data: {
                billing_details: { name: cardholderName.value }
            }
        }
        ).then(function (result) {
            if (result.error) {
                // Display error.message in your UI.
            } else {
                // console.log(result);
                var form = document.getElementById('payment-form');
                var hiddenInput = document.createElement('input');
                hiddenInput.setAttribute('type', 'hidden');
                hiddenInput.setAttribute('name', 'payment_method');
                hiddenInput.setAttribute('value', result.setupIntent.payment_method);
                form.appendChild(hiddenInput);

                // Submit the form
                form.submit();
                // The setup has succeeded. Display a success message.
            }
        });
    });

</script>

@endsection