@extends('layouts.app')

@section('template_title')
    {!! trans('titles.detailRecruitment') !!}
@endsection

@section('content')
    <div class="mx-6">
        <p-view-recruitment :recruitment="{{ json_encode($recruitment) }}" 
                            :invitation-recruitment="{{ json_encode($invitation) }}">
        </p-view-recruitment>
    </div>
@endsection
