@extends('layouts.app')

@section('template_title')
    {!! trans('profile.results') !!}
@endsection

@section('template_fastload_css')
@endsection

@section('content')
    <div class="w-full flex flex-wrap px-6">
        @if(count($users) > 0)
        @foreach ($users as $user)
            @if($user->roles[0]->name != 'Admin')
            <div class="w-full md:w-1/2 sm:px-2 pb-4">
                <div class="block justify-start sm:flex items-center sm:justify-between card-p h-full">
                    <div class="w-full flex flex-wrap sm:flex-no-wrap md:flex-wrap sm:justify-between items-center">
                        <div class="w-full sm:w-3/5 md:w-full lg:w-3/5 xl:4/5 flex lg:flex-1">
                            <div class="pr-2 w-24">
                                @include('partials.avatar-user',['user' => $user, 'size' => 'h-20 w-20'])
                            </div>         
                            <div class="w-auto flex flex-wrap items-center">
                                <div class="w-auto">  
                                        @include('partials.user-card-information',['sizefullname' => 'text-lg xl:text-xl','link' => true])
                                </div>
                            </div>
                        </div>
                        <div class="pt-4 sm:pt-1 sm:pl-1">
                            <div class="flex items-center flex-row sm:flex-col md:flex-row lg:flex-col text-center flex-wrap">
                                    @if($user->roles[0]->name == 'Scout')
                                        <div class="pl-2"> <span class="badge text-sm badge-primary rounded-lg"> {{trans('titles.Scout') }} </span> </div>
                                    @endif
                                    @if($user->roles[0]->name == 'Athlete')
                                        <div class="pl-2"> <span class="badge text-sm badge-success rounded-lg"> {{trans('titles.Athlete') }} </span> </div>
                                    @endif
                                    <div class="pl-2 mt-1">
                                        <a class="btn btn-sm btn-outline-info rounded-full" href="{{ $user->publicLink }}">
                                            <i class="fa fa-eye fa-fw" aria-hidden="true"></i> 
                                            {{ trans('profile.viewProfile') }} 
                                        </a>
                                    </div>
                            </div>   
                        </div>   
                    </div>
                </div>
                </div>
            @endif

            
            @endforeach
            <div class="w-full mt-3">
                    <div class="w-full mb-2">
                            <span class="text-base"> {{ trans('profile.totalUsers') }}  :  {{ $users->total() }} </span> 
                    </div>
                    <div class="w-full mb-2">
                        @role('scout')
                            {{-- {{ $users->appends(Request::only('entity','typeUser','fullname','sport','gender','age','status','sporting_goal','nationality','residence_country','sport_Posición_primaria','sport_Posición_secundaria','sport_Altura'))->links() }} --}}
                            {{ $users->appends(Request::all())->links() }}
                        @endrole
                        @role('admin|athlete', false)
                            {{ $users->appends(Request::only('entity','scout_speciality','fullname'))->links() }}
                        @endrole
                    </div>
            </div>
        @else
        <div class="flex w-full items-center justify-center card-p">
            <div class="text-lg"> {{ trans('profile.noResults') }} </div>
        </div>
        @endif
    </div>
@endsection

@section('footer_scripts')
@append