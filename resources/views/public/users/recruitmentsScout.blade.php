<div class="card-p my-4 sm:mt-0 w-full overflow-hidden">
        <h5 class="font-bold mb-2"> {{ trans('profile.publicRecruitments') }} </h5>
        <div class="w-full pt-2">
            @php
                $publicRecruitments = $user->recruitments->filter(function($r) {
                    return $r->is_public; 
                });
            @endphp
            @if(count($publicRecruitments) == 0)
                <div>
                    {{ trans('dashboard.recruitments.noRecruitments') }}
                </div>
            @endif

            @foreach ($publicRecruitments as $r)
            {{-- @if($r->is_public) --}}
                <div class="flex justify-between items-center text-sm w-full mb-2 border-b pb-2">
                    <div class="sm:w-auto md:w-1/2 lg:w-3/4">
                            <div> 
                                <span class="text-sm text-gray-900">   {{ trans('recruitments.invitation.place') }} : </span>
                                <span class="text-gray-700"> {{ $r->place }} </span>
                            </div>
                            @if($r->addressComplete)
                            <div> 
                                <span class="text-sm text-gray-900">   {{ trans('recruitments.invitation.address') }} : </span>
                                <a href="http://maps.google.com/maps?q={{ $r->addressComplete }}" class="text-gray-700"> {{ $r->addressComplete }} </a>
                            </div>
                            @endif
                            <div> 
                                <span class="text-sm text-gray-900"> {{ trans('recruitments.invitation.date') }} </span>
                                <span class="text-gray-700">  {{$r->day_start}} {{ $r->day_end ? ' - '.$r->day_end : '' }} </span> 
                            </div>
                            <div> 
                                <span class="text-sm text-gray-900"> {{ trans('recruitments.invitation.hour') }} </span>
                                <span class="text-gray-700">  {{ $r->hour }} </span> 
                            </div>
                    </div>
                    @if(Auth::user()->hasRole('athlete'))
                        <div class="w-auto">
                            <a class="my-1 btn btn-sm btn-outline-success rounded-full" title="Show" href="{{ url('p/recruitment/'.$r->id) }}"> {!! trans('recruitments.buttons.show') !!} </a>
                        </div>
                    @endif
                </div>
            {{-- @endif --}}
            @endforeach
        </div>
</div>