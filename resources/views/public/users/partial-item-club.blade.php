<div class="border-b last:border-0 w-full">
    <div class="flex items-center py-2">
        <div class="mb-0">
            <img class="h-20 w-20 float-left object-contain" src="{{ $c->entity->avatar }}" alt="{{ $c->entity->name }}">
        </div>
        <div class="pl-2"> 
            @if($c->entity->type)
                <div class="text-sm font-normal leading-none">{{ trans('clubes.types.'.$c->entity->type->name) }} </div>
            @endif
            <div class="text-base font-semibold">{{ $c->entity->name }} </div> 
        </div>
    </div>
  
    <div class="pb-2">
            @if($c->start)
                <div class="">
                <span class="pr-2"> {{ trans('profile.athlete.from') }}: {{ $c->start }} </span>
                @if(!$c->isCurrent) 
                    <span> {{trans('profile.athlete.to')}}: {{ $c->end }} </span> 
                @else
                    <span> 
                        <span>{{trans('profile.athlete.to')}}: </span> 
                        <span class="badge badge-dark font-light text-sm rounded-lg"> {{ trans('profile.athlete.currentClub') }} </span>
                    </span>
                @endif
                </div>
            @endif
            @if($c->info_aditional)
                <div class="text-gray-700"> {!! $c->info_aditional !!} </div>
            @endif
            @if($user->athlete && count($c->achievements) > 0)
                <div class="py-2">
                    <h6 class="font-semibold"> {{ trans('profile.athlete.achievements') }} </h6>
                </div>
                @foreach ($c->achievementsReverse as $a)
                <div class="flex justify-between pr-2">    
                    <div class="font-semibold">{{ $a->name }} </div>
                    <div style="min-width: 115px;text-align: right;"> 
                        @if($a->month) {{ trans('forms.months.'.$a->month) }} @endif    
                        @if($a->year) {{  $a->year}} @endif
                    </div>
                </div> 
                @endforeach  
            @endif
    </div>
</div>