<div class="card-p my-4 w-full overflow-hidden">
        <h5 class="font-bold mb-2"> {{ trans('profile.posts') }} </h5>
        <div class="w-full pt-2" style="max-height: 20rem;overflow-y: scroll;">
            
            @if(count($user->posts) == 0)
                <div>
                    {{ trans('dashboard.posts.noPosts') }}
                </div>
            @endif

            @foreach ($user->posts->chunk(10) as $chunk)
            @foreach ($chunk as $p)
            <div class="border-b py-1">
                    <div class="flex flex-wrap justify-between items-center">
                        <div class="block w-auto sm:w-1/2 md:w-3/5 lg:w-3/4"> 
                            <div>
                                @php
                                    $reg_exUrl = "/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/";
                                    $body = $p->body;
                                    if(preg_match_all($reg_exUrl, $p->body, $url)) {
                                        // dd($url);
                                        for($row = 0; $row < count($url[0]); $row++) {
                                            $urlFound = $url[0][$row];
                                            $body = str_replace("".$urlFound."", '<a target="_blank" href="' .$url[0][$row]. '">'.$url[0][$row] .'</a>', $body);
                                        }
                                    }
                                @endphp
                                <span class="text-prev"> {!! $body !!} </span>
                                <div class="text-xs text-gray-600">  
                                        {{ $p->timeAgo  }} 
                                </div>
                            </div>
                            <div class="flex">
                                <div class="flex items-center text-sm">
                                        <img class="icon-like" src="/img/icons/like.png" alt="Like">
                                        <span class="pl-2 text-like text-xs text-gray-900">
                                            <strong> {{ count($p->likes) }} </strong> 
                                            {{ trans_choice('dashboard.posts.likes',count($p->likes)) }}
                                        </span>
                                </div>
                                <div class="ml-4 flex items-center text-sm">
                                        <img class="icon-comment" src="/img/icons/comentario.png" alt="Comment">
                                        <span class="pl-2 text-comment text-xs text-gray-900">
                                                <strong> {{ count($p->comments) }} </strong> 
                                                {{ trans_choice('dashboard.posts.comments',count($p->comments)) }}
                                        </span>
                                </div>
                            </div>
                        </div>
                        <div class="w-auto">
                                <a class="btn btn-sm btn-outline-success rounded-full" href="{{ route('posts.show',[$p->id]) }}" data-toggle="tooltip" title="Show">
                                        {!! trans('posts.buttons.show') !!}
                                </a> 
                        </div>
                    </div>
                </div>
            @endforeach
            @endforeach
        </div>
</div>