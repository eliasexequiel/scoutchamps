<div class="w-full overflow-hidden card-p">
    <div class="flex items-center px-2 py-2">
    {{-- <div class="w-1/2 md:w-1/3 mx-auto"> --}}
        @include('partials.avatar-user',['user' => $user, 'size' => 'profile-md'])
        
        {{-- <img class="h-20 w-20 sm:h-24 sm:w-24 block object-cover rounded-full border" src="{{ $user->avatar }}" alt="{{ $user->fullName }}"> --}}
    {{-- </div> --}}
        <div class="ml-2 items-center w-full">
            <div class="flex flex-wrap items-center justify-between">
                <div class="mb-2 md:mb-0 flex flex-col">
                        <div class="mr-2 text-lg sm:text-xl lg:text-2xl leading-tight">{{ $user->fullName }} </div>
                
                        @if( $verifyScoutsClubs && $verifyScoutsClubs->value && $user->scout) <!-- Table config_site -->
                            {{-- <div> --}}
                                <div>
                                @if(!$user->verified)
                                    <div class="badge badge-danger text-white text-xs px-2 py-1 w-auto rounded-full">
                                        {{ trans('sidebar.userNotVerified') }}
                                    </div>
                                @else
                                    <div class="badge badge-success text-white text-xs px-2 py-1 w-auto rounded-full">
                                        {{ trans('sidebar.userVerified') }}
                                    </div>
                                @endif
                                </div>
                            {{-- </div> --}}
                        @endif
                </div>
                
                @if(count($user->roles) > 0)
                    @if($user->roles[0]->name == 'Scout')
                        <div> <span class="badge text-base badge-primary rounded-lg"> {{trans('titles.Scout') }} </span> </div>
                    @endif
                    @if($user->roles[0]->name == 'Athlete')
                        <div> <span class="badge text-base badge-success rounded-lg"> {{trans('titles.Athlete') }} </span> </div>
                    @endif
                @endif
            </div>
            
            
            <div class="hidden lg:flex lg:flex-wrap lg:justify-around pt-2">
                @if(Auth::user()->hasRole('admin'))
                    <div class="flex ml-2 sm:ml-0 items-center sm:block w-auto sm:text-center">
                        <span class="text-lg block text-primary"> {{ $user->visited_count() }} </span>
                        <button class="hover:underline" type="button" data-toggle="modal" data-target="#showModalVisitsProfile"> {{ trans('sidebar.visits.profile') }} </button>
                    </div>
                    <div class="flex ml-2 sm:ml-0 items-center sm:block w-auto sm:text-center">
                        <span class="text-lg block text-primary"> {{ $user->visited_scout_count() }} </span>
                        <button class="hover:underline" type="button" data-toggle="modal" data-target="#showModalScoutVisitedProfile"> {{ trans('sidebar.visits.scouts') }} </button>
                    </div>
                    <div class="flex ml-2 sm:ml-0 items-center sm:block w-auto sm:text-center">
                        <span class="text-lg block text-primary"> {{ $user->followers->count() }} </span>
                        <button class="hover:underline" type="button" data-toggle="modal" data-target="#showModalFollowersProfile"> {{ trans('sidebar.followers') }} </button>
                    </div>
                    <div class="flex ml-2 sm:ml-0 items-center sm:block w-auto sm:text-center">
                        <span class="text-lg block text-primary"> {{ $user->followed->count() }} </span>
                        <button class="hover:underline" type="button" data-toggle="modal" data-target="#showModalFollowingProfile"> {{ trans('sidebar.followed') }} </button>
                    </div>
                @else
                    <div class="flex ml-2 sm:ml-0 items-center sm:block w-auto sm:text-center">
                        <span class="text-lg block text-primary"> {{ $user->visited_count() }} </span>
                        <span class="ml-1 sm:ml-0 font-light"> {{ trans('sidebar.visits.profile') }} </span>
                    </div>
                    <div class="flex ml-2 sm:ml-0 items-center sm:block w-auto sm:text-center">
                        <span class="text-lg block text-primary"> {{ $user->visited_scout_count() }} </span>
                        <span class="ml-1 sm:ml-0 font-light"> {{ trans('sidebar.visits.scouts') }} </span>
                    </div>
                    <div class="flex ml-2 sm:ml-0 items-center sm:block w-auto sm:text-center">
                        <span class="text-lg block text-primary"> {{ $user->followers->count() }} </span>
                        <span class="ml-1 sm:ml-0 font-light"> {{ trans('sidebar.followers') }} </span>
                    </div>
                    <div class="flex ml-2 sm:ml-0 items-center sm:block w-auto sm:text-center">
                        <span class="text-lg block text-primary"> {{ $user->followed->count() }} </span>
                        <span class="ml-1 sm:ml-0 font-light"> {{ trans('sidebar.followed') }} </span>
                    </div>
                    
                @endif
            </div>
            {{-- @role('scout') --}}
                
            {{-- @endrole --}}
        </div>
    </div>
    <div class="flex flex-wrap justify-between lg:hidden pt-2">
        @if(Auth::user()->hasRole('admin'))
            <div class="flex ml-2 sm:ml-0 items-center sm:block w-auto sm:text-center">
                <span class="text-lg block text-primary"> {{ $user->visited_count() }} </span>
                <button class="ml-1 hover:underline" type="button" data-toggle="modal" data-target="#showModalVisitsProfile"> {{ trans('sidebar.visits.profile') }} </button>
            </div>
            <div class="flex ml-2 sm:ml-0 items-center sm:block w-auto sm:text-center">
                <span class="text-lg block text-primary"> {{ $user->visited_scout_count() }} </span>
                <button class="ml-1 hover:underline" type="button" data-toggle="modal" data-target="#showModalScoutVisitedProfile"> {{ trans('sidebar.visits.scouts') }} </button>
            </div>
            <div class="flex ml-2 sm:ml-0 items-center sm:block w-auto sm:text-center">
                <span class="text-lg block text-primary"> {{ $user->followers->count() }} </span>
                <button class="ml-1 hover:underline" type="button" data-toggle="modal" data-target="#showModalFollowersProfile"> {{ trans('sidebar.followers') }} </button>
            </div>
            <div class="flex ml-2 sm:ml-0 items-center sm:block w-auto sm:text-center">
                <span class="text-lg block text-primary"> {{ $user->followed->count() }} </span>
                <button class="ml-1 hover:underline" type="button" data-toggle="modal" data-target="#showModalFollowingProfile"> {{ trans('sidebar.followed') }} </button>
            </div>
        @else
            <div class="flex ml-2 items-center w-auto">
                <span class="text-lg block text-primary"> {{ $user->visited_count() }} </span>
                <span class="ml-1 font-light"> {{ trans('sidebar.visits.profile') }} </span>
            </div>
            <div class="flex ml-2 items-center w-auto">
                <span class="text-lg block text-primary"> {{ $user->visited_scout_count() }} </span>
                <span class="ml-1 font-light"> {{ trans('sidebar.visits.scouts') }} </span>
            </div>
            <div class="flex ml-2 items-center w-auto">
                <span class="text-lg block text-primary"> {{ $user->followers->count() }} </span>
                <span class="ml-1 font-light"> {{ trans('sidebar.followers') }} </span>
            </div>
            <div class="flex ml-2 items-center w-auto">
                <span class="text-lg block text-primary"> {{ $user->followed->count() }} </span>
                <span class="ml-1 font-light"> {{ trans('sidebar.followed') }} </span>
            </div>
        @endif
    </div>
    <div class="px-2 pt-3 flex flex-wrap w-full">
        @if(Auth::user()->id != $user->id)
        @permission('add.followed')
        <div class="pt-1">
                @if($followed)
                    {!! Form::open(array('url' => route('public.profile.unfollow',[$user->uuid]), 'class' => 'form-inline', 'data-toggle' => 'tooltip', 'title' => 'Unfollow')) !!}
                        {!! Form::hidden('_method', 'GET') !!}
                        {!! Form::button(trans('profile.unfollow'), array('class' => 'btn btn-gray capitalize','type' => 'button' ,'data-toggle' => 'modal', 'data-target' => '#confirmUnfollow', 'data-title' => 'Unfollow User', 'data-message' => trans('profile.questionUnfollow',['name' => $user->fullName]))) !!}
                    {!! Form::close() !!}    
                {{-- <a class="btn btn-gray capitalize" href="{{ route('public.profile.unfollow',[$user->uuid]) }}"> {{ __('profile.following') }} </a> --}}
                @else
                    <a class="btn btn-primary capitalize" href="{{ route('public.profile.follow',[$user->uuid]) }}"> {{ __('profile.follow') }} {{ trans('titles.'. $user->profileText) }} </a>
                @endif
        </div>
        @endpermission
        @permission('add.favorites')
        <div class="px-1 pt-1">
                {{-- Favorites --}}
                @if($isFavorite)
                    {!! Form::open(array('url' => route('public.profile.removeFavorite',[$user->uuid]), 'class' => 'form-inline', 'data-toggle' => 'tooltip', 'title' => 'Remove favorite')) !!}
                        {!! Form::hidden('_method', 'GET') !!}
                        {!! Form::button(trans('profile.addedFavorites'), array('class' => 'btn btn-gray-outline btn-favorite-remove capitalize','type' => 'button' ,'data-toggle' => 'modal', 'data-target' => '#confirmFavorite', 'data-title' => 'Remove Favorite', 'data-message' => trans('profile.questionRemoveFavorite',['name' => $user->fullName]))) !!}
                    {!! Form::close() !!}
                    {{-- <a class="btn btn-gray-outline btn-favorite-remove" href="{{ route('public.profile.removeFavorite',[$user->uuid]) }}"> {{ __('profile.addedFavorites') }} </a> --}}
                @else
                    <a class="btn btn-outline-primary btn-favorite-add" href="{{ route('public.profile.addFavorite',[$user->uuid]) }}"> {{ __('profile.favorites') }} </a>
                @endif
        </div>
        @endpermission
        <!-- Si el usuario actual es SCOUT y el usuario que estamos viendo es ATLETA permitimos enviar invitaciones -->
            @role('scout')
            <div class="pr-1 pt-1">
                @php 
                    $following = $user->followed->pluck('following_user_id')->all();
                    $isFollower = in_array(Auth::user()->id, $following);
                @endphp
                @if($user->athlete && ($followed || $isFollower))
                    <button type="button" data-toggle="modal" data-target="#sendInvitationsRecruitments" class="btn btn-info px-1"> {{ __('profile.inviteRecruitments') }} </button>
                @endif
            </div>
            {{-- Send Message --}}
            @if($followed)
                <div class="pt-1">
                    <a class="btn btn-success px-1 capitalize" href="{{ route('messages.index') . '?id=' . $user->id }}"> {{ __('profile.sendMessage') }} </a>
                </div>
            @else
                <div class="pt-1">
                    <button type="button" data-toggle="modal" data-target="#adviceFollowUserToSendMessage" class="btn btn-success px-1 capitalize"> {{ __('profile.sendMessage') }} </a>
                </div>
            @endif
            @endrole
            @role('admin')
            <div class="pt-1">
                <a class="btn btn-success px-1 capitalize" href="{{ route('messages.index') . '?id=' . $user->id }}"> {{ __('profile.sendMessage') }} </a>
            </div>
            @endrole

            @role('athlete')
                @php 
                    $following = $user->followed->pluck('following_user_id')->all();
                    $isFollower = in_array(Auth::user()->id, $following);
                @endphp
                @if($followed && $user->athlete)
                    <div class="pl-1 pt-1">
                        <a class="btn btn-success px-1 capitalize" href="{{ route('messages.index') . '?id=' . $user->id }}"> {{ __('profile.sendMessage') }} </a>
                    </div>
                @elseif($hasConversationInited && $followed && $user->scout)
                    <div class="pl-1 pt-1">
                        <a class="btn btn-success px-1 capitalize" href="{{ route('messages.index') . '?id=' . $user->id }}"> {{ __('profile.sendMessage') }} </a>
                    </div>
                @elseif(!$followed)    
                    <div class="pl-1 pt-1">
                        <button type="button" data-toggle="modal" data-target="#adviceFollowUserToSendMessage" class="btn btn-success px-1 capitalize"> {{ __('profile.sendMessage') }} </a>
                    </div>
                @elseif(!Auth::user()->isPremiun())
                    <div class=" pl-1 pt-1">
                        <button type="button" data-toggle="modal" data-target="#adviceUserPremiumToSendMessage" class="btn btn-success px-1 capitalize"> {{ __('profile.sendMessage') }} </a>
                    </div>
                @else
                    <div class="pl-1 pt-1">
                        <a class="btn btn-success px-1 capitalize" href="{{ route('messages.index') . '?id=' . $user->id }}"> {{ __('profile.sendMessage') }} </a>
                    </div>    
                @endif
            @endrole
        @endif
    </div>
<div class="px-2 w-full pt-2">

    <!-- ATHELTE -->
    @if($user->athlete)
    @can('view', $user) <!-- Policy -->
   
        <div class="pt-2 border-t w-full flex flex-wrap">
                
                @if($user->about)  
                    <div class="py-1 flex flex-wrap justify-between w-full">
                        <div class="font-semibold"> {{ trans('profile.athlete.about') }} </div>
                        @php
                            // $reg_exUrl = "/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/";
                            $about = $user->about;
                        @endphp
                        @if(strlen($user->about) > 160 || substr_count($about,'<p>') > 2)
                            <div class="aboutme-short w-full text-gray-700"> {!! $about !!} </div>
                            <div class="aboutme-large hidden w-full text-gray-700"> {!! $about !!} </div>
                            <div class="w-full">
                                <button type="button" class="text-primary btn-readmore hover:underline"> {{ trans('profile.readmore') }} </button>
                            </div>
                        @else
                            <div class="w-full text-gray-700"> {!! $about !!} </div>
                        @endif
                    </div>
                @endif

                <div class="flex flex-wrap w-full">
                    {{--  Si el usuario SCOUT sigue al atleta debe poder ver su TELEFONO y su CORREO --}}
                    @role('scout')
                        @php
                            $usersFollowed = \Auth::user()->followed->pluck('following_user_id')->all();
                            $isFollowed = in_array($user->id, $usersFollowed);    
                        @endphp
                        @if($user->athlete->age() >= 18)
                            <div class="py-1 flex justify-between w-full space-odd-item xl:w-1/2">
                                <div class="font-semibold"> {{ trans('profile.athlete.phone') }} </div>
                                @if($isFollowed)
                                    <div class="text-gray-700"> @if($user->profile->phone) +{{$user->profile->phone}} @else - @endif </div>
                                @else
                                    <a class="btn btn-primary btn-sm capitalize" href="{{ route('public.profile.follow',[$user->uuid]) }}"> {{ __('profile.follow') }} {{ trans('titles.'. $user->profileText) }} </a>
                                @endif
                            </div>
                            <div class="py-1 flex justify-between w-full space-odd-item xl:w-1/2">
                                <div class="font-semibold"> {{ trans('profile.athlete.email') }} </div>
                                @if($isFollowed)
                                    <div class="text-gray-700"> {{ $user->email }} </div>
                                @else
                                    <a class="btn btn-primary btn-sm capitalize" href="{{ route('public.profile.follow',[$user->uuid]) }}"> {{ __('profile.follow') }} {{ trans('titles.'. $user->profileText) }} </a>
                                @endif
                            </div>
                        @endif

                        {{-- El Scout puede ver el telefono y el email del tutor del atleta si lo sigue. --}}
                        @if($user->athlete->age() < 18)
                            <div class="py-1 flex justify-between w-full space-odd-item xl:w-1/2">
                                <div class="font-semibold"> {{ trans('profile.athlete.tutorEmail') }} </div>
                                @if($isFollowed)
                                    <div class="text-gray-700"> {{ $user->athlete->emailTutor ?? '-' }} </div>
                                @else
                                    <div style="min-width: 97px">
                                        <a class="btn btn-primary btn-sm capitalize" href="{{ route('public.profile.follow',[$user->uuid]) }}"> {{ __('profile.follow') }} {{ trans('titles.'. $user->profileText) }} </a>
                                    </div>
                                @endif
                            </div>
                            <div class="py-1 flex justify-between w-full space-odd-item xl:w-1/2">
                                <div class="font-semibold"> {{ trans('profile.athlete.tutorPhone') }} </div>
                                @if($isFollowed)
                                    <div class="text-gray-700"> @if($user->athlete->phoneTutor) +{{$user->athlete->phoneTutor}} @else - @endif </div>
                                @else
                                    <div style="min-width: 97px">
                                        <a class="btn btn-primary btn-sm capitalize" href="{{ route('public.profile.follow',[$user->uuid]) }}"> {{ __('profile.follow') }} {{ trans('titles.'. $user->profileText) }} </a>
                                    </div>
                                @endif
                            </div>
                        @endif
                    @endrole

                    @if(Auth::user()->hasRole('admin'))
                        @if($user->athlete->age() < 18)
                            {{-- <div class="flex flex-wrap">  --}}
                                <div class="py-1 flex justify-between w-full space-odd-item xl:w-1/2">
                                    <div class="font-semibold"> {{ trans('profile.athlete.tutorFirstName') }} </div>
                                    <div class="text-gray-700"> {{ $user->athlete->firstnameTutor }} </div>
                                </div>
                                <div class="py-1 flex justify-between w-full space-odd-item xl:w-1/2">
                                    <div class="font-semibold"> {{ trans('profile.athlete.tutorLastname') }} </div>
                                    <div class="text-gray-700"> {{ $user->athlete->lastnameTutor }} </div>
                                </div>
                                <div class="py-1 flex justify-between w-full space-odd-item xl:w-1/2">
                                    <div class="font-semibold"> {{ trans('profile.athlete.tutorEmail') }} </div>
                                    <div class="text-gray-700"> {{ $user->athlete->emailTutor }} </div>
                                </div>
                                <div class="py-1 flex justify-between w-full space-odd-item xl:w-1/2">
                                    <div class="font-semibold"> {{ trans('profile.athlete.tutorPhone') }} </div>
                                    <div class="text-gray-700"> @if($user->athlete->phoneTutor) +{{$user->athlete->phoneTutor}} @else - @endif </div>
                                </div>
                            {{-- </div> --}}
                        @endif
                    @endif

                    
                    @if($user->athlete->nationality)  
                    <div class="py-1 flex justify-between w-full space-odd-item xl:w-1/2">
                        <div class="font-semibold"> {{ trans('profile.athlete.nationality') }} </div>
                        <div class="text-gray-700"> {{ $user->athlete->nationality->name }} </div>
                    </div>
                    @endif
                    @if($user->athlete->secondNationality)
                    <div class="py-1 flex justify-between w-full space-odd-item xl:w-1/2">
                        <div class="font-semibold"> {{ trans('profile.athlete.secondNationality') }} </div>
                        <div class="text-gray-700"> {{ $user->athlete->secondNationality->name }} </div>
                    </div>
                    @endif
                    @if($user->athlete->residenceCountry)
                    <div class="py-1 flex justify-between w-full space-odd-item xl:w-1/2">
                        <div class="font-semibold"> {{ trans('profile.athlete.residenceCountry') }} </div>
                        <div class="text-gray-700"> {{ $user->athlete->residenceCountry->name }} </div>
                    </div>
                    @endif
                    @if($user->athlete->residence_city)
                    <div class="py-1 flex justify-between w-full space-odd-item xl:w-1/2">
                        <div class="font-semibold"> {{ trans('profile.athlete.residenceCity') }} </div>
                        <div class="text-gray-700"> {{ $user->athlete->residence_city }} </div>
                    </div>
                    @endif
                    @if($user->athlete->date_of_birth)
                    <div class="py-1 flex justify-between w-full space-odd-item xl:w-1/2">
                        <div class="font-semibold"> {{ trans('profile.athlete.dob') }} </div>
                        <div class="text-gray-700"> {{ $user->athlete->date_of_birth }} </div>
                    </div>
                    @endif
                    @if($user->athlete->height)
                    <div class="py-1 flex justify-between w-full space-odd-item xl:w-1/2">
                        <div class="font-semibold"> {{ trans('profile.athlete.height') }} </div>
                        <div class="text-gray-700"> {{ $user->athlete->height }}  {{ trans('profile.athlete.'. $user->athlete->height_unit) }} </div>
                    </div>
                    @endif
                    @if($user->athlete->status)
                    <div class="py-1 flex justify-between w-full space-odd-item xl:w-1/2">
                        <div class="font-semibold"> {{ trans('profile.athlete.status') }} </div>
                        <div class="text-gray-700"> {{ $user->athlete->status ? trans('fields.status.'.$user->athlete->status) : '-' }} </div>
                    </div>
                    @endif
                    @if($user->athlete->professional_start_date)
                    <div class="py-1 flex justify-between w-full space-odd-item xl:w-1/2">
                        <div class="font-semibold"> {{ trans('profile.athlete.contract') }} </div>
                        <div class="text-gray-700"> {{ $user->athlete->professional_start_date }} - {{ $user->athlete->professional_end_date }} </div>
                    </div>
                    @endif
                    @include('public.users.sport',['user' => $user])
                </div>
        </div>
    @endcan
    @elseif($user->scout)
    
        <div class="pt-2 border-t w-full flex flex-wrap">

                @if($user->about)  
                    <div class="py-1 flex flex-wrap justify-between w-full">
                        <div class="w-full font-semibold"> {{ trans('profile.athlete.about') }} </div>
                        @php
                            $about = $user->about; 
                        @endphp
                        @if(strlen($user->about) > 160 || substr_count($about,'<p>') > 2)
                            <div class="aboutme-short w-full text-gray-700"> {!! $about !!} </div>
                            <div class="aboutme-large hidden w-full text-gray-700"> {!! $about !!} </div>
                            <div class="w-full">
                                <button type="button" class="text-primary btn-readmore hover:underline"> {{ trans('profile.readmore') }} </button>
                            </div>
                        @else
                            <div class="w-full text-gray-700"> {!! $about !!} </div>
                        @endif
                    </div>
                @endif
                <div class="flex flex-wrap w-full">
                    @if($user->scout->speciality)
                    <div class="py-1 flex justify-between w-full space-odd-item xl:w-1/2">
                        <div class="font-semibold"> {{ trans('profile.scout.cargo-scout') }} </div>
                        <div class="text-gray-700"> {{ $user->scout->speciality->name  }} </div>
                    </div>
                    @endif

                    @if($user->scout->specialitySecondary)
                    <div class="py-1 flex justify-between w-full space-odd-item xl:w-1/2">
                        <div class="font-semibold"> {{ trans('profile.scout.cargo-secundario-scout') }} </div>
                        <div class="text-gray-700"> {{ $user->scout->specialitySecondary->name  }} </div>
                    </div>
                    @endif

                    @if($user->scout->nationality)    
                    <div class="py-1 flex justify-between w-full space-odd-item xl:w-1/2">
                        <div class="font-semibold"> {{ trans('profile.athlete.nationality') }} </div>
                        <div class="text-gray-700"> {{ $user->scout->nationality->name }} </div>
                    </div>
                    @endif
                    @if($user->scout->residenceCountry)
                    <div class="py-1 flex justify-between w-full space-odd-item xl:w-1/2">
                        <div class="font-semibold"> {{ trans('profile.athlete.residenceCountry') }} </div>
                        <div class="text-gray-700"> {{ $user->scout->residenceCountry->name  }} </div>
                    </div>
                    @endif

                    @include('public.users.sport',['user' => $user])
                </div>
        </div>
    @endif
</div>

</div>