@extends('layouts.app')

@section('template_title')
  {!! trans('recruitments.showing-recruitment') !!}
@endsection

@section('content')

  <div class="container">
    <div class="w-full flex justify-center">
      <div class="w-full md:w-4/5 lg:w-3/5">

        <div class="card-p">

            <div class="flex justify-between items-center border-b pb-2 mb-2">
                <h4 id="card_title"> {{ trans('recruitments.showing-recruitment') }} </h4>
              <div class="float-right">
                <a href="{{ route('recruitments.index') }}" class="btn btn-light btn-sm float-right">
                  <i class="fa fa-fw fa-mail-reply" aria-hidden="true"></i>
                  {!! trans('recruitments.buttons.back-to-recruitments') !!}
                </a>
              </div>
            </div>

          <div class="w-full">

            <div class="flex flex-wrap w-full justify-between items-center">
              <div class="w-auto pr-2">
                  @if($recruitment->is_public)
                      <span class="badge badge-success text-base rounded-full py-1 px-2"> {{ trans('recruitments.forms.public') }} </span>
                  @else
                    <span class="badge badge-primary text-base rounded-full py-1 px-2"> {{ trans('recruitments.forms.private') }} </span> 
                  @endif
              </div>

            {{-- Place --}}
              <div class="w-auto">
                <div class="mt-1 w-full flex">
                    <strong> {{ trans('recruitments.forms.place') }}: </strong>
                    <span class="pl-2"> {{ $recruitment->place ?? '--' }} </span>
                </div>
              </div>
              {{-- Address --}}
              @if($recruitment->addressComplete)
              <div class="w-auto px-2">
                <div class="mt-1 w-full flex">
                    <strong> {{ trans('recruitments.forms.address') }}: </strong>
                    <a href="http://maps.google.com/maps?q={{ $recruitment->addressComplete }}" class="pl-2"> {{ $recruitment->addressComplete }} </a>
                </div>
              </div>
              @endif
              {{-- Day --}}
              <div class="w-auto px-2">
                <div class="w-full flex">
                  <strong> {{ trans('recruitments.forms.day') }}: </strong>
                  <span class="pl-2"> {{$recruitment->day_start}} {{ $recruitment->day_end ? ' - '.$recruitment->day_end : '' }}
                      <span class="text-gray-700"> {{ $recruitment->hour }} </span>
                  </span>
                </div>
              </div>
              <div>
                  <div>
                    @php 
                      $assistYes = $recruitment->usersInvited->filter(function($u) {
                        return $u->assist == 'yes';
                      });
                    @endphp
                      <span class="text-green-700 text-xl"> {{ count($assistYes) }} </span>
                      <span class="text-xl"> / </span>
                      <span class="text-xl"> {{ $recruitment->usersInvited->count() }} </span>
                      {{ __('recruitments.invitation.assist') }}
                  </div>
              </div>
            </div>
            @if($recruitment->message)
            <div class="w-full text-gray-700 pt-2">
              {{ $recruitment->message }}
            </div>
            @endif
            
            {{-- INVITATIONS --}}
            <div class="pt-4 px-0 col-12">
               <h5 class="pb-2 border-b mb-2"> {{ __('recruitments.invitation.athletesInvited') }} </h5>
              @foreach ($recruitment->usersInvited as $u)
                  @if($u->user->athlete)
                    <div class="w-full items-center justify-between py-1 flex flex-wrap border-b">
                        <div class="w-auto flex">    
                          <div class="pr-4">
                            @include('partials.avatar-user',['user' => $u->user])
                            {{-- <img class="h-20 w-20 object-cover rounded-full border border-gray-800" src="{{ $u->user->avatar }}" alt="{{ $u->user->fullName }}"> --}}
                          </div>
                          <div>
                            @include('partials.user-card-information',['user' => $u->user,'link' => true])
                          </div>
                        </div>
                          @if($u->assist)
                          <div class="pl-4 mt-1 sm:mt-0 w-auto">
                            <div> {{ __('recruitments.invitation.goToEvent') }} </div> 
                              @switch($u->assist)
                                @case('perhaps')
                                    <span class="btnResponseRecruitment responsePerhaps active rounded-full px-2 py-1"> {{ __('recruitments.invitation.perhaps') }} </span>
                                    @break;
                                @case('yes')
                                    <span class="btnResponseRecruitment responseYes active rounded-full px-2 py-1"> {{ __('recruitments.invitation.yes') }} </span>                                  
                                    @break;
                                @case('no')
                                    <span class="btnResponseRecruitment responseNo active rounded-full px-2 py-1"> {{ __('recruitments.invitation.no') }} </span>
                                    @break;
                                @default:
                                    @break;
                              @endswitch
                          </div>
                          @endif
                  </div>
                    @endif
              @endforeach

              @if(count($recruitment->usersInvited) == 0)
                <div class="pt-4">
                    {{ __('recruitments.invitation.noAthletesInvited') }}
                </div>
              @endif
            </div>    

          </div>

        </div>
      </div>
    </div>
  </div>

  @include('modals.modal-delete')

@endsection

@section('footer_scripts')
  @include('scripts.delete-modal-script')
@endsection
