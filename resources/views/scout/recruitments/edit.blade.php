@extends('layouts.app')

@section('template_title')
    {!! trans('recruitments.editing-recruitment',['name' => $recruitment->name]) !!}
@endsection

@section('template_linked_css')
@endsection

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-11 col-md-8 col-lg-7">
                <div class="card-p">
                        <div class="flex items-center justify-between border-b pb-2 mb-2">
                                <h4 id="card_title"> {!! trans('recruitments.editing-recruitment') !!} </h4>
                                <div class="pull-right">
                                    <a href="{{ route('recruitments.index') }}" class="btn btn-light btn-sm float-right" data-toggle="tooltip" data-placement="left" title="{{ trans('recruitments.tooltips.back-to-recruitments') }}">
                                        <i class="fa fa-fw fa-reply-all" aria-hidden="true"></i>
                                        {!! trans('recruitments.buttons.back-to-recruitments') !!}
                                    </a>
                                </div>
                        </div>

                    <div class="w-full">
                        {!! Form::model($recruitment,array('route' => ['recruitments.update', $recruitment->id], 'method' => 'PUT', 'role' => 'form','id' => 'clubForm', 'class' => 'needs-validation')) !!}
                            {!! csrf_field() !!}

                            <div class="row col-12 px-0">
                                    <div class="col-12 px-0 col-md-6 col-lg-4 form-group has-feedback">
                                        {!! Form::label('day_start', trans('recruitments.forms.dayStart'), array('class' => 'col-12 control-label')); !!}
                                        <div class="col-12">
                                                {!! Form::text('day_start',NULL, array('id' => 'name', 'class' => 'datepicker-custom form-control','placeholder' => 'dd/mm/yyyy' )) !!}                                        
                                        </div>
                                    </div>
                                    {{-- End Day --}}
                                    <div class="col-12 px-0 col-md-6 col-lg-4 form-group has-feedback">
                                        {!! Form::label('day_end', trans('recruitments.forms.dayEnd'), array('class' => 'col-12 control-label')); !!}
                                        <div class="col-12">
                                                {!! Form::text('day_end',NULL, array('id' => 'name', 'class' => 'datepicker-custom form-control','placeholder' => 'dd/mm/yyyy' )) !!}                                        
                                        </div>
                                    </div>
                                    <div class="col-12 px-0 col-md-6 col-lg-4 form-group has-feedback">
                                            {!! Form::label('hour', trans('recruitments.forms.hour'), array('class' => 'col-12 control-label')); !!}
                                            <div class="col-12">
                                                    {!! Form::text('hour', NULL, array('class' => 'form-control','placeholder' => 'HH:mm' )) !!}                                        
                                            </div>
                                    </div>
                                </div>
    
                                <div class="col-12 row px-0">
                                    {{-- Place --}}
                                    <div class="col-12 col-md-6 px-0 form-group has-feedback">
                                            {!! Form::label('place', trans('recruitments.forms.place'), array('class' => 'col-12 control-label')); !!}
                                            <div class="col-12">
                                                    {!! Form::text('place', NULL, array('class' => 'form-control' )) !!}                                        
                                            </div>
                                    </div>
    
                                    {{-- Address --}}
                                    <div class="col-12 col-md-6 px-0 form-group has-feedback">
                                            {!! Form::label('address', trans('recruitments.forms.address'), array('class' => 'col-12 control-label')); !!}
                                            <div class="col-12">
                                                    {!! Form::text('address', NULL, array('class' => 'form-control' )) !!}                                        
                                            </div>
                                    </div>
                                </div>
    
    
                                <div class="col-12 row px-0">
                                    {{-- city --}}
                                    <div class="col-12 col-md-6 px-0 form-group has-feedback">
                                            {!! Form::label('city', trans('recruitments.forms.city'), array('class' => 'col-12 control-label')); !!}
                                            <div class="col-12">
                                                    {!! Form::text('city', NULL, array('class' => 'form-control' )) !!}                                        
                                            </div>
                                    </div>
    
                                    {{-- Country --}}
                                    <div class="col-12 col-md-6 px-0 form-group has-feedback">
                                            {!! Form::label('country', trans('recruitments.forms.country') , array('class' => 'col-12 control-label')); !!}
                                            <div class="col-12">
                                                {!! Form::select('country_id',$countries, old('country_id'), array('id' => 'country', 'class' => 'form-control',' placeholder' => trans('clubes.forms.country') )) !!}
                                            </div>
                                    </div>
                                </div>

                                {{-- Message --}}
                                <div class="col-12 px-0 form-group has-feedback">
                                        {!! Form::label('message', trans('recruitments.forms.message'), array('class' => 'col-12 control-label')); !!}
                                        <div class="col-12">
                                                {!! Form::textarea('message', NULL, array('class' => 'form-control','rows' => 4 )) !!}                                        
                                        </div>
                                </div>
                                
                                <div class="col-12 px-0 row">
                                        <label class="col-12 col-sm-6 col-md-auto flex items-center font-semibold">
                                            <input name="is_public" class="mr-2 form-radio form-radio-rounded" {{ $recruitment->is_public ? 'checked="on"' : '' }} type="radio" value="public" {{ request()->input('is_public') == 'public' ? 'checked' : '' }}>
                                            <span class="">
                                                {{ trans('recruitments.forms.public') }}
                                            </span>
                                        </label>   
                                        <label class="col-12 col-sm-6 col-md-auto flex items-center font-semibold">
                                            <input name="is_public" class="mr-2 form-radio form-radio-rounded" {{ $recruitment->is_public ? '' : 'checked="on"' }} type="radio" value="private" {{ request()->input('is_public') == 'private' ? 'checked' : '' }}>
                                            <span class="">
                                                {{ trans('recruitments.forms.private') }}
                                            </span>
                                        </label>    
                                </div>

                            <div class="flex w-full justify-end px-2">
                                {!! Form::button(trans('recruitments.forms.update'), array('class' => 'btn btn-success mb-1 rounded-full','type' => 'submit' )) !!}
                            </div>
                            {!! Form::close() !!}
                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection

@section('footer_scripts')
<script>
    $(window).on("load",function() {
        let lang = $('html').attr('lang');
        $('.datepicker-custom').datepicker({
            language: lang,
            autoClose: true,
            dateFormat: 'dd/mm/yyyy',
            minDate: new Date()
        });

    })
</script>
@endsection