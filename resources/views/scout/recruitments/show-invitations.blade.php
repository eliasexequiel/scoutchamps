@extends('layouts.app')

@section('template_title')
    {!! trans('recruitments.my-recruitments') !!}
@endsection

@section('content')

    {{-- ALERTS  WITH JSON --}}
    <div id="edit-athlete-success" class="hidden mx-6 alert alert-success alert-dismissable fade show" role="alert">
        <a href="#" class="close" aria-label="close">&times;</a>
        <span id="message"></span>
    </div>

    <scout-invitation-recruitment :recruitment="{{ $recruitment }}"></scout-invitation-recruitment>

@endsection