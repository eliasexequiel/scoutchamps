@extends('layouts.app')

@section('template_title')
  {!! trans('usersmanagement.showing-user', ['name' => $user->fullName]) !!}
@endsection

@php
  $levelAmount = trans('usersmanagement.labelUserLevel');
  if ($user->level() >= 2) {
    $levelAmount = trans('usersmanagement.labelUserLevels');
  }
@endphp

@section('content')

  <div class="container">
    <div class="flex w-full justify-center">
      <div class="w-full sm:w-4/5 md:w-3/5">

        <div class="card-p">

            <div class="flex justify-between items-center mb-2 pb-2 border-b">
              <h4 class="font-bold"> {!! $user->fullName !!} </h4>
              <div class="float-right">
                <a href="{{ route('users') }}" class="btn btn-light btn-sm float-right">
                  <i class="fa fa-fw fa-mail-reply" aria-hidden="true"></i>
                  {!! trans('usersmanagement.buttons.back-to-users') !!}
                </a>
              </div>
            </div>

          <div class="flex flex-wrap w-full">

            <div class="w-full flex items-center">
              <div class="w-auto mr-4">
                @include('partials.avatar-user',['user' => $user, 'size' => 'profile-md'])
              </div>
              <div class="w-auto">
                <h4 class="mt-2">  {{ $user->fullName }} </h4>
                
                {{-- @if ($user->profile) --}}
                  <div class="mb-4 flex flex-wrap">
                      <div class="mt-1 mr-2">
                        <a href="/users/{{$user->id}}/edit" class="btn btn-sm rounded-full btn-outline-info" title="{{ trans('usersmanagement.editUser') }}">
                          <i class="fa fa-pencil fa-fw" aria-hidden="true"></i> 
                          <span> {{ trans('usersmanagement.editUser') }} </span>
                        </a>
                      </div>
                      <div>
                        {!! Form::open(array('url' => 'users/' . $user->id, 'class' => 'form-inline', 'title' => trans('usersmanagement.deleteUser'))) !!}
                          {!! Form::hidden('_method', 'DELETE') !!}
                          {!! Form::button('<i class="fa fa-trash-o fa-fw" aria-hidden="true"></i> <span>' . trans('usersmanagement.deleteUser') . '</span>' , array('class' => 'mt-1 btn btn-outline-danger rounded-full btn-sm mr-2','type' => 'button', 'data-toggle' => 'modal', 'data-target' => '#confirmDelete', 'data-title' => 'Delete User', 'data-message' => 'Are you sure you want to delete this user?')) !!}
                        {!! Form::close() !!}
                      </div>
                      <div>
                        <a class="mt-1 btn btn-gray-outline btn-sm rounded-full" title="{{ trans('usersmanagement.showPublicProfile') }}" href="{{ route('public.profile.show',['uuid' => $user->fullname.'_'.$user->uuid]) }}"> 
                          <i class="fa fa-eye fa-fw" aria-hidden="true"></i>
                          <span> {{ trans('usersmanagement.showPublicProfile') }} </span>
                        </a>
                      </div>
                </div>
                {{-- @endif --}}
              </div>
            </div>

            <div class="w-full mt-4">
            @if ($user->email)
            <div class="w-full flex flex-wrap border-b mb-1 pb-2">
              <div class="mr-2"> <strong> {{ trans('usersmanagement.labelEmail') }} </strong> </div>
              <div>
                <span data-toggle="tooltip" data-placement="top" title="{{ trans('usersmanagement.tooltips.email-user', ['user' => $user->email]) }}">
                  {{ HTML::mailto($user->email, $user->email) }}
                </span>
              </div>
            </div>
            @endif

            @if ($user->profile && $user->profile->phone)
            <div class="w-full flex flex-wrap border-b mb-1 pb-2">
              <div class="mr-2"> <strong> {{ trans('usersmanagement.labelPhone') }} </strong> </div>
              <div>
                {{ $user->profile->phone }}
              </div>
            </div>
            @endif

            @if ($user->created_at)
              <div class="w-full flex flex-wrap border-b mb-1 pb-2">
                  <div class="mr-2"> <strong> {{ trans('usersmanagement.labelCreatedAt') }} </strong> </div>
                  <div> {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$user->created_at)->format(config('settings.formatDate')) }} </div>
              </div>
            @endif

            @if ($user->updated_at)
              <div class="w-full flex flex-wrap border-b mb-1 pb-2">
                  <div class="mr-2"> <strong> {{ trans('usersmanagement.labelUpdatedAt') }} </strong> </div>
                  <div> {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$user->updated_at)->format(config('settings.formatDate')) }} </div>
              </div>
            @endif
            @if(count($user->sesions) > 0 && $user->sesions->first()->created_at )   
              <div class="w-full flex flex-wrap border-b mb-1 pb-2">
                  <div class="mr-2"> <strong> {{ trans('usersmanagement.users-table.lastLogin') }}: </strong> </div>
                  <div> {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$user->sesions->first()->created_at)->format(config('settings.formatDate')) }} </div>
              </div>
            @endif

          </div>

          </div>

        </div>
      </div>
    </div>
  </div>

  @include('modals.modal-delete')

@endsection

@section('footer_scripts')
  @include('scripts.delete-modal-script')
  @if(config('usersmanagement.tooltipsEnabled'))
    @include('scripts.tooltips')
  @endif
@endsection
