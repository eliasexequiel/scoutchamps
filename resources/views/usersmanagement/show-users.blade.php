@extends('layouts.app')

@section('template_title')
    {!! trans('usersmanagement.showing-all-users') !!}
@endsection

@section('template_linked_css')
    @if(config('usersmanagement.enabledDatatablesJs'))
        <link rel="stylesheet" type="text/css" href="{{ config('usersmanagement.datatablesCssCDN') }}">
    @endif
    <style type="text/css" media="screen">
        .users-table {
            border: 0;
        }
        .users-table tr td:first-child {
            padding-left: 15px;
        }
        .users-table tr td:last-child {
            padding-right: 15px;
        }
        .users-table.table-responsive,
        .users-table.table-responsive table {
            margin-bottom: 0;
        }

    </style>
    <style>
        .input-search-users,
        .input-search-users input,
        .input-search-users button {
            height: 37px;   
        }
        .input-search-users button img {
            width: 24px;
        }

    </style>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card-p">

                        <div class="flex flex-wrap justify-start md:justify-between items-center pb-2 mb-2">

                            <div class="flex flex-wrap items-center">
                                <h4 class="font-bold pr-4"> {!! trans('usersmanagement.showing-all-users') !!} </h4>

                                <div class="w-auto sm:w-auto my-2 sm:my-0"> 
                                    <span class="badge badge-primary rounded-full text-white">{{ trans('usersmanagement.userTotal') }}: {{ $users->total() }} </span>
                                </div>
                                <div class="btn-group btn-group-xs pl-4">
                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fa fa-ellipsis-v fa-fw" aria-hidden="true"></i>
                                        <span class="sr-only">
                                            {!! trans('usersmanagement.users-menu-alt') !!}
                                        </span>
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a class="dropdown-item dropdown-item-primary" href="/users/create">
                                            <i class="fa fa-fw fa-plus" aria-hidden="true"></i>
                                            {!! trans('usersmanagement.buttons.create-new') !!}
                                        </a>
                                        <a class="dropdown-item dropdown-item-primary" href="{{ route('users.blocked.index') }}">
                                            <i class="fa fa-fw fa-eye" aria-hidden="true"></i>
                                            {!! trans('usersmanagement.show-blocked-users') !!}
                                        </a>
                                    </div>
                                </div>
                            </div>
                        @if(config('usersmanagement.enableSearchUsers'))
                            @include('partials.search-users-form')
                        @endif

                        </div>

                    <div class="w-full">

                        <div class="table-responsive users-table">
                            <table class="table table-striped table-sm data-table primary">
                                {{-- <caption id="user_count">
                                    {{ trans_choice('usersmanagement.users-table.caption', 1, ['userscount' => $users->count()]) }}
                                </caption> --}}
                                <thead class="thead">
                                    <tr>
                                        <th style="min-width: 85px;">{!! trans('usersmanagement.users-table.fname') !!}</th>
                                        <th style="min-width: 85px;">{!! trans('usersmanagement.users-table.lname') !!}</th>
                                        {{-- <th class="hidden-xs">{!! trans('usersmanagement.users-table.email') !!}</th> --}}
                                        <th>{!! trans('usersmanagement.users-table.role') !!}</th>
                                        <th class="hidden-sm hidden-xs hidden-md">{!! trans('usersmanagement.users-table.created') !!}</th>
                                        <th class="hidden-sm hidden-xs hidden-md">{!! trans('usersmanagement.users-table.lastLogin') !!}</th>
                                        {{-- <th class="hidden-sm hidden-xs hidden-md">{!! trans('usersmanagement.users-table.updated') !!}</th> --}}
                                        <th>{!! trans('usersmanagement.users-table.actions') !!}</th>
                                        <th class="no-search no-sort"></th>
                                        <th class="no-search no-sort"></th>
                                    </tr>
                                </thead>
                                <tbody id="users_table">
                                    @forelse($users as $user)
                                        <tr>
                                            <td>{{$user->firstname}}</td>
                                            <td>{{$user->lastname}}</td>
                                            {{-- <td class="hidden-xs"><a href="mailto:{{ $user->email }}" title="email {{ $user->email }}">{{ $user->email }}</a></td> --}}
                                            <td>
                                                @foreach ($user->roles as $user_role)
                                                    @if ($user_role->name == 'Athlete')
                                                        @php $badgeClass = 'success' @endphp
                                                    @elseif ($user_role->name == 'Athlete Premiun' || 
                                                             $user_role->name == 'Athlete Premiun Semester' ||
                                                             $user_role->name == 'Athlete Premiun Anual')
                                                        @php $badgeClass = 'dark' @endphp
                                                    @elseif ($user_role->name == 'Admin')
                                                        @php $badgeClass = 'warning' @endphp
                                                    @elseif ($user_role->name == 'Scout')
                                                        @php $badgeClass = 'info' @endphp
                                                    @else
                                                        @php $badgeClass = 'default' @endphp
                                                    @endif
                                                    <span class="badge rounded-full p-1 text-white badge-{{$badgeClass}}">{{ trans('titles.'.$user_role->name) }}</span>
                                                @endforeach
                                            </td>
                                            <td class="hidden-sm hidden-xs hidden-md"> {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$user->created_at)->format(config('settings.formatDate'))  }}</td>
                                            <td class="hidden-sm hidden-xs hidden-md"> @if(count($user->sesions) > 0 && $user->sesions->first()->created_at ) {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$user->sesions->first()->created_at)->format(config('settings.formatDate'))  }}  @endif </td>
                                            {{-- <td class="hidden-sm hidden-xs hidden-md"> {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$user->updated_at)->format(config('settings.formatDate'))  }}</td> --}}
                                            
                                            <td class="buttons-actions">
                                                <a class="btn btn-sm rounded-full btn-outline-success" href="{{ URL::to('users/' . $user->id) }}" title="Show">
                                                    {!! trans('usersmanagement.buttons.show') !!}
                                                </a>
                                                <a class="btn btn-sm rounded-full btn-outline-info" href="{{ URL::to('users/' . $user->id . '/edit') }}" title="Edit">
                                                    {!! trans('usersmanagement.buttons.edit') !!}
                                                </a>
                                                <a class="btn btn-sm btn-outline-danger rounded-full" href="{{ route('users.block',['id' => $user->id]) }}"> {!! trans('usersmanagement.buttons.block') !!} </a>
                                                @if( $verifyScoutsClubs && $verifyScoutsClubs->value ) <!-- Table config_site -->
                                                    @if($user->scout && !$user->verified)
                                                        {!! Form::open(array('url' => 'users/' . $user->id. '/verify', 'class' => 'form-inline', 'title' => 'Verify')) !!}
                                                            {!! Form::hidden('_method', 'GET') !!}
                                                            {!! Form::button(trans('usersmanagement.buttons.verify'), array('class' => 'btn btn-success rounded-full btn-sm','type' => 'submit', 'style' =>'width: 100%;' , 'title' => 'Verify')) !!}
                                                        {!! Form::close() !!}
                                                    @endif
                                                    @if($user->scout && $user->verified)
                                                        {!! Form::open(array('url' => 'users/' . $user->id. '/unverify', 'class' => 'form-inline', 'title' => 'Unverify')) !!}
                                                            {!! Form::hidden('_method', 'GET') !!}
                                                            {!! Form::button(trans('usersmanagement.buttons.unverify'), array('class' => 'btn btn-danger rounded-full btn-sm','type' => 'submit', 'style' =>'width: 100%;' , 'title' => 'Unverify')) !!}
                                                        {!! Form::close() !!}
                                                    @endif
                                                @endif
                                            </td>
                                        </tr>
                                    @empty
                                        <tr class="text-base text-center py-2">
                                            <td colspan="7"> {{ trans('profile.noResults') }} </td>
                                        </tr>
                                    @endforelse
                                </tbody>
                                {{-- <tbody id="search_results"></tbody>
                                @if(config('usersmanagement.enableSearchUsers'))
                                    <tbody id="search_results"></tbody>
                                @endif --}}

                            </table>

                            
                        </div>
                        @if(config('usersmanagement.enablePagination'))
                        <div class="w-full mt-4">
                            {{ $users->appends(Request::only('createdAt','lastSession','typeUser','fullnameFilter'))->links() }}
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('modals.modal-delete')

@endsection

@section('footer_scripts')
    @if ((count($users) > config('usersmanagement.datatablesJsStartCount')) && config('usersmanagement.enabledDatatablesJs'))
        @include('scripts.datatables')
    @endif
    @include('scripts.delete-modal-script')
    @include('scripts.save-modal-script')
    @if(config('usersmanagement.tooltipsEnabled'))
        @include('scripts.tooltips')
    @endif

    <script>
        $(window).on("load", function () {

            let lang = $('html').attr('lang');
            $('.lastSession').datepicker({
                language: lang,
                autoClose: true,
                dateFormat: 'dd/mm/yyyy',
                maxDate: new Date(),
            });
        });
    </script>
@endsection
