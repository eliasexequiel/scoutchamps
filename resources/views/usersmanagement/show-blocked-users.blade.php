@extends('layouts.app')

@section('template_title')
    {!!trans('usersmanagement.showing-user-blocked')!!}

@endsection

@section('template_linked_css')
    <style type="text/css" media="screen">
        .users-table {
            border: 0;
        }
        .users-table tr td:first-child {
            padding-left: 15px;
        }
        .users-table tr td:last-child {
            padding-right: 15px;
        }
        .users-table.table-responsive,
        .users-table.table-responsive table {
            margin-bottom: .15em;
        }
    </style>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card-p">
                        <div class="flex flex-wrap justify-between items-center pb-2 mb-2 border-b">
                            <div class="flex flex-wrap">
                                <h4 class="font-bold"> {!!trans('usersmanagement.showing-user-blocked')!!} </h4>
                            
                                <div class="w-auto sm:w-auto pl-2 sm:pl-4"> 
                                    <span class="badge badge-primary rounded-full text-white">{{ trans('usersmanagement.userTotal') }}: {{ $users->count() }} </span>
                                </div>

                            </div>
                            <div>
                                <a href="{{ route('users') }}" class="btn btn-light btn-sm" title="{{ trans('usersmanagement.tooltips.back-users') }}">
                                    <i class="fa fa-fw fa-mail-reply" aria-hidden="true"></i>
                                    {!! trans('usersmanagement.buttons.back-to-users') !!}
                                </a>
                            </div>
                        </div>

                    <div class="w-full">

                        @if(count($users) === 0)

                            <tr>
                                <p class="text-center margin-half">
                                    {!! trans('usersmanagement.no-records') !!}
                                </p>
                            </tr>

                        @else

                            <div class="table-responsive users-table">
                                <table class="table table-striped table-sm data-table primary">
                                    
                                    <thead>
                                        <tr>
                                                <th>{!! trans('usersmanagement.users-table.fname') !!}</th>
                                                <th>{!! trans('usersmanagement.users-table.lname') !!}</th>
                                                {{-- <th class="hidden-xs">{!! trans('usersmanagement.users-table.email') !!}</th> --}}
                                                <th>{!! trans('usersmanagement.users-table.role') !!}</th>
                                                {{-- <th class="hidden-xs">{!!trans('usersmanagement.users-table.deleted')!!}</th> --}}
                                                {{-- <th class="hidden-sm hidden-xs hidden-md">{!! trans('usersmanagement.users-table.updated') !!}</th> --}}
                                                <th>{!! trans('usersmanagement.users-table.actions') !!}</th>
                                                <th class="no-search no-sort"></th>
                                                <th class="no-search no-sort"></th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        @foreach($users as $user)
                                            <tr>
                                                    <td>{{$user->firstname}}</td>
                                                    <td>{{$user->lastname}}</td>
                                                    {{-- <td class="hidden-xs"><a href="mailto:{{ $user->email }}" title="email {{ $user->email }}">{{ $user->email }}</a></td> --}}
                                                    <td>
                                                        @foreach ($user->roles as $user_role)
                                                            @if ($user_role->name == 'Athlete')
                                                                @php $badgeClass = 'success' @endphp
                                                            @elseif ($user_role->name == 'Athlete Premiun' || 
                                                                    $user_role->name == 'Athlete Premiun Semester' ||
                                                                    $user_role->name == 'Athlete Premiun Anual')
                                                                @php $badgeClass = 'dark' @endphp
                                                            @elseif ($user_role->name == 'Admin')
                                                                @php $badgeClass = 'warning' @endphp
                                                            @elseif ($user_role->name == 'Scout')
                                                                @php $badgeClass = 'info' @endphp
                                                            @else
                                                                @php $badgeClass = 'default' @endphp
                                                            @endif
                                                            <span class="badge rounded-full p-1 text-white badge-{{$badgeClass}}">{{ trans('titles.'.$user_role->name) }}</span>
                                                        @endforeach
                                                </td>
                                                
                                                {{-- <td class="hidden-xs">  {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$user->deleted_at)->format(config('settings.formatDate'))  }} </td> --}}
                                                <td>
                                                    <span class="flex">
                                                        <a class="btn btn-sm btn-outline-success rounded-full mr-2" href="{{ route('users.unblock',['id' => $user->id]) }}"> {!! trans('usersmanagement.buttons.unblock') !!} </a>
                                                    {{-- {!! Form::model($user, array('action' => array('SoftDeletesController@update', $user->id), 'method' => 'PUT', 'data-toggle' => 'tooltip')) !!}
                                                        {!! Form::button(trans('usersmanagement.buttons.restoreUser'), array('class' => 'btn btn-outline-success rounded-full btn-sm mr-2', 'type' => 'submit', 'title' => 'Restore User')) !!}
                                                    {!! Form::close() !!} --}}
                                                {{-- </td>
                                                <td> --}}
                                                    <a class="btn btn-sm btn-outline-info rounded-full mr-2" href="{{ URL::to('users/' . $user->id) }}" title="Show">
                                                        {!! trans('usersmanagement.buttons.show') !!}
                                                    </a>
                                                    {!! Form::open(array('url' => 'users/' . $user->id, 'class' => 'inline-flex', 'title' => 'Delete')) !!}
                                                        {!! Form::hidden('_method', 'DELETE') !!}
                                                        {!! Form::button(trans('usersmanagement.buttons.destroyUser'), array('class' => 'btn btn-outline-danger rounded-full btn-sm','type' => 'button' ,'data-toggle' => 'modal', 'data-target' => '#confirmDelete', 'data-title' => 'Delete User', 'data-message' => 'Are you sure you want to delete this user ?')) !!}
                                                    {!! Form::close() !!}
                                                {{-- </td>
                                                <td> --}}
                                                </span>
                                                </td>
                                            </tr>
                                        @endforeach

                                    </tbody>
                                </table>
                            </div>

                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('modals.modal-delete')

@endsection

@section('footer_scripts')

    @if (count($users) > 10)
        @include('scripts.datatables')
    @endif
    @include('scripts.delete-modal-script')
    @include('scripts.save-modal-script')
    @include('scripts.tooltips')

@endsection
