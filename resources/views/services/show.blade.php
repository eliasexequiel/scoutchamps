@extends('layouts.app')

@section('template_title')
    {!!trans('planes.title')!!}

@endsection

@section('template_linked_css')
<style type="text/css">
.StripeElement {
    box-sizing: border-box;
    width: 100%;
    color: #495057;
    height: 37px;
    padding: 0.5rem 0.75rem;
    border: 1px solid #cacaca;
    border-radius: .5rem;
    background-color: white;
  }
  
  .StripeElement--focus {
    box-shadow: 1px solid #000;
  }
  
  .StripeElement--invalid {
    border-color: #fa755a;
  }
  
  .StripeElement--webkit-autofill {
    background-color: #fefde5 !important;
  }

  ::-webkit-input-placeholder { /* Edge */
    color: #92797e;
  }

  :-ms-input-placeholder { /* Internet Explorer 10-11 */
    color: #92797e;
  }

  ::placeholder {
    color: #92797e;
  }

</style>
@endsection

@section('content')
    @php 
      $subtotal = $plan->amount_decimal/100;
      $taxes = round($subtotal*0.07,2); 
    @endphp

    <div class="w-full px-6">
        <div class="w-full flex justify-center">
          @if (\Auth::user()->subscribed('main'))
          {{-- Si ya tiene una suscripción activa --}}
          <div class="flex flex-wrap w-full md:w-3/5">
            <div class="w-full">
                <div class="card-p">
                    <h4 class="pb-2 border-b mb-4"> {{ trans('subscriptions.changeSubscription') }}</h4>
                    <form action="{{ route('plan.subscribe',['id' => $plan->id]) }}" method="post" id="payment-form">
                        @csrf 
                        <div class="text-lg"> {{ trans('subscriptions.changeLabel',['name' => $plan->nickname]) }} </div>
                        {{-- Resume --}}
                        <div class="font-semibold pb-1 border-b mt-4 mb-2 w-full">
                            {{ trans('planes.resume') }}
                        </div>
                          <div class="w-full flex flex-wrap justify-end">
                            <div class="flex justify-end w-full">
                              <div class="pr-4 md:pr-8 text-gray-600"> {{ trans('planes.subtotal') }} </div>
                              <div class="w-24 font-semibold"> {{ $subtotal }}  {{ $plan->currency }} </div>
                            </div>
                            {{-- Tax --}}
                            <div class="flex justify-end w-full">
                              <div class="pr-4 md:pr-8 text-gray-600"> {{ trans('planes.taxes') }} </div>
                              <div class="w-24 font-semibold"> {{ number_format($taxes,2) }} {{ $plan->currency }} </div>
                            </div>
                            {{-- Total --}}
                            <div class="flex justify-end pl-8 w-auto border-t mt-1 pb-1">
                                <div class="pr-4 md:pr-8 text-lg text-gray-600"> {{ trans('planes.total') }} </div>
                                <div class="w-24 text-lg font-semibold"> {{ $subtotal + $taxes }} {{ $plan->currency }} </div>
                            </div>
                        </div>

                        <div class="w-full pt-2 flex justify-end items-center">
                          <button class="btn btn-success rounded-full mr-2"> {{ trans('subscriptions.changeButtonAccept') }} </button>
                          <a href="{{ route('plan.index') }}" class="text-primary"> {{ trans('subscriptions.changeButtonCancel') }} </a>
                        </div>    
                    </form>
                </div>
            </div>
          </div>
          @else  
          <div class="flex flex-wrap w-full md:w-3/5">
                <div class="w-full">
                    <div class="card-p">
                        <h4 class="pb-2 border-b mb-2"> {{ trans('planes.premiunService') }}: {{ $plan->nickname }}</h4>

                        <div class="spinner flex items-center h-24">
                            <spinner class="mx-auto"></spinner>
                          </div>

                        <form class="hidden" action="{{ route('plan.subscribe',['id' => $plan->id]) }}" method="post" id="payment-form">
                            @csrf 
                            <div class="font-semibold text-lg pb-1 border-b mb-2 w-full">
                              {{ trans('planes.paymentData') }}
                            </div>
                            <div class="w-full">
                              <!-- Cardholder -->
                              <div class="w-full mb-2">
                                  <input style="font-size: 16px;" class="form-control" name="namecardholder" value="{{ old('namecardholder') }}" placeholder="{{ trans('planes.forms.cardholderUser') }}" type="text">
                              </div>

                              <div id="card-element">
                                <!-- A Stripe Element will be inserted here. -->
                              </div>

                              <!-- Coupon -->
                              <div class="w-full mt-2">
                                <input style="font-size: 16px;" id="coupon" class="form-control" name="coupon" placeholder="{{ trans('planes.forms.applyDiscount') }}" type="text">
                              </div>
                          
                              <!-- Used to display Element errors. -->
                              <div id="error-coupon" class="text-red-600 text-sm"></div>
                              <div id="card-errors" class="text-red-600 text-sm" role="alert"></div>

                              {{--  Cards --}}
                              <div class="flex flex-wrap mt-2">
                              <div class="w-full px-4 sm:w-1/2">
                                <img class="px-12 sm:px-0" src="{{ asset('img/tarjetas.png') }}" alt="Credit Cards">
                              </div>
                              {{-- Resume --}}
                              <div class="w-full sm:w-1/2 px-2 md:border-l">
                                <div class="font-semibold pb-1 text-xl border-b mt-4 mb-2 w-full">
                                  {{ trans('planes.resume') }}
                                </div>
                                  <div class="pl-2 w-full flex flex-wrap">
                                    <div class="flex text-lg w-full">
                                      <div class="pr-4 w-auto text-gray-700"> {{ trans('planes.subtotal') }} </div>
                                      <div class="w-24 text-right font-semibold flex-1 text-right"> {{ $subtotal }}  {{ $plan->currency }} </div>
                                    </div>
                                    {{-- Tax --}}
                                    <div class="flex text-lg w-full">
                                      <div class="pr-4 w-auto text-gray-700"> {{ trans('planes.taxes') }} </div>
                                      <div class="w-24 text-right font-semibold flex-1 text-right"> {{ number_format($taxes,2) }} {{ $plan->currency }} </div>
                                    </div>
                                    {{-- Total --}}
                                    <div class="flex w-full text-xl border-t mt-1 pb-1">
                                        <div class="pr-4 w-auto text-gray-700"> {{ trans('planes.total') }} </div>
                                        <div class="w-24 text-right font-semibold flex-1 text-right"> {{ $subtotal + $taxes }} {{ $plan->currency }} </div>
                                    </div>
                                    {{-- Total con Descuento --}}
                                    <div class="wrapperTotalWithCoupon hidden flex w-full text-xl border-t mt-1 pb-1">
                                        <div class="pr-4 w-auto text-gray-700"> {{ trans('planes.totalWithDiscount') }} </div>
                                        <div id="totalWithCoupon" class="w-24 text-right font-semibold flex-1 text-right">  </div>
                                    </div>
                                  </div>
                              </div>
                            </div>
                            </div>
                            {{-- Fields hidden to use in javascript with Ajax --}}
                            <input type="hidden" name="subtotalInput" value="{{ $subtotal }}">
                            {{-- <input type="hidden" name="taxesInput" value="{{ $subtotal }}"> --}}

                            @if($plan->nickname == 'Monthly')
                              <div class="mt-2 w-full px-2 bg-gray-200 text-xs sm:text-sm border border-gray-500 rounded flex items-center">
                                  <span class="pl-2">{{ trans('planes.advicePlan') }} </span>
                              </div>
                            @endif
                            <div class="w-full pt-2 px-2 flex flex-wrap justify-end">
                                <button id="buttonPay" type="submit" class="btn btn-dark btn-lg rounded-full btn-loading"> 
                                  {{-- <div class="circle-loading"></div> --}}
                                  <div class="lds-ring"><div></div><div></div><div></div><div></div></div>
                                  {{ trans('planes.buttons.pay') }} 
                                </button>
                            </div>
                          </form>
                    </div>
                </div>
            </div>
          </div>
          @endif

          {{-- Key Stripe --}}
          <div id="keyStripe" class="hidden">{{ config('services.stripe.key') }}</div>
    </div>
@endsection


@section('footer_scripts')
<script src="https://js.stripe.com/v3/"></script>

<script type="text/javascript">

    // Create a Stripe client.
    let stripe_public = $('#keyStripe').text();
    var stripe = Stripe(stripe_public);

    // Create an instance of Elements.
    var elements = stripe.elements({
      locale: window.lang
    });

    // Custom styling can be passed to options when creating an Element.
    // (Note that this demo uses a wider set of styles than the guide below.)
    var style = {
    base: {
        color: '#32325d',
        fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
        fontSmoothing: 'antialiased',
        fontSize: '16px',
        '::placeholder': {
        color: '#787a96'
        }
    },
    invalid: {
        color: '#fa755a',
        iconColor: '#fa755a'
    }
    };

    if($('#card-element').length > 0) {
    // Create an instance of the card Element.
    var card = elements.create('card', {hidePostalCode: true,style: style});

    // Add an instance of the card Element into the `card-element` <div>.
    card.mount('#card-element');

    $('.spinner').addClass('hidden');
    $('#payment-form').removeClass('hidden');

    // Handle real-time validation errors from the card Element.
    card.addEventListener('change', function(event) {
    var displayError = document.getElementById('card-errors');
    if (event.error) {
        displayError.textContent = event.error.message;
    } else {
        displayError.textContent = '';
    }
    });

    // Handle form submission.
    var form = document.getElementById('payment-form');
    form.addEventListener('submit', function(event) {
    event.preventDefault();

    $('#buttonPay').prop("disabled",true);
    $('#buttonPay').addClass('running');

    stripe.createToken(card).then(function(result) {
        // console.log(result);
        if (result.error) {
        // Inform the user if there was an error.
          var errorElement = document.getElementById('card-errors');
          errorElement.textContent = result.error.message;
          $('#buttonPay').prop("disabled",false);
          $('#buttonPay').removeClass('running');
        } else {
          // Send the token to your server.
          stripeTokenHandler(result.token);
        }
        
    });
    });

    var couponInput = document.getElementById('coupon');
    couponInput.addEventListener('keyup', delay(function(event) {
      let couponText = event.target.value;
      $('#error-coupon').addClass("hidden");
      $('.wrapperTotalWithCoupon').addClass("hidden");
      if(couponText != '') {
        let subtotal = $('input[name=subtotalInput]').val();
        let response = checkCoupon(couponText,subtotal,7).then(res => {
          // console.log(res);
          $('#error-coupon').addClass("hidden");
          $('.wrapperTotalWithCoupon').removeClass("hidden");
          $('#totalWithCoupon').html(res.totalConDescuento + ' usd');
        },error => {
          $('.wrapperTotalWithCoupon').addClass("hidden");
          $('#totalWithCoupon').html('');
          $('#error-coupon').html("@lang('planes.errorCoupon')");
          $('#error-coupon').removeClass("hidden");
          // console.log("ERROR: " + error);
        });
      }
    },700));

    }
    
    function checkCoupon(couponText,subtotalPlan, taxPorcetaje) {
      return $.ajax({
          type:'POST',
          url: `/api/planes/checkCoupon`,
          data: { coupon: couponText, totalplan: subtotalPlan, porcentajetax: taxPorcetaje  },
          headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
          success: function(result) {
              // console.log(result);
              return result;
          },
          error: function (response, status, error) {
              return error;
              // console.log(error);
          }
      });
    }

    function delay(callback, ms) {
      var timer = 0;
      return function() {
        var context = this, args = arguments;
        clearTimeout(timer);
        timer = setTimeout(function () {
          callback.apply(context, args);
        }, ms || 0);
      };
    }

    


    // Submit the form with the token ID.
    function stripeTokenHandler(token) {
    // Insert the token ID into the form so it gets submitted to the server
    var form = document.getElementById('payment-form');
    var hiddenInput = document.createElement('input');
    hiddenInput.setAttribute('type', 'hidden');
    hiddenInput.setAttribute('name', 'stripeToken');
    hiddenInput.setAttribute('value', token.id);
    form.appendChild(hiddenInput);

    // Submit the form
    form.submit();
    }

</script>
@endsection
