@extends('layouts.app')

@section('template_title')
    {!!trans('planes.title')!!}

@endsection

@section('content')
    <div class="w-full px-6">
        <div class="w-full flex justify-center">
            <div class="flex flex-wrap w-full md:w-3/5">
                @foreach($planes as $plan)
                <div class="mb-2 px-2 w-full sm:w-1/2 md:w-1/3">
                    <div class="card-p h-full w-full text-center">
                        <h5 class="pb-2 border-b mb-2">{{ $plan->nickname }}</h5>
                        
                        <div> 
                            <span class="text-xl">{{ number_format($plan->amount/100, 2) }} </span>
                            <span class="text-sm text-gray-700"> {{ $plan->currency }} </span>
                        </div>
                        {{-- <h5>{{ $plan->description }}</h5> --}}
                        <div class="pt-2">
                            @if(!auth()->user()->subscribedToPlan($plan->id, 'main'))
                                <a href="{{ route('plan.show',['id' => $plan->id]) }}" class="rounded-full btn btn-outline-dark"> {{ trans('planes.buttons.subscribe') }} </a>
                            @else
                                <div class="my-2">
                                    <span class="bg-primary px-2 py-1 rounded-lg text-white"> {{ trans('planes.subscribed') }} </span>
                                </div>
                                {{Form::open(['method'  => 'DELETE', 'route' => ['plan.subscribe.cancel', $plan->id],'id' => 'subscription-cancel'])}}
                                    {{Form::button(trans('planes.buttons.cancel'), array('type' => 'submit', 'class' => 'btn btn-outline-primary mt-2 font-normal rounded-full'))}}
                                {{ Form::close() }}
                            @endif
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection


@section('footer_scripts')
@endsection
