@extends(Auth::user() ? 'layouts.app' : 'layouts.app-guest')

@section('template_title')
    {{ trans('titles.forbbiden') }}
@endsection

@section('content')

    <div class="w-full">
        <div class="w-full max-w-xl mx-auto pt-6 h-32">
            <div class="card-p">
                <div class="flex items-center">
                    <div class="text-lg mr-4"> {{ trans('titles.forbbiden') }} </div>
                    <a class="btn btn-outline-primary" href="{{ route('home') }}"> {{ trans('titles.btnGoDashboard') }} </a>
                </div>
            </div>
        </div>
    </div>

@endsection