<div class="modal fade" id="invitationsOneRecruitment" role="dialog" aria-labelledby="confirmDeleteLabel" aria-hidden="true" tabindex="-1">
        <div class="modal-dialog" role="document">
          <div class="modal-content">

            <div class="modal-body">
              <h4 id="title" class="pb-2 border-b"> {{ trans('recruitments.invitation.title-2') }} </h4>
              <div>
                  {{-- ID RECRUITMENT --}}
                  <input id="idR" name="idR" type="hidden">

                    <div class="w-full">
                      @foreach (\Auth::user()->followed as $u)
                          <div class="py-2 border-b flex w-full">
                            <label class="col-12 flex items-center font-semibold">
                              <input name="users[]" class="mr-2 form-checkbox" value="{{ $u->following->id }}" type="checkbox">
                              <div class="flex items-center">
                                <div class="w-4/5">
                                  <div class="font-thin"> {{ $u->following->fullName }} </div>
                                </div>
                                <div class="pl-2">
                                  
                                </div>
                            </div>
                          </label>
                          </div>
                      @endforeach
                    </div>
              </div>
            </div>
            <div class="modal-footer">
              {!! Form::button('<i class="fa fa-fw fa-close" aria-hidden="true"></i> Cancel', array('class' => 'btn btn-outline pull-left btn-light', 'type' => 'button', 'data-dismiss' => 'modal' )) !!}
              {!! Form::button(trans('modals.form_modal_send'), array('id' => 'sendInvitation','class' => 'btn btn-success rounded-full pull-right', 'type' => 'button' )) !!}
            </div>
          </div>
        </div>
      </div>
    
@section('footer_scripts')

<script type="text/javascript">
$(window).on('load',function(){
    // SHOW MODAL
    $('#invitationsOneRecruitment').on('show.bs.modal', function (e) {
      var id = $(e.relatedTarget).attr('data-id');
      $(this).find('.modal-body input#idR').val(id);
      // var title = $(e.relatedTarget).attr('data-title');
      // $(this).find('.modal-body #title').prepend(title);
    });
  
	$('#sendInvitation').on('click', function(){
    // $(this).data('form').submit();
    let id = $('#invitationsOneRecruitment .modal-body input#idR').val();
    let users = $('#invitationsOneRecruitment .modal-body [name="users[]"]');
    $.ajax({
          type:'POST',
          headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          url: `/recruitments/${id}/invitations/send`,
          data: {users: users},
          success: function(result) {

          },
          error: function (response, status, error) {

          }
    });

  });

});

</script>

@stop