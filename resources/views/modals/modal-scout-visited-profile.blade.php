<div class="modal fade" id="modalScoutVisitedProfile" role="dialog" aria-labelledby="modalScoutVisitedProfileLabel" aria-hidden="true" tabindex="-1">
        <div class="modal-dialog" role="document">
          <div class="modal-content">

            <div class="modal-body">
                <div class="pb-2 border-b flex w-full justify-between">
                    <h4> {{ trans('profile.athlete.scoutVisits') }} </h4>
                    <button type="button" data-dismiss="modal" class="close-icon"></button>
                </div>
                <div>
                        <div class="w-full">
                        @foreach (\Auth::user()->visited_scout() as $user)
                            <div class="py-2 border-b last:border-b-0 flex w-full">
                                    <div class="w-full flex flex-wrap sm:flex-no-wrap items-center sm:justify-between px-1">
                                        <div class="flex w-full">
                                            <div class="pr-2 w-auto">
                                                @include('partials.avatar-user',['user' => $user, 'size' => 'h-16 w-16'])
                                            </div>
                                            <div class="w-1/2 flex flex-wrap items-center">
                                                <div class="w-auto">
                                                        @include('partials.user-card-information',['link' => true])
                                                </div>
                                            </div>
                                        </div>
                                        <div class="w-full sm:w-auto pt-1 pl-1">
                                                <div class="sm:mt-1 w-32 flex justify-end">
                                                        <a class="btn btn-sm btn-outline-info rounded-full" href="{{ route('public.profile.show',['uuid' => $user->uuid]) }}"> 
                                                            <i class="fa fa-eye fa-fw" aria-hidden="true"></i>
                                                            {{ trans('profile.viewProfile') }} 
                                                        </a>
                                                </div>
                                            </div>
                                    </div>
                            </div>
                        @endforeach
                        </div>
                </div>
            </div>
          </div>
        </div>
      </div>
    
