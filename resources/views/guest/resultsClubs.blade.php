@extends('layouts.app-guest')

@section('template_title')
    {!! trans('welcome.clubs') !!}
@endsection

@section('template_fastload_css')
@endsection

@section('content')
    <div class="w-full bg-white">
        @include('partials.publicities.publicity-above')

        <div class="px-6 py-4 mb-2 w-full">
            <div class="mb-12 w-full text-center relative">
                <div class="title-home text-2xl sm:text-3xl">  {{ trans('welcome.clubs') }} </div>
            </div>

            <!-- FILTERS -->
            {!! Form::open(array('route' => 'guest.search.club', 'method' => 'GET', 'class' => 'form w-full')) !!}
            <div class="w-full mx-auto md:w-2/3 lg:w-1/2 p-2 border border-primary rounded">
                    <div class="border-b pb-1 mb-2">
                        <span class="text-base font-normal"> {{ trans('titles.filters') }} </span>
                    </div>
                    <div class="flex flex-wrap">
                        {{-- Country --}}
                        <div class="w-full flex-1 px-2 border-r border-gray-400">
                                <label class="text-gray-900 font-thin mb-0" for="nationality"> {{ trans('profile.filters.country') }} </label>
                                {!! Form::select('country', $countries, request()->input('country'), ['class' => 'mt-2 form-control form-control-sm', 'placeholder' => trans('profile.filters.allCountries') ]) !!}
                        </div>
                            
                        <div class="flex items-end px-2 justify-end">
                            <div>
                                <button class="mt-2 btn btn-sm btn-gray" type="submit"> {{ trans('profile.filters.search') }} </button>
                            </div>
                        </div>
                    </div>
            </div>
            {!! Form::close() !!}
        </div>

        <!-- RESULTS -->
        <div class="flex flex-wrap w-full md:w-11/12 lg:w-4/5 mx-auto">
        @if(count($clubs) > 0)
        @foreach ($clubs as $c)
            <div class="w-1/2 sm:w-1/3 md:w-1/3 lg:w-1/4 xl:w-1/4 flex flex-wrap px-2 mb-4">
            <a class="mx-auto" href="{{ route('guest.club.users',['id' => $c->id]) }}">
                <div class="w-full items-center px-4 md:px-12 py-2">
                    <div class="w-full items-center">
                        <img class="mx-auto h-32 object-contain" src="{{ $c->avatar }}" alt="{{ $c->name }}">
                    </div>
                    <div class="text-center text-base">
                        {{ $c->name }}
                        @if($c->type)
                            <div class="font-normal text-base text-gray-700"> {{ trans('clubes.types.'.$c->type->name) }} </div>
                        @endif
                    </div>
                </div>
            </a>  
            </div>
            
        @endforeach
            <div class="flex justify-center w-full mt-3">
                    <div class="w-auto mb-2">
                        {{ $clubs->appends(Request::only('country'))->links() }}
                    </div>
            </div>
        @else
        <div class="w-full mb-8 flex text-center items-center justify-center">
            <div class="text-xl"> {{ trans('profile.noResults') }} </div>
        </div>
        @endif
        </div>

        {{-- Publicities --}}
        @include('partials.publicities.publicity-below')
        
        <!-- Contacto -->
        @include('welcome.contact')
    </div>
@endsection

@section('footer_scripts')
@append