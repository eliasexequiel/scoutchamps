@extends('layouts.app-guest')

@section('template_title')
    {!! trans('welcome.aboutUs.title') !!}
@endsection

@section('template_fastload_css')
@endsection

@section('content')
    <div class="w-full bg-white">
        @include('partials.publicities.publicity-above')

        <div class="px-6 mb-2 mt-6 w-full">
            <div class="w-full text-center relative">
                <div class="title-home text-2xl sm:text-3xl"> 
                        {{ trans('welcome.aboutUs.title') }}
                </div>
            </div>
        </div>
        <section class="w-11/12 sm:w-5/6 lg:w-2/3 py-6 mx-auto relative">
            <div class="flex flex-wrap justify-center items-center w-full">
                <div class="w-full sm:w-1/2">
                    <div class="w-full text-black text-base mb-6">
                        {{ trans('welcome.aboutUs.text') }}
                    </div>
                    <a class="btn btn-lg btn-primary rounded-lg shadow-lg" href="{{ route('register') }}"> {{ trans('titles.joinnow') }} </a>
                </div>
                <div class="w-full sm:w-1/2 px-4 sm:px-8">
                    <div class="flex w-full">
                        <img class="mx-auto w-full object-contain" src="{{ asset('img/aboutus/main.png') }}" alt="{{ trans('titles.register') }}">
                    </div>
                </div>
            </div>
        </section>

        <!-- Mision - Vision -->
            <div class="w-11/12 sm:w-5/6 lg:w-2/3 py-6 mx-auto relative">
                {{-- Mision --}}
                <div class="w-full flex flex-wrap rounded items-center mb-2 p-4 bg-gray-300">
                    <div class="w-full sm:w-1/4">
                        <img class="mx-auto w-1/2 sm:w-full px-4" src="{{ asset('img/aboutus/mision.png') }} " alt="">
                    </div>
                    <div class="w-full sm:w-3/4">
                        <div class="mb-2">
                            <span class="text-lg border-b-2 border-primary uppercase font-semibold"> {{ trans('welcome.aboutUs.misionTitle') }} </span>
                        </div>
                        <div class="leading-tight">
                                {{ trans('welcome.aboutUs.mision') }}
                        </div>
                    </div>
                </div>

                {{-- Vision --}}
                <div class="w-full flex flex-wrap rounded items-center mb-2 p-4 bg-gray-300">
                    <div class="w-full sm:w-1/4">
                        <img class="mx-auto w-1/2 sm:w-full px-4" src="{{ asset('img/aboutus/vision.png') }} " alt="">
                    </div>
                    <div class="w-full sm:w-3/4">
                        <div class="mb-2">
                            <span class="text-lg border-b-2 border-primary uppercase font-semibold"> {{ trans('welcome.aboutUs.visionTitle') }} </span>
                        </div>
                        <div class="leading-tight">
                                {{ trans('welcome.aboutUs.vision') }}
                        </div>
                    </div>
                </div>
            </div>


        {{-- Publicities --}}
        @include('partials.publicities.publicity-below')
        
        <!-- Contacto -->
        @include('welcome.contact')
    </div>
@endsection
