<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title> {!! trans('titles.app') !!} </title>

        {{-- CSRF Token --}}
        <meta name="csrf-token" content="{{ csrf_token() }}">

        @if(App::environment('prod'))
            @include('partials.googletagmanager-head')
        @endif

        <!-- Fonts -->        
        <link href="{{ mix('/css/app.css') }}" rel="stylesheet">
        <link rel="stylesheet" href=" {{ asset('assets/leaflet/leaflet.css') }} ">
        <link rel="stylesheet" href=" {{ asset('assets/leaflet/leaflet-gesture-handling.min.css') }} ">

        <link rel="shortcut icon" type='image/x-icon' href="{{ asset('img/favicon.ico') }}">
        {{-- Scripts --}}
        <script>
            // window.Laravel = {!! json_encode([ 'csrfToken' => csrf_token() ]) !!};
        </script>


        <style>
            #map {
                height: 450px;
            }

            html {
                scroll-behavior: smooth;
            }

            .howworks-list {
                display: -webkit-box;
                display: -webkit-flex;
                display: -ms-flexbox;
                display: flex;
                -webkit-flex-wrap: wrap;
                -ms-flex-wrap: wrap;
                flex-wrap: wrap;
            }
            .howworks-list > div {
                /* min-width: 35rem; */
                /* -webkit-flex: 1 1 3rem; */
                /* flex: 1; */
            }

        </style>

    </head>
    <body>
        @if(App::environment('prod'))
            @include('partials.googletagmanager-body')
        @endif
        
        @include('partials.nav-welcome')
        
        <div id="app" class="section-wrapper bg-white">

        {{-- Publicity --}}
        @include('partials.publicities.publicity-above')
        
        <!-- MAIN -->
        <section class="w-11/12 sm:w-5/6 py-12 mx-auto relative">
            <div class="flex flex-wrap items-center w-full">
                <div class="w-full sm:w-2/5">
                    <div class="w-full lg:w-4/5 relative">
                        <div class="title-home-main">
                            <div class="text-3xl sm:text-4xl leading-snug uppercase font-semibold"> {{ trans('titles.app') }}: </div>
                            <div class="text-xl sm:text-2xl leading-snug uppercase"> {{ trans('welcome.title-main') }} </div>
                        </div>
                    </div>
                    <div class="w-full text-black text-base mb-6">
                        {{ trans('welcome.text-1') }}
                    </div>
                    <a class="btn btn-lg btn-primary rounded-lg shadow-lg" href="{{ route('register') }}"> {{ trans('titles.joinnow') }} </a>
                </div>
                <div class="w-full sm:w-3/5">
                    <div class="flex w-full">
                        <img class="img-main mx-auto w-full object-contain" src="{{ asset('img/home/main.png') }}" alt="{{ trans('titles.register') }}">
                    </div>
                </div>
            </div>
        </section>

        <!-- COMO FUNCIONA -->
        @include('welcome.howworks')

        <!-- SPORTS -->
        @include('welcome.sports')

        <!-- SERVICES -->
        @include('welcome.services')

        <!-- PLANS ATHLETES -->
        @include('welcome.plans')

        <!-- PLAN SCOUT -->
        @include('welcome.planScout')

        <!-- MAP -->
        <section class="mt-10 w-5/6 lg:w-full mx-auto">
            <div class="mb-10 w-full text-center relative">
                <div class="title-home text-2xl sm:text-3xl"> {{ trans('welcome.titleMap') }} </div>
            </div>

            <div class="w-full" id='map'></div>
        </section>

        {{-- Video --}}
        <section class="my-12 lg:my-20 w-5/6 lg:w-3/4 mx-auto">
            <div class="flex flex-wrap items-center">
                <div class="w-full md:w-1/2">
                    <img class="max-w-xs w-1/2 md:w-3/5 mx-auto" src="{{ asset('img/logo.png') }}" alt="Video">
                </div>
                <div class="w-full md:w-1/2 py-4">
                    <video controls controlsList="nodownload" class="cursor-pointer w-full rounded-lg">
                        <source src="{{ asset('img/video.min.mp4') }}" type="video/mp4">
                    </video>
                    {{-- <iframe class="w-full h-64" src="https://www.youtube.com/embed/4N3dJ9P5Hso" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> --}}
                    {{-- <iframe class="w-full mx-auto px-10" src="https://www.youtube.com/embed/4N3dJ9P5Hso&t" frameborder="0"></iframe> --}}
                </div>
            </div>
        </section>

        <!-- Estadisticas -->
        @include('welcome.statistics')

        <!-- Popular Athletes/Scouts -->
        @include('welcome.popularUsers')

        {{-- Ultimas Noticias --}}
        <section class="w-11/12 lg:w-5/6 my-10 mx-auto relative">
                <div class="w-full text-center relative">
                    <div class="title-home text-2xl sm:text-3xl"> {{ trans('welcome.ultimasNoticias') }} </div>
                </div>
                <div class="px-2 sm:px-8">
                    <p-articles></p-articles>  
                </div>
        </section>

        {{-- Publicities --}}
        @include('partials.publicities.publicity-below')
        
        <!-- Contacto -->
        @include('welcome.contact')



        </div>
        @include('partials.footer')

        
        {{-- Scripts --}}
        <script src="{{ mix('/js/app.js') }}"></script>
        <script src="{{ asset('assets/leaflet/leaflet.js') }}"></script>
        <script src="{{ asset('assets/leaflet/leaflet-gesture-handling.min.js') }}"></script>
        <script src="{{ asset('assets/leaflet/leaflet-providers.js') }}"></script>
        
        <script src="{{ asset('assets/swiper/swiper-bundle.js') }}"></script>
        <script src="{{ asset('assets/swiper/swiper-bundle.min.js') }}"></script>
        
        @yield('footer_scripts')

        <script type="text/javascript">

                    // Check if route Sports is Active
                    let urlAfterHash = window.location.hash.substr(1);
                    if(urlAfterHash) {
                        $('#linkWelcomeSports').addClass('active');
                        $('#linkWelcomeHome').removeClass('active');
                    }

                    let linkWelcome = document.getElementById('linkWelcomeSports');
                    linkWelcome.addEventListener('click',function(e) {
                        $(this).addClass('active');
                        $('#linkWelcomeHome').removeClass('active');
                    })

            var locations = [
                @foreach($usersCountries as $ac)
                    [ "{{ $ac['name'] }}",{{ $ac['totalA'] }},{{ $ac['totalS'] }},{{ $ac['lat'] }}, {{ $ac['lng'] }} ], 
                @endforeach
            ];

            // console.log(locations);

            var cities = L.layerGroup();
            
                
                
                var mbAttr = 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
                        '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
                        'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>';
                var mbUrl = 'https://a.tile.openstreetmap.org/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw';
            
                var grayscale   = L.tileLayer(mbUrl, {id: 'mapbox.light', attribution: ''});
                var streets  = L.tileLayer(mbUrl, {id: 'mapbox.streets',   attribution: ''});
                    
                // var osmAttr = '';
                // var openStreetMap = L.TileLayer.Common.extend({
                //     url: 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
                //     options: {attribution: osmAttr}
                // });

                var corner1 = L.latLng(-170, -170),
                corner2 = L.latLng(100, 190),
                bounds = L.latLngBounds(corner1, corner2);

                var map = L.map('map', {
                    center: [20, -10],
                    zoom: 2.5,
                    minZoom: 2.3,
                    maxBounds: bounds,
                    layers: [grayscale, cities],
                    gestureHandling: true
                });

                L.tileLayer('https://api.maptiler.com/maps/basic/{z}/{x}/{y}.png?key=jEWMMQxH3fQYjZ9KNEQf',{
                    tileSize: 512,
                    zoomOffset: -1,
                    minZoom: 1,
                    attribution: '',
                    crossOrigin: true
                }).addTo(map);
                
                var LeafIcon = L.Icon.extend({
                    options: {
                        iconSize:     [30, 30],
                        iconAnchor:   [30, 30]
                    }
                });
    
                var redIcon = new LeafIcon({iconUrl: '/img/home/icon-red.png'});

                locations.forEach(l => {
                    let content = `<strong class="uppercase"> ${l[0]} </strong> <br>`; 
                    content = l[1] > 0 ? content + `@lang('titles.athletes'): ${l[1]} <br>` : content;
                    content = l[2] > 0 ? content + `@lang('titles.scouts'): ${l[2]}` : content;
                    // L.popup({closeButton: false, autoClose:false, closeOnClick: false })
                    //     .setLatLng([l[3], l[4]])
                    //     .addTo(cities).
                    //     setContent(content)
                    //     .openOn(map);
                    L.marker([ l[3], l[4] ],{icon: redIcon}).bindPopup(content).addTo(cities);
                });
                
                var baseLayers = {
                    "Grayscale": grayscale
                };
            
                var overlays = {
                    "Cities": cities
                };
                

            </script>

            <script type="text/javascript">

                    // (function() {
                        // const Bowser = require("bowser");
                        const browser = Bowser.getParser(window.navigator.userAgent);
                        const browserName = browser.getBrowserName();
                        // var Swiper = require('swiper');

                        if(browserName != 'Safari') {
                            new Swiper(`.swiper-container.welcomeCarouselSports`, {
                                autoplay: true,
                                loop: true,
                                slidesPerView: 6,
                                spaceBetween: 0,
                                breakpoints: {
                                    1350: {
                                        slidesPerView: 6,
                                        spaceBetween: 0
                                    },
                                    1023: {
                                        slidesPerView: 6,
                                        spaceBetween: 0
                                    },
                                    768: {
                                        slidesPerView: 4,
                                        spaceBetween: 0
                                    },
                                    500: {
                                        slidesPerView: 3,
                                        spaceBetween: 0
                                    },
                                    400: {
                                        slidesPerView: 3,
                                        spaceBetween: 0
                                    },
                                    300: {
                                        slidesPerView: 3,
                                        spaceBetween: 0
                                    }
                                },
                                // pagination: {
                                //   el: ".swiper-pagination",
                                //   clickable: true
                                // },
                                navigation: {
                                    nextEl: `.welcomeCarouselSports.swiper-button-next`,
                                    prevEl: `.welcomeCarouselSports.swiper-button-prev`
                                },
                            });

                            // Ocultamos el section que tiene los deportes sin Carousel
                            $('#welcomeSportsPlain').addClass('hidden');
                        } else {
                            // Ocultamos el section que tiene los deportes con Carousel
                            $('#welcomeSports').addClass('hidden');
                        }

                    // })();
                    

            </script>

    </body>
</html>
