<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <meta name="api-base-url" content="{{ route('welcome') }}">

        {{-- CSRF Token --}}
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>@if (trim($__env->yieldContent('template_title')))@yield('template_title') | @endif {{ config('app.name', Lang::get('titles.app')) }}</title>
        <meta name="description" content="">
        <meta name="author" content="Mateo">
        <link rel="shortcut icon" type='image/x-icon' href="{{ asset('img/favicon.ico') }}">

        @if(App::environment('prod'))
            @include('partials.googletagmanager-head')
        @endif
        {{-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries --}}
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        {{-- Fonts --}}
        @yield('template_linked_fonts')

        @yield('template_fastload_css')

        {{-- Styles --}}
        <link href="{{ mix('/css/app.css') }}" rel="stylesheet">
        <link rel="stylesheet" href=" {{ asset('assets/air-datepicker/css/datepicker.css') }} ">

        @yield('template_linked_css')

        {{-- Scripts --}}
        <script>
            window.Laravel = {!! json_encode([ 'csrfToken' => csrf_token() ]) !!};
        </script>

        @yield('head')

    </head>
    <body>
        @if(App::environment('prod'))
            @include('partials.googletagmanager-body')
        @endif
        <div id="app" style="min-height: calc(100vh - 62px);">

            @if (App::environment('prod'))
                {{ \Debugbar::disable() }}
            @endif

            @include('partials.nav-welcome')

            <main class="section-wrapper">

                @yield('content')

            </main>

        </div>

        @include('partials.footer')

        {{-- Scripts --}}
        <script src="{{ mix('/js/app.js') }}"></script>

        
        @if(Config::get('app.locale') == 'es')
            <script src="{{ asset('assets/air-datepicker/js/i18n/datepicker.es.js') }}"></script>
        @elseif(Config::get('app.locale') == 'en')
            <script src="{{ asset('assets/air-datepicker/js/i18n/datepicker.en.js') }}"></script>
        @endif
        
        @yield('footer_scripts')

    </body>
</html>
