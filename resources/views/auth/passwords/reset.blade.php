<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title> {!! trans('titles.app') !!} </title>

    <link href="{{ mix('/css/app.css') }}" rel="stylesheet">
    <link rel="shortcut icon" type='image/x-icon' href="{{ asset('img/favicon.ico') }}">

</head>

<body class="overflow-x-scroll">

    @include('partials.nav-welcome')
    <div id="app" class="section-wrapper" style="min-height: calc(100vh - 160px);">
        <div class="w-full py-2 flex justify-center">
            <div class="w-full max-w-xs">
                <div class="card-p">
                        <h3 class="mb-4 font-semibold">{{ trans('auth.resetPassword') }}</h3>

                    <div class="w-full">
                        @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                        @endif
                        <form method="POST" action="{{ route('password.request') }}">
                            @csrf

                            <input type="hidden" name="token" value="{{ $token }}">

                            <div class="form-group">
                                <label for="email" class="">{{ trans('auth.email') }}</label>

                                <div class="col-12 px-0">
                                    <input id="email" type="email"
                                        class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                        name="email" value="{{ isset($email) ? $email : old('email') }}" required autofocus>

                                    @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="password">{{ trans('auth.password') }}</label>

                                    <div class="col-12 px-0">
                                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                    @if ($errors->has('password'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="password-confirm">{{ trans('auth.confirmPassword') }}</label>
                                <div class="col-12 px-0">
                                    <input id="password-confirm" type="password" class="form-control{{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}" name="password_confirmation" required>

                                    @if ($errors->has('password_confirmation'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="row pt-2">
                                <div class="col-12 px-0">
                                    <button type="submit" class="btn btn-primary rounded"> {{ trans('auth.resetPassword') }} </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>


    @include('partials.footer')
    <script src="{{ mix('/js/app.js') }}"></script>

</body>

</html>