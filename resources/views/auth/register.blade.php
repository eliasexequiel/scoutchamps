<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="shortcut icon" type='image/x-icon' href="{{ asset('img/favicon.ico') }}">
    <title> {!! trans('titles.app') !!} </title>

    @if(App::environment('prod'))
            @include('partials.googletagmanager-head')
        @endif

    <link href="{{ mix('/css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href=" {{ asset('css/datepicker.css') }} ">
    <link rel="stylesheet" href=" {{ asset('assets/chosen/chosen.min.css') }} ">

    <script>
        window.Laravel = {!! json_encode(['csrfToken' => csrf_token()])!!};
    </script>

    <style>
        #app {
            min-height: calc(100vh - 140px);
        }
    </style>
</head>

<body>
    @if(App::environment('prod'))
        @include('partials.googletagmanager-body')
    @endif
    @include('partials.nav-welcome')

    <div id="app" class="section-wrapper bg-white w-full">
        {{-- Publicity --}}
        @include('partials.publicities.publicity-above')

        <div class="w-full bg-white pb-8 pt-2">

            <div class="flex mx-auto w-full md:w-11/12 lg:w-5/6">

                <div class="hidden lg:inline-flex md:w-3/5 lg:w-3/5">
                    <img class="w-full object-contain" src="{{ asset('img/register-login.png') }}" alt="Stadium">
                </div>

                <div class="w-11/12 lg:-ml-10 mx-auto sm:max-w-md lg:w-2/5">
                    
                    <div class="title-home text-2xl sm:text-3xl"> {{ __('auth.register') }} </div>

                    <div class="z-20 bg-white border border-primary w-full rounded-lg">

                        <div class="register-pills flex w-full mb-4 text-primary rounded-t-lg">
                            <div class="nav text-center w-full flex-row nav-pills" id="v-pills-tab" role="tablist" aria-orientation="horizontal">
                                <a class="border-primary border-b w-1/2 nav-link {{ old('currentTab') != '#scout' && session('currentTab') != '#scout' ? 'active' : '' }}"
                                    data-toggle="pill" href="#athlete" role="tab" aria-controls="Athlete"
                                    aria-selected="true">
                                    {{ trans('titles.athlete') }}
                                </a>
                                <a class="border-primary border-b w-1/2 nav-link {{ old('currentTab') == '#scout' || session('currentTab') == '#scout' ? 'show active' : '' }}"
                                    data-toggle="pill" href="#scout" role="tab" aria-controls="Scout"
                                    aria-selected="false">
                                    {{ trans('titles.scout') }}
                                </a>
                            </div>
                        </div>
                        <div class="px-2 pb-4 w-full">
                            <div class="tab-content" id="v-pills-tabContent">
                                <div id="athlete"
                                    class="tab-pane fade {{ old('currentTab') != '#scout' && session('currentTab') != '#scout' ? 'show active' : '' }}"
                                    role="tabpanel" aria-labelledby="Athlete">
                                    {{-- @include('partials.errors') --}}
                                    @include('auth.registerAthlete')
                                </div>
                                <div id="scout"
                                    class="tab-pane fade {{ old('currentTab') == '#scout' || session('currentTab') == '#scout' ? 'show active' : '' }}"
                                    role="tabpanel" aria-labelledby="Scout">
                                    @include('auth.registerScout')
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>

        {{-- Publicities --}}
        @include('partials.publicities.publicity-below')

        <!-- Contacto -->
        @include('welcome.contact')
    </div>

    @include('partials.footer')

    <script src="{{ mix('/js/app.js') }}"></script>

    @if(Config::get('app.locale') == 'es')
    <script src="{{ asset('assets/air-datepicker/js/i18n/datepicker.es.js') }}"></script>
    @elseif(Config::get('app.locale') == 'en')
    <script src="{{ asset('assets/air-datepicker/js/i18n/datepicker.en.js') }}"></script>
    @endif

    <script src="{{ asset('assets/chosen/chosen.jquery.min.js') }}"></script>


    <script type="text/javascript">

        $(window).on("load", function () {

            let placeholderEntityText = "Club"; 
            $("#entity_id").chosen({
                width: "100%"
            });


            let lang = $('html').attr('lang');
            if(lang == 'en') {
                $('.datepicker-custom').datepicker({
                    language: lang,
                    autoClose: true,
                    dateFormat: 'mm/dd/yyyy',
                    maxDate: new Date(),
                    onSelect: function (fd, d, picker) {
                        verifyAge();
                    }
                });
            }
            else {
                $('.datepicker-custom').datepicker({
                    language: lang,
                    autoClose: true,
                    dateFormat: 'dd/mm/yyyy',
                    maxDate: new Date(),
                    onSelect: function (fd, d, picker) {
                        verifyAge();
                    }
                }); 
            }




            $('a[data-toggle="pill"]').on('shown.bs.tab', function (e) {
                // save the latest tab; use cookies if you like 'em better:
                // localStorage.setItem('lastTab', $(this).attr('href'));
                $('.currentTab').val($(this).attr('href'));
            });


            // var dateMask = IMask(
            //   document.getElementById('dob'),
            //   {
            //     mask: Date,
            //     pattern: 'Y-`m-`d',
            //     min: new Date(1940, 0, 1),
            //     max: new Date(2020, 0, 1),
            //     lazy: false
            //   });

            $('#dob').on('change', function () {
                verifyAge();
            });
            function verifyAge() {
                let val = $('#dob').val();
                if (val.length == 10) {

                    let ageAux = 0;
                    if(lang == "en") {
                        ageAux = `${val.substring(0, 2)}/${val.substring(3, 5)}/${val.substring(6)}`
                    } else { 
                        ageAux = `${val.substring(3, 5)}/${val.substring(0, 2)}/${val.substring(6)}`
                    }
                    let age = calcAge(ageAux);
                    // console.log(age);
                    $('#age').val(age);
                    if (age < 18) {
                        $('.blockTutor').removeClass('hidden');
                    } else {
                        $('.blockTutor').addClass('hidden');
                    }
                }
            }

            function calcAge(dateString) {
                var today = new Date();
                var birthDate = new Date(dateString);
                var age = today.getFullYear() - birthDate.getFullYear();
                var m = today.getMonth() - birthDate.getMonth();
                if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
                    age--;
                }
                return age;
            }

            verifyAge();

            // SCOUT REGISTER

            function checkClubSelected() {
                if ($('#entity_id').val() == 'otherClub') {
                    $('.otherClubWrapper').removeClass('hidden');
                } else {
                    $('.otherClubWrapper').addClass('hidden');
                }
            };
            checkClubSelected();
            $('#entity_id').on('change', function () {
                checkClubSelected();
            })

            $('.tooltipFocus').tooltip({
                placement: "top",
                trigger: "focus"
            });


            function getEntities() {
                let value = $('#entity_type_id option:selected').text().trim();
                let query = 'Club'; 
                if(value == 'Team' || value == 'Equipo') query = 'Club';
                if(value == 'University' || value == 'Universidad') query = 'University';
                if(value == 'Company' || value == 'Empresa') query = 'Company';
                if(value == 'Sponsor') query = 'Sponsor'; 
                // console.log(value);
                // console.log(query);
                if(query != '') {
                    $.ajax({
                        type:'GET',
                        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                        url: `/api/clubs?type=${query}`,
                        success: function(result) {
                            // console.log(result);
                            let otherClubText = "@lang('auth.otherClub')"; 
                            fillSelectEntities(result.entities,otherClubText);
                        },
                        error: function (response, status, error) {
                            // console.log(error);
                        }
                    });
                }
            }

            function fillSelectEntities(entities,placeholderOtherClub) {
                $('#entity_id').empty();
                data = `<option value=""> @lang('auth.selectEntity') </option>`;
                data += `<option value="otherClub"> ${placeholderOtherClub} </option>`;
                entities.forEach(ent => {
                    data += `<option value="${ent.id}"> ${ent.name} </option>`;    
                });
                $('#entity_id').append(data);
                $("#entity_id").trigger("chosen:updated");
            }

            $('#entity_type_id').on('change',function(e) {
                getEntities();
            })
            getEntities();
        });
    </script>
</body>

</html>