<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title> {!! trans('titles.app') !!} </title>

    @if(App::environment('prod'))
        @include('partials.googletagmanager-head')
    @endif

    <link href="{{ mix('/css/app.css') }}" rel="stylesheet">
    <link rel="shortcut icon" type='image/x-icon' href="{{ asset('img/favicon.ico') }}">

</head>

<body class="overflow-x-scroll">
    @if(App::environment('prod'))
            @include('partials.googletagmanager-body')
    @endif
    
    @include('partials.nav-welcome')
    <div id="app" class="section-wrapper bg-white" style="min-height: calc(100vh - 120px);">
        {{-- Publicity --}}
        @include('partials.publicities.publicity-above')

        <div class="w-full bg-white pb-8 pt-2 flex justify-center">
            <div class="flex w-full w-full md:w-11/12 lg:w-5/6 xl:w-4/5">

                <div class="hidden md:inline-flex md:w-3/5 lg:w-2/3">
                    <img class="w-full" src="{{ asset('img/register-login.png') }}" alt="Stadium">
                </div>
                <div class="w-11/12 mx-auto md:w-2/5 lg:w-1/3 max-w-sm">
                    
                    <div class="w-full">
                        <div class="title-home text-2xl sm:text-3xl"> {{ __('auth.login') }} </div>
                        
                        <div class="p-6 border border-primary w-full rounded-lg">

                            @include('partials.status')
                            
                            <form method="POST" action="{{ route('login') }}">
                                @csrf

                                <div class="form-group">
                                    <label for="email" class="">{{ trans('auth.email') }}</label>

                                    <div class="col-12 px-0">
                                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                            name="email" value="{{ old('email') }}" autofocus>

                                        @if ($errors->has('email'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="password" class="">{{ trans('auth.password') }}</label>

                                    <div class="col-12 px-0">
                                        <input id="password" type="password"
                                            class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                            name="password" required>

                                        @if ($errors->has('password'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>

                                {{-- <div class="form-group">
                                    <div class="col-12">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="remember"
                                                    {{ old('remember') ? 'checked' : '' }}> {{ __('Remember Me') }}
                                            </label>
                                        </div>
                                    </div>
                                </div> --}}

                                <div class="form-group mt-4 mb-0">
                                    <div class="col-12 px-0 pt-2">
                                        <button type="submit" class="btn btn-primary hover:shadow rounded">
                                            {{ trans('auth.loginBtn') }}
                                        </button>

                                        <a class="btn pl-2 pr-0" href="{{ route('password.request') }}">
                                            {{ trans('auth.forgot') }}
                                        </a>
                                    </div>
                                </div>


                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        {{-- Publicities --}}
        @include('partials.publicities.publicity-below')
        
        <!-- Contacto -->
        @include('welcome.contact')
    </div>


    @include('partials.footer')

    <script src="{{ mix('/js/app.js') }}"></script>

</body>

</html>