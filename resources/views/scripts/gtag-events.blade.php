<script async src="https://www.googletagmanager.com/gtag/js?id=UA-162980607-1"></script>
<script type="text/javascript">
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', 'UA-162980607-1');

  $(window).on("load", function () {
    @if(isset($send_registered))
        console.log("EVENT");
        gtag('event', '{{ $send_registered['event_name'] }}', {
            'event_category': '{{ $send_registered['event_category'] }}', 
            'event_label': '{{ $send_registered['event_label'] }}'
        });

        // Facebook event
        fbq('track', 'Lead');

        // gtag('event', 'Registro', {'event_category': 'Nuevo Usuario', 'event_label': 'Atleta'});
    @endif
  });
</script>