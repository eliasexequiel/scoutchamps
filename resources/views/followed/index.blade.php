@extends('layouts.app')

@section('template_title')
    {!! trans('titles.followed') !!}
@endsection

@section('template_fastload_css')
@endsection

@section('content')
    <div class="w-full px-6 py-2">

        <div class="w-full mb-2">
            <span class="badge badge-primary rounded-full text-white"> {{ trans('profile.totalUsers') }}  :  {{ $users->total() }} </span> 
        </div>
        <div class="flex flex-wrap w-full card-p">
        @if(count($users) > 0)
        @foreach ($users as $user)
                <div class="w-full sm:w-1/2 md:w-1/2 lg:w-1/3 xl:w-1/3 flex flex-wrap px-2 mb-4">
                    <div class="w-full flex flex-wrap sm:flex-no-wrap items-center sm:justify-between px-1 py-2 border-primary border">
                        <div class="pr-2 w-16">
                            @include('partials.avatar-user',['user' => $user, 'size' => 'h-16 w-16'])
                            {{-- <img class="h-20 w-20 sm:h-24 sm:w-24 object-cover object-top block rounded-full border" src="{{ $user->avatar }}" alt="{{ $user->fullName }}"> --}}
                        </div>
                        <div class="w-3/5 sm:w-2/5 flex flex-wrap items-center">
                        <div class="w-auto">
                                @include('partials.user-card-information',['link' => true])
                        </div>
                        </div>
                        <div class="w-full sm:w-auto pt-1 pl-1">
                            <div class="flex items-center flex-row sm:flex-col text-center flex-wrap">
                                @if($user->roles[0]->name == 'Scout')
                                    <div> <span class="badge text-sm badge-primary rounded-full"> {{trans('titles.Scout') }} </span> </div>
                                @endif
                                @if($user->roles[0]->name == 'Athlete')
                                    <div> <span class="badge text-sm badge-success rounded-full"> {{trans('titles.Athlete') }} </span> </div>
                                @endif
                                <div class="pl-2 sm:pl-0 sm:mt-1">
                                    <a class="btn btn-sm btn-outline-info rounded-full" href="{{ route('public.profile.show',['uuid' => $user->uuid]) }}"> 
                                        <i class="fa fa-eye fa-fw" aria-hidden="true"></i>
                                        {{ trans('profile.viewProfile') }} 
                                    </a>
                                </div>
                                {{-- <a class="mt-2 mr-2 btn btn-gray-outline btn-favorite-remove px-6" href="{{ route('public.profile.removeFavorite',[$user->uuid]) }}"> {{ __('profile.favorites') }} </a> --}}
                                {{-- Followed --}}
                                {{-- @if(in_array($user->id,\Auth::user()->followed->pluck('following_user_id')->all()))
                                    <a class="mt-2 btn btn-gray px-6" href="{{ route('public.profile.unfollow',[$user->uuid]) }}"> {{ __('profile.unfollow') }} </a>
                                @else
                                    <a class="mt-2 btn btn-primary px-6" href="{{ route('public.profile.follow',[$user->uuid]) }}"> {{ __('profile.follow') }} </a>
                                @endif --}}
                            </div>   
                        </div>
                    </div>
                </div>

            
            @endforeach
            
            <div class="w-full mt-3">
                    <div class="w-full mb-2">
                        {{ $users->links() }}
                    </div>
            </div>
            @else
            <div class="flex items-center justify-center">
                <div class="text-lg"> {{ trans('profile.noResults') }} </div>
            </div>
            @endif

        </div>
        
    </div>
@endsection

@section('footer_scripts')
@append