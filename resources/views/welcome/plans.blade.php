<section id="planes" class="mt-12 md:mt-20 mb-20 w-11/12 lg:w-5/6 mx-auto">
    <div class="hidden md:flex flex-wrap">
    @foreach ($plans as $p)
        <div class="w-1/2 md:w-1/4 mb-16 md:mb-2 px-2">
            <div class="shadow-lg rounded-tl-lg rounded-tr-lg plan">
            <div style class="h-48 lg:h-44 pt-20 relative text-center rounded-tl-lg rounded-tr-lg {{ $p->type == 'Premium' ? 'bg-primary' : 'border-primary border-2'}}">
                <div class="-mt-12 w-full absolute top-0">
                    <img class="mx-auto h-32 w-auto" src="{{ $p->image }}" alt="{{ $p->name }}">
                </div>
                @if($p->name != 'Free')
                <div class="mt-2 leading-none text-white font-semibold text-2xl xl:text-3xl">
                    @if(app()->getLocale() == 'en')
                        {{ $p->type == 'Premium' ? 'Premium' : '' }} {{ trans('titles.athlete') }}
                    @else
                        {{ trans('titles.athlete') }} {{ $p->type == 'Premium' ? 'Premium' : '' }}
                    @endif
                </div>
                <div class="text-white leading-none font-semibold  md:text-xl lg:text-2xl">
                    {{ trans('titles.'.$p->name) }} {{ $p->price }} <span class="text-base"> USD </span>
                </div>
                @else
                <div class="mt-2 text-primary leading-none font-semibold text-2xl xl:text-3xl">
                        @if(app()->getLocale() == 'en') 
                        {{ trans('titles.'.$p->name) }} {{ trans('titles.athlete') }}
                        @else
                        {{ trans('titles.athlete') }} {{ trans('titles.'.$p->name) }}
                        @endif
                </div>
                @endif
                @if($p->name == 'Semester')
                    <span class="text-white font-semibold text-lg"> {{ trans('welcome.savePlan') }} 10% </span>
                @elseif($p->name == 'Anual')
                    <span class="text-white font-semibold text-lg"> {{ trans('welcome.savePlan') }} 20% </span>
                @endif
            </div>
            {{-- Items --}}
            <div class="px-2">
                @foreach ($p->items as $item)
                    @if($item->value != '')
                        <div class="text-sm border-b last:border-b-0 border-primary py-1">
                            <div class="px-2">
                                @if(is_numeric($item->value) || $item->value == 'Unlimited')
                                    {{ $item->item->name }}:
                                    <span> 
                                        @if($item->value == 'Unlimited') 
                                            {{ trans('titles.unlimited') }} 
                                        @else 
                                            {{ $item->value }} 
                                        @endif 
                                    </span>
                                @else
                                    {{ $item->item->name }}
                                @endif
                            </div>
                        </div>
                    @endif
                @endforeach
            </div>
            {{-- Choose --}}
            <div class="text-center select-button">
                <a class="btn btn-lg btn-primary rounded-lg" href="{{ route('plan.show',[$p->stripe_plan]) }}"> {{ trans('welcome.choosePlan') }} </a>
            </div>
            </div>
        </div>
    @endforeach
</div>

{{-- PLANS MOBILE --}}
<div class="w-full sm:w-3/4 block mx-auto md:hidden mobile-plans">
        <div class="plans-pills flex w-full border border-primary rounded-t-lg">
                <div class="nav text-center w-full flex-row nav-pills" id="v-pills-tab" role="tablist" aria-orientation="horizontal">
                    @foreach ($plans as $p)
                        <a class="p-1 border-primary border-b w-1/4 nav-link {{ $loop->first ? 'active' : '' }}" data-toggle="pill" href="#{{ $p->name }}" role="tab" aria-controls="{{ $p->name }}" aria-selected="true">
                            <div>
                                <img class="mx-auto object-contain h-20 w-auto" src="{{ $p->image }}" alt="{{ $p->name }}">
                            </div>
                            @if($p->name != 'Free')
                            <div class="leading-none">
                                    @if(app()->getLocale() == 'en')
                                        {{ $p->type == 'Premium' ? 'Premium' : '' }} {{ trans('titles.athlete') }}
                                    @else
                                        {{ trans('titles.athlete') }} {{ $p->type == 'Premium' ? 'Premium' : '' }}
                                    @endif
                            </div>
                            <div class="leading-none">
                                    {{ trans('titles.'.$p->name) }} 
                                <span class="block price mt-1 text-base">{{ $p->price }} <span class="text-sm"> USD </span> </span>
                            </div>
                            @else
                                <div class="mt-4 leading-none font-semibold">
                                        @if(app()->getLocale() == 'en') 
                                        {{ trans('titles.'.$p->name) }} {{ trans('titles.athlete') }}
                                        @else
                                        {{ trans('titles.athlete') }} {{ trans('titles.'.$p->name) }}
                                        @endif
                                </div>
                            @endif
                            @if($p->name == 'Semester')
                                <span class="plan-save font-semibold text-sm"> {{ trans('welcome.savePlan') }} 10% </span>
                            @elseif($p->name == 'Anual')
                                <span class="plan-save font-semibold text-sm"> {{ trans('welcome.savePlan') }} 20% </span>
                            @endif

                        </a>
                    @endforeach
                </div>
        </div>
        <div class="pb-4 w-full">
                <div class="tab-content" id="v-pills-tabContent">
                        @foreach ($plans as $p)
                        <div id="{{ $p->name }}" class="tab-pane fade {{ $loop->first ? 'show active' : '' }}" role="tabpanel" aria-labelledby="{{ $p->name }}">
                            <div class="shadow-lg pt-4 rounded-bl-lg rounded-br-lg">
                            {{-- Items --}}
                            <div class="px-2">
                                @foreach ($p->items as $item)
                                    @if($item->value != '')
                                        <div class="text-sm border-b last:border-b-0 border-primary py-1">
                                            <div class="px-2">
                                                @if(is_numeric($item->value) || $item->value == 'Unlimited')
                                                    {{ $item->item->name }}:
                                                    <span>
                                                        @if($item->value == 'Unlimited') 
                                                            {{ trans('titles.unlimited') }} 
                                                        @else 
                                                            {{ $item->value }} 
                                                        @endif
                                                    </span>
                                                @else
                                                    {{ $item->item->name }}
                                                @endif
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                            {{-- Choose --}}
                            <div class="text-center pb-2 pt-6">
                                <a class="btn btn-lg btn-primary rounded-lg" href="{{ route('plan.show',[$p->stripe_plan]) }}"> {{ trans('welcome.choosePlan') }} </a>
                            </div>
                            </div>
                        </div>
                    @endforeach

                </div>
        </div>
</div>

</section>