<section class="mt-10 w-full xl:w-11/12 mx-auto">
    <div class="flex flex-wrap justify-center">

        {{-- Athletes Count --}}
        {{-- <div class="w-1/2 mt-2 sm:w-1/3 md:w-1/5 px-2">
            <div class="px-4 py-4 w-full bg-gray-200 rounded-lg">
                <img class="mx-auto h-16 w-auto" src="{{asset('img/home/cantidadAtletas.png')}}" alt="{{ trans('welcome.atlethesRegistered') }}">
                <div class="h-12 leading-snug my-2 uppercase text-base flex items-center justify-center text-center">
                        {{ trans('welcome.atlethesRegistered') }}
                </div>
                <div class="mx-auto h-1 w-1/2 bg-primary"></div>
                <div class="text-center text-4xl">
                    {{ $athletesCount }}
                </div>
            </div>
        </div> --}}
        
        {{-- Scout registered --}}
        {{-- <div class="w-1/2 mt-2 sm:w-1/3 md:w-1/5 px-2">
            <div class="px-4 py-4 w-full bg-gray-200 rounded-lg">
                <img class="mx-auto h-16 w-auto" src="{{asset('img/home/cantidadScout.png')}}" alt="{{ trans('welcome.scoutsRegistered') }}">
                <div class="h-12 my-2 w-full flex items-center justify-center text-center">
                        <div class="leading-snug uppercase text-base">
                            {{ trans('welcome.scoutsRegistered') }}
                        </div>
                </div>
                <div class="mx-auto h-1 w-1/2 bg-primary"></div>
                <div class="text-center text-4xl">
                    {{ $scoutsCount }}
                </div>
            </div>
        </div> --}}
        
        {{-- Scouts with Athletes --}}
        {{-- <div class="w-1/2 mt-2 sm:w-1/3 md:w-1/5 px-2">
            <div class="px-4 py-4 w-full bg-gray-200 rounded-lg">
                <img class="mx-auto h-16 w-auto" src="{{asset('img/home/cantidadConversacion.png')}}" alt="{{ trans('welcome.contactBetweenScoutAndAthletes') }}">
                <div class="h-12 my-2 w-full flex items-center justify-center text-center">
                        <div class="leading-snug uppercase text-base">
                            {!! trans('welcome.contactBetweenScoutAndAthletes') !!} 
                        </div>
                </div>
                <div class="mx-auto h-1 w-1/2 bg-primary"></div>
                <div class="text-center text-4xl">
                    {{ $conversationsScoutsAthletes ?? 0 }}
                </div>
            </div>
        </div> --}}


        {{-- Visits --}}
        <div class="w-1/2 mt-2 sm:w-1/3 md:w-1/5 px-2">
            <div class="px-4 py-4 w-full bg-gray-200 rounded-lg">
                <img class="mx-auto h-16 w-auto" src="{{asset('img/home/cantidadVisitas.png')}}" alt="{{ trans('welcome.visits') }}">
                <div class="h-12 my-2 w-full flex items-center justify-center text-center">
                        <div class="leading-snug uppercase text-base">
                            {{ trans('welcome.visits') }}
                        </div>
                </div>
                <div class="mx-auto h-1 w-1/2 bg-primary"></div>
                <div class="text-center text-4xl">
                    {{ $visits }}
                </div>
            </div>
        </div>

        {{-- Countries with Athletes /Scouts --}}
        <div class="w-1/2 mt-2 sm:w-1/3 md:w-1/5 px-2">
            <div class="px-4 py-4 w-full bg-gray-200 rounded-lg">
                <img class="mx-auto h-16 w-auto" src="{{asset('img/home/cantidadPaises.png')}}" alt="{{ trans('welcome.countriesAthletesScouts') }}">
                <div class="h-12 my-2 w-full flex items-center justify-center text-center">
                        <div class="leading-snug uppercase text-base">
                            {{ trans('welcome.countriesAthletesScouts') }}
                        </div>
                </div>
                <div class="mx-auto h-1 w-1/2 bg-primary"></div>
                <div class="text-center text-4xl">
                    {{ $countriesUsers }}
                </div>
            </div>
        </div>

    </div>
</section>