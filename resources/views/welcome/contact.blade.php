<section class="w-full bg-gray-300 py-8 relative">
        <div class="w-full w-11/12 sm:w-5/6 lg:w-3/5 mx-auto text-center">
            <div class="mb-8 w-full text-center relative">
                <div class="title-home text-2xl sm:text-3xl"> {{ trans('welcome.contact.title') }} </div>
            </div>
            <div class="flex flex-wrap justify-center">
                <a href="mailto:{{ $contactEmail }}" class="flex font-normal items-center mb-1 px-2">
                    <img class="pr-2 w-8" src="{{ asset('img/home/contact_email.png') }}" alt="E-mail">
                    <div class="text-base">  {{ trans('welcome.contact.email') }}: {{ $contactEmail }} </div>
                </a>
            </div>
        </div>
</section>