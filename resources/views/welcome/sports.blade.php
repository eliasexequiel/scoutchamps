<section id="welcomeSports" class="w-full bg-white py-12 relative">
    <div class="w-11/12 sm:w-5/6 lg:w-3/4 mx-auto">
        <div class="mb-12 w-full text-center relative">
            <div class="title-home text-2xl sm:text-3xl"> {{ trans('welcome.sports') }} </div>
        </div>
        <div class="relative px-6">
            <div id="welcomeCarouselSports" class="swiper-container welcomeCarouselSports">
                <div class="swiper-wrapper">
                    @foreach ($sports as $s)
                        <div class="swiper-slide">
                            <div class="px-2">
                                <a href="{{ route('guest.sport.users',['id' => $s->id]) }}" class="">
                                    <img src="{{ $s->getAvatarHome() }}" alt="{{ $s->name }}">
                                </a>
                            </div>
                        </div>
                    @endforeach

                </div>
            </div>
            <div class="welcomeCarouselSports swiper-button-next">
                <span class="arrow-right"></span>
            </div>
            <div class="welcomeCarouselSports swiper-button-prev">
                <span class="arrow-left"></span>
            </div>
        </div>
    </div>
</section>

<section id="welcomeSportsPlain" class="w-full bg-white py-12 relative">
    <div class="w-full sm:w-5/6 lg:w-4/5 mx-auto">
        <div class="mb-12 w-full text-center relative">
            <div class="title-home text-2xl sm:text-3xl"> {{ trans('welcome.sports') }} </div>
        </div>
        <div class="relative px-6">
            <div class="flex flex-wrap justify-center mx-auto">
                    @foreach ($sports as $s)
                        <div class="w-32 sm:w-40 mb-4">
                            <div class="px-2">
                                <a href="{{ route('guest.sport.users',['id' => $s->id]) }}" class="">
                                    <img src="{{ $s->getAvatarHome() }}" alt="{{ $s->name }}">
                                </a>
                            </div>
                        </div>
                    @endforeach

            </div>
        </div>
    </div>
</section>