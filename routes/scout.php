<?php 

/*************** SCOUTS *******************/
Route::group(['middleware' => ['auth', 'activated','blocked', 'role:scout', 'currentUser']], function () { 
    Route::resource('recruitments', 'RecruitmentsController');

    Route::post('recruitments/invitations/send', ['as' => 'recruitments.invitations.send', 'uses' => 'RecruitmentsController@sendInvitations']);
    
    Route::get('recruitments/{id}/invitations',['as' => 'recruitments.invitations.show', 'uses' => 'RecruitmentsController@showInvitations']);

    Route::get('api/recruitments','RecruitmentsController@getRecruitments');
    Route::get('api/recruitments/{id}','RecruitmentsController@getRecruitment');
    Route::post('api/recruitments/{id}/invitations/send', ['as' => 'onerecruitment.invitations.send', 'uses' => 'RecruitmentsController@recruitmentSendInvitations']);
    Route::delete('api/recruitments/{id}/user/{idUser}', ['as' => 'onerecruitment.invitations.delete', 'uses' => 'RecruitmentsController@removeUserFromRecruitment']);

});