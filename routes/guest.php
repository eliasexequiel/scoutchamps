<?php

Route::group(['middleware' => ['web','activity']], function () {
    Route::get('/', 'WelcomeController@welcome')->name('welcome');
    
    Route::get('/g/aboutus',['as'   => 'guest.aboutus', 'uses' => 'WelcomeController@showAboutUs']);
    Route::get('/g/faqs',['as'   => 'guest.faqs', 'uses' => 'WelcomeController@showFaqs']);

    Route::get('g/search', ['as'   => 'guest.search', 'uses' => 'WelcomeController@searchUser']);
    Route::get('g/searchclub', ['as'   => 'guest.search.club', 'uses' => 'WelcomeController@searchClub']);
    Route::get('g/club/{id}/users', ['as'   => 'guest.club.users', 'uses' => 'WelcomeController@usersClub']);
    Route::get('g/sport/{id}/users', ['as'   => 'guest.sport.users', 'uses' => 'WelcomeController@usersSport']);

    Route::get('api/clubs', 'Admin\ClubesController@getClubs');
    
    Route::get('api/articles', 'Admin\ArticleController@getArticles');
    Route::get('/conditions',['as'   => 'conditions', 'uses' => 'PublicController@showTermsAndConditions']);
    Route::get('/privacy',['as'   => 'privacy', 'uses' => 'PublicController@showPrivacyPolicy']);


});